# Patently Obvious
Reverse Engineering the 50-year-old HP-35 Scientific Calculator from its 45-Year-Old Patent - [US 4,001,569](https://patents.google.com/patent/US4001569A/en) - into a Fully-Functional Replica

## Remoticon 2021
I described this project in [my Remoticon 2021 presentation about the HP-35](https://hackaday.com/2022/04/07/remoticon-2021-rob-weinstein-builds-an-hp-35-from-the-patent-up).

## Visuals
[![Rob's HP-35 Replica](/uploads/a5e3abca4b7ec83955bb35c04e87e198/robs-hp35-replica-3quarter-view-crop-739x710.png)](https://vimeo.com/648116637)

Click image for video...


## License
FPGA Design Files: 

> OpenBSD License                                                         
>                                                                         
> Copyright (c) 2022 Robert J. Weinstein                                  
>                                                                         
> Permission to use, copy, modify, and distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
>                                                                         
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Drawings:

> ![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by.svg) \
> Copyright � 2022 by Robert J. Weinstein \
> Creative Commons Attribution 4.0 International (CC BY 4.0) \
> https://creativecommons.org/licenses/by/4.0/

## Project status
Maintenance only.

