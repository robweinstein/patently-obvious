## HP-35 Drawings
__BOLD__ indicates the main logic diagrams for the HP-35's integrated circuits

### The Whole Calculator
| Number | Title |
| ------ | ----------- |
| RJW2037 | HP-35 Calculator - The Whole Enchilada - Testbench Diagram |
| RJW2066 | HP-35 Serial Operation - Animated Demonstration |

### Control and Timing Circuit 16
| Number | Title |
| ------ | ----------- |
| __RJW2025__ | __HP-35 Control and Timing Circuit 16 - Logic and Timing Diagrams__ |
| RJW2026 | HP-35 Control and Timing Circuit - Detailed Timing for All Instructions |
| RJW2027 | HP-35 Control and Timing Circuit - Barber-Pole Timing for JSB-RET Instructions |
| RJW2028 | HP-35 Control and Timing Circuit - Barber-Pole Timing for BRH Instruction |
| RJW2029 | HP-35 Control and Timing Circuit - Barber-Pole Timing for Status Operations |
| RJW2030 | HP-35 Control and Timing Circuit - Barber-Pole Timing for Pointer Operations |
| RJW2031 | HP-35 Control and Timing Circuit - Barber-Pole Timing for KEY-ENTRY Instruction |
| RJW2032 | HP-35 Control and Timing Circuit - Barber-Pole Timing for Arithmetic Instructions |
| RJW2033 | HP-35 Control and Timing Circuit - Key Entry Timing |

### HP-35 Read Only Memory 18
| Number | Title |
| ------ | ----------- |
| __RJW2022__ | __HP-35 Read Only Memory 18 - Logic and Timing Diagrams__ |
| RJW2024 | HP-35 Read Only Memory - Word Select Output Simulation Results |

### Arithmetic and Register Circuit 20
| Number | Title |
| ------ | ----------- |
| __RJW2039__ | __HP-35 Arithmetic and Register Circuit 20 - Logic and Timing Diagrams__ |
| RJW2014 | HP-35 Arithmetic and Register Circuit - Display Operation Logic and Timing Diagrams |
| RJW2015 | HP-35 Display Operation Animation |
| RJW2021 | HP-35 Arithmetic and Register Circuit - Testbench Logic Diagram |
| RJW2070 | HP-35 Arithmetic and Register Circuit - Testbench for Serial Adder 84 |


### Output Display Unit 14 
| Number | Title |
| ------ | ----------- |
| __RJW2047__ | __HP-35 Cathode Driver Logic and Timing Diagrams__ |
| __RJW2048__ | __HP-35 Anode Driver Logic and Timing Diagrams__ |
| RJW2067 | HP-35 Output Display Unit 14 - LED Inductive Drive Operation (Animation) |
| RJW2068 | HP-35 Output Display Unit 14 - Decimal Point LED Inductive Drive Operation (Animation) |

### Hardware Replica Prototype
| Number | Title |
| ------ | ----------- |
| RJW2043 | HP-35 Hardware Replica Prototype - Schematic Diagram |
| RJW2054 | HP-35 Hardware Replica Prototype - Cathode Driver Circuit  Layout Diagram |
| RJW2055 | HP-35 Hardware Replica Prototype - Anode Driver Circuit  Layout Diagram |
| RJW2056 | HP-35 Hardware Replica Prototype - Physical Layout Diagram |

