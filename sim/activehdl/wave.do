onerror { resume }
transcript off
add wave -noreg -logic {/tb/PHI1}
add wave -noreg -logic {/tb/PHI2}
add wave -noreg -logic {/tb/IA}
add wave -noreg -logic {/tb/IS}
add wave -noreg -logic {/tb/WS}
add wave -noreg -logic {/tb/SYNC}
add wave -noreg -logic {/tb/CARRY}
add wave -noreg -logic {/tb/A}
add wave -noreg -logic {/tb/B}
add wave -noreg -logic {/tb/C}
add wave -noreg -logic {/tb/D}
add wave -noreg -logic {/tb/E}
add wave -noreg -logic {/tb/START}
add wave -noreg -logic {/tb/BCD}
add wave -noreg -logic {/tb/SA}
add wave -noreg -logic {/tb/SB}
add wave -noreg -logic {/tb/SC}
add wave -noreg -logic {/tb/SD}
add wave -noreg -logic {/tb/SE}
add wave -noreg -logic {/tb/SF}
add wave -noreg -logic {/tb/SG}
add wave -noreg -logic {/tb/SDP}
add wave -noreg -logic {/tb/C_CL}
add wave -noreg -logic {/tb/O1}
add wave -noreg -logic {/tb/O2}
add wave -noreg -logic {/tb/O3}
add wave -noreg -logic {/tb/O4}
add wave -noreg -logic {/tb/O5}
add wave -noreg -logic {/tb/O6}
add wave -noreg -logic {/tb/O7}
add wave -noreg -logic {/tb/O8}
add wave -noreg -logic {/tb/O9}
add wave -noreg -logic {/tb/O10}
add wave -noreg -logic {/tb/O11}
add wave -noreg -logic {/tb/O12}
add wave -noreg -logic {/tb/O13}
add wave -noreg -logic {/tb/O14}
add wave -noreg -logic {/tb/O15}
add wave -noreg -logic {/tb/ROW0}
add wave -noreg -logic {/tb/ROW1}
add wave -noreg -logic {/tb/ROW2}
add wave -noreg -logic {/tb/ROW3}
add wave -noreg -logic {/tb/ROW4}
add wave -noreg -logic {/tb/ROW5}
add wave -noreg -logic {/tb/ROW6}
add wave -noreg -logic {/tb/ROW7}
add wave -noreg -logic {/tb/COL0}
add wave -noreg -logic {/tb/COL2}
add wave -noreg -logic {/tb/COL3}
add wave -noreg -logic {/tb/COL4}
add wave -noreg -logic {/tb/COL6}
add wave -noreg -logic {/tb/rst}
add wave -noreg -logic {/tb/T1}
add wave -noreg -logic {/tb/T2}
add wave -noreg -logic {/tb/T3}
add wave -noreg -logic {/tb/T4}
add wave -noreg -logic {/tb/go}
add wave -noreg -logic {/tb/sim_start_pulse}
add wave -noreg -logic {/tb/sim_end_pulse}
add wave -noreg -logic {/tb/dumpReady}
add wave -noreg -logic {/tb/loop_done_1}
add wave -noreg -logic {/tb/loop_done_2}
add wave -noreg -logic {/tb/loop_done_3}
add wave -noreg -hexadecimal -literal -signed2 {/tb/fd}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i}
add wave -noreg -hexadecimal -literal -signed2 {/tb/j}
add wave -noreg -hexadecimal -literal -signed2 {/tb/N}
add wave -noreg -hexadecimal -literal -signed2 {/tb/P}
add wave -noreg -hexadecimal -literal -signed2 {/tb/setP}
add wave -noreg -hexadecimal -literal -signed2 {/tb/testP}
add wave -noreg -hexadecimal -literal -signed2 {/tb/TestIterations}
add wave -noreg -logic {/tb/load_instruction}
add wave -noreg -logic {/tb/fetch_instruction}
add wave -noreg -logic {/tb/exec_instruction}
add wave -noreg -logic {/tb/grab_em}
add wave -noreg -logic {/tb/compare_em}
add wave -noreg -logic {/tb/save_em}
add wave -noreg -hexadecimal -literal {/tb/instruction}
add wave -noreg -hexadecimal -literal {/tb/is_shifter}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sysCount}
add wave -noreg -hexadecimal -literal -signed2 {/tb/nextSysCount}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsBegin}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsEnd}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsBeginFetch}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsEndFetch}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsBeginExecute}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wsEndExecute}
add wave -noreg -hexadecimal -literal -signed2 {/tb/digitCount}
add wave -noreg -logic {/tb/SYNCr}
add wave -noreg -hexadecimal -literal {/tb/ROM_Address_58_HoldReg_27}
add wave -noreg -hexadecimal -literal {/tb/ROM_Address_58_HoldReg_55}
add wave -noreg -hexadecimal -literal {/tb/Status_Bits_62_HoldReg_27}
add wave -noreg -hexadecimal -literal {/tb/Status_Bits_62_HoldReg_55}
add wave -noreg -hexadecimal -literal {/tb/Return_Address_60_HoldReg_27}
add wave -noreg -hexadecimal -literal {/tb/Return_Address_60_HoldReg_55}
add wave -noreg -hexadecimal -literal {/tb/ia_debug_shifter}
add wave -noreg -hexadecimal -literal {/tb/ia_address_HoldReg_27}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_ptrHoldingReg_27}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_ptrHoldingReg_55}
add wave -noreg -hexadecimal -literal {/tb/holdRegA}
add wave -noreg -hexadecimal -literal {/tb/holdRegB}
add wave -noreg -hexadecimal -literal {/tb/holdRegC}
add wave -noreg -hexadecimal -literal {/tb/holdRegD}
add wave -noreg -hexadecimal -literal {/tb/holdRegE}
add wave -noreg -hexadecimal -literal {/tb/holdRegF}
add wave -noreg -hexadecimal -literal {/tb/holdRegM}
add wave -noreg -logic {/tb/IA_valid}
add wave -noreg -hexadecimal -literal {/tb/IA_shifter}
add wave -noreg -hexadecimal -literal {/tb/IS_shifter}
add wave -noreg -hexadecimal -literal {/tb/HoldRegIa}
add wave -noreg -hexadecimal -literal -signed2 {/tb/iHoldRegIa}
add wave -noreg -hexadecimal -literal {/tb/HoldRegIs}
add wave -noreg -hexadecimal -literal -signed2 {/tb/iHoldRegIs}
add wave -noreg -hexadecimal -literal -signed2 {/tb/branchTarget}
add wave -noreg -hexadecimal -literal -signed2 {/tb/jsrTarget}
add wave -noreg -literal {/tb/pressingThisKey}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_ptrHoldingReg}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_carry}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_opcode}
add wave -noreg -literal {/tb/s_mnemonic}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_sub}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_ci}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_fs}
add wave -noreg -literal {/tb/s_fs}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_ptr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_address}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_n_field}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_p_field}
add wave -noreg -hexadecimal -literal -signed2 {/tb/i_romnum}
add wave -noreg -literal {/tb/sBinCARRY_FF}
add wave -noreg -hexadecimal -literal {/tb/oRomSel}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iType}
add wave -noreg -literal {/tb/p_sMnemonic}
add wave -noreg -literal {/tb/p_sFieldSelect}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iDestAddr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iRomNum}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iStatusBitN}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iPointerValP}
add wave -noreg -hexadecimal -literal -signed2 {/tb/p_iConstantN}
add wave -noreg -hexadecimal -literal {/tb/p_bvDestAddr}
add wave -noreg -hexadecimal -literal {/tb/p_bvRomNum}
add wave -noreg -hexadecimal -literal {/tb/p_bvStatusBitN}
add wave -noreg -hexadecimal -literal {/tb/p_bvPointerValP}
add wave -noreg -hexadecimal -literal {/tb/p_bvConstantN}
add wave -noreg -literal {/tb/p_sOctDestAddr}
add wave -noreg -literal {/tb/p_sOctRomNum}
add wave -noreg -literal {/tb/p_sDecStatusBitN}
add wave -noreg -literal {/tb/p_sDecPointerValP}
add wave -noreg -literal {/tb/p_sDecConstantN}
add wave -noreg -hexadecimal -literal {/tb/wfm_ROM}
add wave -noreg -hexadecimal -literal {/tb/wfm_IA}
add wave -noreg -hexadecimal -literal {/tb/wfm_IS}
add wave -noreg -literal {/tb/wfm_CARRY}
add wave -noreg -literal {/tb/wfm_isMnemonic}
add wave -noreg -literal {/tb/wfm_isWS}
add wave -noreg -literal {/tb/wfm_isDestAddr}
add wave -noreg -literal {/tb/wfm_isRomNum}
add wave -noreg -literal {/tb/wfm_isFlagNum}
add wave -noreg -literal {/tb/wfm_isPtr}
add wave -noreg -literal {/tb/wfm_isN}
add wave -noreg -hexadecimal -literal {/tb/wfm_rPC}
add wave -noreg -hexadecimal -literal {/tb/wfm_rStatFlags}
add wave -noreg -hexadecimal -literal {/tb/wfm_rRA}
add wave -noreg -hexadecimal -literal -signed2 {/tb/wfm_rPtr}
add wave -noreg -hexadecimal -literal {/tb/wfm_rA}
add wave -noreg -hexadecimal -literal {/tb/wfm_rB}
add wave -noreg -hexadecimal -literal {/tb/wfm_rC}
add wave -noreg -hexadecimal -literal {/tb/wfm_rD}
add wave -noreg -hexadecimal -literal {/tb/wfm_rE}
add wave -noreg -hexadecimal -literal {/tb/wfm_rF}
add wave -noreg -hexadecimal -literal {/tb/wfm_rM}
add wave -noreg -literal {/tb/sTestNameRunning}
add wave -noreg -literal {/tb/sTestNameFinished}
add wave -noreg -literal {/tb/sRightAnswer}
add wave -noreg -hexadecimal -literal {/tb/resultA}
add wave -noreg -hexadecimal -literal {/tb/resultB}
add wave -noreg -logic {/tb/expected_carry}
add wave -noreg -logic {/tb/actual_carry}
add wave -noreg -logic {/tb/carry_match}
add wave -noreg -logic {/tb/all_regs_match}
add wave -noreg -hexadecimal -literal -signed2 {/tb/good_count}
add wave -noreg -hexadecimal -literal -signed2 {/tb/bad_count}
add wave -noreg -literal {/tb/s_dut_state}
add wave -noreg -hexadecimal -literal {/tb/state2string.state_val}
add wave -noreg -literal {/tb/state2string.state2string}
add wave -noreg -literal {/tb/columnsForKeyPress.keyName}
add wave -noreg -hexadecimal -literal {/tb/columnsForKeyPress.row}
add wave -noreg -hexadecimal -literal {/tb/columnsForKeyPress.columnsForKeyPress}
add wave -noreg -logic {/tb/fullAdd.x}
add wave -noreg -logic {/tb/fullAdd.y}
add wave -noreg -logic {/tb/fullAdd.ci}
add wave -noreg -hexadecimal -literal {/tb/fullAdd.fullAdd}
add wave -noreg -logic {/tb/fullAdd.func_fullAdd.sum}
add wave -noreg -logic {/tb/fullAdd.func_fullAdd.co}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sParseIs.iType}
add wave -noreg -literal {/tb/sParseIs.sMnemonic}
add wave -noreg -literal {/tb/sParseIs.sFieldSelect}
add wave -noreg -literal {/tb/sParseIs.sOctDestAddr}
add wave -noreg -literal {/tb/sParseIs.sOctRomNum}
add wave -noreg -literal {/tb/sParseIs.sDecStatusBitN}
add wave -noreg -literal {/tb/sParseIs.sDecPointerValP}
add wave -noreg -literal {/tb/sParseIs.sDecConstantN}
add wave -noreg -hexadecimal -literal {/tb/sParseIs.instruction}
add wave -noreg -literal {/tb/simKeyPress1ms.inKey}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIdle}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s001x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s000x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0000x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sNop}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sAddrOut}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s1x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sJsb}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sJsbWait}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sBrhWait}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sBrhOut}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s01x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s010x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0100x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sArithP}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sArithWP}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sArithWait}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0010x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00100x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sSst}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sSstDecr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sSstThis}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRst}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRstDecr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRstThis}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00101x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIst}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIstDecr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIstThis}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sStDone}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sCst}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sCstDecr}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sCstThis}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0011x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00110x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sSpt}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sSptDone}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIpt}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIptSetC}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sIptRstC}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00111x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sPtd}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sPtdNow}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sPti}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sPtiNow}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0001x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s00011x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sLdc}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sType5Wait}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sType6}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s000010x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRomSel}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s0000101x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sExtKey}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sKey}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sKeyOut}
add wave -noreg -hexadecimal -literal -signed2 {/tb/s000011x}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRet}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sRetOut}
add wave -noreg -hexadecimal -literal -signed2 {/tb/sDataStore}
cursor "Cursor 1" 0ps  
transcript on
