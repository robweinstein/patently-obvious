onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix octal /tb/wfm_ROM
add wave -noupdate -radix octal /tb/wfm_IA
add wave -noupdate -radix octal /tb/wfm_IS
add wave -noupdate /tb/wfm_CARRY
add wave -noupdate /tb/wfm_isMnemonic
add wave -noupdate /tb/wfm_isWS
add wave -noupdate -radix octal /tb/wfm_isDestAddr
add wave -noupdate -radix octal /tb/wfm_isRomNum
add wave -noupdate /tb/wfm_isFlagNum
add wave -noupdate /tb/wfm_isPtr
add wave -noupdate /tb/wfm_isN
add wave -noupdate -radix octal /tb/wfm_rPC
add wave -noupdate /tb/wfm_rStatFlags
add wave -noupdate -radix octal /tb/wfm_rRA
add wave -noupdate -radix unsigned /tb/wfm_rPtr
add wave -noupdate -radix hexadecimal /tb/wfm_rA
add wave -noupdate -radix hexadecimal /tb/wfm_rB
add wave -noupdate -radix hexadecimal /tb/wfm_rC
add wave -noupdate -radix hexadecimal /tb/wfm_rD
add wave -noupdate -radix hexadecimal /tb/wfm_rE
add wave -noupdate -radix hexadecimal /tb/wfm_rF
add wave -noupdate -radix hexadecimal /tb/wfm_rM
add wave -noupdate /tb/sTestNameRunning
add wave -noupdate /tb/sTestNameFinished
add wave -noupdate /tb/sRightAnswer
add wave -noupdate -radix hexadecimal /tb/resultA
add wave -noupdate -radix hexadecimal /tb/resultB
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1560 ps} 0}
configure wave -namecolwidth 170
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {19063009979556 ps} {19063010001076 ps}
