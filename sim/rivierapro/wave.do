onerror { resume }
set curr_transcript [transcript]
transcript off

add wave /tb/rst
add wave /tb/PWO
add wave /tb/CLOCK_OSC
add wave /tb/PHI1
add wave /tb/PHI2
add wave /tb/IA
add wave /tb/IS
add wave /tb/WS
add wave /tb/SYNC
add wave /tb/CARRY
add wave /tb/A
add wave /tb/B
add wave /tb/C
add wave /tb/D
add wave /tb/E
add wave /tb/START
add wave /tb/BCD
add wave /tb/SA
add wave /tb/SB
add wave /tb/SC
add wave /tb/SD
add wave /tb/SE
add wave /tb/SF
add wave /tb/SG
add wave /tb/SDP
add wave /tb/C_CL
add wave /tb/O1
add wave /tb/O2
add wave /tb/O3
add wave /tb/O4
add wave /tb/O5
add wave /tb/O6
add wave /tb/O7
add wave /tb/O8
add wave /tb/O9
add wave /tb/O10
add wave /tb/O11
add wave /tb/O12
add wave /tb/O13
add wave /tb/O14
add wave /tb/O15
add wave /tb/ROW0
add wave /tb/ROW1
add wave /tb/ROW2
add wave /tb/ROW3
add wave /tb/ROW4
add wave /tb/ROW5
add wave /tb/ROW6
add wave /tb/ROW7
add wave -logic /tb/COL0
add wave -logic /tb/COL2
add wave -logic /tb/COL3
add wave -logic /tb/COL4
add wave -logic /tb/COL6
add wave -literal /tb/sTestNameRunning
add wave -literal /tb/pressingThisKey
add wave -literal /tb/sTestNameFinished
add wave -literal /tb/sRightAnswer
add wave -literal /tb/resultA
add wave -literal /tb/resultB
add wave -vgroup {Execution Trace Dump} \
	( -literal /tb/wfm_ROM ) \
	( -oct -literal /tb/wfm_IA ) \
	( -oct -literal /tb/wfm_IS ) \
	( -literal /tb/wfm_CARRY ) \
	( -literal /tb/wfm_isMnemonic ) \
	( -literal /tb/wfm_isWS ) \
	( -literal /tb/wfm_isDestAddr ) \
	( -literal /tb/wfm_isRomNum ) \
	( -literal /tb/wfm_isFlagNum ) \
	( -literal /tb/wfm_isPtr ) \
	( -literal /tb/wfm_isN ) \
	( -oct -literal /tb/wfm_rPC ) \
	( -literal /tb/wfm_rStatFlags ) \
	( -oct -literal /tb/wfm_rRA ) \
	( -unsigned -literal /tb/wfm_rPtr ) \
	( -literal /tb/wfm_rA ) \
	( -literal /tb/wfm_rB ) \
	( -literal /tb/wfm_rC ) \
	( -literal /tb/wfm_rD ) \
	( -literal /tb/wfm_rE ) \
	( -literal /tb/wfm_rF ) \
	( -literal /tb/wfm_rM )
add wave -literal /tb/wfm_ROM
add wave -oct -literal /tb/wfm_IA
add wave -oct -literal /tb/wfm_IS
add wave -literal /tb/wfm_CARRY
add wave -literal /tb/wfm_isMnemonic
add wave -literal /tb/wfm_isWS
add wave -literal /tb/wfm_isDestAddr
add wave -literal /tb/wfm_isRomNum
add wave -literal /tb/wfm_isFlagNum
add wave -literal /tb/wfm_isPtr
add wave -literal /tb/wfm_isN
add wave -oct -literal /tb/wfm_rPC
add wave -literal /tb/wfm_rStatFlags
add wave -logic /tb/wfm_rStatFlags(11)
add wave -logic /tb/wfm_rStatFlags(10)
add wave -logic /tb/wfm_rStatFlags(9)
add wave -logic /tb/wfm_rStatFlags(8)
add wave -logic /tb/wfm_rStatFlags(7)
add wave -logic /tb/wfm_rStatFlags(6)
add wave -logic /tb/wfm_rStatFlags(5)
add wave -logic /tb/wfm_rStatFlags(4)
add wave -logic /tb/wfm_rStatFlags(3)
add wave -logic /tb/wfm_rStatFlags(2)
add wave -logic /tb/wfm_rStatFlags(1)
add wave -logic /tb/wfm_rStatFlags(0)
add wave -oct -literal /tb/wfm_rRA
add wave -unsigned -literal /tb/wfm_rPtr
add wave -literal /tb/wfm_rA
add wave -literal /tb/wfm_rB
add wave -literal /tb/wfm_rC
add wave -literal /tb/wfm_rD
add wave -literal /tb/wfm_rE
add wave -literal /tb/wfm_rF
add wave -literal /tb/wfm_rM
add wave -logic /tb/inst_arc/display_ff
add wave -oct /tb/ROM_Address_58_HoldReg_55
add wave /tb/Status_Bits_62_HoldReg_27
add wave /tb/Status_Bits_62_HoldReg_55
add wave -oct /tb/Return_Address_60_HoldReg_27
add wave -oct /tb/Return_Address_60_HoldReg_55
add wave -oct /tb/ia_address_HoldReg_27
add wave /tb/i_ptrHoldingReg_27
add wave /tb/i_ptrHoldingReg_55
add wave /tb/holdRegA
add wave /tb/holdRegB
add wave /tb/holdRegC
add wave /tb/holdRegD
add wave /tb/holdRegE
add wave /tb/holdRegF
add wave /tb/holdRegM
add wave /tb/IA_valid
add wave /tb/IA_shifter
add wave /tb/IS_shifter
add wave -oct /tb/HoldRegIa
add wave -oct /tb/HoldRegIs
add wave /tb/iHoldRegIa
add wave /tb/iHoldRegIs
add wave -label ROM0/active /tb/inst_rom0/active
add wave -label ROM1/active /tb/inst_rom1/active
add wave -label ROM2/active /tb/inst_rom2/active
add wave /tb/p_iType
add wave /tb/p_sMnemonic
add wave /tb/p_sFieldSelect
add wave -oct /tb/p_bvDestAddr
add wave /tb/p_iRomNum
add wave -height 16 /tb/p_iStatusBitN
add wave /tb/p_iPointerValP
add wave /tb/p_iConstantN
add wave /tb/WS
add wave /tb/CARRY
add wave -label C&T/CARRY_FF /tb/inst_ctc/CARRY_FF
add wave -expand -vgroup /tb/inst_arc \
	/tb/inst_arc/t1 \
	/tb/inst_arc/t2 \
	/tb/inst_arc/t3 \
	/tb/inst_arc/t4
add wave -expand -vgroup /tb/inst_ctc \
	/tb/inst_ctc/IA \
	/tb/inst_ctc/WS \
	/tb/inst_ctc/SYNC \
	/tb/inst_ctc/ROW0 \
	/tb/inst_ctc/ROW1 \
	/tb/inst_ctc/ROW2 \
	/tb/inst_ctc/ROW3 \
	/tb/inst_ctc/ROW4 \
	/tb/inst_ctc/ROW5 \
	/tb/inst_ctc/ROW6 \
	/tb/inst_ctc/ROW7 \
	/tb/inst_ctc/PHI1 \
	/tb/inst_ctc/PHI2 \
	/tb/inst_ctc/PWO \
	/tb/inst_ctc/IS \
	/tb/inst_ctc/CARRY \
	/tb/inst_ctc/COL0 \
	/tb/inst_ctc/COL2 \
	/tb/inst_ctc/COL3 \
	/tb/inst_ctc/COL4 \
	/tb/inst_ctc/COL6 \
	/tb/inst_ctc/debug_lfsr_fb_xnor \
	/tb/inst_ctc/debug_lfsr_fb_xor \
	/tb/inst_ctc/pwor \
	/tb/inst_ctc/q \
	/tb/inst_ctc/T1 \
	/tb/inst_ctc/T2 \
	/tb/inst_ctc/T3 \
	/tb/inst_ctc/T4 \
	/tb/inst_ctc/b5 \
	/tb/inst_ctc/b14 \
	/tb/inst_ctc/b18 \
	/tb/inst_ctc/b26 \
	/tb/inst_ctc/b35 \
	/tb/inst_ctc/b45 \
	/tb/inst_ctc/b54 \
	/tb/inst_ctc/b55 \
	/tb/inst_ctc/b19_b26 \
	/tb/inst_ctc/stepaddr \
	/tb/inst_ctc/kdn \
	/tb/inst_ctc/keydown \
	/tb/inst_ctc/newkey \
	/tb/inst_ctc/keyset_s0 \
	/tb/inst_ctc/keybuf \
	/tb/inst_ctc/JSB \
	/tb/inst_ctc/BRH \
	/tb/inst_ctc/PTONLY \
	/tb/inst_ctc/UP2PT \
	/tb/inst_ctc/ARITHW \
	/tb/inst_ctc/ISTW \
	/tb/inst_ctc/STDECN \
	/tb/inst_ctc/SST \
	/tb/inst_ctc/RST \
	/tb/inst_ctc/IST \
	/tb/inst_ctc/SPT \
	/tb/inst_ctc/IPTR \
	/tb/inst_ctc/IPTS \
	/tb/inst_ctc/PTD \
	/tb/inst_ctc/PTI \
	/tb/inst_ctc/TKR \
	/tb/inst_ctc/RET \
	/tb/inst_ctc/sr \
	/tb/inst_ctc/abuf \
	/tb/inst_ctc/ptr \
	/tb/inst_ctc/CARRY_FF \
	/tb/inst_ctc/dSr28 \
	/tb/inst_ctc/iamux \
	/tb/inst_ctc/dPtr4 \
	/tb/inst_ctc/sum2sr \
	/tb/inst_ctc/sr2sr \
	/tb/inst_ctc/makesum \
	/tb/inst_ctc/makesum_r \
	/tb/inst_ctc/chgstat \
	/tb/inst_ctc/sr2x \
	/tb/inst_ctc/p2x \
	/tb/inst_ctc/pn2x \
	/tb/inst_ctc/one2y \
	/tb/inst_ctc/srn2y \
	/tb/inst_ctc/sr2y \
	/tb/inst_ctc/sr2ci \
	/tb/inst_ctc/co2ci \
	/tb/inst_ctc/sum2p \
	/tb/inst_ctc/sumn2p \
	/tb/inst_ctc/pwq \
	/tb/inst_ctc/ld_dig0 \
	/tb/inst_ctc/set_ws \
	/tb/inst_ctc/clr_ws \
	/tb/inst_ctc/ws_out \
	/tb/inst_ctc/ws_req \
	/tb/inst_ctc/ws_oe \
	/tb/inst_ctc/ws_in \
	/tb/inst_ctc/stq \
	/tb/inst_ctc/stqzero \
	/tb/inst_ctc/arith_cyc \
	/tb/inst_ctc/ipt_cyc \
	/tb/inst_ctc/ist_cyc \
	/tb/inst_ctc/arith_cset \
	/tb/inst_ctc/arith_crst \
	/tb/inst_ctc/ist_cset \
	/tb/inst_ctc/ist_crst \
	/tb/inst_ctc/ipt_cset \
	/tb/inst_ctc/ipt_crst \
	/tb/inst_ctc/x_in \
	/tb/inst_ctc/y_in \
	/tb/inst_ctc/c_in \
	/tb/inst_ctc/sum \
	/tb/inst_ctc/co \
	/tb/inst_ctc/cor \
	/tb/inst_ctc/NewArch \
	/tb/inst_ctc/T1 \
	/tb/inst_ctc/T2 \
	/tb/inst_ctc/T3 \
	/tb/inst_ctc/T4
add wave -vgroup /tb/inst_adrv \
	/tb/inst_adrv/SDP \
	/tb/inst_adrv/SA \
	/tb/inst_adrv/SB \
	/tb/inst_adrv/SC \
	/tb/inst_adrv/SD \
	/tb/inst_adrv/SE \
	/tb/inst_adrv/SF \
	/tb/inst_adrv/SG \
	/tb/inst_adrv/C_CL \
	/tb/inst_adrv/PHI1 \
	/tb/inst_adrv/PHI2 \
	/tb/inst_adrv/A \
	/tb/inst_adrv/B \
	/tb/inst_adrv/C \
	/tb/inst_adrv/D \
	/tb/inst_adrv/E \
	/tb/inst_adrv/LOW_BATT \
	/tb/inst_adrv/CLOCK_OSC \
	/tb/inst_adrv/CLOCK_2X \
	/tb/inst_adrv/reset \
	/tb/inst_adrv/Q0 \
	/tb/inst_adrv/QL \
	/tb/inst_adrv/Q1 \
	/tb/inst_adrv/Q2 \
	/tb/inst_adrv/Q3 \
	/tb/inst_adrv/Q4 \
	/tb/inst_adrv/debug_count \
	/tb/inst_adrv/NewArch
add wave -vgroup /tb/inst_cdrv \
	( -logic /tb/inst_cdrv/START ) \
	( -logic /tb/inst_cdrv/C_CL ) \
	/tb/inst_cdrv/O1 \
	/tb/inst_cdrv/O2 \
	/tb/inst_cdrv/O3 \
	/tb/inst_cdrv/O4 \
	/tb/inst_cdrv/O5 \
	/tb/inst_cdrv/O6 \
	/tb/inst_cdrv/O7 \
	/tb/inst_cdrv/O8 \
	/tb/inst_cdrv/O9 \
	/tb/inst_cdrv/O10 \
	/tb/inst_cdrv/O11 \
	/tb/inst_cdrv/O12 \
	/tb/inst_cdrv/O13 \
	/tb/inst_cdrv/O14 \
	/tb/inst_cdrv/O15 \
	/tb/inst_cdrv/A \
	/tb/inst_cdrv/ACn \
	/tb/inst_cdrv/AnCn \
	/tb/inst_cdrv/XnSn \
	/tb/inst_cdrv/X \
	/tb/inst_cdrv/Xn \
	/tb/inst_cdrv/O1n \
	/tb/inst_cdrv/O2n \
	/tb/inst_cdrv/O3n \
	/tb/inst_cdrv/O4n \
	/tb/inst_cdrv/O5n \
	/tb/inst_cdrv/O6n \
	/tb/inst_cdrv/O7n \
	/tb/inst_cdrv/O8n \
	/tb/inst_cdrv/O9n \
	/tb/inst_cdrv/O10n \
	/tb/inst_cdrv/O11n \
	/tb/inst_cdrv/O12n \
	/tb/inst_cdrv/O13n \
	/tb/inst_cdrv/O14n \
	/tb/inst_cdrv/O15n \
	/tb/inst_cdrv/NewArch
add wave -divider {Named row}
wv.cursors.add -time 13361325001ns -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.cursors.subcursor.add -time 2474410us+3 -name {Cursor 1}
wv.cursors.setactive -name {Cursor 1}
wv.zoom.range -from 830ms -to 1154766312500ps
wv.time.unit.auto.set
transcript $curr_transcript
