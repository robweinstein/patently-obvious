//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Entire HP-35 Calculator
//
// Description:
//      This testbench connects the five main circuits comprising the HP-35
//      calculator:
//        - Control and Timing Circuit 16
//        - Arithmetic and Register Circuit 20
//        - Read-Only Memory Circuit 18 (3 Instances)
//        - Anode Driver of Output Display Unit 14 
//        - Cathode Driver of Output Display Unit 14 
// 
// Notes:
//      Some passages of US 4,001,569 are quoted verbatim as comments in this
//      module and are indicated like this:
//         '569 (col): "<quoted text>"
//      Where "col" is the column number in the patent document.
// 
// Include Files :
//      state_names.v
//      35bn_rom0_binary.txt
//      35bn_rom1_binary.txt
//      35bn_rom2_binary.txt
//
// Output Files : tracedump.txt
//
// Conventions:
//    - UPPER case for signals described in the '569 patent.
//    - Internal wires and registers are 'lower' case.
//    - State name parameters are camelCase starting with a lower case 's',
//      e.g., 'sIdle'.
//    - Other parameters are first letter 'Upper'.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2037 - HP-35 Calculator - The Whole Enchilada - Testench Diagram
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

    // Include the parameter that defines the state names:
    `include "../../src/state_names.v"

    // ---------------------------------------------------------------------------------------------
    // Signal Declarations
    // 

    reg     PWO;                // '569 (4) "A power on circuit 36 in power supply unit 24 supplies
                                // a signal forcing the calculator to start up in a known condition
                                // when power is supplied thereto."
                                // '569 (7) "As the system power comes on, the PWO signal is held at
                                // logic l for at least 20 milliseconds." 

    reg     CLOCK_OSC = 1'b0;   // '569 (12) "The clock generator uses an external LC series circuit
                                // to set the oscillator frequency."
                                // "The square-wave oscillator frequency (all times in this section
                                // will be referred to an 800 KHz oscillator frequency..."

    wire    PHI1, PHI2;         // '569 (12) "The two-phase clock signals [PHI1] and [PHI2] are
                                // generated from flip-flops BL and B1 [in the Anode Driver] and the
                                // 800 KHz oscillator 100'. They are on for 625 nsec and separated
                                // by 625 [nsec] as shown in FIG. 18."

    wire    IA;                 // '569 (3) "The Ia line 32 serially carries the addresses of the
                                // instructions to be read from ROM�s 0-7. These addresses originate
                                // from control and timing circuit 16, which contains an instruction
                                // address register that is incremented each word time unless a JUMP
                                // SUBROUTINE or a BRANCH instruction is being executed.  Each
                                // address is transferred to ROM�s 0-7 during bit times b19-b26 and
                                // is stored in an address register of each ROM. However, only one
                                // ROM is active at a time, and only the active ROM responds to an
                                // address by outputting an instruction on the Is line 28. Control
                                // is transferred between ROM�s by a ROM SELECT instruction.  This
                                // technique allows a single eight-bit address, plus eight special
                                // instructions, to address up to eight ROM's of 256 words each."

    tri0    IS;                 // '569 (3) "The Is buss 28 carries 10-bit instructions from the
                                // active ROM in the read-only memory circuit 18 to the other
                                // ROM�s, control and timing circuit 16, arithmetic and register
                                // circuit 20, and auxiliary data storage circuit 25, each of which
                                // decodes the instructions locally and responds to or acts upon
                                // them if they pertain thereto and ignores them if they do not.
                                // For instance, the ADD instruction affects arithmetic and register
                                // circuit 20 but is ignored by control and timing circuit 16.
                                // Similarly, the SET STATUS BIT 5 instruction sets status flip-flop
                                // 5 in control and timing circuit 16 but is ignored by arithmetic
                                // and register circuit 20."

    tri0    WS;                 // '569 (3) "The WS buss 30 carries an enable signal from control
                                // and timing circuit 16 or one of the ROM�s in read-only memory
                                // circuit 18 to arithmetic and register circuit 20 to enable the
                                // instruction being executed thereby. Thus, in the example of the
                                // previous paragraph, addition occurs only during digit 2 since the
                                // adder in the arithmetic and register circuit 20 is enabled by WS
                                // buss 30 only during this portion of the word. When WS buss 30 is
                                // low, the contents of the registers in arithmetic and register
                                // circuit 20 are recirculated unchanged."

    wire    SYNC;               // '569 (3)  "The SYNC buss 26 carries synchronization signals
                                // from control and timing circuit 16 to ROM�s 0-7 in read-only
                                // memory circuit 18 and to arithmetic and register circuit 20
                                // to synchronize the calculator system.  It provides one output
                                // each word time. This output also functions as a 10-bit wide
                                // window (b45-b54) during which Is buss 28 is active."

    wire    CARRY;              // '569 (4) "The carry line 34 transmits the status of the carry
                                // output of the adder in arithmetic and register circuit 20 to
                                // control and timing circuit 16. The control and timing circuit
                                // uses this information to make conditional branches, dependent
                                // upon the numerical value of the contents of the registers in
                                // arithmetic and register circuit 20."

    wire    A, B, C, D, E;      // '569 (4) "The primary outputs of the calculator are five output
                                // lines 38 connected between a display decoder of arithmetic and
                                // register circuit 20 and an anode driver of output display unit
                                // 14.  Data for a seven-segment display plus a decimal point is
                                // time-multiplexed onto these five output lines."

    wire    START;              // '569 (4) "A start line 40 is connected from the display decoder
                                // of arithmetic and register circuit 20 to the auxiliary data
                                // storage circuit 25 and a cathode driver of output display unit 14
                                // and indicates when the digit 0 occurs."

    tri0    BCD;                // '569 (4) "A BCD input/output line 35 interconnects the auxiliary
                                // data storage circuit 25 and the C register of arithmetic and
                                // register circuit 20. This line always outputs the contents of the
                                // C register of arithmetic and register circuit 20 unless a specifc
                                // instruction to input to the C register of the arithmetic and
                                // register circuit is being executed."

    wire    SA, SB, SC, SD,     // '569 (12) "... the display information is partially decoded in 
            SE, SF, SG, SDP;    // arithmetic and register circuit 20 and completely decoded into 
                                // seven segment plus decimal point signals in the bipolar anode
                                // driver of output display unit 14."

    wire    C_CL;               // '569 (12) "One other periodic signal is derived in the anode
                                // driver.  Once each digit time a signal (counter-clock) [C_CL] is
                                // sent to the cathode driver of output display unit 14 (the
                                // trailing edge of this signal will step the display to the next
                                // digit)."

    wire    O1,  O2,  O3,  O4,  // '569 (13) "The cathode driver of output display unit 14 comprises
            O5,  O6,  O7,  O8,  // a 15 position shift register for scanning the 15 digit display
            O9,  O10, O11, O12, // once each word time.�This scanning operation moves from digit to
            O13, O14, O15;      // digit in response to counter clock signals from the anode driver."

    wire    ROW0, ROW1, ROW2,   // '569 (5) "System counter 42 is also employed as a keyboard
            ROW3, ROW4, ROW5,   // scanner as shown in FIG. 5. The most significant three bits of
            ROW6, ROW7;         // system counter 42 go to a one-of-eight decoder 48, which
                                // sequentially selects one of the keyboard row lines 50."
    
    reg     COL0, COL2, COL3,   // '569 (5) "The least significant three bits of the system counter
            COL4, COL6;         // count modulo seven and go to a one-of-eight multiplexor 52, which
                                // sequentially selects one of the keyboard column lines 54 (during
                                // 16 clock times no key is scanned)."
                
    //----------------------------------------------------------------------------------------------
    // Testbench signals
    reg         rst;
//    reg         T1 = 1'b0, T2 = 1'b0, T3 = 1'b0, T4 = 1'b0;
    wire        T1, T2, T3, T4;
    reg         go = 1'b0;
    reg         sim_start_pulse = 1'b0;
    reg         sim_end_pulse   = 1'b0;
    reg         dumpReady = 1'b0;
    reg         loop_done_1;
    reg         loop_done_2;
    reg         loop_done_3;
    integer     fd; // File Descriptor
    integer     i, j, N, P, setP, testP;
    integer     TestIterations;
    reg         load_instruction = 1'b0;
    reg         fetch_instruction = 1'b0;
    reg         exec_instruction = 1'b0;
    reg         grab_em;
    reg         compare_em;
    reg         save_em;
    reg [9:0]   instruction     = 'b0;
    reg [55:0]  is_shifter      = 'b0;

    integer     sysCount        = 0;
    integer     nextSysCount    = 1;
    integer     wsBegin         = -1;
    integer     wsEnd           = -1;
    integer     wsBeginFetch    = -1;
    integer     wsEndFetch      = -1;
    integer     wsBeginExecute  = -1;
    integer     wsEndExecute    = -1;
    integer     digitCount      = 0;
    reg         SYNCr           = 1'b0; // Registered version of SYNC for edge detect.

    reg [7:0]   ROM_Address_58_HoldReg_27,    ROM_Address_58_HoldReg_55;
    reg [11:0]  Status_Bits_62_HoldReg_27,    Status_Bits_62_HoldReg_55;
    reg [7:0]   Return_Address_60_HoldReg_27, Return_Address_60_HoldReg_55;
    reg [7:0]   ia_debug_shifter;
    reg [7:0]   ia_address_HoldReg_27;
    integer     i_ptrHoldingReg_27  = 0;
    integer     i_ptrHoldingReg_55  = 0;
    reg [56:1]  holdRegA        = 'b0;
    reg [56:1]  holdRegB        = 'b0;
    reg [56:1]  holdRegC        = 'b0;
    reg [56:1]  holdRegD        = 'b0;
    reg [56:1]  holdRegE        = 'b0;
    reg [56:1]  holdRegF        = 'b0;
    reg [56:1]  holdRegM        = 'b0;

    reg         IA_valid    = 1'b0; // Asserted during the bit times b19 through b26, when IA is valid.
    reg [7:0]   IA_shifter  = 'b0;  // 8-bit serial-to-parallel address shifter for IA.
    reg [9:0]   IS_shifter  = 'b0;  // 10-bit serial-to-parallel instruction shifter for IS.
    reg [7:0]   HoldRegIa   = 'b0;  // Captures the address at the end of the shift sequence as an 8-bit register.
    integer     iHoldRegIa  = 0;    //    "      "     "    "   "   "  "   "    "      "     "  an integer.
    reg [9:0]   HoldRegIs   = 'b0;  // Captures the instruction at the end of the shift sequence as a 10-bit register.
    integer     iHoldRegIs  = 0;    //    "      "       "      "   "   "  "   "    "      "     "  an integer.

    integer     branchTarget = 0;
    integer     jsrTarget = 0;
    string      pressingThisKey = "";  // No press.

    integer     i_ptrHoldingReg;
    integer     i_carry;
    integer     i_opcode;

    // For fdisplay:
    string      s_mnemonic;
    integer     i_sub;
    integer     i_ci;
    integer     i_fs;
    string      s_fs;
    integer     i_ptr;
    integer     i_address;
    integer     i_n_field;
    integer     i_p_field;
    integer     i_romnum; 
    string      sBinCARRY_FF;
    reg [2:0]   oRomSel;    // Convert the three ROMSEL bits into a digit for display.

    // Outputs from parseIs() task:
    integer  p_iType;
    string   p_sMnemonic;
    string   p_sFieldSelect;
    integer  p_iDestAddr;
    integer  p_iRomNum;
    integer  p_iStatusBitN;
    integer  p_iPointerValP;
    integer  p_iConstantN;
    wire [7:0]  p_bvDestAddr;       // Convert p_iDestAddr (integer) to bit vector so I can display it in octal without leading zeros.
    wire [2:0]  p_bvRomNum;         // <ditto>
    wire [3:0]  p_bvStatusBitN;     // <ditto>
    wire [3:0]  p_bvPointerValP;    // <ditto>
    wire [3:0]  p_bvConstantN;      // <ditto>

    // Outputs from sParseIs() task:
    string   p_sOctDestAddr;
    string   p_sOctRomNum;
    string   p_sDecStatusBitN;
    string   p_sDecPointerValP;
    string   p_sDecConstantN;

    // Execution trace dump for waveform display...
    reg [2:0]   wfm_ROM;
    reg [7:0]   wfm_IA;
    reg [9:0]   wfm_IS;
    string      wfm_CARRY;
    string      wfm_isMnemonic;
    string      wfm_isWS;
    string      wfm_isDestAddr;
    string      wfm_isRomNum;
    string      wfm_isFlagNum;
    string      wfm_isPtr;
    string      wfm_isN;
    reg [7:0]   wfm_rPC;
    reg [11:0]  wfm_rStatFlags;
    reg [7:0]   wfm_rRA;
    integer     wfm_rPtr;
    reg [56:1]  wfm_rA;
    reg [56:1]  wfm_rB;
    reg [56:1]  wfm_rC;
    reg [56:1]  wfm_rD;
    reg [56:1]  wfm_rE;
    reg [56:1]  wfm_rF;
    reg [56:1]  wfm_rM;

    string      sTestNameRunning;
    string      sTestNameFinished;
    string      sRightAnswer;
    reg [56:1]  resultA;
    reg [56:1]  resultB;

    reg         expected_carry  = 1'b0;
    reg         actual_carry    = 1'b0;
    reg         carry_match     = 1'b0;
    reg         all_regs_match  = 1'b0;

    integer     good_count;
    integer     bad_count; 

//    reg [1:62]  state, next;
    string      s_dut_state;


// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 

    function string state2string;
        input [1:62] state_val;
        begin : func_st2str
            case (1'b1)
                // Main Instruction Parsing Loop
                state_val[sIdle]        : state2string = "sIdle";       // Power On (PWO) state, this bit is set, all others reset.  Test for Type 1.                                       
                state_val[s0x]          : state2string = "s0x";         // Test for Type 2.                                                                                                 
                state_val[s00x]         : state2string = "s00x";        // Test for Type 3 or Type 4.                                                                                       
                state_val[s001x]        : state2string = "s001x";       // Test for Type 4.                                                                                                 
                state_val[s000x]        : state2string = "s000x";       // Test for Type 5.                                                                                                 
                state_val[s0000x]       : state2string = "s0000x";      // Test for Type 6.                                                                                                 
                state_val[sNop]         : state2string = "sNop";        // Instruction failed tests for Types 1 through 6, so it's a No Op.                                                 
                state_val[sAddrOut]     : state2string = "sAddrOut";    // Return point for instructions that use the default ROM Address 58.                                               
                // Type 1 (Jump/Branch) Instructions                                                                                                                                        
                state_val[s1x]          : state2string = "s1x";         // Decoded a Type 1 instruction so test for JSB or BRH.                                                             
                state_val[sJsb]         : state2string = "sJsb";        // Decoded JSB (Jump Subroutine).                                                                                   
                state_val[sJsbWait]     : state2string = "sJsbWait";    // Wait for completeion of JSB.                                                                                     
                state_val[sBrhWait]     : state2string = "sBrhWait";    // Decoded BRH (Conditional Branch).                                                                                
                state_val[sBrhOut]      : state2string = "sBrhOut";     // Drive the captured branch address out Ia.                                                                        
                // Type 2 (Arithmetic) Instructions                                                                                                                                         
                state_val[s01x]         : state2string = "s01x";        // Decoded a Type 2 (Arithmetic) instruction so parse the first Word Select bit.                                    
                state_val[s010x]        : state2string = "s010x";       // Parse the second Word Select bit.                                                                                
                state_val[s0100x]       : state2string = "s0100x";      // Parse the third Word Select bit.                                                                                 
                state_val[sArithP]      : state2string = "sArithP";     // Word Select is 'Pointer Only'.                                                                                   
                state_val[sArithWP]     : state2string = "sArithWP";    // Word Select is 'Up to Pointer'.                                                                                  
                state_val[sArithWait]   : state2string = "sArithWait";  // Wait for arithmetic instruction to complete in the A&R chip.                                                     
                // Type 3 (Status Flag) Instructions                                                                                                                                              
                state_val[s0010x]       : state2string = "s0010x";      // Decoded a Type 3 (Status) instruction so parse the next two bits to narrow it down.                              
                state_val[s00100x]      : state2string = "s00100x";     // Determine whether the instruction is Set Status Flag (F=00) or Reset Status Flag (F=10).                         
                state_val[sSst]         : state2string = "sSst";        // Decoded the Set Status Flag instruction.                                                                         
                state_val[sSstDecr]     : state2string = "sSstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sSstThis]     : state2string = "sSstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sRst]         : state2string = "sRst";        // Decoded the Reset Status Flag instruction.                                                                       
                state_val[sRstDecr]     : state2string = "sRstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sRstThis]     : state2string = "sRstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[s00101x]      : state2string = "s00101x";     // Determine whether the instruction is Interrogate Status Flag (F=01) or Clear All Status Flags (F=11).            
                state_val[sIst]         : state2string = "sIst";        // Decoded the Interrogate Status Flag instruction.                                                                 
                state_val[sIstDecr]     : state2string = "sIstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sIstThis]     : state2string = "sIstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sStDone]      : state2string = "sStDone";     // Wait for the last status bit position.                                                                           
                state_val[sCst]         : state2string = "sCst";        // Decoded the Clear All Status Flags instruction.                                                                  
                state_val[sCstDecr]     : state2string = "sCstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sCstThis]     : state2string = "sCstThis";    // The selected status bit is now at the end of the 28-bit shift register so begin clearing to the end of the field.
                // Type 4 (Pointer) Instructions                                                                                                                                            
                state_val[s0011x]       : state2string = "s0011x";      // Decoded a Type 4 (Pointer) instruction so parse the next two bits to narrow it down.                             
                state_val[s00110x]      : state2string = "s00110x";     // Determine whether the instruction is Set Pointer (F=00) or Interrogate Pointer (F=10).                           
                state_val[sSpt]         : state2string = "sSpt";        // Decoded the Set Pointer instruction.                                                                             
                state_val[sSptDone]     : state2string = "sSptDone";    // Set Pointer is done so wait until it's time to enter the address output state.                                   
                state_val[sIpt]         : state2string = "sIpt";        // Decoded the Interrogate Pointer instruction.                                                                     
                state_val[sIptSetC]     : state2string = "sIptSetC";    // If pointer is equal to this instruction's P field then set carry.      
                state_val[sIptRstC]     : state2string = "sIptRstC";    // If pointer is NOT equal to this instruction's P field then RESET carry.
                state_val[s00111x]      : state2string = "s00111x";     // Determine whether the instruction is Decrement Pointer (F=01) or Increment Pointer (F=11).                       
                state_val[sPtd]         : state2string = "sPtd";        // Decoded the Decrement Pointer instruction.                                                                       
                state_val[sPtdNow]      : state2string = "sPtdNow";     // The four clock periods in which the pointer is decremented.                                                      
                state_val[sPti]         : state2string = "sPti";        // Decoded the Increment Pointer instruction.                                                                       
                state_val[sPtiNow]      : state2string = "sPtiNow";     // The four clock periods in which the pointer is incremented.                                                      
                // Type 5 (Data Entry/Display) Instructions                                                                                                                                 
                state_val[s0001x]       : state2string = "s0001x";      // Decoded a Type 5 (Data Entry/Display) instruction so parse the next two bits to narrow it down.
                state_val[s00011x]      : state2string = "s00011x";     // Determine whether the instruction is LDC (F=01) or other (F=11).
                state_val[sLdc]         : state2string = "sLdc";        // Decoded the LOAD CONSTANT (LDC) instruction.  Generate Word Select at pointer-only then decrement pointer.
                state_val[sType5Wait]   : state2string = "sType5Wait";  // All Type 5 instructions other than LDC are executed in the the A&R circuit so wait here for completion.
                // Type 6 (Misc) Instructions                                                                                                                                               
                state_val[sType6]       : state2string = "sType6";      // Decoded a Type 6 instruction so parse the next two bits to narrow it down.                                       
                state_val[s000010x]     : state2string = "s000010x";    // Determine whether the instruction is ROM Select (F=00) or one of two Key Entry instructions (F=10).              
                state_val[sRomSel]      : state2string = "sRomSel";     // Instruction is ROM Select so wait here while the ROMs execute the function.                                      
                state_val[s0000101x]    : state2string = "s0000101x";   // Determine whether the instruction is External Key-Code Entry or Keyboard Entry.                                  
                state_val[sExtKey]      : state2string = "sExtKey";     // Decoded the External Key-Code Entry instruction that's not supported in the HP-35 so just wait.                  
                state_val[sKey]         : state2string = "sKey";        // Decoded the Keyboard Entry instruction.                                                                          
                state_val[sKeyOut]      : state2string = "sKeyOut";     // Shift the contents of the Key-Code Buffer 56 to the address line, Ia.                                            
                state_val[s000011x]     : state2string = "s000011x";    // Determine whether the instruction is Return from Subroutine (F=01) or Data Store (F=11).                         
                state_val[sRet]         : state2string = "sRet";        // Decoded the Subroutine Return instruction.                                                                       
                state_val[sRetOut]      : state2string = "sRetOut";     // Shift the contents of the Return Address 60 shift register field to the address line, Ia.                        
                state_val[sDataStore]   : state2string = "sDataStore";  // Decoded a Data Storage instruction that's not part of the C&T chip so just wait. (Not supported in the HP-35.)   
            endcase
        end
    endfunction

    // ---------------------------------------------------------------------------------------------
    // For the specified key, this function returns the five COL inputs as they would be driven by
    // the ROWs of the keyboard matrix when that key is pressed.  The keyboard matrix is connected
    // as follows:
    //                                                   keydown
    //                                                     ^
    //                                                     |
    //                                 ----------------------------------------
    //                               /                                          \
    //      System Counter [2:0]--> /   110      100      011      010      000  \ 
    //                             /   COL6     COL4     COL3     COL2     COL0   \
    //                             ------------------------------------------------
    //    ----------------------         ^        ^        ^        ^        ^ 
    //                          |        |        |        |        |        |
    //              000 -> ROW0 |->----[x^y]----[log]----[ln]-----[e^x]----[clr]   
    //                          |        |        |        |        |        |
    //              101 -> ROW5 |->--[sqrt(x)]--[arc]----[sin]----[cos]----[tan]   
    //                          |        |        |        |        |        |
    //              001 -> ROW1 |->----[1/x]---[x<->y]--[roll]----[sto]----[rcl]   
    //                          |        |        |        |        |        |
    //    System    111 -> ROW7 |->---[Enter]-----|------[chs]----[eex]----[clx]   
    //    Counter               |        |        |        |        |        |
    //    [5:3]     110 -> ROW6 |->---[Minus]----[7]------[8]------[9]-------|-  
    //                          |        |        |        |        |        |
    //              010 -> ROW2 |->---[Plus]-----[4]------[5]------[6]-------|-
    //                          |        |        |        |        |        |
    //              011 -> ROW3 |->---[Mult]-----[1]------[2]------[3]-------|-
    //                          |        |        |        |        |        |
    //              100 -> ROW4 |->--[Divide]----[0]------[.]-----[pi]-------|-
    //                          |
    //    ---------------------- 
    //
    function [4:0] columnsForKeyPress;
        input string keyName;
        input [7:0] row;
        begin : func_keyMatrix
            case (keyName)
                // Column 6                              C6      C4      C3      C2      C0
                "key_x^y"       : columnsForKeyPress = {row[0], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 0,6
                "key_sqrt_x"    : columnsForKeyPress = {row[5], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 5,6
                "key_1/x"       : columnsForKeyPress = {row[1], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 1,6
                "key_enter"     : columnsForKeyPress = {row[7], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 7,6
                "key_minus"     : columnsForKeyPress = {row[6], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 6,6
                "key_plus"      : columnsForKeyPress = {row[2], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 2,6
                "key_mult"      : columnsForKeyPress = {row[3], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 3,6
                "key_div"       : columnsForKeyPress = {row[4], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 4,6
                // Column 4                              C6      C4      C3      C2      C0      
                "key_log"       : columnsForKeyPress = {1'b0, row[0],   1'b0,   1'b0,   1'b0};   // Row,Col = 0,4
                "key_arc"       : columnsForKeyPress = {1'b0, row[5],   1'b0,   1'b0,   1'b0};   // Row,Col = 5,4
                "key_x<->y"     : columnsForKeyPress = {1'b0, row[1],   1'b0,   1'b0,   1'b0};   // Row,Col = 1,4
                "key_spare74"   : columnsForKeyPress = {1'b0, row[7],   1'b0,   1'b0,   1'b0};   // Row,Col = 7,4
                "key_7"         : columnsForKeyPress = {1'b0, row[6],   1'b0,   1'b0,   1'b0};   // Row,Col = 6,4
                "key_4"         : columnsForKeyPress = {1'b0, row[2],   1'b0,   1'b0,   1'b0};   // Row,Col = 2,4
                "key_1"         : columnsForKeyPress = {1'b0, row[3],   1'b0,   1'b0,   1'b0};   // Row,Col = 3,4
                "key_0"         : columnsForKeyPress = {1'b0, row[4],   1'b0,   1'b0,   1'b0};   // Row,Col = 4,4
                // Column 3                              C6      C4      C3      C2      C0      
                "key_ln"        : columnsForKeyPress = {1'b0,   1'b0, row[0],   1'b0,   1'b0};   // Row,Col = 0,3
                "key_sin"       : columnsForKeyPress = {1'b0,   1'b0, row[5],   1'b0,   1'b0};   // Row,Col = 5,3
                "key_roll"      : columnsForKeyPress = {1'b0,   1'b0, row[1],   1'b0,   1'b0};   // Row,Col = 1,3
                "key_chs"       : columnsForKeyPress = {1'b0,   1'b0, row[7],   1'b0,   1'b0};   // Row,Col = 7,3
                "key_8"         : columnsForKeyPress = {1'b0,   1'b0, row[6],   1'b0,   1'b0};   // Row,Col = 6,3
                "key_5"         : columnsForKeyPress = {1'b0,   1'b0, row[2],   1'b0,   1'b0};   // Row,Col = 2,3
                "key_2"         : columnsForKeyPress = {1'b0,   1'b0, row[3],   1'b0,   1'b0};   // Row,Col = 3,3
                "key_."         : columnsForKeyPress = {1'b0,   1'b0, row[4],   1'b0,   1'b0};   // Row,Col = 4,3
                // Column 2                              C6      C4      C3      C2      C0      
                "key_e^x"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[0],   1'b0};   // Row,Col = 0,2
                "key_cos"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[5],   1'b0};   // Row,Col = 5,2
                "key_sto"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[1],   1'b0};   // Row,Col = 1,2
                "key_eex"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[7],   1'b0};   // Row,Col = 7,2
                "key_9"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[6],   1'b0};   // Row,Col = 6,2
                "key_6"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[2],   1'b0};   // Row,Col = 2,2
                "key_3"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[3],   1'b0};   // Row,Col = 3,2
                "key_pi"        : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[4],   1'b0};   // Row,Col = 4,2
                // Column 0                              C6      C4      C3      C2      C0      
                "key_clr"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[0]};   // Row,Col = 0,0
                "key_tan"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[5]};   // Row,Col = 5,0
                "key_rcl"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[1]};   // Row,Col = 1,0
                "key_clx"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[7]};   // Row,Col = 7,0
                "key_spr60"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[6]};   // Row,Col = 6,0
                "key_spr20"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[2]};   // Row,Col = 2,0
                "key_spr30"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[3]};   // Row,Col = 3,0
                "key_spr40"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[4]};   // Row,Col = 4,0
                // No key pressed                        C6      C4      C3      C2      C0      
                default         : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0,   1'b0};   // No key press.
            endcase
        end
    endfunction

    function [1:0] fullAdd;
    // output  sum;         // Sum
    // output  co;          // Carry out
        input   x;           // Augend
        input   y;           // Addend
        input   ci;          // Carry In
        begin : func_fullAdd
//            reg sum = 1'b0;
//            reg co  = 1'b0;
            reg sum;
            reg co;
            sum = x ^ y ^ ci;
            co  = x & y | x & ci | y & ci;
            fullAdd = {sum, co};
        end
    endfunction

    // ---------------------------------------------------------------------------------------------
    // Parse the 10-bit instruction word into its type, mnemonic, and other embedded fields.
    // Outputs are all strings so they can be empty strings for instructions that don't have certain
    // fields (except iType which is an integer).
    // Example call:
    //     sParseIs(iType, sMnemonic, sFieldSelect, sOctDestAddr, sOctRomNum, sDecStatusBitN, sDecPointerValP, sDecConstantN, instruction);
    task sParseIs;
        output integer  iType;
        output string   sMnemonic;
        output string   sFieldSelect;
        output string   sOctDestAddr;
        output string   sOctRomNum;
        output string   sDecStatusBitN;
        output string   sDecPointerValP;
        output string   sDecConstantN;
        input  [9:0]    instruction;
        begin : task_parse_instruction
            // Default values for instructions that don't contain these fields.
            iType           = -1;
            sMnemonic       = "fail";
            sFieldSelect    = "";
            sOctDestAddr    = "";
            sOctRomNum      = "";
            sDecStatusBitN  = "";
            sDecPointerValP = "";
            sDecConstantN   = "";
            casez (instruction)
                // ---------------------------------------------------------------------------------
                10'b?????????1 : begin  // Type 1 Instructions
                    iType       = 1;
//                    iDestAddr   = instruction[9:2];
                    $swrite(sOctDestAddr, "%o", instruction[9:2]); // Convert 'instruction[9:2]' into an OCTAL string.
                    if (instruction[1]) 
                        sMnemonic = "CBRH";
                    else
                        sMnemonic = "JSR";
                end
                // ---------------------------------------------------------------------------------
                10'b????????10 : begin  // Type 2 Instructions
                    iType       = 2;
                    case (instruction[4:2])
                        // Parse Field Select
                        3'b000 : sFieldSelect = "P";    // Pointer position.
                        3'b100 : sFieldSelect = "WP";   // Up to pointer position.
                        3'b010 : sFieldSelect = "X";    // Exponent field (with sign).
                        3'b110 : sFieldSelect = "XS";   // Exponent sign.
                        3'b001 : sFieldSelect = "M";    // Mantissa field without sign.
                        3'b101 : sFieldSelect = "MS";   // Mantissa field with sign.
                        3'b011 : sFieldSelect = "W";    // Entire word.
                        3'b111 : sFieldSelect = "S";    // Mantissa sign only.
                    endcase

                    case (instruction[9:5])
                        // Parse Arithmetic Instructions
                        // -- Class 1) Clear
                        5'b10111 : sMnemonic = "CLRA";      // 0 -> A
                        5'b00001 : sMnemonic = "CLRB";      // 0 -> B
                        5'b00110 : sMnemonic = "CLRC";      // 0 -> C
                        // -- Class 2) Transfer/Exchange    
                        5'b01001 : sMnemonic = "MOVAB";     // A -> B
                        5'b00100 : sMnemonic = "MOVBC";     // B -> C
                        5'b01100 : sMnemonic = "MOVCA";     // C -> A
                        5'b11001 : sMnemonic = "XCHAB";     // A <-> B
                        5'b10001 : sMnemonic = "XCHBC";     // B <-> C
                        5'b11101 : sMnemonic = "XCHAC";     // A <-> C
                        // -- Class 3) Add/Subtract         
                        5'b01110 : sMnemonic = "ADDACC";    // A + C -> C
                        5'b01010 : sMnemonic = "SUBACC";    // A - C -> C
                        5'b11100 : sMnemonic = "ADDABA";    // A + B -> A
                        5'b11000 : sMnemonic = "SUBABA";    // A - B -> A
                        5'b11110 : sMnemonic = "ADDACA";    // A + C -> A
                        5'b11010 : sMnemonic = "SUBACA";    // A - C -> A
                        5'b10101 : sMnemonic = "ADDCCC";    // C + C -> C
                        // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                        5'b00000 : sMnemonic = "CMP0B";     // 0 - B            (Compare B to zero)
                        5'b01101 : sMnemonic = "CMP0C";     // 0 - C            (Compare C to zero)
                        5'b00010 : sMnemonic = "CMPAC";     // A - C            (Compare A and C)
                        5'b10000 : sMnemonic = "CMPAB";     // A - B            (Compare A and B)
                        5'b10011 : sMnemonic = "CMPA1";     // A - 1            (Compare A to one)
                        5'b00011 : sMnemonic = "CMPC1";     // C - 1            (Compare C to one)    
                        // -- Class 5) Complement           
                        5'b00101 : sMnemonic = "TCC";       // 0 - C -> C       (Tens Complement register C)
                        5'b00111 : sMnemonic = "NCC";       // 0 - C - 1 -> C   (Nines Complement register C)
                        // -- Class 6) Increment            
                        5'b11111 : sMnemonic = "INCA";      // A + 1 -> A       (Increment register A)
                        5'b01111 : sMnemonic = "INCC";      // C + 1 -> C       (Increment register C)
                        // -- Class 7) Decrement            
                        5'b11011 : sMnemonic = "DECA";      // A - 1 -> A       (Decrement register A)
                        5'b01011 : sMnemonic = "DECC";      // C - 1 -> C       (Decrement register C)
                        // -- Class 8) Shift                
                        5'b10110 : sMnemonic = "SHRA";      // A >> 1           (Shift Right register A)
                        5'b10100 : sMnemonic = "SHRB";      // B >> 1           (Shift Right register B)
                        5'b10010 : sMnemonic = "SHRC";      // C >> 1           (Shift Right register C)
                        5'b01000 : sMnemonic = "SHLA";      // A << 1           (Shift Left register A)
                    endcase
                end
                // ---------------------------------------------------------------------------------
                10'b??????0100 : begin  // Type 3 Instructions (Status Operations)
                    iType       = 3;
//                    iStatusBitN = instruction[9:6];
                    $swrite(sDecStatusBitN, "%d", instruction[9:6]); // Convert 'instruction[9:6]' into a DECIMAL string.
                    case (instruction[5:4])
                        2'b00 : sMnemonic = "SSTN"; // Set Status Flag N
                        2'b01 : sMnemonic = "ISTN"; // Interrogate Status Flag N
                        2'b10 : sMnemonic = "RSTN"; // Reset Status Flag N
                        2'b11 : sMnemonic = "CSTN"; // Clear All Flags  (If N=0 then clear all flags, if N>0 then clear flags N down to 0)
                    endcase
                end
                // ---------------------------------------------------------------------------------
                10'b??????1100 : begin  // Type 4 Instructions (Pointer Operations)
                    iType       = 4;
//                    iPointerValP = instruction[9:6];
                    $swrite(sDecPointerValP, "%d", instruction[9:6]); // Convert 'instruction[9:6]' into an DECIMAL string.
                    case (instruction[5:4])
                        2'b00 : sMnemonic = "SPTP"; // Set Pointer to P     (P-field = New Pointer Value)
                        2'b01 : sMnemonic = "PTD";  // Decrement Pointer    (P-field = xxxx)
                        2'b10 : sMnemonic = "IPTP"; // Interrogate Pointer  (Set Carry if Pointer = P-field)
                        2'b11 : sMnemonic = "PTI";  // Increment Pointer    (P-field = xxxx)
                    endcase
                end
                // ---------------------------------------------------------------------------------
                10'b??????1000 : begin  // Type 5 Instructions (Data Entry and Display)
                    iType       = 5;
                    casez (instruction[6:4])
                        3'b?00 : sMnemonic = "AvailType5";
                        3'b?01 : begin 
                            sMnemonic = "LDC";
//                            iConstantN = instruction[9:6];
                            $swrite(sDecConstantN, "%d", instruction[9:6]); // Convert 'instruction[9:6]' into an DECIMAL string.
                        end
                        3'b01? : begin 
                            case (instruction[9:7])
                                3'b000 : sMnemonic = "DSPTOG";  // Display Toggle
                                3'b001 : sMnemonic = "XCHCM";   // C -> M -> C              (Exchange Memory)
                                3'b010 : sMnemonic = "PUSHC";   // C -> C -> D -> E -> F    (Up Stack or Push C) 
                                3'b011 : sMnemonic = "POPA";    // F -> F -> E -> D -> A    (Down Stack or Pop A)
                                3'b100 : sMnemonic = "DSPOFF";  // Display Off
                                3'b101 : sMnemonic = "RCLM";    // M -> M -> C              (Recall Memory)
                                3'b110 : sMnemonic = "ROTDN";   // C -> F -> E -> D -> C    (Rotate Down)
                                3'b111 : sMnemonic = "CLRALL";  // 0 -> A, B, C, D, E, F, M (Clear all 56 bits of all registers)
                            endcase
                        end
                        3'b11? : begin 
                            if (instruction[7])
                                sMnemonic = "MOVBCDC";  // BCD -> C                 (Load 56-bit data storage value into register C)
                            else 
                                sMnemonic = "MOVISA";   // Is -> A                  (Load 56-bit instruction sequence into register A)
                        end
                        default : sMnemonic = "BogusType5";
                    endcase
                end
                // ---------------------------------------------------------------------------------
                10'b?????10000 : begin  // Type 6 Instructions (ROM Select and Misc.)
                    iType       = 6;
                    casez (instruction[9:5])
                        5'b???00 : begin                        // ROM Select
                            sMnemonic = "ROMSEL";
//                            iRomNum   = instruction[9:7];
                            $swrite(sOctRomNum, "%o", instruction[9:7]); // Convert 'instruction[9:7]' into an OCTAL string.
                        end
                        5'b???_01 : sMnemonic = "RET";      // Subroutine Return
                        5'b??_010 : sMnemonic = "EKEY";     // External Keyboard Entry (Not implemented in HP-35)
                        5'b??_110 : sMnemonic = "KEYJMP";   // Keyboard Entry (Jump to ROM address specified by key-code)
                        5'b1?0_11 : sMnemonic = "SNDA";     // Send Address from C Register to Aux Data Storage (Not implemented in HP-35)
                        5'b101_11 : sMnemonic = "SNDD";     // Send Data from C Register to Aux Data Storage (Not implemented in HP-35)
                        default   : sMnemonic = "BogusType6";
                    endcase
                end
                // ---------------------------------------------------------------------------------
                10'b????100000 : begin  // Type 7 Instructions (Reserved for Program Storage MOS Circuit)
                    iType       = 7;
                    sMnemonic   = "BogusType7";
                end
                // ---------------------------------------------------------------------------------
                10'b???1000000 : begin  // Type 8 Instructions (Reserved for Program Storage MOS Circuit)
                    iType       = 8;
                    sMnemonic   = "BogusType8";
                end
                // ---------------------------------------------------------------------------------
                10'b???0000000 : begin  // Type 9 Instructions (Available)
                    iType       = 9;
                    sMnemonic   = "BogusType9";
                end
                // ---------------------------------------------------------------------------------
                10'b0000000000 : begin  // Type 10 Instructions (No Operation NOP)
                    iType       = 10;
                    sMnemonic   = "NOP";
                end
                // ---------------------------------------------------------------------------------
                default : begin  // Parse error
                    iType       = 99;
                    sMnemonic   = "ParseError";
                end
                // ---------------------------------------------------------------------------------
            endcase
        end
    endtask
    // ---------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------
    task simKeyPress1ms;
        input  string inKey;
        begin : task_sim_key1
            pressingThisKey = inKey;                            // Press key.
            wait (Status_Bits_62_HoldReg_55[0] == 1'b1); #1;    // Hold key until it's recognized.
            #1ms;                                               // Keep holding key for a millisecond.
            pressingThisKey = "";                               // Release the key.
            wait (Status_Bits_62_HoldReg_55[8] == 1'b0); #1;    // Wait until the key service routine returns to the main wait loop.
            wait (Status_Bits_62_HoldReg_55[8] == 1'b1); #1;    // Wait until we finish the debounce wait loop and the key is released.
            #1ms;                                               // Wait an extra millisecond before permitting next key press.
        end
    endtask
    // ---------------------------------------------------------------------------------------------

// -------------------------------------------------------------------------------------------------
// Test Processes

    // Generate 800 kHz clock
    always #625 CLOCK_OSC = ~CLOCK_OSC;

    // Generate 200 kHz clock
//    always #2.5us PHI2 = ~PHI2;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    initial begin
        PWO = 1'b0;
        #10;
        PWO = 1'b1;
        #300us;
        PWO = 1'b0;
        #10ms;
//        wait;
    end

    assign {T1, T2, T3, T4} = {inst_ctc.T1, inst_ctc.T2, inst_ctc.T3, inst_ctc.T4};

    // -----------------------------------------------
    // Two-Process System Counter
    // --
    // Combinational next count process...
    always @* begin : proc_nextsyscount
        if (~SYNC & SYNCr)  // Falling edge of SYNC
            nextSysCount = 0;
        else
            nextSysCount = sysCount + 1;
    end
    // --
    // Registered count process...
    always@(posedge PHI2 or posedge rst) begin : proc_syscount
        SYNCr <= SYNC;
        if (rst) begin
            sysCount <= 0;
            digitCount <= 0;
        end
        else begin
            sysCount <= nextSysCount;
            digitCount <= nextSysCount / 4;
        end
        s_dut_state <= state2string(inst_ctc.inst_mpc.next); // For fdisplay.
    end
    // --
    // End Two-Process System Counter
    // -----------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Process to simulate a keypress.  
    // 
    // For the specified key, this process connects one of the eight ROW outputs to one of the five
    // COL inputs based on the HP-35 keyboard matrix layout.
    //   
    // To press a key, simply assign the appropriate string value to pressingThisKey.
    // For example:
    //      pressingThisKey <= "key_sqrt_x";
    //      #1ms; 
    //      pressingThisKey <= "";  // No press.
    // 
    always @* begin : proc_pressKey
        {COL6,COL4,COL3,COL2,COL0} = columnsForKeyPress(pressingThisKey,{ROW7,ROW6,ROW5,ROW4,ROW3,ROW2,ROW1,ROW0});
    end
    // ---------------------------------------------------------------------------------------------

    always@(posedge PHI2 or posedge rst) begin : proc_rom_capture
        if (rst) begin
            IA_valid <= 1'b0;   // Asserted during the bit times b19 through b26, when IA is valid.
            IA_shifter <= 'b0;  // 8-bit serial-to-parallel address shifter for IA.
            IS_shifter <= 'b0;  // 10-bit serial-to-parallel instruction shifter for IS.
            HoldRegIa  <= 'b0;  // Captures the address at the end of the shift sequence as an 8-bit register.
            iHoldRegIa <= 0;    //    "      "     "    "   "   "  "   "    "      "     "  an integer.
            HoldRegIs  <= 'b0;  // Captures the instruction at the end of the shift sequence as a 10-bit register.
            iHoldRegIs <= 0;    //    "      "       "      "   "   "  "   "    "      "     "  an integer.
        end
        else begin
            if (nextSysCount == 19) 
                IA_valid <= 1'b1;
            else if (sysCount == 26) 
                IA_valid <= 1'b0;

            if (IA_valid) 
                IA_shifter <= {IA, IA_shifter[7:1]};   // Shift right.

            if (sysCount == 27) begin
                HoldRegIa  <= IA_shifter;   // Capture the address in an 8-bit register.
                iHoldRegIa <= IA_shifter;   // Capture the address in an integer.
            end

            if (SYNC) 
                IS_shifter <= {IS, IS_shifter[9:1]};   // Shift right.

            if (sysCount == 55) begin
                HoldRegIs  <= IS_shifter;   // Capture the instruction in a 10-bit register.
                iHoldRegIs <= IS_shifter;   // Capture the instruction in an integer.
//                parseIs(p_iType, p_sMnemonic, p_sFieldSelect, p_iDestAddr, p_iRomNum, p_iStatusBitN, p_iPointerValP, p_iConstantN, IS_shifter);
                sParseIs(p_iType, p_sMnemonic, p_sFieldSelect, p_sOctDestAddr, p_sOctRomNum, p_sDecStatusBitN, p_sDecPointerValP, p_sDecConstantN, IS_shifter);

                // Convert the three ROMSEL bits into a digit for display.
                oRomSel <= 3'b???;
                case (1'b1)
                    inst_rom0.active : oRomSel <= 3'o0;
                    inst_rom1.active : oRomSel <= 3'o1;
                    inst_rom2.active : oRomSel <= 3'o2;
                endcase
            end
        end
    end

    // If register value is 1, then output a "1", else output a "" (null string).
    assign sBinCARRY_FF = (inst_ctc.CARRY_FF)? "1" : "";

    // Convert the three ROMSEL bits into a digit for display.
//    assign oRomSel = (inst_rom0.active)? 3'o0 : (inst_rom1.active)? 3'o1 : (inst_rom2.active)? 3'o2 : 3'b???;


//    assign p_bvDestAddr     = (p_iDestAddr    < 0)? 8'b???????? : p_iDestAddr;      // Convert integer to bit vector so I can display it in octal without leading zeros.
//    assign p_bvRomNum       = (p_iRomNum      < 0)? 3'b???      : p_iRomNum;        // <ditto>
//    assign p_bvStatusBitN   = (p_iStatusBitN  < 0)? 4'b????     : p_iStatusBitN;    // <ditto>
//    assign p_bvPointerValP  = (p_iPointerValP < 0)? 4'b????     : p_iPointerValP;   // <ditto>
//    assign p_bvConstantN    = (p_iConstantN   < 0)? 4'b????     : p_iConstantN;     // <ditto>

    // -------------------------------------------------------------------------
    // Test results pipeline
    always@(posedge PHI2 or posedge rst) begin : proc_tpipe
        if (rst) begin
            fetch_instruction   <= 1'b0;
            exec_instruction    <= 1'b0;
            grab_em             <= 1'b0;
            compare_em          <= 1'b0;
            save_em             <= 1'b0;


//            good_count      <= 0;
//            bad_count       <= 0;
//
//            clearInstructionCounts();
        end
        else begin
            if (nextSysCount == 0) begin
                fetch_instruction <= load_instruction;
                exec_instruction  <= fetch_instruction;
                grab_em  <= exec_instruction;
            end
            else begin
                // 'grab_em' pulses simultaneously with START.
                grab_em  <= 1'b0;  // End the pulse.  
            end

            compare_em <= grab_em;
            save_em <= compare_em;

            if (nextSysCount == 0) begin
            end

            if (grab_em) begin
                // Capture expected results:
                // Capture actual results:
            end
        end
    end
    // -------------------------------------------------------------------------
//
//    assign all_regs_match = 1'b1;
//
    // ---------------------------------------------------------------------------------------------
    // Debug:  Capture and hold the register contents at the midpoint and end of every word cycle.
    //
    // The shift register contents at bit time 27 and bit time 55:
    // 
    //          |<-- ROM Address 58 --->|<--------- Status Bits 62 -------->|<- Return Address 60 ->|
    //          |                       |                                   |                       |
    // sr[28:1] |28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1|
    //
    always@(posedge PHI2 or posedge rst) begin : proc_hold_regs
        if (rst) begin
            ROM_Address_58_HoldReg_27       <= 'b0;
            Status_Bits_62_HoldReg_27       <= 'b0;
            Return_Address_60_HoldReg_27    <= 'b0;
            ROM_Address_58_HoldReg_55       <= 'b0;
            Status_Bits_62_HoldReg_55       <= 'b0;
            Return_Address_60_HoldReg_55    <= 'b0;
            ia_address_HoldReg_27           <= 'b0;
            i_ptrHoldingReg_27              <= 0;
            i_ptrHoldingReg_55              <= 0;

            holdRegA    <= 'b0;
            holdRegB    <= 'b0;
            holdRegC    <= 'b0;
            holdRegD    <= 'b0;
            holdRegE    <= 'b0;
            holdRegF    <= 'b0;
            holdRegM    <= 'b0;
        end
        else begin
            if (sysCount == 27) begin
                ROM_Address_58_HoldReg_27       <= inst_ctc.sr[28:21];
                Status_Bits_62_HoldReg_27       <= inst_ctc.sr[20:9];
                Return_Address_60_HoldReg_27    <= inst_ctc.sr[8:1];
                ia_address_HoldReg_27           <= ia_debug_shifter;
                i_ptrHoldingReg_27              <= inst_ctc.ptr[4:1];
            end
            if (sysCount == 55) begin
                ROM_Address_58_HoldReg_55       <= inst_ctc.sr[28:21];
                Status_Bits_62_HoldReg_55       <= inst_ctc.sr[20:9]; 
                Return_Address_60_HoldReg_55    <= inst_ctc.sr[8:1];  
                i_ptrHoldingReg_55              <= inst_ctc.ptr[4:1];
            end
            if (START) begin
                holdRegA    <= inst_arc.regA[56:1];
                holdRegB    <= inst_arc.regB;
                holdRegC    <= inst_arc.regC[56:1];
                holdRegD    <= inst_arc.regD;
                holdRegE    <= inst_arc.regE;
                holdRegF    <= inst_arc.regF;
                holdRegM    <= inst_arc.regM;
            end

        end

        // --
        if (rst)
            ia_debug_shifter    <= 'b0;
        else
            ia_debug_shifter    <= {IA,ia_debug_shifter[7:1]};  // Shift right
        // --
    end
    // End debug
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Trace instruction execution into a dump file.
    always@(posedge PHI2 or posedge rst) begin : proc_dump_inst
        if (rst) 
            dumpReady <= 1'b0;
        else if (sim_start_pulse) begin
            $fdisplay(fd, "ROM IA  IS   CARRY isMnemonic isWS isDestAddr isRomNum isFlagNum isPtr isN rPC rStatFlags   rRA rPtr rA             rB             rC             rD             rE             rF             rM");
            $fdisplay(fd, "oct oct oct  bin   str        str  oct        oct      dec       dec   dec oct bin          oct dec  bcd            bcd            bcd            bcd            bcd            bcd            bcd");
            //             0   000 0000 0     xxxxxxxxxx xx   000        0        00        00    00  000 000000000000 000 00   00000000000000 00000000000000 00000000000000 00000000000000 00000000000000 00000000000000 00000000000000 
            $fdisplay(fd, "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            dumpReady <= 1'b1;
        end
        else if (dumpReady && sysCount == 0) begin
            $fdisplay(fd, "%1o   %3o %4o %1s     %10s %2s   %3s        %1s        %2s        %2s    %2s  %3o %12b %3o %2d   %14h %14h %14h %14h %14h %14h %14h", 
                      oRomSel,
                      HoldRegIa,
                      HoldRegIs,
                      sBinCARRY_FF,
                      p_sMnemonic,          // From parsing the serial IS stream.
                      p_sFieldSelect,       //                "
                      p_sOctDestAddr,       //                "
                      p_sOctRomNum,         //                "
                      p_sDecStatusBitN,     //                "
                      p_sDecPointerValP,    //                "
                      p_sDecConstantN,      //                "
                      ROM_Address_58_HoldReg_55,
                      Status_Bits_62_HoldReg_55,
                      Return_Address_60_HoldReg_55,
                      i_ptrHoldingReg_55,
                      inst_arc.regA[56:1],
                      inst_arc.regB,
                      inst_arc.regC[56:1],
                      inst_arc.regD,
                      inst_arc.regE,
                      inst_arc.regF,
                      inst_arc.regM
                     );
            // Same signals for waveform display...
            wfm_ROM         <= oRomSel;                                                   
            wfm_IA          <= HoldRegIa;                                                 
            wfm_IS          <= HoldRegIs;                                                 
            wfm_CARRY       <= sBinCARRY_FF;                                              
            wfm_isMnemonic  <= p_sMnemonic;          // From parsing the serial IS stream.
            wfm_isWS        <= p_sFieldSelect;       //                "                  
            wfm_isDestAddr  <= p_sOctDestAddr;       //                "                  
            wfm_isRomNum    <= p_sOctRomNum;         //                "                  
            wfm_isFlagNum   <= p_sDecStatusBitN;     //                "                  
            wfm_isPtr       <= p_sDecPointerValP;    //                "                  
            wfm_isN         <= p_sDecConstantN;      //                "                  
            wfm_rPC         <= ROM_Address_58_HoldReg_55;                                 
            wfm_rStatFlags  <= Status_Bits_62_HoldReg_55;                                 
            wfm_rRA         <= Return_Address_60_HoldReg_55;                              
            wfm_rPtr        <= i_ptrHoldingReg_55;                                        
            wfm_rA          <= inst_arc.regA[56:1];
            wfm_rB          <= inst_arc.regB;
            wfm_rC          <= inst_arc.regC[56:1];
            wfm_rD          <= inst_arc.regD;
            wfm_rE          <= inst_arc.regE;
            wfm_rF          <= inst_arc.regF;
            wfm_rM          <= inst_arc.regM;
        end
    end
    // ---------------------------------------------------------------------------------------------

    initial begin
        fd = $fopen("tracedump.txt", "w");
        sim_start_pulse = 1'b0;
        sim_end_pulse   = 1'b0;
        loop_done_1 = 1'b0;
        loop_done_2 = 1'b0;
        loop_done_3 = 1'b0;
        wait (rst == 1'b1);
        wait (rst == 1'b0);
        wait (PWO == 1'b0);
        @(posedge PHI2);
        sim_start_pulse = 1'b1;
        @(posedge PHI2);
        sim_start_pulse = 1'b0;
        @(posedge PHI2);
//        afterCall_ready = 1'b0;

        // =========================================
        // *** Press Some Keys ***
        // =========================================

        #60ms;

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Instant Arithmetic, Page 1
//        sTestNameRunning = "To add 3 to 12";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "15.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To add 3 to 12"
//        // Instant Arithmetic, Page 1
//        sTestNameRunning = "To subtract 3 from 12";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_minus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "9.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To subtract 3 from 12"
//        // Instant Arithmetic, Page 2
//        sTestNameRunning = "To multiply 3 times 12";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "36.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To multiply 3 times 12"
//        // Instant Arithmetic, Page 2
//        sTestNameRunning = "To divide 3 into 12";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_div");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "4.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To divide 3 into 12"
//        // Instant Arithmetic, Page 2
//        sTestNameRunning = "To double 3";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "6.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To double 3"
//        // Instant Arithmetic, Page 2
//        sTestNameRunning = "To square 3";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "9.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "To square 3"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Serial Calculation, Page 3
//        sTestNameRunning = "Find the sum of the first five odd numbers";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "25.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Find the sum of the first five odd numbers"
//        // Serial Calculation, Page 3
//        sTestNameRunning = "Find the product of the first five even numbers";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_8");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "3840.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Find the product of the first five even numbers"
//        // Serial Calculation, Page 4
//        sTestNameRunning = "((2 + 3)/4 + 5)*6";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "37.5";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "((2 + 3)/4 + 5)*6"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Sum of Products, Page 4
//        sTestNameRunning = "(12 x 1.58) + (8 x 2.67) + (16 x 0.54)";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_8");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_8");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "48.96";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(12 x 1.58) + (8 x 2.67) + (16 x 0.54)"
//        // -----------------------------------------------------------------------------------------
//
//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Product of Sums, Page 5
//        sTestNameRunning = "(7 + 3) * (5 + 11) * (13 + 17)";
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "4800.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(7 + 3) * (5 + 11) * (13 + 17)"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // The Operational Stack, Page 7
//        sTestNameRunning = "(3 x 4) + (5 x 6)";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "42.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(3 x 4) + (5 x 6)"
//        // -----------------------------------------------------------------------------------------
//
//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Some Simple Problems, Page 8
//        sTestNameRunning = "(a) Square root of 49";
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_sqrt_x");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "7.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(a) Square root of 49"
//        //
//        // Some Simple Problems, Page 8
//        sTestNameRunning = "(b) Reciprocal of 25";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_1/x");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = ".04";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(b) Reciprocal of 25"
//        //
//        // Some Simple Problems, Page 8
//        sTestNameRunning = "(c) Hypotenuse of a right triangle of sides 3, 4";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_sqrt_x");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "5.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(c) Hypotenuse of a right triangle of sides 3, 4"
//        //
//        // Some Simple Problems, Page 9
//        sTestNameRunning = "(d) Find the area of a circle with a 3 foot radius";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_pi");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "28.27433389";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "(d) Find the area of a circle with a 3 foot radius"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Powers of Numbers, Page 9
//        sTestNameRunning = "2x2x2x2x2x2x2 = 2^7";
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "128.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "2x2x2x2x2x2x2 = 2^7"
//        //
//        // Problems in Finance, Page 10
//        sTestNameRunning = "Compound Interest:  Value=(1+Rate)^Years";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "2.292018319";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Compound Interest:  Value=(1+Rate)^Years"
//        //
//        // Problems in Finance, Page 10
//        sTestNameRunning = "Annual Growtn Rate:  1+Rate=(Final_sales/Initial_sales)^(1/Years)";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_minus");
//        simKeyPress1ms("key_1/x");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_eex");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_eex");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "1.057551118";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Annual Growtn Rate:  1+Rate=(Final_sales/Initial_sales)^(1/Years)"
//        //
//        // Problems in Finance, Page 11
//        sTestNameRunning = "Monthly Payment = (Principal x Rate) / (1 - (1/(1+Rate)^Num_payments))";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_x^y");
//        simKeyPress1ms("key_1/x");
//        simKeyPress1ms("key_minus");
//        simKeyPress1ms("key_div");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "179.86";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Monthly Payment = (Principal x Rate) / (1 - (1/(1+Rate)^Num_payments))"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Big and Little Numbers, Page 12
//        sTestNameRunning = "Square 987654";
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_8");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "9.754604237e11";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Square 987654"
//        //
//        // Big and Little Numbers, Page 12
//        sTestNameRunning = "Reciprocal of previous result:  1/(987654^2)";
//        simKeyPress1ms("key_1/x");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "1.025156916e-12";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Reciprocal of previous result: 1/(987654^2)"
//        //
//        // Big and Little Numbers, Page 12
//        sTestNameRunning = "Enter Exponent:  1.56 x 10^12";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_eex");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "1.56e12";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Enter Exponent:  1.56 x 10^12"
//        //
//        // Big and Little Numbers, Page 13
//        sTestNameRunning = "Exact powers of 10 using EEX";
//        simKeyPress1ms("key_clx");
//        simKeyPress1ms("key_eex");
//        simKeyPress1ms("key_6");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "1.e06";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Exact powers of 10 using EEX"
//        //
//        // Big and Little Numbers, Page 13
//        sTestNameRunning = "Negative powers of 10 using CHS";
//        simKeyPress1ms("key_clx");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_eex");
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_1");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "9.109e-31";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Negative powers of 10 using CHS"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Negative Numbers, Page 14
//        sTestNameRunning = "Compute the product:  (-3)(-4)(-5)(-6)";
//        simKeyPress1ms("key_clx");
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_mult");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "360.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Compute the product:  (-3)(-4)(-5)(-6)"
//        //
//        // More Memory, Page 15
//        sTestNameRunning = "Using STO and RCL";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_sto");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = ".36";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Using STO and RCL"
//        //
//        // Rearranging the Stack, Page 16
//        sTestNameRunning = "Find 9th root of 512";
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_1/x");
//        simKeyPress1ms("key_x<->y");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "2.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Find 9th root of 512"
//        // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // HP-35 Operating Manual, Section 1
//        // Logarithms and Trigonometry, Page 17
//        sTestNameRunning = "Find antilog base 10 of 2:  2^10";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "100.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Find antilog base 10 of 2:  2^10"
//        //
//        // Logarithms and Trigonometry, Page 18
//        sTestNameRunning = "Find altitude from barometric pressure difference: = 25000 x ln(30/pressure)";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_9");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_ln");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "29012.19233";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Find altitude from barometric pressure difference: = 25000 x ln(30/pressure)"
//        //
//        // Logarithms and Trigonometry, Page 18
//        sTestNameRunning = "Trig functions:  sin(30.5 deg)";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_sin");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = ".5075383628";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Trig functions:  sin(30.5 deg)"
//        //
//        // Logarithms and Trigonometry, Page 19
//        sTestNameRunning = "Trig functions:  cos(150 deg)";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_cos");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "-.8660254041";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Trig functions:  cos(150 deg)"
//        //
//        // Logarithms and Trigonometry, Page 19
//        sTestNameRunning = "Trig functions:  tan(-25.6 deg)";
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_tan");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "-.4791197214";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Trig functions:  tan(-25.6 deg)"
//        //
//        // Logarithms and Trigonometry, Page 19
//        sTestNameRunning = "Inverse Trig functions:  arcsin(.3 deg)";
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_arc");
//        simKeyPress1ms("key_sin");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "17.45760312";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Inverse Trig functions:  arcsin(.3 deg)"
//        //
//        // Logarithms and Trigonometry, Page 19
//        sTestNameRunning = "Inverse Trig functions:  arccos(-.7 deg)";
//        simKeyPress1ms("key_chs");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_arc");
//        simKeyPress1ms("key_cos");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "134.427004";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Inverse Trig functions:  arccos(-.7 deg)"
//        //
//        // Logarithms and Trigonometry, Page 19
//        sTestNameRunning = "Inverse Trig functions:  arctan(10.2 deg)";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_arc");
//        simKeyPress1ms("key_tan");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "84.40066068";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Inverse Trig functions:  arccos(-.7 deg)"
//        //
//        // Logarithms and Trigonometry, Page 20
//        sTestNameRunning = "Degree-minutes-seconds to decimal degrees:  35 deg, 17 min, 47 sec";
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_7");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_sto");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_plus");
//        simKeyPress1ms("key_rcl");
//        simKeyPress1ms("key_div");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
//        sRightAnswer = "35.29638889";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of "Degree-minutes-seconds to decimal degrees:  35 deg, 17 min, 47 sec"
//        // This is the last example in Section 1 of the HP-35 Operating Manual.
//        // -----------------------------------------------------------------------------------------

        // -----------------------------------------------------------------------------------------
        // HP-35 Operating Manual, Section 3
        // Sample Problem 1, Solution on Page 32
        sTestNameRunning = "Evaluate:  (3x4) + (5x6) + (7x8)";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_8");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_plus");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "98.";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  (3x4) + (5x6) + (7x8)"
        //
        // Sample Problem 2, Solution on Page 32
        sTestNameRunning = "Evaluate:  (3+4)(5+6)(7+8)";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_8");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "1155.";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  (3+4)(5+6)(7+8)"
        //
        // Sample Problem 3, Solution on Page 32
        sTestNameRunning = "Evaluate:  ((4x5)/7 + 29/(3x11))((19/(2+4) + (13+pi)/4))";
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_9");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_9");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_pi");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "26.90641536";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  ((4x5)/7 + 29/(3x11))((19/(2+4) + (13+pi)/4))"
        //
        // Sample Problem 4, Solution on Page 32
        sTestNameRunning = "Evaluate:  1/(1/3 + 1/6)";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_1/x");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "2.";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  1/(1/3 + 1/6)"
        //
        // Sample Problem 5, Solution on Page 32
        sTestNameRunning = "Evaluate:  3+1/(7+(1/(15+(1/1+(1/292)))))";
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_9");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_1/x");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_plus");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "3.141592653";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  3+1/(7+(1/(15+(1/1+(1/292)))))"
        //
        // Sample Problem 6, Solution on Page 32
        sTestNameRunning = "Evaluate:  60 arccos(cos(45)cos(150) + sin(45)sin(150)cos(60))";
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_cos");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_cos");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_sin");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_sin");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_cos");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_arc");
        simKeyPress1ms("key_cos");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "6949.392474";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Evaluate:  60 arccos(cos(45)cos(150) + sin(45)sin(150)cos(60))"
        //
        // Sample Problem 7a, Solution on Page 33
        sTestNameRunning = "(a) Find sides x and y of a right triangle when R=5, theta=30";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_tan");
        simKeyPress1ms("key_x<->y");
        simKeyPress1ms("key_cos");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "x = 4.33012702";
        resultA = wfm_rA;
        resultB = wfm_rB;
        #500ms;
        simKeyPress1ms("key_mult");
        sRightAnswer = "y = 2.5";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "(a) Find sides x and y of a right triangle when R=5, theta=30"
        //
        // Sample Problem 7b, Solution on Page 33
        sTestNameRunning = "(b) Find angle and radius of a right triangle when x=4, y=3";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_arc");
        simKeyPress1ms("key_tan");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "angle = 36.86989764";
        resultA = wfm_rA;
        resultB = wfm_rB;
        #20ms;
        simKeyPress1ms("key_sin");
        simKeyPress1ms("key_div");
        sRightAnswer = "radius = 5.000000003";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "(b) Find angle and radius of a right triangle when x=4, y=3"
        //
        // Sample Problem 8a, Solution on Page 32
        sTestNameRunning = "Convert the following to centimeters:  (a) 5ft 3in";
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_.");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_sto");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_plus");
        simKeyPress1ms("key_rcl");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "160.02";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Convert the following to centimeters:  (a) 5ft 3in"
        //
        // Sample Problem 8b, Solution on Page 32
        sTestNameRunning = "Convert continued:  (b)  37in";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_rcl");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "93.98";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Convert continued:  (b)  37in"
        //
        // Sample Problem 8c, Solution on Page 32
        sTestNameRunning = "Convert continued:  (c)  24in";
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_rcl");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "60.96";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Convert continued:  (c)  24in"
        //
        // Sample Problem 8d, Solution on Page 32
        sTestNameRunning = "Convert continued:  (d)  36in";
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_rcl");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "91.44";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Convert continued:  (d)  36in"
        //
        // Sample Problem 9, Solution on Page 32
        sTestNameRunning = "Annual rate of return after tax:  Dur=6.5 mo, TaxRate=35%, Buy=$2341, Sell=$2672";
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_7");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_4");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_sto");
        simKeyPress1ms("key_minus");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_enter");
        simKeyPress1ms("key_.");
        simKeyPress1ms("key_3");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_minus");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_rcl");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_2");
        simKeyPress1ms("key_mult");
        simKeyPress1ms("key_6");
        simKeyPress1ms("key_.");
        simKeyPress1ms("key_5");
        simKeyPress1ms("key_div");
        simKeyPress1ms("key_1");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_0");
        simKeyPress1ms("key_mult");
        sTestNameFinished = sTestNameRunning; sTestNameRunning = "done";
        sRightAnswer = "16.96710808";
        resultA = wfm_rA;
        resultB = wfm_rB;
        // End of "Annual rate of return after tax:  Dur=6.5 mo, TaxRate=35%, Buy=$2341, Sell=$2672"
        //
        // This is the last sample problem in Section 3 of the HP-35 Operating Manual.
        // -----------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------
    // Rob's quick and dirty tests... 
    // -----------------------------------------------------------------------------------------

//        // -----------------------------------------------------------------------------------------
//        // Stack movement
//        sTestNameRunning = "Stack movement";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_enter");
//        sTestNameFinished = "1 Enter";
//        sRightAnswer = "1.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_enter");
//        sTestNameFinished = "2 Enter";
//        sRightAnswer = "2.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        sTestNameFinished = "3 Enter";
//        sRightAnswer = "3.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_4");
//        sTestNameFinished = "4 Enter";
//        sRightAnswer = "4.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//
//        simKeyPress1ms("key_roll");
//        sTestNameFinished = "Roll Down";
//        sRightAnswer = "3.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_roll");
//        sTestNameFinished = "Roll Down";
//        sRightAnswer = "2.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_roll");
//        sTestNameFinished = "Roll Down";
//        sRightAnswer = "1.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_roll");
//        sTestNameFinished = "Roll Down";
//        sRightAnswer = "4.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//
//        simKeyPress1ms("key_x<->y");
//        sTestNameFinished = "Swap x<->y";
//        sRightAnswer = "3.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        simKeyPress1ms("key_x<->y");
//        sTestNameFinished = "Swap x<->y";
//        sRightAnswer = "4.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Stack movement
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Calculator forensics:  arcsin(arccos(arctan(tan(cos(sin(9)))))) = 9.004076901
//        sTestNameRunning = "Calculator forensics test";
//        simKeyPress1ms("key_9");    // S0 rise to S8 rise is about 63 ms.
//        simKeyPress1ms("key_sin");  // S0 rise to S8 rise is about 716 ms.
//        simKeyPress1ms("key_cos");  // S0 rise to S8 rise is about 755 ms.
//        simKeyPress1ms("key_tan");  // S0 rise to S8 rise is about 508 ms.
//        simKeyPress1ms("key_arc");  // S0 rise to S8 rise is about 34 ms.
//        simKeyPress1ms("key_tan");  // S0 rise to S8 rise is about 457 ms.
//        simKeyPress1ms("key_arc");  // S0 rise to S8 rise is about 34 ms.
//        simKeyPress1ms("key_cos");  // S0 rise to S8 rise is about 886 ms.
//        simKeyPress1ms("key_arc");  // S0 rise to S8 rise is about 34 ms.
//        simKeyPress1ms("key_sin");  // S0 rise to S8 rise is about 750 ms.
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "9.004076901";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Calculator forensics:  arcsin(arccos(arctan(tan(cos(sin(9)))))) = 9.004076901
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // e^(ln(2.02)) should equal 2.02 with new ROMs
//        sTestNameRunning = "Old ROM bug";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_ln");
//        simKeyPress1ms("key_e^x");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "2.02";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of e^(ln(2.02)) should equal 2.02 with new ROMs
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // divide two 3-digit numbers
//        sTestNameRunning = "Divide two 3-digit numbers";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_div");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = ".2697368421";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Add two 3-digit numbers
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Take 10^0.1
//        sTestNameRunning = "Take 10^0.1";
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_.");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_x^y");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "1.258925412";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Take 10^0.1
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Take e^2
//        sTestNameRunning = "Take e^2";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_e^x");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "7.389056098";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Take e^x
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // 'pi' then enter
//        sTestNameRunning = "Pi then Enter";
//        simKeyPress1ms("key_pi");
//        simKeyPress1ms("key_enter");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "3.141592654";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of 'pi' then enter
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Take sqrt(2)
//        sTestNameRunning = "Take sqrt(2)";
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_sqrt_x");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "1.414213562";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Take sqrt(2)
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Take cos(0)
//        sTestNameRunning = "Take cos(0)";
//        simKeyPress1ms("key_0");
//        simKeyPress1ms("key_cos");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "1.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Take cos(0)
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Calculate 1/x, x=5
//        sTestNameRunning = "Calculate 1/x, x=5";
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_1/x");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = ".2";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Calculate 1/x, x=5
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Take log(5)
//        sTestNameRunning = "Take log(5)";
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_log");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = ".6989700041";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Take log(5)
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Multiply two 3-digit numbers
//        sTestNameRunning = "Multiply two 3-digit numbers";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_mult");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "56088.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Add two 3-digit numbers
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;
//
//        // -----------------------------------------------------------------------------------------
//        // Add two 3-digit numbers
//        sTestNameRunning = "Add two 3-digit numbers";
//        simKeyPress1ms("key_1");
//        simKeyPress1ms("key_2");
//        simKeyPress1ms("key_3");
//        simKeyPress1ms("key_enter");
//        simKeyPress1ms("key_4");
//        simKeyPress1ms("key_5");
//        simKeyPress1ms("key_6");
//        simKeyPress1ms("key_plus");
//        sTestNameFinished = sTestNameRunning;
//        sRightAnswer = "579.";
//        resultA = wfm_rA;
//        resultB = wfm_rB;
//        // End of Add two 3-digit numbers
//        // -----------------------------------------------------------------------------------------
//
//        #100ms;

        #500ms; // Run extra to help read final waveform results.


        // =========================================
        // *** Finish Up ***
        // =========================================
        loop_done_1 = 1'b1;
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_2 = 1'b1;
        wait (nextSysCount != 0); // Advance into the machine cycle.
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_3 = 1'b1;


        repeat (56) @(posedge PHI2);
        repeat (56) @(posedge PHI2);
        repeat (10) @(posedge PHI2);

        sim_end_pulse = 1'b1;
        @(posedge PHI2);
        sim_end_pulse = 1'b0;
        @(posedge PHI2);

        $fclose(fd);
        $stop;
    end

// =================================================================================================
// Instances
// =================================================================================================

    //-----------------------------------
    // Control and Timing Circuit 16
    //-----------------------------------
    control_and_timing_16 #(
        .NewArch    (0)             // parameter    // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_ctc (
        // Output Ports
        .IA      (IA),              // output           // ('569 item 32) Serial ROM Address                          
        .WS      (WS),              // inout            // ('569 item 30) Pointer Word Select                         
        .SYNC    (SYNC),            // output reg       // ('569 item 26) Word Cycle Sync                             
        .ROW0    (ROW0),            // output reg       // ('569 item 50) Keyboard Row Driver 0                       
        .ROW1    (ROW1),            // output reg       //                   "      "    "    1                       
        .ROW2    (ROW2),            // output reg       //                   "      "    "    2                       
        .ROW3    (ROW3),            // output reg       //                   "      "    "    3                       
        .ROW4    (ROW4),            // output reg       //                   "      "    "    4                       
        .ROW5    (ROW5),            // output reg       //                   "      "    "    5                       
        .ROW6    (ROW6),            // output reg       //                   "      "    "    6                       
        .ROW7    (ROW7),            // output reg       //                   "      "    "    7                       
        // Input Ports                                                                                                
        .PHI1    (1'b0),            // input            // Bit-Rate Clock Input, Phase 1, not used.              
        .PHI2    (PHI2),            // input            // Bit-Rate Clock Input, Phase 2                              
        .PWO     (PWO),             // input            // ('569 item 36) PoWer On pulse.  '569 (7) "As the system    
                                                        //    power comes on, the PWO signal is held at               
                                                        //    logic l for at least 20 milliseconds."                  
        .IS      (IS),              // input            // ('569 item 28) Serial Instruction from ROM Output          
        .CARRY   (CARRY),           // input            // ('569 item 34) Carry flag from Arithmetic & Register block.
        .COL0    (COL0),            // input            // ('569 item 54) Keyboard Column Input 0                     
        .COL2    (COL2),            // input            //                   "       "      "   2                     
        .COL3    (COL3),            // input            //                   "       "      "   3                     
        .COL4    (COL4),            // input            //                   "       "      "   4                     
        .COL6    (COL6)             // input            //                   "       "      "   6                     
    );

    //-----------------------------------
    // Arithmetic and Register Circuit 20
    //-----------------------------------
    arithmetic_and_register_20 #(
        .NewArch    (0)         // parameter        // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_arc (
        // Output Ports
        .A      (A),            // output reg       // ('569 item 38) Partially decoded LED segment sequence, bit A.
        .B      (B),            // output reg       //                    "        "     "    "        "    , bit B.
        .C      (C),            // output reg       //                    "        "     "    "        "    , bit C.
        .D      (D),            // output reg       //                    "        "     "    "        "    , bit D.
        .E      (E),            // output reg       //                    "        "     "    "        "    , bit E.
        .START  (START),        // output reg       // ('569 item 40) Word synchronization pulse for digit scanner in Cathode Driver.
        .CARRY  (CARRY),        // output reg       // ('569 item 34) Status of the carry output of this block's adder, sent to the Control & Timing block.
        // Input Ports
        .PHI1   (1'b0),         // input            // Bit-Rate Clock Input, Phase 1
        .PHI2   (PHI2),         // input            // Bit-Rate Clock Input, Phase 2
        .IS     (IS),           // input            // ('569 item 28) Serial Instruction from ROM Output
        .WS     (WS),           // input            // ('569 item 30) ROM Word Select
        .SYNC   (SYNC),         // input            // ('569 item 26) Word Cycle Sync
        .BCD    (BCD)           // inout            // ('569 item 35) BCD input/output line 35 from/to auxiliary data storage circuit 25.
    );

    //-----------------------------------
    // Read Only Memory Circuit 18, ROM 0
    //-----------------------------------
    read_only_memory_18 #(
        .RomNum (0),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
//        .RomFilename("35v2_rom0_binary.txt")    // parameter [80*8:1]  
        .RomFilename("35bn_rom0_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom0 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (1'b0),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );

    //-----------------------------------
    // Read Only Memory Circuit 18, ROM 1
    //-----------------------------------
    read_only_memory_18 #(
        .RomNum (1),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
//        .RomFilename("35v2_rom1_binary.txt")    // parameter [80*8:1]  
        .RomFilename("35bn_rom1_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom1 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (1'b0),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );

    //-----------------------------------
    // Read Only Memory Circuit 18, ROM 2
    //-----------------------------------
    read_only_memory_18 #(
        .RomNum (2),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
//        .RomFilename("35v2_rom2_binary.txt")    // parameter [80*8:1]  
        .RomFilename("35bn_rom2_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom2 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (1'b0),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );

    //---------------------------------------
    // Anode Driver of Output Display Unit 14
    //---------------------------------------
    anode_driver_14 #(
        .NewArch    (0)             // parameter    // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_adrv (
        // Output Ports
        .SDP        (SDP),          // output       // LED Segment Decimal Point.
        .SA         (SA),           // output       // LED Segment 'a'.
        .SB         (SB),           // output       // LED Segment 'b'.
        .SC         (SC),           // output       // LED Segment 'c'.
        .SD         (SD),           // output       // LED Segment 'd'.
        .SE         (SE),           // output       // LED Segment 'e'.
        .SF         (SF),           // output       // LED Segment 'f'.
        .SG         (SG),           // output       // LED Segment 'g'.
        .C_CL       (C_CL),         // output       // Counter Clock to Cathode Driver.
        .PHI1       (PHI1),         // output       // Clock Phase 1.
        .PHI2       (PHI2),         // output       // Clock Phase 2.
        // Input PortS                          
        .A          (A),            // input        // A input from Arithmetic and Register chip.
        .B          (B),            // input        // B   "    "       "       "     "      "
        .C          (C),            // input        // C   "    "       "       "     "      "
        .D          (D),            // input        // D   "    "       "       "     "      "
        .E          (E),            // input        // E   "    "       "       "     "      "
        .LOW_BATT   (1'b0),         // input        // Low battery indicator, 0 = Good, 1 = Low.
        .CLOCK_OSC  (CLOCK_OSC),    // input        // Clock input operating at the HP-35's oscillator rate, 800 kHz.
        .CLOCK_2X   (1'b0)          // input        // Clock input operating at 2x the HP-35's oscillator rate.
    );

    //-----------------------------------------
    // Cathode Driver of Output Display Unit 14
    //-----------------------------------------
     cathode_driver_14 #(
         .NewArch    (0)        // parameter        // 1 = Use new architecture, 0 = Like the patent.
     )
     inst_cdrv (
         // Output Ports
         .O1    (O1),           // output       // LED Cathode for Digit 1.
         .O2    (O2),           // output       //  "     "     "    "   2.
         .O3    (O3),           // output       //  "     "     "    "   3.
         .O4    (O4),           // output       //  "     "     "    "   4.
         .O5    (O5),           // output       //  "     "     "    "   5.
         .O6    (O6),           // output       //  "     "     "    "   6.
         .O7    (O7),           // output       //  "     "     "    "   7.
         .O8    (O8),           // output       //  "     "     "    "   8.
         .O9    (O9),           // output       //  "     "     "    "   9.
         .O10   (O10),          // output       //  "     "     "    "   10.
         .O11   (O11),          // output       //  "     "     "    "   11.
         .O12   (O12),          // output       //  "     "     "    "   12.
         .O13   (O13),          // output       //  "     "     "    "   13.
         .O14   (O14),          // output       //  "     "     "    "   14.
         .O15   (O15),          // output       //  "     "     "    "   15.
         // Input Ports                
         .START (~START),        // input        // ('569 item 40) Word synchronization
                                                // pulse for digit scanner in Cathode Driver.
         .C_CL  (~C_CL)          // input        // '569: "Once each digit time a signal
                                                // (counter-clock) is sent to the cathode
                                                // driver of output display unit 14 (the
                                                // trailing edge of this signal will step
                                                // the display to the next digit)."
     );

// =================================================================================================

endmodule



