//-----------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-----------------------------------------------------------------------
//
// FileName:
//      anode_driver_14.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Anode Driver
//
// Description:
//      This module emulates the "Anode Driver" of Output Display Unit 14
//      described in patent US 4,001,569 which discloses the HP-45 calculator.  
// 
//      Some passages of US 4,001,569 are quoted verbatim as comments in this
//      module and are indicated like this:
//         '569 (col): "<quoted text>"
//      Where "col" is the column number in the patent document.
//
//      '569 (12):  "Anode Driver.  As dicussed above, [in the Arithmetic and
//       Register Circuit section] the display information is partially
//       decoded in arithmetic and register circuit 20 and completely decoded
//       into seven segment plus decimal point signals in the bipolar anode
//       driver of output display unit 14. The anode driver also includes the
//       basic clock generator for the system and a circuit for detecting low
//       battery voltage to turn on all the decimal points."
//
// Acknowledgments:
//      From the Hewlett Packard Journal, June 1972:
//      
//         France Rode
//         France Rode came to HP in 1962, designed counter 
//         circuits for two years, then headed the group that 
//         developed the arithmetic unit of the 5360 Computing 
//         Counter. He left HP in 1969 to join a small new company, 
//         and in 1971 he came back to HP Laboratories. For the 
//         HP-35, he designed the arithmetic and register circuit 
//         and two of the special bipolar chips. France holds the 
//         degree Deploma Engineer from Ljubljana University in 
//         Yugoslavia. In 1962 he received the MSEE degree from 
//         Northwestern University. When he isn't designing logic 
//         circuits he likes to ski, play chess, or paint.
//         
//         David S. Cochran 
//         Dave Cochran is HP Laboratories' top algorithm designer 
//         and microprogrammer, having now performed those 
//         functions for both the 9100A and HP-35 Calculators. He 
//         was project leader for the HP-35. Since 1956 when he 
//         came to HP, Dave has helped give birth to the 204B Audio 
//         Oscillator, the 3440A Digital Voltmeter, and the 9120A 
//         Printer, in addition to his work in calculator architecture. 
//         He holds nine patents on various types of circuits and has 
//         authored several papers. IEEE member Cochran is a 
//         graduate of Stanford University with BS and MS degrees 
//         in electrical engineering, received in 1958 and 1960. 
//         His ideal vacation is skiing in the mountains of Colorado.  
//         
// Parameters:
// 
//    NewArch - Selects one of two implementation architectures:
//       0 = The original architecture whose outputs are combinational logic
//           decoded from a 5-stage ripple counter built from toggle flip-flops
//           as depicted in Figure 17 of the '569 patent.
//       1 = Employs a New Architecture whose outputs are all registered logic
//           decoded from synchronous counters.  This registered implementation
//           requires the input clock to be at twice the frequency of the
//           original, i.e., 1.6 MHz rather than 800 kHz. 
//
//    ActiveHigh - Selects the active level of the eight anode driver outputs:
//       0 = Sa through Sg and Sdp are active low per the circuit depicted in
//           Figures 17 and 18 of the '569 patent.
//       1 = SA through SG and SDP are active high which may be useful for
//           driving external transistors.  Only available when NewArch = 1.
// 
// IncludeFiles : None
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2048 - HP-35 Anode Driver Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
`timescale 1ns / 1ps

module anode_driver_14 #(
    parameter   NewArch = 0,    // 1 = Use new architecture.
                                    // 0 = Like the patent.
    parameter   ActiveHigh = 0  // 1 = Outputs, SA through SG and SDP are
                                    //     active high.
                                    // 0 = Like the patent.  Only for NewArch.
)
(
    // Output Ports
    output      SDP,            // LED Segment Decimal Point.
    output      SA,             // LED Segment 'a'.
    output      SB,             // LED Segment 'b'.
    output      SC,             // LED Segment 'c'.
    output      SD,             // LED Segment 'd'.
    output      SE,             // LED Segment 'e'.
    output      SF,             // LED Segment 'f'.
    output      SG,             // LED Segment 'g'.
    output      C_CL,           // Counter Clock to Cathode Driver.
    output      PHI1,           // Clock Phase 1.
    output      PHI2,           // Clock Phase 2.
    // Input Ports                  
    input       A,              // A input from Arithmetic and Register chip.
    input       B,              // B   "    "       "       "     "      "
    input       C,              // C   "    "       "       "     "      "
    input       D,              // D   "    "       "       "     "      "
    input       E,              // E   "    "       "       "     "      "
    input       LOW_BATT,       // Low battery indicator, 0 = Good, 1 = Low.
    input       CLOCK_OSC,      // Only used when NewArch = 0.  Clock input
                                //    operating at the HP-35's oscillator
                                //    rate.
    input       CLOCK_2X        // Only used when NewArch = 1.  Clock input
                                //    operating at 2x the HP-35's oscillator
                                //    rate.
);

    generate

        if (!NewArch) begin : scope_oldarch
            // -----------------------------------------------------------------
            // This generate block tries to be faithful to Fig. 17 in the '569
            // patent.
            //  
            // It employs a 5-stage ripple counter comprising edge-triggered
            // flip-flops wired to toggle when the appropriate clock edge
            // arrives from the previous stage.  The flip-flops shown in '569,
            // Fig. 17 are drawn as follows:
            // 
            //                     ___________
            //                    |           |
            //                    |        Q1 |---
            //                    |           |
            //                 ---| T-  B1    |
            //                    |        __ |
            //                 ---| S      Q1 |---
            //                    |___________|
            //                    
            // The nomenclature of this symbol is no longer in current use so I
            // made some assumptions as follows:
            //
            //    B1      Reference designator 
            //    Q1      True-polarity output
            //    Q1(bar) Inverted-polarity output
            //    T-      Clock input.  "T" indicates 'toggle' and "-" indicates
            //            that the outputs transition on the falling edge of T.
            //    S       Asynchronous set input, active high. 
            // 
            // My least certain interpretation of Fig. 17 is the exact function
            // of the "T-" input.  Because the other circuit in Output Display
            // Unit 14, the Cathode Driver, employs cross-coupled NOR latches
            // rather than edge-triggered flip-flops, my first thoughts were
            // that this logic symbol may represent similar technology.  To 
            // address this uncertainty, I reviewed the various types of latches
            // and flip-flops that could have been employed and ruled out types
            // that wouldn't result in the timing diagram shown in '569, Figure
            // 17.  The types I considered were:
            //
            //    a) The classic pulse-triggered JK flip-flop (two NOR gates and
            //       two AND gates) with both J and K inputs tied high.
            //    b) A master-slave pulse-triggered JK flip-flop with both J and
            //       K inputs tied high.  The same functionality as the TI
            //       SN7476. 
            //    c) A master-slave negative-edge-triggered JK flip-flop with
            //       both J and K inputs tied high.  The same functionality as
            //       the TI SN74LS76A. 
            //
            // I decided that (a) won't work with the timing shown in the patent
            // because of "race around" that occurs when the clock pulse is held
            // high longer than the propagation delay of the device.
            //  
            // I think either (b) or (c) will work.  In both cases, the Q
            // outputs change in response to the falling edge of clock.  The
            // inputs for (b) must be stable during the clock high time while
            // the inputs for (c) need only be stable for tsu prior to the
            // falling edge.  In both cases, the J and K inputs are tied high
            // and will meet the setup requirements.  Since either (b) or (c)
            // will work, I decided to code each flip-flop like (c), a simple
            // negative-edge-triggered register, configured to toggle, with
            // async set or reset as the case may be.
            // 
            wire    reset = B & D & ~PHI1;  // B & D can only occur simultane-
                                            // ously during T2.  This is used
                                            // to asynchronously preload the
                                            // flip-flops to the state they
                                            // should exhibit at the end of bit-
                                            // time T2.   
                
            reg     Q0 = 1'b0;  // Oscillator output. '569 (12): "(all times in
                                // this section will be referred to an 800 KHz
                                // oscillator frequency, which translates to a
                                // 200 KHz clock for the calculator, the actual
                                // frequency being somewhat less)"
            
            reg     QL = 1'b0;  // '569 (12): "The square-wave oscillator fre-
                                // quency is divided by flip-flop BL to 400 KHz."
            
            reg     Q1 = 1'b0;  // '569 (12): "Flip-flops B1 and B2 are clocked
            reg     Q2 = 1'b0;  // off alternate phases of flip-flop [BL] to
                                // provide two 200 KHz square waves as shown in
                                // FIG. 18."
            
            reg     Q3 = 1'b0;  // '569 (12): "Flip-flop B3 is clocked from
            reg     Q4 = 1'b0;  // flip-flop B2 and in turn clocks flip-flop B4
                                // to provide further count-down of the basic
                                // clock frequency."

            // Useful in simulation to see the state of all Q registers at a
            // glance.
            wire [5:0]  debug_count = {Q4, Q3, Q2, Q1, QL, Q0};
            
            always @* begin : proc_OSC
                Q0 = CLOCK_OSC;
            end
            
            always @(negedge Q0) begin : proc_BL
                QL <= ~QL;
            end
            
            always @(negedge QL or posedge reset) begin : proc_B1
                if (reset)
                    Q1 <= 1'b1;
                else
                    Q1 <= ~Q1;
            end
            
            always @(negedge ~QL) begin : proc_B2
                Q2 <= ~Q2;
            end
            
            // Q3 is coded to toggle on falling-edge of ~Q2 which differs from
            // Figure 17 but is required to match timing in Figure 18.         
            always @(negedge ~Q2 or posedge reset) begin : proc_B3  
                if (reset)                                          
                    Q3 <= 1'b0;
                else
                    Q3 <= ~Q3;
            end

            // Q4 is coded to toggling on falling-edge of Q3 which differs from
            // Figure 17 but is required to match timing in Figure 18.        
            always @(negedge Q3) begin : proc_B4    
                if (reset)                          
                    Q4 <= 1'b1;
                else
                    Q4 <= ~Q4;
            end
            
            // The following combinational output assignments are coded by
            // inspection from Figure 17.
            assign SDP  = ~((E | LOW_BATT) & ~Q4 & ~Q3 & Q2 & Q1);
            assign SC   = ~( C & ~Q4 &  Q3 & ~Q2);
            assign SB   = ~( B &  Q4 & ~Q3 & ~Q2);
            assign SA   = ~((A &       ~Q3 &  Q2 & ~Q1)
                        |(A &        Q3 & ~Q2 & ~Q1));
            assign SD   = ~( D &  Q4 & ~Q3 &  Q2);
            assign SE   = ~( D & ~Q4 &  Q3 &  Q2);
            assign SF   = ~( E &  Q4 & ~Q3 &        Q1);
            assign SG   = ~( E & ~Q4 &  Q3 &        Q1);
            assign C_CL = ~((E & ~Q4 &        Q2 &  Q1)
                        |(    ~Q4 &  Q3 &  Q2 &  Q1));
            assign PHI1 = ~( Q2 & ~QL & Q0);
            assign PHI2 = ~(~Q2 &  QL & Q0);

        // ---------------------------------------------------------------------
        // End of generate block that's faithful to Fig. 17 in the '569 patent.
        // ---------------------------------------------------------------------
        end

        if (NewArch) begin : scope_new
            // -----------------------------------------------------------------
            // This generate block employs a New Architecture whose outputs are
            // all registered logic decoded from synchronous counters.
            // 
            // In the New Architecture, each "bit time", T1 to T4, is logically
            // subdivided into eight "bit divisions".  PHI1 and PHI2 each occupy
            // a single division.  All other signals occupy a multiple of two
            // divisions.  In order to generate PHI1 and PHI2 as clocked
            // outputs, the input clock must be twice the frequency of that
            // which is specified in the patent.
            // 
            // xT1 to xT4 and xdiv[2:0] are signals used in the New Architecture
            // module to represent the four bit times and the eight subdivisions
            // of each bit time, respectively.  These signals are updated one
            // clock period prior to the actual times they represent, hence the
            // 'x' prefix indicating 'next'.  The 'next' values are used because
            // all output signals are registered (delayed by one clock period)
            // in the New Architecture module.  For example, PHI2 must be
            // asserted during bit division 7 so the result of (xdiv == 7) is
            // fed to the input of PHI2's flip-flop whose output, subsequently,
            // will be asserted on the next rising edge of CLOCK_2X.
            //  
            reg [2:0]   xdiv = 3'd0;    // Counter for the eight subdivisions of
                                        //    each bit time.  The 'x' prefix
                                        //    means "next", indicating that this
                                        //    counter's value represents the
                                        //    division number of the next clock
                                        //    period. 

            reg         xT1  = 1'b1;    // xT1 thru xT4 comprise a one-hot 
            reg         xT2  = 1'b0;    //    counter where each bit indicates
            reg         xT3  = 1'b0;    //    which of the four bit times is 
            reg         xT4  = 1'b0;    //    active.  The 'x' prefix means
                                        //    "next", indicating that these four
                                        //    bits each go active one clock
                                        //    period before the actual bit time.

            reg         phi_1  = 1'b0;  // Clock Phase 1.
            reg         phi_2  = 1'b0;  // Clock Phase 2.
            reg         c_clk  = 1'b0;  // Counter Clock to Cathode Driver.

            reg         seg_a  = 1'b0;  // seg_a thru seg_g and seg_dp are the
            reg         seg_b  = 1'b0;  //    registered segment driver outputs. 
            reg         seg_c  = 1'b0;  //
            reg         seg_d  = 1'b0;  //
            reg         seg_e  = 1'b0;  //
            reg         seg_f  = 1'b0;  //
            reg         seg_g  = 1'b0;  //
            reg         seg_dp = 1'b0;  //

            // '569 (13):  "The anodes are strobed according to the sequence in FIG. 18. The primary
            // reason for sequentially exciting the anodes is to reduce the peak cathode transistor
            // current. Since the decay time is approximately twice the buildup time, it works out
            // that the peak cathode current is about 2.5 times the peak current in any segment. The
            // LED�s are more effcient when excited at a low duty cycle. This means high currents
            // for short periods (80 ma. anode current, 250 ma. cathode current).  FIG. 18 also
            // shows the relationship between the anode strobing sequence and the display output
            // signals (A-E) from arithmetic and register circuit 20."
            //
            // Timing diagram for New Architecture, compare to Figure 18 in '569.
            //
            //            (rising edges) 
            // CLOCK_2X   | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
            //            
            // Bit Time         |<---- T1 ----->|<---- T2 ----->|<---- T3 ----->|<---- T4 ----->|<---- T1 ----->|
            // Bit Division |6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|
            //                  .               .               .               .               .               .
            //              ->| |<-Signals with 'x' prefix lead by one clock period. 
            //                 _______________  .               .               .              _______________  .
            // xT1        ____|               |_______________________________________________|               |___
            //                  .              _______________  .               .               .               .
            // xT2        ____________________|               |___________________________________________________
            //                  .               .              _______________  .               .               .
            // xT3        ____________________________________|               |___________________________________
            //            ____  .               .               .              _______________  .               .
            // xT4            |_______________________________________________|               |___________________
            //                  .               .               .               .               .               .
            // xdiv[2:0]    |7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|
            //            ________________   _____________   _____________   _____________   _____________   _____
            // PHI1             .         |_|   .         |_|   .         |_|   .         |_|   .         |_|   .
            //            ____   _____________   _____________   _____________   _____________   _____________   _
            // PHI2           |_|             |_|             |_|             |_|             |_|             |_|
            //            ______________     ___________________________________________     ___________     _____
            // C_CL             .       |___|   .               .               .       |_E_|   .       |___|     
            //            __________         _______________________________________________________         _____
            // SE               .   |___D___|   .               .               .               .   |___D___|
            //            ______________         _______________________________________________________         _
            // SG               .       |___E___|               .               .               .       |___E___|
            //            __________________         _______________________________________________________      
            // SC               .           |___C___|           .               .               .           |___C_
            //            ______________________         _______________________________________________________  
            // SA               .               |___A___|       .               .               .               |_
            //            __________________________         _____________________________________________________
            // SD               .               .   |___D___|   .               .               .               . 
            //            ______________________________         _________________________________________________
            // SF               .               .       |___E___|               .               .               . 
            //            __________________________________         _____________________________________________
            // SB               .               .           |___B___|           .               .               . 
            //            ______________________________________________________________     _____________________
            // SDP              .               .               .               .       |_E_|   .               .
            //                   _______________________________                .                _________________
            // A          ______|_______a_______________a_______|_______________________________|_________________
            //            ______                 _______________________________ _______________                 _
            // B          ______|_______________|_______b_______________b_______|_______dp______|_______________|_
            //            ______ _______________________________________________________________ _________________
            // C          ______|_______c_______________c_______________c_______________c_______|_________________
            //                   _______________ _______________                .                _______________ _
            // D          ______|_______e_______|_______d_______|_______________________________|_______________|_
            //            ______ _______________ _______________                 _______________ _______________ _
            // E          ______|_______g_______|_______f_______|_______________|_______dp______|_______________|_
            //                  .               .               .               .               .               .
            // Bit Time         |<---- T1 ----->|<---- T2 ----->|<---- T3 ----->|<---- T4 ----->|<---- T1 ----->|
            // Bit Division |6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|1|2|3|4|5|6|7|0|
            //
            always @(posedge CLOCK_2X) begin : proc_xdiv
                xdiv <= xdiv + 1;
            end

            always @(posedge CLOCK_2X) begin : proc_xT
                if (xdiv == 7) begin                                    // If we're in the last division of any bit time, then... 
                    if (B & D)                                          //   if Arithmetic and Register circuit drives B & D simultaneously, then...
                        {xT1, xT2, xT3, xT4} <= 4'b0010;                //      jump to bit time T3 on the next clock (because B & D can only be asserted simultaneously during bit time T2).
                    else                                                //   otherwise,
                        {xT1, xT2, xT3, xT4} <= {xT4, xT1, xT2, xT3};   //      shift to the next bit time.
                end
            end

            always @(posedge CLOCK_2X) begin : proc_reg_outputs
                phi_1   <= (xdiv == 5);
                phi_2   <= (xdiv == 7);

                // '569 (12):  "One other periodic signal is derived in the anode driver.  Once each
                // digit time a signal (counter-clock) is sent to the cathode driver of output
                // display unit 14 (the trailing edge of this signal will step the display to the
                // next digit)."
                // 
                // '569 (12-13):  "The display consists of 15 characters while the basic calculator
                // word cycle consists of 14 digits. The extra character is the decimal point. As
                // explained above, a BCD two is placed in register B at the digit position of the
                // decimal point. The display decoder 94 in arithmetic and register circuit 20
                // indicates this by a signal on outputs B and E during bit time T4 (see FIG. 12).
                // When this condition is decoded by the anode driver, the decimal point is excited
                // and an extra counter clock signal is given to step the display to the next
                // position (see FIGS. 18, 19, and 20). Therefore all remaining digits in register A
                // are displaced one digit in the display."

                c_clk   <= (       xT1 & (xdiv == 4 | xdiv == 5)        // Step for normal digit.
                             | E & xT4 & (xdiv == 4 | xdiv == 5));      // Extra step for decimal point.

                seg_e   <= D & xT1 & (xdiv == 2 | xdiv == 3 | xdiv == 4 | xdiv == 5);
                seg_g   <= E & xT1 & (xdiv == 4 | xdiv == 5 | xdiv == 6 | xdiv == 7);
                seg_c   <= C & xT1 & (xdiv == 6 | xdiv == 7)
                         | C & xT2 & (xdiv == 0 | xdiv == 1);
                seg_a   <= A & xT2 & (xdiv == 0 | xdiv == 1 | xdiv == 2 | xdiv == 3);
                seg_d   <= D & xT2 & (xdiv == 2 | xdiv == 3 | xdiv == 4 | xdiv == 5);
                seg_f   <= E & xT2 & (xdiv == 4 | xdiv == 5 | xdiv == 6 | xdiv == 7);
                seg_b   <= B & xT2 & (xdiv == 6 | xdiv == 7)
                         | B & xT3 & (xdiv == 0 | xdiv == 1);
                seg_dp  <= (E | LOW_BATT) & xT4 & (xdiv == 4 | xdiv == 5);
                // '569 (13):  "Since the anode driver operates from the battery voltage directly
                // and drives the decimal point segment, a circuit is provided that senses when the
                // voltage drops below a certain limit and thereupon turns on all decimal points."
                // ---
                // See LOW_BATT in seg_dp equation, above.
            end

            assign PHI1 = ~phi_1; 
            assign PHI2 = ~phi_2; 
            assign C_CL = ~c_clk; 

            // Combinational process that, based on the parameter ActiveHigh, determines
            // whether the outputs, SA through SG and SDP, are active high or active low.
            assign {SA, SB, SC, SD, SE, SF, SG, SDP} = (ActiveHigh)? {seg_a, seg_b, seg_c, seg_d, seg_e, seg_f, seg_g, seg_dp} :
                                                                    ~{seg_a, seg_b, seg_c, seg_d, seg_e, seg_f, seg_g, seg_dp};

        // ---------------------------------------------------------------------
        // End of generate block that employs a New Architecture.
        // ---------------------------------------------------------------------
        end

    endgenerate

endmodule 

