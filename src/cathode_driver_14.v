//-----------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-----------------------------------------------------------------------
//
// FileName:
//      cathode_driver_14.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Cathode Driver
//
// Description:
//      This module emulates the "Cathode Driver" part of Output Display
//      Unit 14 described in patent US 4,001,569 which discloses the HP-45
//      calculator.  
// 
//      Some passages of US 4,001,569 are quoted verbatim as comments in this
//      module and are indicated like this:
//         '569 (col): "<quoted text>"
//      Where "col" is the column number in the patent document.
//
// Acknowledgments:
//      From the Hewlett Packard Journal, June 1972:
//      
//         France Rode
//         France Rode came to HP in 1962, designed counter 
//         circuits for two years, then headed the group that 
//         developed the arithmetic unit of the 5360 Computing 
//         Counter. He left HP in 1969 to join a small new company, 
//         and in 1971 he came back to HP Laboratories. For the 
//         HP-35, he designed the arithmetic and register circuit 
//         and two of the special bipolar chips. France holds the 
//         degree Deploma Engineer from Ljubljana University in 
//         Yugoslavia. In 1962 he received the MSEE degree from 
//         Northwestern University. When he isn't designing logic 
//         circuits he likes to ski, play chess, or paint. 
//         
//         David S. Cochran 
//         Dave Cochran is HP Laboratories' top algorithm designer 
//         and microprogrammer, having now performed those 
//         functions for both the 9100A and HP-35 Calculators. He 
//         was project leader for the HP-35. Since 1956 when he 
//         came to HP, Dave has helped give birth to the 204B Audio 
//         Oscillator, the 3440A Digital Voltmeter, and the 9120A 
//         Printer, in addition to his work in calculator architecture. 
//         He holds nine patents on various types of circuits and has 
//         authored several papers. IEEE member Cochran is a 
//         graduate of Stanford University with BS and MS degrees 
//         in electrical engineering, received in 1958 and 1960. 
//         His ideal vacation is skiing in the mountains of Colorado.  
//         
// Parameters:
// 
//    NewArch - Selects one of two implementation architectures:
//       0 = The original architecture using S-R latches as depicted in Figure
//           22 of the '569 patent.
//       1 = Employs a New Architecture using D-type flip-flops.
//
//    ActiveHigh - Selects the active level of the 15 cathode driver outputs:
//       0 = O1 to O15 are active low per the circuit depicted in Figure 22 of
//           the '569 patent.
//       1 = O1 to O15 are active high which may be useful for driving external
//           transistors.  Only available when NewArch = 1.
// 
// IncludeFiles : None
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2047 - HP-35 Cathode Driver, Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
`timescale 1ns / 1ps

module cathode_driver_14 #(
    parameter       NewArch = 0,    // 1 = Use new architecture,
                                    // 0 = Like the patent.

    parameter       ActiveHigh = 0  // 1 = Outputs, O1 to O15 are active high,
                                    // 0 = Like the patent.  Only for NewArch.
)
(
    // Output Ports
    output          O1,             // LED Cathode for Display Digit 1.
    output          O2,             //  "     "     "     "      "   2.
    output          O3,             //  "     "     "     "      "   3.
    output          O4,             //  "     "     "     "      "   4.
    output          O5,             //  "     "     "     "      "   5.
    output          O6,             //  "     "     "     "      "   6.
    output          O7,             //  "     "     "     "      "   7.
    output          O8,             //  "     "     "     "      "   8.
    output          O9,             //  "     "     "     "      "   9.
    output          O10,            //  "     "     "     "      "   10.
    output          O11,            //  "     "     "     "      "   11.
    output          O12,            //  "     "     "     "      "   12.
    output          O13,            //  "     "     "     "      "   13.
    output          O14,            //  "     "     "     "      "   14.
    output          O15,            //  "     "     "     "      "   15.
    // Input Ports                  
    input           START,          // '569 (11): Display decoder 94 also
                                    // applies a START signal to line 40.
                                    // This signal is a word synchronization
                                    // pulse, which resets the digit scanner
                                    // in the cathode driver of output display
                                    // unit 14 to assure that the cathode driver
                                    // will select digit 1 when the digit 1
                                    // information is on outputs A, B, C, D, and
                                    // E. The timing for this signal is shown in
                                    // FIG. 14.
                                    
    input           C_CL            // '569 (12): "Once each digit time a signal
                                    // (counter-clock) is sent to the cathode
                                    // driver of output display unit 14 (the
                                    // trailing edge of this signal will step
                                    // the display to the next digit)."
);

    generate

        if (!NewArch) begin : scope_oldarch
            // -----------------------------------------------------------------
            // This generate block tries to be faithful to Fig. 22 in the '569
            // patent.
            //  
            // It employs cross-coupled NOR gate S-R latches and a single edge-
            // triggered toggle flip-flop as shown in '569, Fig 22.  The 
            // incoming counter clock, C_CL, is split into a 2-phase clock that
            // drives a string of 15 cathode driver stages.  When START goes
            // high, the arming latch, X and Xn, is asynchronously reset; you
            // might call this the 'half-cocked' state.  The clock phase 
            // splitter latch, A and An, isn't affected by the 'half-cocked'
            // state so it continues to do its job and the shifter will respond
            // normally to pulses on C_CL.  When START returns low, the circuit
            // enters the 'fully-cocked' or 'armed' state waiting for the first
            // low-going pulse on C_CL.  In the armed state, the clock phase
            // splitter flip-flop is reset.  When armed, the first low-going
            // pulse on C_CL resets (asserts) the first shifter stage, O15 and
            // clears the armed state by setting the X latch.  Each subsequent
            // low-going pulse on C_CL resets (asserts) the next output latch
            // and sets (deasserts) the current one, effectively stepping the
            // active output to the next cathode.  The step sequence is:
            //
            //    O15 -> O1 -> O2 -> O3 -> ... -> O13 -> O14 -> all off
            //
            // Note that O15 is first, followed by O1, O2, etc.  Also notice
            // that unless the circuit is re-armed during O14, then O14 will be
            // the last cathode output to be asserted. In other words, the
            // sequence doesn't automatically wrap back to O15 - a new START
            // pulse is needed for that.
            // ---------------------------------------------------------------------
            reg     A = 1'b0;

            wire    Sn;
            wire    X, Xn;
            wire    ACn;
            wire    AnCn;
            wire    XnSn;
            wire    O1n, O2n , O3n , O4n , O5n , O6n , O7n , O8n;
            wire    O9n, O10n, O11n, O12n, O13n, O14n, O15n;
                
            assign Sn   = ~START;
            assign ACn  =  A & ~C_CL;
            assign AnCn = ~A & ~C_CL;
            assign XnSn = ~X & Sn;

            // -----------------------------------------------------------------
            // Operation of the arming latch, X and Xn, and the clock phase
            // splitting flip-flop, A and An, are depicted in the following
            // timing diagram.
            // 
            //   Legend:                __      __
            //      Circuit Inputs:  __|    or    |__
            //                          __      __
            //      Responses:       __/    or    \__
            // 
            //          _______
            // START __|       |_____________________________________________
            //       __         _____________________________________________
            //    Sn   \_______/
            //       __             _________________________________________
            //     X   \___________/
            //          ___________
            //    Xn __/           \_________________________________________
            //         |       |   |
            //   Half- |<----->|   |
            //  Cocked |       |<->| Armed
            //       __________       _______________________________
            //     A __________\_____/                               \_______
            //       ________________                                 _______
            //    An __________/     \_______________________________/
            //       ______________   _____________________________   _______
            //  C_CL               |_|                             |_|
            //                      _
            //  AnCn ______________/ \_______________________________________
            //                                                      _
            //   ACn ______________________________________________/ \_______
            //       _______________________________________________
            //  O15n _______________/                               \________
            //                                                       ________
            //   O1n _______________________________________________/
            // 
            // -----------------------------------------------------------------
            // The arming latch, X and Xn.  The latch is reset when START is
            // high; this puts the circuit into the 'half-cocked' state.  When
            // START returns low, the circuit enters the 'fully-cocked' or
            // 'armed' state. 
            srlatch inst_FFX  (.Q (X),   .Qn (Xn),   .S (O15n & Sn),  .R (START));
            // -----------------------------------------------------------------
            // Toggle flip-flop for splitting the clock phase.  It's initialized
            // when the circuit is armed (START was high and returned low).
            always @(posedge C_CL or posedge XnSn) begin : proc_A                         
                if (XnSn)       // If asynchronously armed, then...
                    A <= 1'b0;  //    reset A.
                else            // else if trailing (rising) edge of C_CL, then...
                    A <= ~A;    //    toggle A.
            end
            // -----------------------------------------------------------------

            // -----------------------------------------------------------------
            // These 15 S-R latches comprise the cathode shift register.  Since
            // the outputs of this circuit are active-low, a cathode is driven
            // when its corresponding latch is reset so reset should be
            // considered the asserted state.  Notice that the 2-phase clock
            // signals, ACn and AnCn, are alternately connected to the 'R'
            // input of each instance.  Except for the first stage, the clocks
            // are qualified with the previous stage being asserted (reset).
            // For the first stage, its clock is qualified with the circuit
            // being in the 'armed' state (Xn & Sn).  Each stage is deasserted
            // (set) when the next stage is asserted (reset).  You can think of
            // it as a make-before-break arrangement.  The final stage, O14,
            // will also be deasserted when the next stage's clock pulse occurs
            // regardless of whether the next stage, O15, was asserted.  This
            // allows O14 to deassert even if there was no START pulse to re-arm
            // the sequence thus cleanly terminating the sequence either with or
            // without a START.
            srlatch inst_FF15 (.Q (O15), .Qn (O15n), .S (O1n ),       .R (AnCn & Xn & Sn));
            srlatch inst_FF1  (.Q (O1 ), .Qn (O1n ), .S (O2n ),       .R (ACn  & O15n));
            srlatch inst_FF2  (.Q (O2 ), .Qn (O2n ), .S (O3n ),       .R (AnCn & O1n ));
            srlatch inst_FF3  (.Q (O3 ), .Qn (O3n ), .S (O4n ),       .R (ACn  & O2n ));
            srlatch inst_FF4  (.Q (O4 ), .Qn (O4n ), .S (O5n ),       .R (AnCn & O3n ));
            srlatch inst_FF5  (.Q (O5 ), .Qn (O5n ), .S (O6n ),       .R (ACn  & O4n ));
            srlatch inst_FF6  (.Q (O6 ), .Qn (O6n ), .S (O7n ),       .R (AnCn & O5n ));
            srlatch inst_FF7  (.Q (O7 ), .Qn (O7n ), .S (O8n ),       .R (ACn  & O6n ));
            srlatch inst_FF8  (.Q (O8 ), .Qn (O8n ), .S (O9n ),       .R (AnCn & O7n ));
            srlatch inst_FF9  (.Q (O9 ), .Qn (O9n ), .S (O10n),       .R (ACn  & O8n ));
            srlatch inst_FF10 (.Q (O10), .Qn (O10n), .S (O11n),       .R (AnCn & O9n ));
            srlatch inst_FF11 (.Q (O11), .Qn (O11n), .S (O12n),       .R (ACn  & O10n));
            srlatch inst_FF12 (.Q (O12), .Qn (O12n), .S (O13n),       .R (AnCn & O11n));
            srlatch inst_FF13 (.Q (O13), .Qn (O13n), .S (O14n),       .R (ACn  & O12n));
            srlatch inst_FF14 (.Q (O14), .Qn (O14n), .S (O15n | ACn), .R (AnCn & O13n));
            // -----------------------------------------------------------------

        // ---------------------------------------------------------------------
        // End of generate block that's faithful to Fig. 22 in the '569 patent.
        // ---------------------------------------------------------------------
        end

        if (NewArch) begin : scope_new
            // -----------------------------------------------------------------
            // This generate block employs a New Architecture using D-type flip-
            // flops.
            // 
            // '569 (12):  "Once each digit time a signal (counter-clock) is
            // sent to the cathode driver of output display unit 14 (the
            // trailing edge of this signal will step the display to the next
            // digit)."
            // 
            //    Note that in this New Architecture implementation, the
            //    _leading edge_ of C_CL steps the display to the next digit
            //    (as opposed to the trailing edge as described in the patent,
            //    above).  The decision to deviate from the patent was based
            //    on oscilloscope screenshots taken from an actual Cathode
            //    Driver circuit in an operating HP-35.
            //
            // '569 (13):  "The cathode driver of output display unit 14 com-
            // prises a 15 position shift register for scanning the 15 digit
            // display once each word time.  This scanning operation moves from
            // digit to digit in response to counter clock signals from the
            // anode driver. Once each word time a START signal arrives from
            // arithmetic and register circuit 20 to restart the procedure."
            // 
            // Specifics of this implementation:
            //     "a 15 position shift register" -> q[1:15]
            //     "counter clock signals"        -> C_CL
            //     "a START signal"               -> START
            //
            // In this implementation, the START signal asynchonously presets
            // the 'start_req' signal. When START returns low, start_req remains
            // asserted until the first C_CL falling edge occurs which causes
            // the simultaneous assertion of q[15] and deassertion of start_req.
            // At this point, first stage of the shift register, q[15], is
            // asserted causing the cathode driver to select digit 15, the left-
            // most digit in the display.  All subsequent falling edges of C_CL
            // step the asserted output through the remaining shift register
            // stages in order:
            //  
            //    q[15] -> q[1] -> q[2] -> q[3] ... q[13] -> q[14] -> ?
            //
            // Note that what occurs at the end of stage q[14] depends on
            // START.  If START is asserted during q[14], then the next falling
            // edge of C_CL will step the shifter to q[15], restarting the
            // the sequence.  However, if START is not asserted during q[14],
            // then the next falling edge of C_CL will result in the shifter
            // going empty with no digits selected.
            //  
            // -----------------------------------------------------------------
            // Operation of the start request flip-flop and the 15-position
            // shift register are depicted in the following timing diagrams.
            // 
            //   Legend:                __      __
            //      Circuit Inputs:  __|    or    |__
            //                          __      __
            //      Responses:       __/    or    \__
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // Case in which START occurs when the 15-position shift register is
            // empty:
            //              _______
            //     START __|       |____________________________________________
            //              ___________
            // start_req __/           \________________________________________
            //           ______________   _____________________________   ______
            //      C_CL               |_|                             |_|
            //                          _______________________________         
            //     q[15] ______________/                               \________
            //                                                          ________
            //      q[1] ______________________________________________/        
            //       .                                                          
            //       .                                                          
            //       .   
            //     q[14] _______________________________________________________
            //
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // Case in which START occurs during display digit 14 (normal
            // restart):
            //              _______
            //     START __|       |____________________________________________
            //              ___________
            // start_req __/           \________________________________________
            //           ______________   _____________________________   ______
            //      C_CL               |_|                             |_|
            //                          _______________________________         
            //     q[15] ______________/                               \________
            //                                                          ________
            //      q[1] ______________________________________________/        
            //       .                                                          
            //       .                                                          
            //       .   ______________                                         
            //     q[14]               \________________________________________
            // 
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // Case in which the decimal point lights in display digit 14:
            //              _______
            //     START __|       |____________________________________________
            //              ___________
            // start_req __/           \________________________________________
            //           ______   _____   _____________________________   ______
            //      C_CL       |_|     |_|                             |_|
            //                          _______________________________         
            //     q[15] ______________/                               \________
            //                                                          ________
            //      q[1] ______________________________________________/        
            //       .                                                          
            //       .                                                          
            //       .   ______                                                 
            //     q[13]       \________________________________________________
            //                  _______                                         
            //     q[14] ______/       \________________________________________
            // 
            // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // The start request flip-flop, start_req, is asynchronously preset
            // when when the START input is high to record that a new shift
            // sequence is requested.  When START returns low, the next falling
            // edge of C_CL clears start_req.  start_req is analogous to the S-R
            // latch, X and Xn, in Fig. 22 in the '569 patent.
            // 
            reg start_req = 1'b0;
            reg [1:15]q = 'b0;

            always @(posedge START or negedge C_CL) begin : proc_arm
                if (START)              // When the START pulse is present, then...
                    start_req <= 1'b1;  //    preset start_req.
                else                    // else if falling edge of C_CL, then...
                    start_req <= 1'b0;  //    clear the request.
            end
            
            // The 15-position shift register, q[1:15].  The first stage gets a
            // '1' (high) as a result of the first C_CL falling edge after START
            // went from high to low.  All subsequent falling edges of C_CL will
            // shift the '1' through the remaining stages.  Note that there's no
            // automatic wrap from stage 14 to stage 15, another START is
            // required.
            //  
            always @(negedge C_CL) begin : proc_shift
                q[15]   <= start_req & ~START;
                q[1:14] <= {q[15],q[1:13]}; // q[15] -> q[1] -> q[2] -> q[3] ... q[13] -> q[14]
            end

            // Based on the parameter ActiveHigh, this statement determines
            // whether the outputs, O1..O15, are  active high or active low.
            // 
            if (ActiveHigh)
                assign {O1, O2, O3, O4, O5, O6, O7, O8, O9, O10, O11, O12, O13, O14, O15} = q[1:15];
            else
                assign {O1, O2, O3, O4, O5, O6, O7, O8, O9, O10, O11, O12, O13, O14, O15} = ~q[1:15];

        end
        // ---------------------------------------------------------------------
        // End of generate block that employs a New Architecture.
        // ---------------------------------------------------------------------

    endgenerate
endmodule

// -----------------------------------------------------------------------------
// Provide a classic cross-coupled NOR gate S-R latch for the old architecture
// version (NewArch = 0).
// -----------------------------------------------------------------------------
module srlatch (
    // Output Ports
    output Q , // Q Output
    output Qn, // Q-bar Output
    // Input Ports                  
    input  S,  // Async Set Input
    input  R   // Async Reset Input
);

    wire qint;
    wire qintn;

    nor #1 g1(qint,  R, qintn);
    nor #1 g2(qintn, S, qint );

    // Filter out the X's
    assign Q  = (qint  !== 1'b0);   // Case inequality returns TRUE  if qint  == 1'bx
    assign Qn = (qintn === 1'b1);   // Case equality   returns FALSE if qintn == 1'bx;

endmodule
// -----------------------------------------------------------------------------
// End of NOR latch.
// -----------------------------------------------------------------------------


