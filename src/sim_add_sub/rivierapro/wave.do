onerror { resume }
set curr_transcript [transcript]
transcript off

add wave -literal /tb/NewArch
add wave -logic /tb/rst
add wave -logic /tb/phi2
add wave /tb/i
add wave -logic /tb/go
add wave /tb/c_in
add wave /tb/first_bit
add wave /tb/sub
add wave /tb/t1
add wave /tb/t2
add wave /tb/t3
add wave /tb/t4
add wave /tb/sum1
add wave /tb/sum2
add wave /tb/use_sum2
add wave /tb/carry
add wave /tb/x_shifter
add wave /tb/y_shifter
add wave /tb/sum_shifter
add wave /tb/corr_shifter
add wave /tb/tv_op
add wave /tb/tv_cin
add wave /tb/tv_opx
add wave /tb/tv_opy
add wave /tb/tv_cout
add wave /tb/tv_res
add wave /tb/go_shifter
add wave /tb/i_hold
add wave /tb/tv_op_hold
add wave /tb/tv_cin_hold
add wave /tb/tv_opx_hold
add wave /tb/tv_opy_hold
add wave /tb/tv_res_hold
add wave /tb/tv_cout_hold
add wave /tb/carry_r
add wave /tb/sum_in_shifter
add wave /tb/iteration
add wave /tb/op_applied
add wave /tb/cin_applied
add wave /tb/opx_applied
add wave /tb/opy_applied
add wave /tb/result_expected
add wave /tb/carry_expected
add wave /tb/result_actual
add wave /tb/carry_actual
add wave /tb/carry_match
add wave /tb/result_match
add wave -logic /tb/compare_em
add wave /tb/good_count
add wave /tb/bad_count
add wave /tb/save_em
add wave -expand -vgroup /tb/inst_serial_adder \
	( -vgroup /tb/inst_serial_adder/inst_adder1 \
		/tb/inst_serial_adder/inst_adder1/sum \
		/tb/inst_serial_adder/inst_adder1/co \
		/tb/inst_serial_adder/inst_adder1/x \
		/tb/inst_serial_adder/inst_adder1/y \
		/tb/inst_serial_adder/inst_adder1/ci \
	) \
	( -vgroup /tb/inst_serial_adder/inst_adder2 \
		/tb/inst_serial_adder/inst_adder2/sum \
		/tb/inst_serial_adder/inst_adder2/co \
		/tb/inst_serial_adder/inst_adder2/x \
		/tb/inst_serial_adder/inst_adder2/y \
		/tb/inst_serial_adder/inst_adder2/ci \
	) \
	/tb/inst_serial_adder/SUM1 \
	/tb/inst_serial_adder/SUM2 \
	/tb/inst_serial_adder/USE_SUM2 \
	/tb/inst_serial_adder/CARRY \
	/tb/inst_serial_adder/X_IN \
	/tb/inst_serial_adder/Y_IN \
	/tb/inst_serial_adder/C_IN \
	/tb/inst_serial_adder/FIRST_BIT \
	/tb/inst_serial_adder/SUB \
	/tb/inst_serial_adder/T1 \
	/tb/inst_serial_adder/T2 \
	/tb/inst_serial_adder/T3 \
	/tb/inst_serial_adder/T4 \
	/tb/inst_serial_adder/PHI2 \
	/tb/inst_serial_adder/sum1_int \
	/tb/inst_serial_adder/sum2_int \
	/tb/inst_serial_adder/co1 \
	/tb/inst_serial_adder/co2 \
	/tb/inst_serial_adder/ci1 \
	/tb/inst_serial_adder/y_in2 \
	/tb/inst_serial_adder/co1_r \
	/tb/inst_serial_adder/co2_r \
	/tb/inst_serial_adder/NewArch
wv.cursors.add -time 30033us -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.zoom.range -from 29621853516ps -to 30090955078ps
wv.time.unit.auto.set
transcript $curr_transcript
