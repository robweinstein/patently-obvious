//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Serial Adder
//
// Description:
//      Generates a series of test vectors that are applied to the serial adder
//      module whose outputs are then compared to the expected results.
// 
//      Because the DUT is a serial adder/subtractor, this testbench provides
//      the following shift registers:
//
//         Length | Name         | Purpose
//         ---------------------------------------------------------------------
//           12   | x_shifter    | Supplies a 3-digit X Operand to the DUT.
//           12   | y_shifter    | Supplies a 3-digit Y Operand to the DUT.
//           12   | sum_shifter  | Captures 3 digits of the DUT's main output,
//                |              | 'SUM1'.
//            4   | corr_shifter | Captures 1 digit of the DUT's correction
//                |              | output, 'SUM2'.
// 
//      The DUT is designed to perform BCD arithmetic as explained in HP's '569
//      patent:
//          '569: "In serial decimal adder/subtractor 84 a correction (addition
//          of 6) to a BCD sum must be made if the sum exceeds nine (a similar 
//          correction for subtraction is necessary).  It is not known if a 
//          correction is needed until the first three bits of the sum have been 
//          generated. This is accomplished by adding a four-bit holding
//          register 86 (A60 - A57) and inserting the corrected sum into a
//          portion 88 (A56 - A53) of register A if a carry is generated."
//
//      The testbench's 'sum_shifter' holds the uncorrected BCD sum (or differ-
//      ence) as each bit is shifted out of the DUT's SUM1 output.  Similarly,
//      the testbench's 'corr_shifter' holds the corrected BCD sum (or differ-
//      ence) as each bit is shifted out of the DUT's SUM2 output.  When the
//      fourth bit of the BCD result is available at the DUT's 'SUM1' and 'SUM2'
//      outputs (the first three bits are stored in 'sum_shifter' and
//      'corr_shifter', respectively) the DUT's 'USE_SUM2' output indicates
//      whether the 'SUM2' and 'corr_shifter' contents should be used as the
//      final result for the current digit.
// 
//      The testbench employs a function called 'make_test_vector' that
//      generates all the bits needed for a single 3-digit calculation:
//          
//           3 bits:  Opcode (000 = Add, 001 = Subtract)
//           1 bit:   Carry In                      
//          12 bits:  X-Operand                     
//          12 bits:  Y-Operand                     
//          12 bits:  Expected Result              
//           1 bit:   Expected Carry Out            
// 
//      The main test loop calls 'make_test_vector', loads the Opcode, Carry
//      In, and X and Y Operands into registers that feed the DUT.  The input
//      values are shifted through the DUT for twelve clock periods and the
//      DUT's outputs are stored.  Finally, the DUT's results (sum and carry
//      out) are compared with the expected results and the 'good_count' is
//      incremented if the results match, otheriwse, the 'bad_count' is 
//      incremented.  The test vector, the actual results, and the good/bad
//      count values are all written to a single line in an output file.  This
//      whole process is repeated for the number of iterations specified by
//      the 'TestIterations' variable.  The test is a success if it completes
//      with 'bad_count' = zero.
// 
// Include Files : None
//
// Output Files : add_sub_output.csv
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2018 - HP-35 Instruction Decoder within Arithmetic & Register Block
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

    localparam NewArch = 0;

    // Inputs to DUT
//  reg     x_in = 1'b0;        // input        // Bit serial augend when adding.  Bit serial minuend when subtracting.
//  reg     y_in = 1'b0;        // input        // Bit serial addend when adding.  Bit serial subtrahend when subtracting.
    reg     c_in = 1'b0;        // input        // Carry/borrow input (active high in both cases) from previous word cycle.
    reg     first_bit;          // input        // Indicates the first bit period of the word cycle.  When asserted, C_IN is included in sum.
    reg     sub = 1'b0;         // input        // Add/Subtract control input.  0 = add, 1 = subtract.
    reg     t1 = 1'b0;          // input        // One-hot T-state counter indicating the active bit in the current digit.  T1 = LSbit, T4 = MSbit.
    reg     t2 = 1'b0;          // input        // One-hot T-state counter indicating the active bit in the current digit.  T1 = LSbit, T4 = MSbit.
    reg     t3 = 1'b0;          // input        // One-hot T-state counter indicating the active bit in the current digit.  T1 = LSbit, T4 = MSbit.
    reg     t4 = 1'b0;          // input        // One-hot T-state counter indicating the active bit in the current digit.  T1 = LSbit, T4 = MSbit.
    reg     phi2 = 1'b0;        // input        // Bit-Rate Clock Input, Phase 2.

    // Output from DUT
    wire    sum1;       // output       // Serial sum or difference.  Sum = X_IN + Y_IN + previous carry.  Difference = X_IN - Y_IN + previous borrow.
    wire    sum2;       // output       // Corrected sum (SUM1 + 6).
    wire    use_sum2;   // output reg   // Asserted during T4 if the corrected sum should be used.
    wire    carry;      // output reg   // ('569 item 34) Carry out when adding, borrow out when subtracting (active high in both cases). Valid during T4 of each digit time.  Sent to the Control & Timing block.

    // Testbench signals
    integer     i;
    integer     TestIterations;
    integer     fd; // File Descriptor
    reg         rst;
    reg         start_pulse;
    reg         go = 1'b0;
    reg         carry_r = 1'b0;
    reg [11:0]  go_shifter = 'b0;
    reg [11:0]  x_shifter = 'b0;
    reg [11:0]  y_shifter = 'b0;
    reg [11:0]  sum_shifter;
    reg [3:0]   corr_shifter;

    integer     i_hold;
    reg [2:0]   tv_op_hold = 'b0;
    reg         tv_cin_hold = 1'b0;
    reg [11:0]  tv_opx_hold = 'b0;
    reg [11:0]  tv_opy_hold = 'b0;
    reg [11:0]  tv_res_hold = 'b0;
    reg         tv_cout_hold = 1'b0;

    integer     iteration;
    reg [2:0]   op_applied      = 'b0;
    reg         cin_applied     = 1'b0;
    reg [11:0]  opx_applied     = 'b0;
    reg [11:0]  opy_applied     = 'b0;
    reg [11:0]  result_expected = 'b0;
    reg         carry_expected  = 1'b0;

    reg [11:0]  result_actual = 'b0;
    reg         carry_actual = 1'b0;

    reg         sum_in_shifter = 1'b0;
    reg         compare_em = 1'b0;
    reg         save_em = 1'b0;
    wire        result_match;
    wire        carry_match;
    integer     good_count = 0;
    integer     bad_count = 0;

    reg [40:0]  test_vector;
    `define OpCode      [40:38]
    `define CarryIn     [37]

    `define OperandX    [36:25]
    `define OperandX_2  [36:33]
    `define OperandX_1  [32:29]
    `define OperandX_0  [28:25]

    `define OperandY    [24:13]
    `define OperandY_2  [24:21]
    `define OperandY_1  [20:17]
    `define OperandY_0  [16:13]

    `define CarryOut    [12]
    `define Result      [11:0]
    `define Result_2    [11:8]
    `define Result_1    [7:4]
    `define Result_0    [3:0]

    reg [2:0]   tv_op;
    reg         tv_cin;
    reg [11:0]  tv_opx;
    reg [11:0]  tv_opy;
    reg         tv_cout;
    reg [11:0]  tv_res;

    // --------------------------------------------------------------------------------------------
    // Test Vector Generator
    //
    function [40:0] make_test_vector;
        input mode;
        begin
            reg [2:0]   op;
            reg         cout;
            reg         cin;
            reg         cin_initial;
            integer     i_opx   [0:2];  // X operand, array with one element for each digit.
            integer     i_opy   [0:2];  // Y operand,   "    "    "     "     "   "     "
            integer     i_res   [0:2];  // Result,      "    "    "     "     "   "     "
            integer     i;

            cout        = 1'b0;
            cin         = $urandom; // Randomly choose carry-in.
            cin_initial = cin;      // Save the first carry-in for use by the testbench.
            op[0]       = $urandom; // Randomly choose add or subtract.
            op[2:1]     = 2'b00;

            // Perform digit-by-digit arithmetic
            for (i=0; i<3; i=i+1) begin
                i_opx[i] = $urandom_range(9,0); // Must be 0 to 9
                i_opy[i] = $urandom_range(9,0); // Must be 0 to 9

                if (op[0]) begin                                // If opcode = 'subtract', then...
                    i_res[i] = i_opx[i] - i_opy[i] - cin;       //    compute the difference including the borrow,
                    if (i_res[i] < 0) begin                     //    if subtraction result is negative, then...
                        i_res[i] = i_res[i] + 10;               //        take the 10's complement of the negative difference
                        cout = 1'b1;                            //        and set the carry
                    end                                         //
                    else begin                                  //    otherwise subtraction result was positive, so...
                        cout = 1'b0;                            //        clear the carry
                    end                                         
                end                                             
                else begin                                      // Otherwise opcode is 'add', so...
                    i_res[i] = i_opx[i] + i_opy[i] + cin;       //    compute the sum including the carry,
                    if (i_res[i] > 9) begin                     //    if the sum exceeds the precision, then
                        i_res[i] = i_res[i] - 10;               //        subtract the carry weight
                        cout = 1'b1;                            //        and set the carry
                    end                                         //
                    else begin                                  //    otherwise...
                        cout = 1'b0;                            //        clear the carry
                    end
                end
                cin = cout;
            end
            make_test_vector`OpCode = op;
            make_test_vector`CarryIn = cin_initial;

            make_test_vector`OperandX_2 = i_opx[2];
            make_test_vector`OperandX_1 = i_opx[1];
            make_test_vector`OperandX_0 = i_opx[0];

            make_test_vector`OperandY_2 = i_opy[2];
            make_test_vector`OperandY_1 = i_opy[1];
            make_test_vector`OperandY_0 = i_opy[0];

            make_test_vector`CarryOut = cout;
            make_test_vector`Result_2 = i_res[2];
            make_test_vector`Result_1 = i_res[1];
            make_test_vector`Result_0 = i_res[0];
        end
    endfunction
    //
    // End of Test Vector Generator
    // --------------------------------------------------------------------------------------------

    // --------------------------------------------------------------------------------------------

    // Generate 200 kHz clock
    always #2.5us phi2 = ~phi2;

    always@(posedge phi2) begin : proc_start
        if (go)
            t1 <= 1'b1;
        else
            t1 <= t4;
        t2 <= t1;
        t3 <= t2;
        t4 <= t3;
    end

    always@(posedge phi2) begin : proc_shifters
        if (go) begin
            sub <= tv_op[0];
            c_in <= tv_cin;
            go_shifter <= 12'b100000000000;
            x_shifter <= tv_opx;
            y_shifter <= tv_opy;
        end
        else begin
            go_shifter <= {1'b0, go_shifter[11:1]};
            x_shifter <= {1'b0, x_shifter[11:1]};
            y_shifter <= {1'b0, y_shifter[11:1]};
        end
        //
        if (use_sum2)
            sum_shifter[11:8] <= {sum2, corr_shifter[3:1]};
        else
            sum_shifter[11:8] <= {sum1, sum_shifter[11:9]};
        //
        sum_shifter[7:0] <= {sum_shifter[8:1]};
        corr_shifter     <= {sum2, corr_shifter[3:1]};
        //
        if (go_shifter[11]) begin
            // Hold all the test vector elements for comparison at the end of each iteration.
            i_hold          <= i;       // Iteration Number
            tv_op_hold      <= tv_op;   // Opcode (0 = Add, 1 = Subtract)
            tv_cin_hold     <= tv_cin;  // Carry In
            tv_opx_hold     <= tv_opx;  // X-Operand
            tv_opy_hold     <= tv_opy;  // Y-Operand
            tv_res_hold     <= tv_res;  // Expected Results
            tv_cout_hold    <= tv_cout; // Expected Carry Out
        end
        //
        sum_in_shifter <= go_shifter[0];
        compare_em <= sum_in_shifter;
        save_em <= compare_em;
        if (go_shifter[0])
            carry_r <= carry;  // Capture the carry at the last bit.

        if (sum_in_shifter) begin
            // At the end of each iteration, capture the final results as well as the test vector that was being held.
            iteration       <= i_hold;          // Iteration Number
            op_applied      <= tv_op_hold;      // Opcode (0 = Add, 1 = Subtract)
            cin_applied     <= tv_cin_hold;     // Carry In
            opx_applied     <= tv_opx_hold;     // X-Operand
            opy_applied     <= tv_opy_hold;     // Y-Operand
            result_expected <= tv_res_hold;     // Expected Result to be compared with Actual Result
            carry_expected  <= tv_cout_hold;    // Expected Carry Out to be compared with Actual Carry Out
            result_actual   <= sum_shifter;     // Actual Result
            carry_actual    <= carry_r;         // Actual Carry Out
        end
        //
        if (compare_em) begin
            if (result_match && carry_match) 
                good_count <= good_count + 1;
            else
                bad_count <= bad_count + 1;
        end
        //
        // Write the results of each test vector to a file.
        if (start_pulse)
            $fdisplay(fd, "Iter, Op, Cin, Xoperand, Yoperand, ExpectedResult, ExpectedCarry, ActualResult, ActualCarry, NumGood, NumBad");
        else if (save_em) begin
            $fdisplay(fd, "%d, %d, %d, %h, %h, %h, %h, %h, %h, %d, %d", iteration, op_applied, cin_applied, opx_applied, opy_applied, result_expected, carry_expected, result_actual, carry_actual, good_count, bad_count);
        end
    end

    assign first_bit = go_shifter[11];
    assign result_match = (result_actual == result_expected)? 1'b1 : 1'b0;
    assign carry_match  = (carry_actual  == carry_expected)?  1'b1 : 1'b0;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    initial begin
        fd = $fopen("add_sub_output.csv", "w");
//        TestIterations = 1000000;
        TestIterations = 1000;
        start_pulse = 1'b0;
        phi2 = 1'b0;
        sub <= 1'b0;
        {tv_op,tv_cin,tv_opx,tv_opy,tv_cout,tv_res} = 'b0;

        wait (rst == 1'b1);
        wait (rst == 1'b0);
        @(posedge phi2);
        start_pulse = 1'b1;
        @(posedge phi2);
        start_pulse = 1'b0;
        @(posedge phi2);

        for (i=0; i<TestIterations; i=i+1) begin
            {tv_op,tv_cin,tv_opx,tv_opy,tv_cout,tv_res} = make_test_vector(1'b0);
            go <= 1'b1;
            @(posedge phi2);
            go <= 1'b0;
            repeat (11) @(posedge phi2);
        end

        repeat (12) @(posedge phi2);

        $fclose(fd);
        $stop;
    end

// -------------------------------------------------------
// Instances

    serial_adder_84 #(
        .NewArch    (NewArch)       // parameter      // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_serial_adder (
        // Output Ports
        .SUM1       (sum1),         // output       // Serial sum or difference.  Sum = X_IN + Y_IN + previous carry.  Difference = X_IN - Y_IN - previous borrow.
        .SUM2       (sum2),         // output       // Corrected sum (SUM1 + 6).
        .USE_SUM2   (use_sum2),     // output reg   // Asserted during T4 if the corrected sum should be used.
        .CARRY      (carry),        // output reg   // ('569 item 34) Carry out when adding, borrow out when subtracting (active high in both cases). Valid during T4 of each digit time.  Sent to the Control & Timing block.
        // Input Ports                  
        .X_IN       (x_shifter[0]), // input        // Bit serial augend when adding.  Bit serial minuend when subtracting.
        .Y_IN       (y_shifter[0]), // input        // Bit serial addend when adding.  Bit serial subtrahend when subtracting.
        .C_IN       (c_in),         // input        // Carry/borrow input (active high in both cases) from previous word cycle.
        .FIRST_BIT  (first_bit),    // input        // Indicates the first bit period of the word cycle.  When asserted, C_IN is included in sum.
        .SUB        (sub),          // input        // Add/Subtract control input.  0 = add, 1 = subtract.
        .T1         (t1),           // input        // One-hot T-state counter indicating the active bit in the current digit.  T1 = LSbit, T4 = MSbit.
        .T2         (t2),           // input        //  "
        .T3         (t3),           // input        //  "
        .T4         (t4),           // input        //  "
        .PHI2       (phi2)          // input        // Bit-Rate Clock Input, Phase 2.
    );

endmodule

