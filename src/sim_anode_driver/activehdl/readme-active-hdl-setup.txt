22-May-2020
Rather than the New Workspace wizard, I now use Active-HDL's TCL commands 'workspace' and 'design' to setup the workspace.  To do this:

   1.  Launch Active-HDL
   2.  In the console, type:
          > do rob-new-workspace.do
          > cd ..
   3.  Launch the other .do files as usual.

The contents of rob-new-workspace.do are:

   # Create workspace namded "activehdl" and open it.
   workspace create activehdl

   # Create a design named "work" with "work" library as work and add to workspace.
   design create -a -nodesdir work .

<end>