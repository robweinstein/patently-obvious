# Create workspace namded "activehdl" and open it.
workspace create activehdl

# Create a design named "work" with "work" library as work and add to workspace.
# design create [-a] [-nodesdir] <design_name> <path>
#    [-a] Specifes that this design will be added to the current workspace.
#    [-nodesdir] Specifies that a separate design directory will NOT be created, 
#                all design files are saved to the folder specified by the <path>
#                argument.
#    <design_name> Specifies the following:
#                   o The logical name of the design
#                   o Its working library (i.e. the names of the *.adf and *.lib files)
#                   o The name of the design folder storing all design files and sources.
#    <path> Specifes the location of the newly created design.
#
design create -a -nodesdir work .
