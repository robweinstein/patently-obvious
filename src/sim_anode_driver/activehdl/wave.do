onerror { resume }
transcript off
add wave -noreg -logic {/tb/rst}
add wave -noreg -logic {/tb/A}
add wave -noreg -logic {/tb/B}
add wave -noreg -logic {/tb/C}
add wave -noreg -logic {/tb/D}
add wave -noreg -logic {/tb/E}
add wave -noreg -logic {/tb/C_CL}
add wave -noreg -logic {/tb/SE}
add wave -noreg -logic {/tb/SG}
add wave -noreg -logic {/tb/SC}
add wave -noreg -logic {/tb/SA}
add wave -noreg -logic {/tb/SD}
add wave -noreg -logic {/tb/SF}
add wave -noreg -logic {/tb/SB}
add wave -noreg -logic {/tb/SDP}
add wave -noreg -decimal -literal -signed2 {/tb/syscnt}
add wave -noreg -logic {/tb/SYNC}
add wave -noreg -logic {/tb/IS}
add wave -noreg -logic {/tb/START}
add wave -noreg -hexadecimal -literal -signed2 {/tb/digit}
add wave -noreg -logic {/tb/syncr}
add wave -noreg -logic {/tb/T1}
add wave -noreg -logic {/tb/T2}
add wave -noreg -logic {/tb/T3}
add wave -noreg -logic {/tb/T4}
add wave -noreg -logic {/tb/dspt}
add wave -noreg -logic {/tb/dspn}
add wave -noreg -logic {/tb/display_ff}
add wave -noreg -hexadecimal -literal {/tb/regA}
add wave -noreg -hexadecimal -literal {/tb/regB}
add wave -noreg -logic {/tb/t2dp}
add wave -noreg -hexadecimal -literal {/tb/dsbf}
add wave -noreg -logic {/tb/dsbf_dp}
add wave -noreg -vgroup "TB Signals for NewArch = 1"  {/tb/C_CL_new} {/tb/SE_new} {/tb/SG_new} {/tb/SC_new} {/tb/SA_new} {/tb/SD_new} {/tb/SF_new} {/tb/SB_new} {/tb/SDP_new}
add wave -noreg -vgroup "/tb/inst_anode_driver_newArch"  {/tb/inst_anode_driver_newArch/NewArch} {/tb/inst_anode_driver_newArch/ActiveHigh} {/tb/inst_anode_driver_newArch/PHI1} {/tb/inst_anode_driver_newArch/PHI2} {/tb/inst_anode_driver_newArch/C_CL} {/tb/inst_anode_driver_newArch/SE} {/tb/inst_anode_driver_newArch/SG} {/tb/inst_anode_driver_newArch/SC} {/tb/inst_anode_driver_newArch/SA} {/tb/inst_anode_driver_newArch/SD} {/tb/inst_anode_driver_newArch/SF} {/tb/inst_anode_driver_newArch/SB} {/tb/inst_anode_driver_newArch/SDP} {/tb/inst_anode_driver_newArch/A} {/tb/inst_anode_driver_newArch/B} {/tb/inst_anode_driver_newArch/C} {/tb/inst_anode_driver_newArch/D} {/tb/inst_anode_driver_newArch/E} {/tb/inst_anode_driver_newArch/LOW_BATT} {/tb/inst_anode_driver_newArch/CLOCK_OSC} {/tb/inst_anode_driver_newArch/CLOCK_2X} {/tb/inst_anode_driver_newArch/scope_new/xdiv} {/tb/inst_anode_driver_newArch/scope_new/xT1} {/tb/inst_anode_driver_newArch/scope_new/xT2} {/tb/inst_anode_driver_newArch/scope_new/xT3} {/tb/inst_anode_driver_newArch/scope_new/xT4} {/tb/inst_anode_driver_newArch/scope_new/seg_e} {/tb/inst_anode_driver_newArch/scope_new/seg_g} {/tb/inst_anode_driver_newArch/scope_new/seg_c} {/tb/inst_anode_driver_newArch/scope_new/seg_a} {/tb/inst_anode_driver_newArch/scope_new/seg_d} {/tb/inst_anode_driver_newArch/scope_new/seg_f} {/tb/inst_anode_driver_newArch/scope_new/seg_b} {/tb/inst_anode_driver_newArch/scope_new/seg_dp}
cursor "Cursor 1" 1003125ns  
transcript on
