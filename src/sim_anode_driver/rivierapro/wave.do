onerror { resume }
set curr_transcript [transcript]
transcript off

add wave /tb/rst
add wave /tb/a
add wave /tb/b
add wave /tb/c
add wave /tb/d
add wave /tb/e
add wave /tb/low_batt
add wave /tb/clk
add wave /tb/clock_2x
add wave /tb/sdp
add wave /tb/sa
add wave /tb/sb
add wave /tb/sc
add wave /tb/sd
add wave /tb/se
add wave /tb/sf
add wave /tb/sg
add wave /tb/c_cl
add wave /tb/phi1
add wave /tb/phi2
add wave -expand -vgroup /tb/inst_anode_driver \
	/tb/inst_anode_driver/SDP \
	/tb/inst_anode_driver/SA \
	/tb/inst_anode_driver/SB \
	/tb/inst_anode_driver/SC \
	/tb/inst_anode_driver/SD \
	/tb/inst_anode_driver/SE \
	/tb/inst_anode_driver/SF \
	/tb/inst_anode_driver/SG \
	/tb/inst_anode_driver/C_CL \
	/tb/inst_anode_driver/PHI1 \
	/tb/inst_anode_driver/PHI2 \
	/tb/inst_anode_driver/A \
	/tb/inst_anode_driver/B \
	/tb/inst_anode_driver/C \
	/tb/inst_anode_driver/D \
	/tb/inst_anode_driver/E \
	/tb/inst_anode_driver/LOW_BATT \
	/tb/inst_anode_driver/CLOCK_OSC \
	/tb/inst_anode_driver/CLOCK_2X \
	/tb/inst_anode_driver/reset \
	/tb/inst_anode_driver/Q0 \
	/tb/inst_anode_driver/QL \
	/tb/inst_anode_driver/Q1 \
	/tb/inst_anode_driver/Q2 \
	/tb/inst_anode_driver/Q3 \
	/tb/inst_anode_driver/Q4
wv.cursors.add -time 973750ns -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.zoom.range -from 968904999ps -to 1000160ns
wv.time.unit.auto.set
transcript $curr_transcript
