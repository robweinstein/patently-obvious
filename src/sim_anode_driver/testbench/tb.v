//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Anode Driver 14
//
// Description:
// 
// Include Files : None
//
// Output Files : add_sub_output.csv
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2048 HP-35 Anode Driver Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();


    integer     syscnt = 0;
    integer     remainder;
    integer     digit;
    wire        T1, T2, T3, T4;
    wire        SYNC;
    wire        IS;
    wire        START;
    reg         syncr;
    reg [3:0]   dsbf = 4'b0000;         // Display Buffer digit.
    reg         dsbf_dp = 1'b0;         // Display Buffer decimal point.
    reg         a, b, c, d, e, f, g;    // 7-segment decoder output.

    reg         dspt = 1'b0;    // Decoded instruction: Display Toggle
    reg         dspn = 1'b0;    // Decoded instruction: Display Off

    // '569: "Arithmetic and register circuit 16 contains seven, fourteen-digit (56 bit) dynamic
    // registers A�F and M ..."
    //
    // Note the registers are 1-based because the '569 patent says this:  "... the end bit (B01) of
    // the B register ..."
    reg [56:1]  regA;                   // 56-bit shift register to display
    reg [56:1]  regB;                   // 56-bit shift register for display attributes

    //                         M          E
    //                         SMMMMMMMMMMSEE
    reg [56:1]  loadValA = 56'h92109876543910;  // A 9 in either sign location will display minus sign.
    reg [56:1]  loadValB = 56'h02000000000000;  // 9 = blank, 2 = decimal point

    // '569:  "The display flip-flop in arithmetic and register circuit 20 controls blanking of
    // all the LED�s. When it is reset, the 1111 code is set into the display buffer 96, which is
    // decoded so that no segments are on. There is one instruction to reset this flip-flop
    // I9 I8 I7 = (100) and another to toggle it (000). The toggle feature is convenient for
    // blinking the display."                           
    reg         display_ff;             
                                        
    // '569:  "The decimal point is handled in a similar way. A 2 (0010) is placed in register B at
    // the decimal point location. At time T2 the decimal point buffer flip-flop is set by B01.  Any
    // digit with a one in the second position will set the decimal point (i.e., 2, 3, 6, or 7)."                                                    
    reg         t2dp;                   

    // Basic clock, ce, and reset
    reg rst;

    // Inputs to DUT
    reg A;            // input        // A input from Arithmetic and Register chip.
    reg B;            // input        // B   "    "       "       "     "      "
    reg C;            // input        // C   "    "       "       "     "      "
    reg D;            // input        // D   "    "       "       "     "      "
    reg E;            // input        // E   "    "       "       "     "      "
    reg LOW_BATT;     // input        // Low battery indicator, 0 = Good, 1 = Low.
    reg CLOCK_OSC;          // input        // Clock input operating at the HP-35's oscillator rate.
    reg CLOCK_2X;     // input        // Clock input operating at 2x the HP-35's oscillator rate.

    // Output from DUT with NewArch = 0
    wire SDP;          // output       // LED Segment Decimal Point.
    wire SA;           // output       // LED Segment 'a'.
    wire SB;           // output       // LED Segment 'b'.
    wire SC;           // output       // LED Segment 'c'.
    wire SD;           // output       // LED Segment 'd'.
    wire SE;           // output       // LED Segment 'e'.
    wire SF;           // output       // LED Segment 'f'.
    wire SG;           // output       // LED Segment 'g'.
    wire C_CL;         // output       // Counter Clock to Cathode Driver.
    wire PHI1;         // output       // Clock Phase 1.
    wire PHI2;         // output       // Clock Phase 2.

    // Output from DUT with NewArch = 1
    wire SDP_new;      // output       // LED Segment Decimal Point.
    wire SA_new;       // output       // LED Segment 'a'.
    wire SB_new;       // output       // LED Segment 'b'.
    wire SC_new;       // output       // LED Segment 'c'.
    wire SD_new;       // output       // LED Segment 'd'.
    wire SE_new;       // output       // LED Segment 'e'.
    wire SF_new;       // output       // LED Segment 'f'.
    wire SG_new;       // output       // LED Segment 'g'.
    wire C_CL_new;     // output       // Counter Clock to Cathode Driver.
    wire PHI1_new;     // output       // Clock Phase 1.
    wire PHI2_new;     // output       // Clock Phase 2.

    initial begin
      rst = 1'b1;
      #10;
      rst = 1'b0;
      #1000000;
    end

    // Generate 800 kHz clock
    always #625 CLOCK_OSC = ~CLOCK_OSC;
    always #312.5 CLOCK_2X = ~CLOCK_2X;

    always @(posedge PHI2) begin : proc_syscnt
        if (syscnt == 55)
            syscnt <= 0;
        else
            syscnt <= syscnt + 1;
    end

    assign digit     = (syscnt / 4);
    assign remainder = (syscnt % 4);
    assign T1 = (remainder == 0)? 1'b1 : 1'b0;
    assign T2 = (remainder == 1)? 1'b1 : 1'b0;
    assign T3 = (remainder == 2)? 1'b1 : 1'b0;
    assign T4 = (remainder == 3)? 1'b1 : 1'b0;


    assign SYNC  = (syscnt >= 45 && syscnt <=54)? 1'b1 : 1'b0;
    assign IS    = (syscnt == 11)? 1'b1 : 1'b0;
    assign START = (syscnt == 0)? 1'b1 : 1'b0;

    always @(posedge PHI2) begin : proc_reg
        syncr <= SYNC;
    end

    always @(posedge PHI2) begin : proc_shift
        if (syscnt == 55) begin
            regA[56:1]  <= loadValA;
            regB[56:1]  <= loadValB;
        end
        else begin
            regA[56:1]  <= {regA[1], regA[56:2]};
            regB[56:1]  <= {regB[1], regB[56:2]};
        end
    end

    // Borrowed, with slight modifications, from arithmetic_and_register.v
    // ---------------------------------------------------------------------------------------------
    // '569 (11):  "The display blanking is handled as follows. At time T4 the BCD digit is gated 
    // from register A into display buffer 96. If this digit is to be blanked, register B will
    // contain a 9 (1001) so that at T4 the end bit (B01) of the B register will be a 1 (an 8 would
    // therefore also work).  The input to display buffer 96 is OR-ED with B01 and will be set to
    // 1111 if the digit is to be blanked.  The decimal point is handled in a similar way.  A 2
    // (0010) is placed in register B at the decimal point location.  At time T2 the decimal point
    // buffer flip-flop is set by B01. Any digit with a one in the second position will set the
    // decimal point (i.e., 2, 3, 6, or 7).
    //     One other special decoding feature is required. A minus sign is represented in tens
    // complement notation or sign and magnitude notation by the digit 9 in the sign location.
    // However, the display must show only a minus sign (i.e., segment g).  The digit 9 in register
    // A in digit position 2 (exponent sign) or position 13 (mantissa sign) must be displayed as
    // minus.  The decoding circuitry uses the pulse on Is buss 28 at bit time b11 (see FIG. 3) to
    // know that the digit 9 in digit position 2 of register A should be a minus and uses the SYNC
    // pulse to know that the digit 9 in digit position 13 of register A should also be a minus.
    // The pulse on Is buss 28 at bit time b11 can be set by a mask option, which allows the minus
    // sign of the exponent to appear in other locations for other uses of the calculator circuits."
    always@(posedge PHI2) begin : proc_display_buffer_96
        if (!SYNC && syncr)                             // If we've reached the last clock period of the execution cycle, then...
            if (dspn)                                   //    If the Display Off instruction is issued, then...
                display_ff <= 1'b0;                     //       clear the Display flip-flop.
            else if (dspt)                              //    Otherwise, if the Display Toggle instruction is issued, then...
                display_ff <= ~display_ff;              //       toggle the Display flip-flop.
        //
        if (T2)                                         // If at time T2, then...
            t2dp <= regB[1];                            //    set the decimal point buffer flip-flop only if the current digit in register B holds a '2'. 
        //
        if (T4) begin                                   // If at time T4, then...
            dsbf_dp <= t2dp;                            //    capture the decimal point flip-flop.
                                                        // Also at time T4...
            if (regB[1] || display_ff)                  //    If the current digit in register B holds a '9' or the display flip-flop is set, then...
                dsbf <= 4'b1111;                        //       blank the digit.
            else if (!SYNC && (IS || syncr))            //    Otherwise, if the current digit is in one of the two sign positions, then...
               if ({regA[1],regA[56:54]} == 4'b1001)    //       if the current digit in register A holds a '9', then...
                   dsbf <= 4'b1110;                     //          select the "minus" sign.
               else                                     //       Otherwise (the current digit in register A isn't a '9'), so...
                   dsbf <= 4'b1111;                     //          blank the digit.
            else                                        //    Otherwise (the current digit in register B isn't a '9' AND the display flip-flop isn't set AND the current digit isn't one of the two sign positions), so...
                dsbf <= {regA[1],regA[56:54]};          //       send the current digit in register A to the display decoder 94.
        end
    end
    // ---------------------------------------------------------------------------------------------

    // Borrowed, with slight modifications, from arithmetic_and_register.v
    // ---------------------------------------------------------------------------------------------
    // '569 (10):  "For increased power savings display decoder 94 is partitioned to partially
    // decode the BCD data into seven segments and a decimal point in arithmetic and register
    // circuit 20 by using only five output lines (A-E) 38 with time as the other parameter.
    // Information for seven segments (a-g) and a decimal point (dp) are time shared on the five
    // output lines A-E.  The output wave forms for output lines A-E are shown in FIG. 12. For
    // example, output line D carries the segment e information during T1 (the first bit time of
    // each digit time) and the segment d information during T2 (the second bit time of each digit
    // time); and output E carries the segment g information during T1, the segment F information
    // during T2, and the decimal point (dp) during T4."
    always @* begin : proc_display_decoder_94
        case (dsbf)
            4'b0000 : {a, b, c, d, e, f, g} = 7'b1111110;   // BCD 0
            4'b0001 : {a, b, c, d, e, f, g} = 7'b0110000;   // BCD 1
            4'b0010 : {a, b, c, d, e, f, g} = 7'b1101101;   // BCD 2
            4'b0011 : {a, b, c, d, e, f, g} = 7'b1111001;   // BCD 3
            4'b0100 : {a, b, c, d, e, f, g} = 7'b0110011;   // BCD 4
            4'b0101 : {a, b, c, d, e, f, g} = 7'b1011011;   // BCD 5
            4'b0110 : {a, b, c, d, e, f, g} = 7'b1011111;   // BCD 6
            4'b0111 : {a, b, c, d, e, f, g} = 7'b1110000;   // BCD 7
            4'b1000 : {a, b, c, d, e, f, g} = 7'b1111111;   // BCD 8
            4'b1001 : {a, b, c, d, e, f, g} = 7'b1111011;   // BCD 9
            4'b1110 : {a, b, c, d, e, f, g} = 7'b0000001;   // BCD E = Minus sign.  This is not specified as BCD E in the patent.
            4'b1111 : {a, b, c, d, e, f, g} = 7'b0000000;   // BCD F = Blanking code.
            default : {a, b, c, d, e, f, g} = 7'b0000000;
        endcase

        case (1'b1)
            T1 : begin
                A = a;          // During T1, output line A carries the segment 'a' information.
                B = 1'b0;       //   "    T1,   "     "   B is always zero.
                C = c;          //   "    T1,   "     "   C carries the segment 'c' information.
                D = e;          //   "    T1,   "     "   D    "     "    "     'e'      "     .
                E = g;          //   "    T1,   "     "   E    "     "    "     'g'      "     .
            end
            T2 : begin
                A = a;          // During T2, output line A carries the segment 'a' information.
                B = b;          //   "    T2,   "     "   B    "     "    "     'b'      "     .
                C = c;          //   "    T2,   "     "   C    "     "    "     'c'      "     .
                D = d;          //   "    T2,   "     "   D    "     "    "     'd'      "     .
                E = f;          //   "    T2,   "     "   E    "     "    "     'f'      "     .
            end
            T3 : begin
                A = 1'b0;       // During T3, output line A is always zero.                    
                B = b;          //   "    T3,   "     "   B carries the segment 'b' information.
                C = c;          //   "    T3,   "     "   C    "     "    "     'c'      "     .
                D = 1'b0;       //   "    T3,   "     "   D is always zero.
                E = 1'b0;       //   "    T3,   "     "   E is always zero.
            end
            T4 : begin
                A = 1'b0;       // During T4, output line A is always zero.
                B = dsbf_dp;    //   "    T4,   "     "   B carries the decimal point.
                C = c;          //   "    T4,   "     "   C carries the segment 'c' information.
                D = 1'b0;       //   "    T4,   "     "   D is always zero.
                E = dsbf_dp;    //   "    T4,   "     "   E carries the decimal point.
            end
            default : {A, B, C, D, E} = 5'b00000;   // Should always be in T1 - T4 but include default for latch mitigation.
        endcase
    end
    // ---------------------------------------------------------------------------------------------



    initial begin
        LOW_BATT = 1'b0;    // input        // Low battery indicator, 0 = Good, 1 = Low.
        CLOCK_OSC = 1'b0;   // input        // Clock input operating at the HP-35's oscillator rate.
        CLOCK_2X = 1'b1;    // input        // Clock input operating at 2x the HP-35's oscillator rate.
        dsbf = 4'b0000;     // Display Buffer digit.
        dsbf_dp = 1'b0;     // Display Buffer decimal point.
        dspt = 1'b0;        // Decoded instruction: Display Toggle
        dspn = 1'b0;        // Decoded instruction: Display Off
        display_ff = 1'b0;
        t2dp = 1'b0;
        syncr = 1'b0;
        regA[56:1]  = loadValA;
        regB[56:1]  = loadValB;

        wait (rst == 1'b0);
        #10
        @(posedge CLOCK_OSC);
        @(posedge CLOCK_OSC);
        @(posedge CLOCK_OSC);
        #1000000
        $stop;
    end

// -------------------------------------------------------
// Instances

    anode_driver_14 #(
        .NewArch    (0),            // parameter    // 1 = Use new architecture, 0 = Like the patent.
        .ActiveHigh (0)             // parameter    // 1 = Outputs, SA through SG and SDP are active high, 0 = Like the patent.  Only for NewArch.
    )
    inst_anode_driver (
        // Output Ports
        .SDP        (SDP),          // output       // LED Segment Decimal Point.
        .SA         (SA),           // output       // LED Segment 'a'.
        .SB         (SB),           // output       // LED Segment 'b'.
        .SC         (SC),           // output       // LED Segment 'c'.
        .SD         (SD),           // output       // LED Segment 'd'.
        .SE         (SE),           // output       // LED Segment 'e'.
        .SF         (SF),           // output       // LED Segment 'f'.
        .SG         (SG),           // output       // LED Segment 'g'.
        .C_CL       (C_CL),         // output       // Counter Clock to Cathode Driver.
        .PHI1       (PHI1),         // output       // Clock Phase 1.
        .PHI2       (PHI2),         // output       // Clock Phase 2.
        // Input Ports                          
        .A          (A),            // input        // A input from Arithmetic and Register chip.
        .B          (B),            // input        // B   "    "       "       "     "      "
        .C          (C),            // input        // C   "    "       "       "     "      "
        .D          (D),            // input        // D   "    "       "       "     "      "
        .E          (E),            // input        // E   "    "       "       "     "      "
        .LOW_BATT   (LOW_BATT),     // input        // Low battery indicator, 0 = Good, 1 = Low.
        .CLOCK_OSC  (CLOCK_OSC),    // input        // Clock input operating at the HP-35's oscillator rate.
        .CLOCK_2X   (CLOCK_2X)      // input        // Clock input operating at 2x the HP-35's oscillator rate.
    );

    anode_driver_14 #(
        .NewArch    (1),            // parameter    // 1 = Use new architecture, 0 = Like the patent.
        .ActiveHigh (0)             // parameter    // 1 = Outputs, SA through SG and SDP are active high, 0 = Like the patent.  Only for NewArch.
    )
    inst_anode_driver_newArch (
        // Output Ports
        .SDP        (SDP_new),      // output       // LED Segment Decimal Point.
        .SA         (SA_new),       // output       // LED Segment 'a'.
        .SB         (SB_new),       // output       // LED Segment 'b'.
        .SC         (SC_new),       // output       // LED Segment 'c'.
        .SD         (SD_new),       // output       // LED Segment 'd'.
        .SE         (SE_new),       // output       // LED Segment 'e'.
        .SF         (SF_new),       // output       // LED Segment 'f'.
        .SG         (SG_new),       // output       // LED Segment 'g'.
        .C_CL       (C_CL_new),     // output       // Counter Clock to Cathode Driver.
        .PHI1       (PHI1_new),     // output       // Clock Phase 1.
        .PHI2       (PHI2_new),     // output       // Clock Phase 2.
        // Input Ports                          
        .A          (A),            // input        // A input from Arithmetic and Register chip.
        .B          (B),            // input        // B   "    "       "       "     "      "
        .C          (C),            // input        // C   "    "       "       "     "      "
        .D          (D),            // input        // D   "    "       "       "     "      "
        .E          (E),            // input        // E   "    "       "       "     "      "
        .LOW_BATT   (LOW_BATT),     // input        // Low battery indicator, 0 = Good, 1 = Low.
        .CLOCK_OSC  (CLOCK_OSC),    // input        // Clock input operating at the HP-35's oscillator rate.
        .CLOCK_2X   (CLOCK_2X)      // input        // Clock input operating at 2x the HP-35's oscillator rate.
    );

endmodule

