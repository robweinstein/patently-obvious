28-Sep-2019

This testbench runs randomly selected HP-35 instructions in a loop
and saves the result to a file, arith_reg_output.csv.  The number
of random instructions to run is specified by a variable called
TestIterations.

The main simulation loop looks like this:

  --------------------------------------------------------------
  for (i = 1; i <= TestIterations/10; i = i+1) begin
      for (j = 1; j <= 10; j = j+1) begin
          simRandomType2();
          wait (nextSysCount == 0); 
          afterCall_ready = 1'b0;
      end
      loadRegsABCRandomUsingBCD(ia_regA, ia_regB, ia_regC);   // Re-randomize A, B, and C.
  end
  --------------------------------------------------------------

The inner loop calls simRandomType2() which runs a randomly-chosen
instruction with a randomly-chosen Word Select modifier. The inner
loop runs ten times, then the outer loop calls 
loadRegsABCRandomUsingBCD(ia_regA, ia_regB, ia_regC) which reloads
registers A, B, and C with new randomly-chosen values.  This way,
the inner loop runs ten times using an initial set of random A, B,
and C register values then the three register values are refreshed
with new random values for the next ten iterations.  It's done this
way as a tradeoff between speed and randomness because loading the
registers is a slow process.

The simulation process can be run two ways:

   do h

or

   do h-nowave

The first way builds everything and runs the simulation with a file
output and a waveform display.  The second way builds everything and
runs the simulation with only a file output, no waveform display.
The second way is much faster.

-Rob

