
The testbench is run from the simulator's command console by invoking the main
script like this:

> do h

The scripts are described below.

------------------------------------------

Rob's Generic Riviera-Pro Directory Structure:

proj_name\
  |
  |-- src\
       |
       |-- my_module\
             |
             |-- sim_my_module\
             |     |
             |     |-- testbench\
             |     |     |-- tb.vhd
             |     |
             |     |-- rivierapro\  (simulation working directory)
             |           |-- h
             |           |-- ntb
             |           |-- _rivierapro_(version).bat
             |           |-- ba.do
             |           |-- btb.do
             |           |-- load.do
             |           |-- wave.do
             |           |-- ba.f
             |
             |-- my_module.vhd
             |-- my_submodule1.vhd
             |-- my_submodule2.vhd


Rob's Simulation Files:

----

h
Riviera-Pro do script for compiling all modules and running the simulation.
Calls other "do" files like this:
   
   do ba.do
   do load.do
   do wave.do
   run -all

----

ntb
ntb = New TestBench. Riviera-Pro do script for compiling only the testbench
and running the simulation.  Faster than running h.  Calls other "do" files
like this:

   do btb.do  <-- Note "btb" not "ba".
   do load.do
   do wave.do
   run -all
   
----

ntb-nowave
ntb-nowave = New TestBench - No Waveform.  Riviera-Pro do script for compiling
only the testbench and running the simulation without a waveform window.
About 9 times faster than running ntb.  Calls other "do" files like this:

   do btb.do  <-- Note "btb" not "ba".
   do load.do
   run -all
   
----

_rivierapro_(version).bat
Batch file for launching Riviera-Pro.  Contains this:

   c:\Aldec\Riviera-PRO-2015.10\bin\riviera.exe

----

ba.do
Riviera-Pro do script for compiling all modules. Called by the h script.
Contains this:

   echo "*************************************"
   echo "Building test environment"
   echo "*************************************"
   vcom -2008 -work work -f ba.f

----

btb.do
Riviera-Pro do script for compiling only the testbench. Called by the ntb
script.  Contains this:

   echo "*************************************"
   echo "Building test environment"
   echo "*************************************"
   vcom -2008 -work work ../tb.vhd

----

load.do
Riviera-Pro do script for elaborating the design. Called by both the h and
ntb scripts. Contains this:

   # Loads the design into the simulator
   #
   # Options:
   #    -ieee_nowarn
   #       Suppresses the numeric_std warnings.
   #
   #    +access +r +p+/tb
   #       Allows waveform viewer to see all signals in the testbench (/tb)
   #       and below.  Runs about 8% to 15% slower.
   #
   asim -ieee_nowarn +access +r +p+/tb work.tb

----

wave.do
Riviera-Pro do script for loading the observed signals into the waveform
viewer. Called by both the h and ntb scripts.

----

ba.f
File list specifying all VHDL files needed to run the simulation. Called by
the ba.do script.  Contains this:

   ../../my_submodule2.vhd
   ../../my_submodule1.vhd
   ../../my_module.vhd
   
   ../tb.vhd

----

A module-level simulation is invoked as follows:

1. From a command prompt window, change directory into the rivierapro\ folder
and invoke Riviera-Pro by running _rivierapro_(version).bat.  Alternately,
using Windows Explorer, navigate to the rivierapro\ folder and invoke
Riviera-Pro by double clicking _rivierapro_(version).bat.

2. The first time you run the sim, type "vlib work" (no quotes) at the
Riviera-Pro command line to create the work library.  Also type 
"do vhdl-library-setup.tcl" (no quotes) to create the Altera Megafunction,
altera_mf, libraries.

3a (First time simulating the design).  Run ba.do to compile all your files.
Once you get ba.do to run successfully, then run step 3b.

3b (After getting ba.do to run successfully). Type "do h" (no quotes) at the
Riviera-Pro command line to compile all source code, elaborate the design,
launch the waveform window, and run the simulation.

4a. To rerun the simulation after changing only the testbench, type "do ntb"
at the Riviera-Pro command line. This will compile just the testbench,
elaborate the design, launch the waveform window, and run the simulation.

4b. To rerun the simulation without a waveform display after changing only
the testbench, type "do ntb" at the Riviera-Pro command line. This will
compile just the testbench, elaborate the design, and run the simulation.
