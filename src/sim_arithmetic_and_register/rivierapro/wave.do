onerror { resume }
set curr_transcript [transcript]
transcript off

add wave -literal /tb/NewArch
add wave /tb/PHI2
add wave /tb/IS
add wave /tb/WS
add wave /tb/SYNC
add wave /tb/BCD
add wave /tb/A
add wave /tb/B
add wave /tb/C
add wave /tb/D
add wave /tb/E
add wave /tb/START
add wave /tb/CARRY
add wave -logic /tb/rst
add wave -logic /tb/go
add wave /tb/t1
add wave /tb/t2
add wave /tb/t3
add wave /tb/t4
add wave /tb/instruction
add wave /tb/is_shifter
add wave /tb/BCD_shifter
add wave /tb/request_BCD
add wave /tb/BCD_request_value
add wave /tb/fetching_BCD
add wave /tb/BCD_load_value
add wave /tb/BCD_shifter(1)
add wave /tb/BCD_out_en
add wave /tb/BCD_in
add wave /tb/BCD
add wave /tb/sysCount
add wave /tb/nextSysCount
add wave /tb/digitCount
add wave /tb/holdRegA
add wave /tb/holdRegB
add wave /tb/holdRegC
add wave /tb/holdRegD
add wave /tb/holdRegE
add wave /tb/holdRegF
add wave /tb/holdRegM
add wave -dec /tb/ia_regA
add wave -dec /tb/ia_regB
add wave -dec /tb/ia_regC
add wave -dec /tb/ia_regD
add wave -dec /tb/ia_regE
add wave -dec /tb/ia_regF
add wave -dec /tb/ia_regM
add wave /tb/i_carry
add wave /tb/carry_ff_66
add wave /tb/tbRegA
add wave /tb/tbRegB
add wave /tb/tbRegC
add wave /tb/tbRegD
add wave /tb/tbRegE
add wave /tb/tbRegF
add wave /tb/tbRegM
add wave /tb/load_instruction
add wave /tb/fetch_instruction
add wave /tb/exec_instruction
add wave /tb/grab_em
add wave /tb/compare_em
add wave /tb/save_em
add wave /tb/loop_done_1
add wave /tb/loop_done_2
add wave /tb/loop_done_3
add wave /tb/s_mnemonic
add wave /tb/i_sub
add wave /tb/i_ci
add wave /tb/s_fs
add wave /tb/i_ptr
add wave /tb/beforeCall_s_mnemonic
add wave /tb/beforeCall_s_fs
add wave /tb/beforeCall_i_ptr
add wave -dec /tb/beforeCall_ia_regA
add wave -dec /tb/afterCall_ia_regA
add wave /tb/afterCall_ready
add wave /tb/pipe1_beforeCall_i_sub
add wave /tb/pipe1_beforeCall_i_ci
add wave -dec /tb/pipe1_beforeCall_ia_regA
add wave -dec /tb/pipe1_beforeCall_ia_regB
add wave -dec /tb/pipe1_beforeCall_ia_regC
add wave -dec /tb/pipe1_beforeCall_ia_regD
add wave -dec /tb/pipe1_beforeCall_ia_regE
add wave -dec /tb/pipe1_beforeCall_ia_regF
add wave -dec /tb/pipe1_beforeCall_ia_regM
add wave /tb/pipe1_beforeCall_s_mnemonic
add wave /tb/pipe1_beforeCall_s_fs
add wave /tb/pipe1_beforeCall_i_ptr
add wave -dec /tb/pipe1_afterCall_ia_regA
add wave -dec /tb/pipe1_afterCall_ia_regB
add wave -dec /tb/pipe1_afterCall_ia_regC
add wave -dec /tb/pipe1_afterCall_ia_regD
add wave -dec /tb/pipe1_afterCall_ia_regE
add wave -dec /tb/pipe1_afterCall_ia_regF
add wave -dec /tb/pipe1_afterCall_ia_regM
add wave /tb/pipe1_afterCall_i_co
add wave /tb/pipe1_afterCall_ready
add wave -divider ---
add wave /tb/pipe2_beforeCall_i_sub
add wave /tb/pipe2_beforeCall_i_ci
add wave -dec /tb/pipe2_beforeCall_ia_regA
add wave -dec /tb/pipe2_beforeCall_ia_regB
add wave -dec /tb/pipe2_beforeCall_ia_regC
add wave /tb/pipe2_beforeCall_s_mnemonic
add wave /tb/pipe2_beforeCall_s_fs
add wave /tb/pipe2_beforeCall_i_ptr
add wave -dec /tb/pipe2_afterCall_ia_regA
add wave -dec /tb/pipe2_afterCall_ia_regB
add wave -dec /tb/pipe2_afterCall_ia_regC
add wave /tb/pipe2_afterCall_i_co
add wave /tb/pipe2_afterCall_ready
add wave -divider ---
add wave /tb/pipe3_beforeCall_i_sub
add wave /tb/pipe3_beforeCall_i_ci
add wave -dec /tb/pipe3_beforeCall_ia_regA
add wave -dec /tb/pipe3_beforeCall_ia_regB
add wave -dec /tb/pipe3_beforeCall_ia_regC
add wave /tb/pipe3_beforeCall_s_mnemonic
add wave /tb/pipe3_beforeCall_s_fs
add wave /tb/pipe3_beforeCall_i_ptr
add wave -dec /tb/pipe3_afterCall_ia_regA
add wave -dec /tb/pipe3_afterCall_ia_regB
add wave -dec /tb/pipe3_afterCall_ia_regC
add wave /tb/pipe3_afterCall_i_co
add wave /tb/pipe3_afterCall_ready
add wave -divider ---
add wave -dec /tb/modeled_beforeCall_ia_regA
add wave -dec /tb/modeled_beforeCall_ia_regB
add wave -dec /tb/modeled_beforeCall_ia_regC
add wave -literal /tb/modeled_beforeCall_s_mnemonic
add wave -literal /tb/modeled_beforeCall_s_fs
add wave -literal /tb/modeled_beforeCall_i_ptr
add wave -literal /tb/modeled_beforeCall_i_sub
add wave -literal /tb/modeled_beforeCall_i_ci
add wave -dec /tb/modeled_afterCall_ia_regA
add wave -dec /tb/modeled_afterCall_ia_regB
add wave -dec /tb/modeled_afterCall_ia_regC
add wave /tb/modeled_afterCall_i_co
add wave /tb/modeled_afterCall_ready
add wave /tb/expected_regA
add wave /tb/actual_regA
add wave /tb/expected_carry
add wave /tb/actual_carry
add wave /tb/regA_match
add wave /tb/regB_match
add wave /tb/regC_match
add wave /tb/regD_match
add wave /tb/regE_match
add wave /tb/regF_match
add wave /tb/regM_match
add wave /tb/carry_match
add wave /tb/all_regs_match
add wave /tb/good_count
add wave /tb/bad_count
add wave -expand -vgroup /tb/inst_a_and_r \
	( -logic /tb/inst_a_and_r/PHI2 ) \
	/tb/inst_a_and_r/IS \
	/tb/inst_a_and_r/SYNC \
	/tb/inst_a_and_r/BCD \
	/tb/inst_a_and_r/WS \
	( -logic /tb/inst_a_and_r/START ) \
	( -logic /tb/inst_a_and_r/CARRY ) \
	/tb/inst_a_and_r/bcd2c \
	/tb/inst_a_and_r/regC(1) \
	/tb/inst_a_and_r/bcd_in \
	/tb/inst_a_and_r/t1 \
	/tb/inst_a_and_r/t2 \
	/tb/inst_a_and_r/t3 \
	/tb/inst_a_and_r/t4 \
	/tb/inst_a_and_r/isbuf \
	/tb/inst_a_and_r/isreg \
	/tb/inst_a_and_r/istype2 \
	/tb/inst_a_and_r/istype5 \
	/tb/inst_a_and_r/syncr \
	/tb/inst_a_and_r/wsr \
	/tb/inst_a_and_r/ws2 \
	/tb/inst_a_and_r/ws3 \
	/tb/inst_a_and_r/ws4 \
	/tb/inst_a_and_r/ws1 \
	/tb/inst_a_and_r/ds1 \
	/tb/inst_a_and_r/dsn1 \
	/tb/inst_a_and_r/display_ff \
	/tb/inst_a_and_r/regA \
	/tb/inst_a_and_r/regB \
	/tb/inst_a_and_r/dC60 \
	/tb/inst_a_and_r/dC56 \
	/tb/inst_a_and_r/dC55 \
	/tb/inst_a_and_r/dC54 \
	/tb/inst_a_and_r/dC53 \
	/tb/inst_a_and_r/dC52 \
	/tb/inst_a_and_r/regC \
	/tb/inst_a_and_r/regD \
	/tb/inst_a_and_r/regE \
	/tb/inst_a_and_r/regF \
	/tb/inst_a_and_r/regM \
	( -expand -vgroup /tb/inst_a_and_r/inst_instruction_decoder \
		/tb/inst_a_and_r/inst_instruction_decoder/debug_string \
		/tb/inst_a_and_r/inst_instruction_decoder/sub \
		/tb/inst_a_and_r/inst_instruction_decoder/c_in \
		/tb/inst_a_and_r/inst_instruction_decoder/a2x \
		/tb/inst_a_and_r/inst_instruction_decoder/c2x \
		/tb/inst_a_and_r/inst_instruction_decoder/b2y \
		/tb/inst_a_and_r/inst_instruction_decoder/c2y \
		/tb/inst_a_and_r/inst_instruction_decoder/a2a \
		/tb/inst_a_and_r/inst_instruction_decoder/is2a \
		/tb/inst_a_and_r/inst_instruction_decoder/b2a \
		/tb/inst_a_and_r/inst_instruction_decoder/c2a \
		/tb/inst_a_and_r/inst_instruction_decoder/d2a \
		/tb/inst_a_and_r/inst_instruction_decoder/res2a \
		/tb/inst_a_and_r/inst_instruction_decoder/hld2a \
		/tb/inst_a_and_r/inst_instruction_decoder/sra \
		/tb/inst_a_and_r/inst_instruction_decoder/a2hld \
		/tb/inst_a_and_r/inst_instruction_decoder/s22hld \
		/tb/inst_a_and_r/inst_instruction_decoder/b2b \
		/tb/inst_a_and_r/inst_instruction_decoder/a2b \
		/tb/inst_a_and_r/inst_instruction_decoder/c2b \
		/tb/inst_a_and_r/inst_instruction_decoder/srb \
		/tb/inst_a_and_r/inst_instruction_decoder/c2c \
		/tb/inst_a_and_r/inst_instruction_decoder/con2c \
		/tb/inst_a_and_r/inst_instruction_decoder/bcd2c \
		/tb/inst_a_and_r/inst_instruction_decoder/a2c \
		/tb/inst_a_and_r/inst_instruction_decoder/b2c \
		/tb/inst_a_and_r/inst_instruction_decoder/d2c \
		/tb/inst_a_and_r/inst_instruction_decoder/m2c \
		/tb/inst_a_and_r/inst_instruction_decoder/res2c \
		/tb/inst_a_and_r/inst_instruction_decoder/src \
		/tb/inst_a_and_r/inst_instruction_decoder/d2d \
		/tb/inst_a_and_r/inst_instruction_decoder/c2d \
		/tb/inst_a_and_r/inst_instruction_decoder/e2d \
		/tb/inst_a_and_r/inst_instruction_decoder/e2e \
		/tb/inst_a_and_r/inst_instruction_decoder/d2e \
		/tb/inst_a_and_r/inst_instruction_decoder/f2e \
		/tb/inst_a_and_r/inst_instruction_decoder/f2f \
		/tb/inst_a_and_r/inst_instruction_decoder/c2f \
		/tb/inst_a_and_r/inst_instruction_decoder/e2f \
		/tb/inst_a_and_r/inst_instruction_decoder/m2m \
		/tb/inst_a_and_r/inst_instruction_decoder/c2m \
		/tb/inst_a_and_r/inst_instruction_decoder/dspt \
		/tb/inst_a_and_r/inst_instruction_decoder/dspn \
		/tb/inst_a_and_r/inst_instruction_decoder/isreg \
		/tb/inst_a_and_r/inst_instruction_decoder/istype2 \
		/tb/inst_a_and_r/inst_instruction_decoder/istype5 \
		/tb/inst_a_and_r/inst_instruction_decoder/WS \
		/tb/inst_a_and_r/inst_instruction_decoder/ws1 \
		/tb/inst_a_and_r/inst_instruction_decoder/ds1 \
		/tb/inst_a_and_r/inst_instruction_decoder/dsn1 \
	)
wv.cursors.add -time 15402500ns -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.cursors.subcursor.add -time 423932500ns -name {Cursor 1}
wv.cursors.add -time 88760600ns -name {Cursor 2}
wv.cursors.setactive -name {Cursor 1}
wv.zoom.range -from 0fs -to 425937500ns
wv.time.unit.auto.set
transcript $curr_transcript
