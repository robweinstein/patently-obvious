//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Arithmetic and Register Circuit 20
//
// Description:
// 
// Include Files : None
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2021 - HP-35 Arithmetic & Register Block, Testbench Logic Diagram
//    RJW2018 - HP-35 Instruction Decoder within Arithmetic & Register Block
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 15-Feb-2019 rjw
//    Baseline.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

// -----------------------------------------------------------------------------
// Signal Declarations
// 
    localparam NewArch = 0;

    // Type 2 Instructions
    `define CLRA    10'b10111_000_10
    `define CLRB    10'b00001_000_10
    `define CLRC    10'b00110_000_10
    `define MOVAB   10'b01001_000_10
    `define MOVBC   10'b00100_000_10
    `define MOVCA   10'b01100_000_10
    `define XCHAB   10'b11001_000_10
    `define XCHBC   10'b10001_000_10
    `define XCHAC   10'b11101_000_10
    `define ADDACC  10'b01110_000_10
    `define SUBACC  10'b01010_000_10
    `define ADDABA  10'b11100_000_10
    `define SUBABA  10'b11000_000_10
    `define ADDACA  10'b11110_000_10
    `define SUBACA  10'b11010_000_10
    `define ADDCCC  10'b10101_000_10
    `define CMP0B   10'b00000_000_10
    `define CMP0C   10'b01101_000_10
    `define CMPAC   10'b00010_000_10
    `define CMPAB   10'b10000_000_10
    `define CMPA1   10'b10011_000_10
    `define CMPC1   10'b00011_000_10
    `define TCC     10'b00101_000_10
    `define NCC     10'b00111_000_10
    `define INCA    10'b11111_000_10
    `define INCC    10'b01111_000_10
    `define DECA    10'b11011_000_10
    `define DECC    10'b01011_000_10
    `define SHRA    10'b10110_000_10
    `define SHRB    10'b10100_000_10
    `define SHRC    10'b10010_000_10
    `define SHLA    10'b01000_000_10

    // Type 5 Instructions
    `define AI0     10'b0000_00_1000
    `define AIx     10'b????_00_1000
    `define AIF     10'b1111_00_1000

    `define LDC0    10'b0000_01_1000
    `define LDC1    10'b0001_01_1000
    `define LDC2    10'b0010_01_1000
    `define LDC3    10'b0011_01_1000
    `define LDC4    10'b0100_01_1000
    `define LDC5    10'b0101_01_1000
    `define LDC6    10'b0110_01_1000
    `define LDC7    10'b0111_01_1000
    `define LDC8    10'b1000_01_1000
    `define LDC9    10'b1001_01_1000
    `define LDCA    10'b1010_01_1000
    `define LDCB    10'b1011_01_1000
    `define LDCC    10'b1100_01_1000
    `define LDCD    10'b1101_01_1000
    `define LDCE    10'b1110_01_1000
    `define LDCF    10'b1111_01_1000

    `define DSPTOG  10'b000_01z_1000
    `define XCHCM   10'b001_01z_1000
    `define PUSHC   10'b010_01z_1000
    `define POPA    10'b011_01z_1000
    `define DSPOFF  10'b100_01z_1000
    `define RCLM    10'b101_01z_1000
    `define ROTDN   10'b110_01z_1000
    `define CLRALL  10'b111_01z_1000

    `define MOVISA  10'bzz0_11z_1000
    `define MOVBCDC 10'bzz1_11z_1000

    // Type 10 Instruction
    `define NOP     10'b0000000000

    // Inputs to DUT
    reg         PHI1 = 1'b0;        // input            // Bit-Rate Clock Input, Phase 1
    reg         PHI2;               // input            // Bit-Rate Clock Input, Phase 2
    reg         IS;                 // input            // ('569 item 28) Serial Instruction from ROM Output
    wire        WS;                 // input            // ('569 item 30) ROM Word Select
    reg         SYNC;               // input            // ('569 item 26) Word Cycle Sync
    wire        BCD;                // inout            // ('569 item 35) BCD input/output line 35 from/to auxiliary data storage circuit 25.

    // Output from DUT
    wire        A;                  // output reg       // ('569 item 38) Partially decoded LED segment sequence, bit A.
    wire        B;                  // output reg       //                    "        "     "    "        "    , bit B.
    wire        C;                  // output reg       //                    "        "     "    "        "    , bit C.
    wire        D;                  // output reg       //                    "        "     "    "        "    , bit D.
    wire        E;                  // output reg       //                    "        "     "    "        "    , bit E.
    wire        START;              // output reg       // ('569 item 40) Word synchronization pulse for digit scanner in Cathode Driver.
    wire        CARRY;              // output reg       // ('569 item 34) Status of the carry output of this block's adder, sent to the Control & Timing block.

    // Testbench signals
    reg         rst;
    reg         t1 = 1'b0, t2 = 1'b0, t3 = 1'b0, t4 = 1'b0;
    reg         go = 1'b0;
    reg         sim_start_pulse = 1'b0;
    reg         sim_end_pulse   = 1'b0;
    reg         loop_done_1;
    reg         loop_done_2;
    reg         loop_done_3;
    integer     fd; // File Descriptor
    integer     i, j;
    integer     TestIterations;
    reg         load_instruction = 1'b0;
    reg         fetch_instruction = 1'b0;
    reg         exec_instruction = 1'b0;
    reg         grab_em;
    reg         compare_em;
    reg         save_em;
    reg [9:0]   instruction     = 'b0;
    reg [55:0]  is_shifter      = 'b0;
    reg [56:1]  holdRegA        = 'b0;
    reg [56:1]  holdRegB        = 'b0;
    reg [56:1]  holdRegC        = 'b0;
    reg [56:1]  holdRegD        = 'b0;
    reg [56:1]  holdRegE        = 'b0;
    reg [56:1]  holdRegF        = 'b0;
    reg [56:1]  holdRegM        = 'b0;
    reg         request_BCD;              // Request pulse to load the BCD_shifter.  Driven by task.
    reg [56:1]  BCD_request_value;        // Value to be loaded into BCD_shifter.  Driven by task.
    reg         fetching_BCD;             // Asserted while the BCD -> C instruction is being shifted into A&R.
    reg [56:1]  BCD_load_value;           // Value to be loaded into BCD_shifter.
    reg [56:1]  BCD_shifter;              // Emulate the Data Storage Circuit 25.
    reg         BCD_out_en;               // Drive BCD pin during A&R's BCD -> C instruction.
    wire        BCD_in;                   // The input from the A&R's BCD pin.
    integer     sysCount        = 0;
    integer     nextSysCount    = 1;
    integer     wsBegin         = -1;
    integer     wsEnd           = -1;
    integer     wsBeginFetch    = -1;
    integer     wsEndFetch      = -1;
    integer     wsBeginExecute  = -1;
    integer     wsEndExecute    = -1;
    integer     digitCount      = 0;
    reg         carry_ff_66;

    integer     ia_zero   [13:0] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    integer     ia_regA   [13:0];   // Integer arrays
    integer     ia_regB   [13:0];
    integer     ia_regC   [13:0];
    integer     ia_regD   [13:0];
    integer     ia_regE   [13:0];
    integer     ia_regF   [13:0];
    integer     ia_regM   [13:0];
    integer     ia_dummy  [13:0];
    integer     i_carry;
    integer     i_opcode;
    string      s_mnemonic;
    integer     i_sub;
    integer     i_ci;
    integer     i_fs;
    string      s_fs;
    integer     i_ptr;

    string      beforeCall_s_mnemonic;
    integer     beforeCall_i_sub;
    integer     beforeCall_i_ci;
    integer     beforeCall_ia_regA [13:0];
    integer     beforeCall_ia_regB [13:0];
    integer     beforeCall_ia_regC [13:0];
    integer     beforeCall_ia_regD [13:0];
    integer     beforeCall_ia_regE [13:0];
    integer     beforeCall_ia_regF [13:0];
    integer     beforeCall_ia_regM [13:0];
    string      beforeCall_s_fs;
    integer     beforeCall_i_ptr;
    integer     afterCall_ia_regA [13:0];
    integer     afterCall_ia_regB [13:0];
    integer     afterCall_ia_regC [13:0];
    integer     afterCall_ia_regD [13:0];
    integer     afterCall_ia_regE [13:0];
    integer     afterCall_ia_regF [13:0];
    integer     afterCall_ia_regM [13:0];
    integer     afterCall_i_co;
    reg         afterCall_ready;

    string      pipe1_beforeCall_s_mnemonic;
    integer     pipe1_beforeCall_i_sub;
    integer     pipe1_beforeCall_i_ci;
    integer     pipe1_beforeCall_ia_regA [13:0];
    integer     pipe1_beforeCall_ia_regB [13:0];
    integer     pipe1_beforeCall_ia_regC [13:0];
    integer     pipe1_beforeCall_ia_regD [13:0];
    integer     pipe1_beforeCall_ia_regE [13:0];
    integer     pipe1_beforeCall_ia_regF [13:0];
    integer     pipe1_beforeCall_ia_regM [13:0];
    string      pipe1_beforeCall_s_fs;
    integer     pipe1_beforeCall_i_ptr;
    integer     pipe1_afterCall_ia_regA [13:0];
    integer     pipe1_afterCall_ia_regB [13:0];
    integer     pipe1_afterCall_ia_regC [13:0];
    integer     pipe1_afterCall_ia_regD [13:0];
    integer     pipe1_afterCall_ia_regE [13:0];
    integer     pipe1_afterCall_ia_regF [13:0];
    integer     pipe1_afterCall_ia_regM [13:0];
    integer     pipe1_afterCall_i_co;
    reg         pipe1_afterCall_ready;

    string      pipe2_beforeCall_s_mnemonic;
    integer     pipe2_beforeCall_i_sub;
    integer     pipe2_beforeCall_i_ci;
    integer     pipe2_beforeCall_ia_regA [13:0];
    integer     pipe2_beforeCall_ia_regB [13:0];
    integer     pipe2_beforeCall_ia_regC [13:0];
    integer     pipe2_beforeCall_ia_regD [13:0];
    integer     pipe2_beforeCall_ia_regE [13:0];
    integer     pipe2_beforeCall_ia_regF [13:0];
    integer     pipe2_beforeCall_ia_regM [13:0];
    string      pipe2_beforeCall_s_fs;
    integer     pipe2_beforeCall_i_ptr;
    integer     pipe2_afterCall_ia_regA [13:0];
    integer     pipe2_afterCall_ia_regB [13:0];
    integer     pipe2_afterCall_ia_regC [13:0];
    integer     pipe2_afterCall_ia_regD [13:0];
    integer     pipe2_afterCall_ia_regE [13:0];
    integer     pipe2_afterCall_ia_regF [13:0];
    integer     pipe2_afterCall_ia_regM [13:0];
    integer     pipe2_afterCall_i_co;
    reg         pipe2_afterCall_ready;

    string      pipe3_beforeCall_s_mnemonic;
    integer     pipe3_beforeCall_i_sub;
    integer     pipe3_beforeCall_i_ci;
    integer     pipe3_beforeCall_ia_regA [13:0];
    integer     pipe3_beforeCall_ia_regB [13:0];
    integer     pipe3_beforeCall_ia_regC [13:0];
    integer     pipe3_beforeCall_ia_regD [13:0];
    integer     pipe3_beforeCall_ia_regE [13:0];
    integer     pipe3_beforeCall_ia_regF [13:0];
    integer     pipe3_beforeCall_ia_regM [13:0];
    string      pipe3_beforeCall_s_fs;
    integer     pipe3_beforeCall_i_ptr;
    integer     pipe3_afterCall_ia_regA [13:0];
    integer     pipe3_afterCall_ia_regB [13:0];
    integer     pipe3_afterCall_ia_regC [13:0];
    integer     pipe3_afterCall_ia_regD [13:0];
    integer     pipe3_afterCall_ia_regE [13:0];
    integer     pipe3_afterCall_ia_regF [13:0];
    integer     pipe3_afterCall_ia_regM [13:0];
    integer     pipe3_afterCall_i_co;
    reg         pipe3_afterCall_ready;

    string      modeled_beforeCall_s_mnemonic;
    integer     modeled_beforeCall_i_sub;
    integer     modeled_beforeCall_i_ci;
    integer     modeled_beforeCall_ia_regA [13:0];
    integer     modeled_beforeCall_ia_regB [13:0];
    integer     modeled_beforeCall_ia_regC [13:0];
    integer     modeled_beforeCall_ia_regD [13:0];
    integer     modeled_beforeCall_ia_regE [13:0];
    integer     modeled_beforeCall_ia_regF [13:0];
    integer     modeled_beforeCall_ia_regM [13:0];
    string      modeled_beforeCall_s_fs;
    integer     modeled_beforeCall_i_ptr;
    integer     modeled_afterCall_ia_regA [13:0];
    integer     modeled_afterCall_ia_regB [13:0];
    integer     modeled_afterCall_ia_regC [13:0];
    integer     modeled_afterCall_ia_regD [13:0];
    integer     modeled_afterCall_ia_regE [13:0];
    integer     modeled_afterCall_ia_regF [13:0];
    integer     modeled_afterCall_ia_regM [13:0];
    integer     modeled_afterCall_i_co;
    reg         modeled_afterCall_ready;

    reg [56:1]  expected_regA;
    reg [56:1]  expected_regB;
    reg [56:1]  expected_regC;
    reg [56:1]  expected_regD;
    reg [56:1]  expected_regE;
    reg [56:1]  expected_regF;
    reg [56:1]  expected_regM;
    reg         expected_carry;

    reg [56:1]  actual_regA;
    reg [56:1]  actual_regB;
    reg [56:1]  actual_regC;
    reg [56:1]  actual_regD;
    reg [56:1]  actual_regE;
    reg [56:1]  actual_regF;
    reg [56:1]  actual_regM;
    reg         actual_carry;

    reg         regA_match;
    reg         regB_match;
    reg         regC_match;
    reg         regD_match;
    reg         regE_match;
    reg         regF_match;
    reg         regM_match;
    reg         carry_match;
    reg         all_regs_match;

    integer     good_count;
    integer     bad_count; 

    reg [56:1]  tbRegA;
    reg [56:1]  tbRegB;
    reg [56:1]  tbRegC;
    reg [56:1]  tbRegD;
    reg [56:1]  tbRegE;
    reg [56:1]  tbRegF;
    reg [56:1]  tbRegM;

    integer     CLRA_count, CLRA_P_count, CLRA_WP_count, CLRA_X_count, CLRA_XS_count, CLRA_M_count, CLRA_MS_count, CLRA_W_count, CLRA_S_count;
    integer     CLRB_count, CLRB_P_count, CLRB_WP_count, CLRB_X_count, CLRB_XS_count, CLRB_M_count, CLRB_MS_count, CLRB_W_count, CLRB_S_count;
    integer     CLRC_count, CLRC_P_count, CLRC_WP_count, CLRC_X_count, CLRC_XS_count, CLRC_M_count, CLRC_MS_count, CLRC_W_count, CLRC_S_count;
    integer     MOVAB_count, MOVAB_P_count, MOVAB_WP_count, MOVAB_X_count, MOVAB_XS_count, MOVAB_M_count, MOVAB_MS_count, MOVAB_W_count, MOVAB_S_count;
    integer     MOVBC_count, MOVBC_P_count, MOVBC_WP_count, MOVBC_X_count, MOVBC_XS_count, MOVBC_M_count, MOVBC_MS_count, MOVBC_W_count, MOVBC_S_count;
    integer     MOVCA_count, MOVCA_P_count, MOVCA_WP_count, MOVCA_X_count, MOVCA_XS_count, MOVCA_M_count, MOVCA_MS_count, MOVCA_W_count, MOVCA_S_count;
    integer     XCHAB_count, XCHAB_P_count, XCHAB_WP_count, XCHAB_X_count, XCHAB_XS_count, XCHAB_M_count, XCHAB_MS_count, XCHAB_W_count, XCHAB_S_count;
    integer     XCHBC_count, XCHBC_P_count, XCHBC_WP_count, XCHBC_X_count, XCHBC_XS_count, XCHBC_M_count, XCHBC_MS_count, XCHBC_W_count, XCHBC_S_count;
    integer     XCHAC_count, XCHAC_P_count, XCHAC_WP_count, XCHAC_X_count, XCHAC_XS_count, XCHAC_M_count, XCHAC_MS_count, XCHAC_W_count, XCHAC_S_count;
    integer     ADDACC_count, ADDACC_P_count, ADDACC_WP_count, ADDACC_X_count, ADDACC_XS_count, ADDACC_M_count, ADDACC_MS_count, ADDACC_W_count, ADDACC_S_count;
    integer     SUBACC_count, SUBACC_P_count, SUBACC_WP_count, SUBACC_X_count, SUBACC_XS_count, SUBACC_M_count, SUBACC_MS_count, SUBACC_W_count, SUBACC_S_count;
    integer     ADDABA_count, ADDABA_P_count, ADDABA_WP_count, ADDABA_X_count, ADDABA_XS_count, ADDABA_M_count, ADDABA_MS_count, ADDABA_W_count, ADDABA_S_count;
    integer     SUBABA_count, SUBABA_P_count, SUBABA_WP_count, SUBABA_X_count, SUBABA_XS_count, SUBABA_M_count, SUBABA_MS_count, SUBABA_W_count, SUBABA_S_count;
    integer     ADDACA_count, ADDACA_P_count, ADDACA_WP_count, ADDACA_X_count, ADDACA_XS_count, ADDACA_M_count, ADDACA_MS_count, ADDACA_W_count, ADDACA_S_count;
    integer     SUBACA_count, SUBACA_P_count, SUBACA_WP_count, SUBACA_X_count, SUBACA_XS_count, SUBACA_M_count, SUBACA_MS_count, SUBACA_W_count, SUBACA_S_count;
    integer     ADDCCC_count, ADDCCC_P_count, ADDCCC_WP_count, ADDCCC_X_count, ADDCCC_XS_count, ADDCCC_M_count, ADDCCC_MS_count, ADDCCC_W_count, ADDCCC_S_count;
    integer     CMP0B_count, CMP0B_P_count, CMP0B_WP_count, CMP0B_X_count, CMP0B_XS_count, CMP0B_M_count, CMP0B_MS_count, CMP0B_W_count, CMP0B_S_count;
    integer     CMP0C_count, CMP0C_P_count, CMP0C_WP_count, CMP0C_X_count, CMP0C_XS_count, CMP0C_M_count, CMP0C_MS_count, CMP0C_W_count, CMP0C_S_count;
    integer     CMPAC_count, CMPAC_P_count, CMPAC_WP_count, CMPAC_X_count, CMPAC_XS_count, CMPAC_M_count, CMPAC_MS_count, CMPAC_W_count, CMPAC_S_count;
    integer     CMPAB_count, CMPAB_P_count, CMPAB_WP_count, CMPAB_X_count, CMPAB_XS_count, CMPAB_M_count, CMPAB_MS_count, CMPAB_W_count, CMPAB_S_count;
    integer     CMPA1_count, CMPA1_P_count, CMPA1_WP_count, CMPA1_X_count, CMPA1_XS_count, CMPA1_M_count, CMPA1_MS_count, CMPA1_W_count, CMPA1_S_count;
    integer     CMPC1_count, CMPC1_P_count, CMPC1_WP_count, CMPC1_X_count, CMPC1_XS_count, CMPC1_M_count, CMPC1_MS_count, CMPC1_W_count, CMPC1_S_count;
    integer     TCC_count, TCC_P_count, TCC_WP_count, TCC_X_count, TCC_XS_count, TCC_M_count, TCC_MS_count, TCC_W_count, TCC_S_count;
    integer     NCC_count, NCC_P_count, NCC_WP_count, NCC_X_count, NCC_XS_count, NCC_M_count, NCC_MS_count, NCC_W_count, NCC_S_count;
    integer     INCA_count, INCA_P_count, INCA_WP_count, INCA_X_count, INCA_XS_count, INCA_M_count, INCA_MS_count, INCA_W_count, INCA_S_count;
    integer     INCC_count, INCC_P_count, INCC_WP_count, INCC_X_count, INCC_XS_count, INCC_M_count, INCC_MS_count, INCC_W_count, INCC_S_count;
    integer     DECA_count, DECA_P_count, DECA_WP_count, DECA_X_count, DECA_XS_count, DECA_M_count, DECA_MS_count, DECA_W_count, DECA_S_count;
    integer     DECC_count, DECC_P_count, DECC_WP_count, DECC_X_count, DECC_XS_count, DECC_M_count, DECC_MS_count, DECC_W_count, DECC_S_count;
    integer     SHRA_count, SHRA_P_count, SHRA_WP_count, SHRA_X_count, SHRA_XS_count, SHRA_M_count, SHRA_MS_count, SHRA_W_count, SHRA_S_count;
    integer     SHRB_count, SHRB_P_count, SHRB_WP_count, SHRB_X_count, SHRB_XS_count, SHRB_M_count, SHRB_MS_count, SHRB_W_count, SHRB_S_count;
    integer     SHRC_count, SHRC_P_count, SHRC_WP_count, SHRC_X_count, SHRC_XS_count, SHRC_M_count, SHRC_MS_count, SHRC_W_count, SHRC_S_count;
    integer     SHLA_count, SHLA_P_count, SHLA_WP_count, SHLA_X_count, SHLA_XS_count, SHLA_M_count, SHLA_MS_count, SHLA_W_count, SHLA_S_count;

// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 
    task wsLookupTask;
    // Set the word select endpoints from a given field select string.
        output integer  ws_begin;
        output integer  ws_end;
        input string    field_select;
        input integer   pointer_val;
        begin : task_ws_lookup
            case (field_select)
                "P"     : begin ws_begin = pointer_val; ws_end = pointer_val; end   // Pointer position.
                "WP"    : begin ws_begin = 0;           ws_end = pointer_val; end   // Up to pointer position.
                "X"     : begin ws_begin = 0;           ws_end = 2;  end            // Exponent field (with sign).
                "XS"    : begin ws_begin = 2;           ws_end = 2;  end            // Exponent sign.
                "M"     : begin ws_begin = 3;           ws_end = 12; end            // Mantissa field without sign.
                "MS"    : begin ws_begin = 3;           ws_end = 13; end            // Mantissa field with sign.
                "W"     : begin ws_begin = 0;           ws_end = 13; end            // Entire word.
                "S"     : begin ws_begin = 13;          ws_end = 13; end            // Mantissa sign only.
                default : begin ws_begin = -1;          ws_end = -1; end            // If no valid field select.
            endcase
        end
    endtask

    task buildInstruction;
    // Build the 10-bit instruction from a given instruction mnemonic and field-select (both strings).
        output reg [9:0]    op_code;
        output integer      ws_begin;
        output integer      ws_end;
        input string        op_mnemonic;
        input string        field_select;
//      input integer       pointer_val = -1;   // Default arguments supported in SystemVerilog only.
        input integer       pointer_val;
        begin : task_make_instruction 
            integer itype = -1;
            case (op_mnemonic)
                // Type 2 Instructions
                // -- Class 1) Clear
                "CLRA"      : begin itype = 2; op_code = 10'b10111_zzz_10; end  // 0 -> A
                "CLRB"      : begin itype = 2; op_code = 10'b00001_zzz_10; end  // 0 -> B
                "CLRC"      : begin itype = 2; op_code = 10'b00110_zzz_10; end  // 0 -> C
                // -- Class 2) Transfer/Exchange
                "MOVAB"     : begin itype = 2; op_code = 10'b01001_zzz_10; end  // A -> B
                "MOVBC"     : begin itype = 2; op_code = 10'b00100_zzz_10; end  // B -> C
                "MOVCA"     : begin itype = 2; op_code = 10'b01100_zzz_10; end  // C -> A
                "XCHAB"     : begin itype = 2; op_code = 10'b11001_zzz_10; end  // A <-> B
                "XCHBC"     : begin itype = 2; op_code = 10'b10001_zzz_10; end  // B <-> C
                "XCHAC"     : begin itype = 2; op_code = 10'b11101_zzz_10; end  // A <-> C
                // -- Class 3) Add/Subtract
                "ADDACC"    : begin itype = 2; op_code = 10'b01110_zzz_10; end  // A + C -> C
                "SUBACC"    : begin itype = 2; op_code = 10'b01010_zzz_10; end  // A - C -> C
                "ADDABA"    : begin itype = 2; op_code = 10'b11100_zzz_10; end  // A + B -> A
                "SUBABA"    : begin itype = 2; op_code = 10'b11000_zzz_10; end  // A - B -> A
                "ADDACA"    : begin itype = 2; op_code = 10'b11110_zzz_10; end  // A + C -> A
                "SUBACA"    : begin itype = 2; op_code = 10'b11010_zzz_10; end  // A - C -> A
                "ADDCCC"    : begin itype = 2; op_code = 10'b10101_zzz_10; end  // C + C -> C
                // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                "CMP0B"     : begin itype = 2; op_code = 10'b00000_zzz_10; end  // 0 - B            (Compare B to zero)
                "CMP0C"     : begin itype = 2; op_code = 10'b01101_zzz_10; end  // 0 - C            (Compare C to zero)
                "CMPAC"     : begin itype = 2; op_code = 10'b00010_zzz_10; end  // A - C            (Compare A and C)
                "CMPAB"     : begin itype = 2; op_code = 10'b10000_zzz_10; end  // A - B            (Compare A and B)
                "CMPA1"     : begin itype = 2; op_code = 10'b10011_zzz_10; end  // A - 1            (Compare A to one)
                "CMPC1"     : begin itype = 2; op_code = 10'b00011_zzz_10; end  // C - 1            (Compare C to one)    
                // -- Class 5) Complement
                "TCC"       : begin itype = 2; op_code = 10'b00101_zzz_10; end  // 0 - C -> C       (Tens Complement register C)
                "NCC"       : begin itype = 2; op_code = 10'b00111_zzz_10; end  // 0 - C - 1 -> C   (Nines Complement register C)
                // -- Class 6) Increment
                "INCA"      : begin itype = 2; op_code = 10'b11111_zzz_10; end  // A + 1 -> A       (Increment register A)
                "INCC"      : begin itype = 2; op_code = 10'b01111_zzz_10; end  // C + 1 -> C       (Increment register C)
                // -- Class 7) Decrement
                "DECA"      : begin itype = 2; op_code = 10'b11011_zzz_10; end  // A - 1 -> A       (Decrement register A)
                "DECC"      : begin itype = 2; op_code = 10'b01011_zzz_10; end  // C - 1 -> C       (Decrement register C)
                // -- Class 8) Shift
                "SHRA"      : begin itype = 2; op_code = 10'b10110_zzz_10; end  // A >> 1           (Shift Right register A)
                "SHRB"      : begin itype = 2; op_code = 10'b10100_zzz_10; end  // B >> 1           (Shift Right register B)
                "SHRC"      : begin itype = 2; op_code = 10'b10010_zzz_10; end  // C >> 1           (Shift Right register C)
                "SHLA"      : begin itype = 2; op_code = 10'b01000_zzz_10; end  // A << 1           (Shift Left register A)
                // Type 5 Instructions
//              "AIx"       : begin itype = 5; op_code = 10'b????_00_1000; end  // Available Instructions (16)
                "LDC0"      : begin itype = 5; op_code = 10'b0000_01_1000; end  // Load Constant, 0 -> C at Pointer, post-decrement Pointer
                "LDC1"      : begin itype = 5; op_code = 10'b0001_01_1000; end  //  "      "      1 -> C       "      "       "        "
                "LDC2"      : begin itype = 5; op_code = 10'b0010_01_1000; end  //  "      "      2 -> C       "      "       "        "
                "LDC3"      : begin itype = 5; op_code = 10'b0011_01_1000; end  //  "      "      3 -> C       "      "       "        "
                "LDC4"      : begin itype = 5; op_code = 10'b0100_01_1000; end  //  "      "      4 -> C       "      "       "        "
                "LDC5"      : begin itype = 5; op_code = 10'b0101_01_1000; end  //  "      "      5 -> C       "      "       "        "
                "LDC6"      : begin itype = 5; op_code = 10'b0110_01_1000; end  //  "      "      6 -> C       "      "       "        "
                "LDC7"      : begin itype = 5; op_code = 10'b0111_01_1000; end  //  "      "      7 -> C       "      "       "        "
                "LDC8"      : begin itype = 5; op_code = 10'b1000_01_1000; end  //  "      "      8 -> C       "      "       "        "
                "LDC9"      : begin itype = 5; op_code = 10'b1001_01_1000; end  //  "      "      9 -> C       "      "       "        "
                "LDCA"      : begin itype = 5; op_code = 10'b1010_01_1000; end  //  "      "      A -> C       "      "       "        "
                "LDCB"      : begin itype = 5; op_code = 10'b1011_01_1000; end  //  "      "      B -> C       "      "       "        "
                "LDCC"      : begin itype = 5; op_code = 10'b1100_01_1000; end  //  "      "      C -> C       "      "       "        "
                "LDCD"      : begin itype = 5; op_code = 10'b1101_01_1000; end  //  "      "      D -> C       "      "       "        "
                "LDCE"      : begin itype = 5; op_code = 10'b1110_01_1000; end  //  "      "      E -> C       "      "       "        "
                "LDCF"      : begin itype = 5; op_code = 10'b1111_01_1000; end  //  "      "      F -> C       "      "       "        "
                "DSPTOG"    : begin itype = 5; op_code = 10'b000_01z_1000; end  // Display Toggle
                "XCHCM"     : begin itype = 5; op_code = 10'b001_01z_1000; end  // C -> M -> C              (Exchange Memory)
                "PUSHC"     : begin itype = 5; op_code = 10'b010_01z_1000; end  // C -> C -> D -> E -> F    (Up Stack or Push C) 
                "POPA"      : begin itype = 5; op_code = 10'b011_01z_1000; end  // F -> F -> E -> D -> A    (Down Stack or Pop A)
                "DSPOFF"    : begin itype = 5; op_code = 10'b100_01z_1000; end  // Display Off
                "RCLM"      : begin itype = 5; op_code = 10'b101_01z_1000; end  // M -> M -> C              (Recall Memory)
                "ROTDN"     : begin itype = 5; op_code = 10'b110_01z_1000; end  // C -> F -> E -> D -> C    (Rotate Down)
                "CLRALL"    : begin itype = 5; op_code = 10'b111_01z_1000; end  // 0 -> A, B, C, D, E, F, M (Clear all 56 bits of all registers)
                "MOVISA"    : begin itype = 5; op_code = 10'bzz0_11z_1000; end  // Is -> A                  (Load 56-bit instruction sequence into register A)
                "MOVBCDC"   : begin itype = 5; op_code = 10'bzz1_11z_1000; end  // BCD -> C                 (Load 56-bit data storage value into register C)
                // Type 10 Instruction
                "NOP"       : begin itype = 10; op_code = 10'b0000000000; end   // No Operation             (All registers recirculate)
                // If none of the above
                default     : begin itype = -1; op_code = 10'bzzzzzzzzzz; end
            endcase

            case (field_select)
                "P"     : begin if (itype == 2) op_code[4:2] = 3'b000;  ws_begin = pointer_val; ws_end = pointer_val; end   // Pointer position.
                "WP"    : begin if (itype == 2) op_code[4:2] = 3'b001;  ws_begin = 0;           ws_end = pointer_val; end   // Up to pointer position.
                "X"     : begin if (itype == 2) op_code[4:2] = 3'b010;  ws_begin = 0;           ws_end = 2;  end            // Exponent field (with sign).
                "XS"    : begin if (itype == 2) op_code[4:2] = 3'b011;  ws_begin = 2;           ws_end = 2;  end            // Exponent sign.
                "M"     : begin if (itype == 2) op_code[4:2] = 3'b100;  ws_begin = 3;           ws_end = 12; end            // Mantissa field without sign.
                "MS"    : begin if (itype == 2) op_code[4:2] = 3'b101;  ws_begin = 3;           ws_end = 13; end            // Mantissa field with sign.
                "W"     : begin if (itype == 2) op_code[4:2] = 3'b110;  ws_begin = 0;           ws_end = 13; end            // Entire word.
                "S"     : begin if (itype == 2) op_code[4:2] = 3'b111;  ws_begin = 13;          ws_end = 13; end            // Mantissa sign only.
                default : begin                                         ws_begin = -1;          ws_end = -1; end            // If no valid field select.
            endcase
        end
    endtask

    task issueInstruction;
    // Using the given mnemonic, field-select, and pointer value, build the 10-bit instruction, then issue the instruction at the right time.
        input string        op_mnemonic;
        input string        field_select;
        input integer       pointer_val;
        begin : task_issue_instr 
            wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, op_mnemonic, field_select, pointer_val);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1;
        end
    endtask

    task loadBCDandIssueMOVBCDC;
    // Simultaneously load the BCD shifter with a 14-digit value and issue the MOVBCBC (BCD -> C) instruction.
        input integer intArrayBCDval [13:0];
        begin : task_load_bcd_move_c
            wait (nextSysCount == 0);
            BCD_request_value = intArray2Reg(intArrayBCDval);                   // Value to be loaded into BCD_shifter.
            buildInstruction(instruction, wsBegin, wsEnd, "MOVBCDC", "", -1);   // Build the 10-bit instruction to be shifted into Is.
            request_BCD = 1'b1;                                                 // Begin pulse that requests loading the BCD_shifter.
            load_instruction = 1'b1;                                            // Begin pulse that loads the is_shifter.
            @(posedge PHI2);                                                    // Wait a clock.
            buildInstruction(instruction, wsBegin, wsEnd, "", "", -1);          // Clear the instruction value.
            load_instruction = 1'b0;                                            // End pulse.
            request_BCD = 1'b0;                                                 // End pulse. 
            #1;                                                                 // Nudge simulator time forward to complete the pulse.
        end
    endtask

    task addSubIntRegs;
    // Add two 14-digit integer arrays :        ci + x + y -> (r, co)
    // Subtract two 14-digit integer arrays :   ci - x - y -> (r, co)
    // Example calls:
    //    addSubIntRegs(0, 0, ia_regC, ia_regC, ia_regC, i_carry, "M", -1); // ADDCCC:  ia_regC[M] + ia_regC[M] -> (ia_regC[M], i_carry) 
    //    addSubIntRegs(0, 0, ia_regA, ia_regC, ia_regC, i_carry, "M", -1); // ADDACC:  ia_regA[M] + ia_regC[M] -> (ia_regC[M], i_carry) 
    //    addSubIntRegs(0, 0, ia_regA, ia_regC, ia_regA, i_carry, "M", -1); // ADDACA:  ia_regA[M] + ia_regC[M] -> (ia_regA[M], i_carry) 
    //    addSubIntRegs(0, 0, ia_regA, ia_regB, ia_regA, i_carry, "M", -1); // ADDABA:  ia_regA[M] + ia_regB[M] -> (ia_regA[M], i_carry) 
    //
    //    addSubIntRegs(0, 0, ia_regA, ia_regC, ia_regC, i_carry, "M", -1); // SUBACC:  ia_regA[M] - ia_regC[M] -> (ia_regC[M], i_carry) 
    //    addSubIntRegs(0, 0, ia_regA, ia_regC, ia_regA, i_carry, "M", -1); // SUBACA:  ia_regA[M] - ia_regC[M] -> (ia_regA[M], i_carry) 
    //    addSubIntRegs(0, 0, ia_regA, ia_regB, ia_regA, i_carry, "M", -1); // SUBABA:  ia_regA[M] - ia_regB[M] -> (ia_regA[M], i_carry) 
    //
    //    addSubIntRegs(.sub(0), .ci(0), .x(ia_regA), .y(ia_regB), .r(ia_regA), .co(i_carry), .fs("M"), .p(-1)); // ADDABA:  ia_regA[M] + ia_regB[M] -> (ia_regA[M], i_carry)
        input   integer sub;        // Subtract command, 1 = subtract
        input   integer ci;         // Carry/borrow in
        input   integer x [13:0];   // X input array (augend or minuend)
        input   integer y [13:0];   // Y input array (addend or subtrahend)
        inout   integer r [13:0];   // Result array
        output  integer co;         // Carry/borrow out
        input   string  fs;         // Field-select
        input   integer p;          // Pointer value when field-select is "P" or "WP".
        begin : task_add_regs
            integer tempReg [13:0];
            integer wsBegin;
            integer wsEnd;
            integer digit;

            wsLookupTask(wsBegin, wsEnd, fs, p);

            if (sub)                                                            // If asked to subtract, then...
                for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  //    For each digit in the specified field-select range...
                        tempReg[digit] = x[digit] - y[digit] - ci;              //       Compute the difference including the borrow.
                        if (tempReg[digit] < 0) begin                           //       if subtraction result is negative, then...
                            tempReg[digit] = tempReg[digit] + 10;               //           take the 10's complement of the negative difference
                            co = 1;                                             //           and also set the carry
                        end                                                     //       
                        else begin                                              //       otherwise subtraction result was positive, so...
                            co = 0;                                             //           clear the carry
                        end                                                     //   
                    ci = co;                                                    //       Use carry-out of current digit as carry-in of next digit. 
                end                                                                         
            else                                                                // Otherwise we were asked to add, so...
                for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  //    For each digit in the specified field-select range...
                    tempReg[digit] = x[digit] + y[digit] + ci;                  //       Compute the sum including the carry,
                    if (tempReg[digit] > 9) begin                               //       if the sum exceeds the precision, then
                        tempReg[digit] = tempReg[digit] - 10;                   //           subtract the carry weight
                        co = 1;                                                 //           and also set the carry
                    end                                                         //       
                    else begin                                                  //       otherwise the sum didn't exceed the precision, so...
                        co = 0;                                                 //           clear the carry
                    end                                                         //         
                    ci = co;                                                    //       Use carry-out of current digit as carry-in of next digit. 
                end
            
            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin      // For all digits in the field select...          
                r[digit] = tempReg[digit];                                      //    copy the sum to the result register.
            end
        end
    endtask

    task shiftRightReg;
    // Shift right the digits in the specified register and field-select.
    // Example calls:
    //    shiftRightReg(ia_regA, "M", -1); // SHRA:  ia_regA[M]  >> 1 -> ia_regA[M]
    //    shiftRightReg(ia_regB, "X", -1); // SHRB:  ia_regB[X]  >> 1 -> ia_regB[X]
    //    shiftRightReg(ia_regC, "WP", 3); // SHRC:  ia_regC[WP] >> 1 -> ia_regC[WP]
        inout   integer inReg [13:0];   // Register to be shifted.
        output  integer co;             // Carry/borrow out
        input   string  fs;             // Field-select
        input   integer p;              // Pointer value when field-select is "P" or "WP".
        begin : task_shift_right
            integer tempReg [13:0];
            integer wsBegin;
            integer wsEnd;
            integer digit;

            wsLookupTask(wsBegin, wsEnd, fs, p);

            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For each digit in the specified field-select range...
                if (digit == wsEnd)                                         //    if we've reached the leftmost digit in the range, then...
                    tempReg[digit] = 0;                                     //       clear that digit,
                else                                                        //    otherwise, we're working on any digit other than the leftmost, so...
                    tempReg[digit] = inReg[digit+1];                        //       copy into the current digit the value from the digit immediately to the left.
            end                                                             // Point to next digit.
            
            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For all digits in the field select...          
                inReg[digit] = tempReg[digit];                              //    copy the sum to the result register.
            end
            co = 0;
        end
    endtask

    task shiftLeftReg;
    // Shift left the digits in the specified register and field-select.
    // Example calls:
    //    shiftLeftReg(ia_regA, "M", -1); // SHLA:  ia_regA[M] << 1 -> ia_regA[M]
        inout   integer inReg [13:0];   // Register to be shifted.
        output  integer co;             // Carry/borrow out
        input   string  fs;             // Field-select
        input   integer p;              // Pointer value when field-select is "P" or "WP".
        begin : task_shift_left
            integer tempReg [13:0];
            integer wsBegin;
            integer wsEnd;
            integer digit;

            wsLookupTask(wsBegin, wsEnd, fs, p);

            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For each digit in the specified field-select range...
                if (digit == wsBegin)                                       //    if we're at the rightmost digit in the range, then...
                    tempReg[digit] = 0;                                     //       clear that digit,
                else                                                        //    otherwise, we're working on any digit other than the rightmost, so...
                    tempReg[digit] = inReg[digit-1];                        //       copy into the current digit the value from the digit immediately to the right.
            end                                                             // Point to next digit.
            
            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For all digits in the field select...          
                inReg[digit] = tempReg[digit];                              //    copy the sum to the result register.
            end
            co = 0;
        end
    endtask

    task moveRegs;
    // Move the contents of the specified source register to the specified destination register, only digits specified by field-select.
    // Example calls:
    //    moveRegs(ia_regA, ia_regB, "M", -1); // MOVAB:  ia_regA[M]  -> ia_regB[M]
    //    moveRegs(ia_regB, ia_regC, "X", -1); // MOVBC:  ia_regB[X]  -> ia_regC[X]
    //    moveRegs(ia_regC, ia_regA, "WP", 3); // MOVCA:  ia_regC[WP] -> ia_regA[WP]
        input   integer srcReg  [13:0]; // Source register.
        inout   integer destReg [13:0]; // Destination register.
        output  integer co;             // Carry/borrow out
        input   string  fs;             // Field-select
        input   integer p;              // Pointer value when field-select is "P" or "WP".
        begin : task_move
            integer wsBegin;
            integer wsEnd;
            integer digit;

            wsLookupTask(wsBegin, wsEnd, fs, p);

            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For each digit in the specified field-select range...
                destReg[digit] = srcReg[digit];                             //    copy the source register into the destination register
            end                                                             // Point to next digit.
            co = 0;
        end
    endtask

    task exchangeRegs;
    // Exchange the contents of the specified register pair, only digits specified by field-select.
    // Example calls:
    //    exchangeRegs(ia_regA, ia_regB, "M", -1); // XCHAB:  ia_regA[M]  <-> ia_regB[M]
    //    exchangeRegs(ia_regA, ia_regC, "X", -1); // XCHAC:  ia_regA[X]  <-> ia_regC[X]
    //    exchangeRegs(ia_regB, ia_regC, "WP", 3); // XCHBC:  ia_regB[WP] <-> ia_regC[WP]
        inout   integer inRegX [13:0];  // Register to be exchanged.
        inout   integer inRegY [13:0];  // Register to be exchanged.
        output  integer co;             // Carry/borrow out
        input   string  fs;             // Field-select
        input   integer p;              // Pointer value when field-select is "P" or "WP".
        begin : task_exchange
            integer tempRegX [13:0];
            integer tempRegY [13:0];
            integer wsBegin;
            integer wsEnd;
            integer digit;

            wsLookupTask(wsBegin, wsEnd, fs, p);

            tempRegX = inRegX;                                              // Save each incoming register.
            tempRegY = inRegY;                                              // Save each incoming register.
            for (digit = wsBegin; digit <= wsEnd; digit = digit + 1) begin  // For each digit in the specified field-select range...
                inRegX[digit] = tempRegY[digit];                            //    copy the selected fields in the other register's saved version into this register
                inRegY[digit] = tempRegX[digit];                            //    copy the selected fields in the other register's saved version into this register
            end                                                             // Point to next digit.
            co = 0;
        end
    endtask

    task loadConstantRegC;
    // Load the DUT's C register with the incoming 14-digit integer array.
        input integer  intArray_regTemp   [13:0];
        begin : task_load_c
            integer     digit;
            integer     num_digits = 14;
            // Load register C digit-by-digit 
            for (digit = num_digits - 1; digit >= 0; digit = digit-1) begin
                case (intArray_regTemp[digit])
                    0 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC0",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    1 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC1",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    2 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC2",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    3 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC3",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    4 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC4",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    5 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC5",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    6 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC6",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    7 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC7",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    8 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC8",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    9 : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "LDC9",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                    default : begin wait (nextSysCount == 0); buildInstruction(instruction, wsBegin, wsEnd, "NOP",   "P", digit);  load_instruction = 1'b1; @(posedge PHI2); buildInstruction(instruction, wsBegin, wsEnd, "", "", -1); load_instruction = 1'b0; #1; end
                endcase
            end
        end
    endtask

    task loadAllRegsRandom;
    // Generate an array of rendom integers for each register.
    // Use integers so we can perform digit-by-digit arithmetic on their contents.
    // Uses LDC which takes 14 word cycles to load a single 14-digit constant.
        output integer  intArray_regA   [13:0];
        output integer  intArray_regB   [13:0];
        output integer  intArray_regC   [13:0];
        output integer  intArray_regD   [13:0];
        output integer  intArray_regE   [13:0];
        output integer  intArray_regF   [13:0];
        output integer  intArray_regM   [13:0];
        begin : task_load_all
            integer     digit;
            integer     num_digits = 14;

            // Generate random contents for all registers, digit-by-digit
            for (digit = 0; digit < num_digits; digit = digit+1) begin
                intArray_regA[digit] = $urandom_range(9,0); // Set current digit to random value in the range 0 to 9.
                intArray_regB[digit] = $urandom_range(9,0); 
                intArray_regC[digit] = $urandom_range(9,0); 
                intArray_regD[digit] = $urandom_range(9,0); 
                intArray_regE[digit] = $urandom_range(9,0); 
                intArray_regF[digit] = $urandom_range(9,0); 
                intArray_regM[digit] = $urandom_range(9,0); 
            end

            issueInstruction("CLRALL", "", -1);
            loadConstantRegC(intArray_regM);
            issueInstruction("XCHCM", "",  -1);
            loadConstantRegC(intArray_regF);
            issueInstruction("PUSHC", "",  -1);
            loadConstantRegC(intArray_regE);
            issueInstruction("PUSHC", "",  -1);
            loadConstantRegC(intArray_regD);
            issueInstruction("PUSHC", "",  -1);
            loadConstantRegC(intArray_regB);
            issueInstruction("XCHBC", "W", -1);
            loadConstantRegC(intArray_regA);
            issueInstruction("XCHAC", "W", -1);
            loadConstantRegC(intArray_regC);
        end
    endtask

    task loadAllRegsRandomUsingBCD;
    // Generate an array of random integers for each register.
    // Use integers so we can perform digit-by-digit arithmetic on their contents.
    // Uses MOVBCDC so the C register can be loaded in 1 word cycle.
        output integer  intArray_regA   [13:0];
        output integer  intArray_regB   [13:0];
        output integer  intArray_regC   [13:0];
        output integer  intArray_regD   [13:0];
        output integer  intArray_regE   [13:0];
        output integer  intArray_regF   [13:0];
        output integer  intArray_regM   [13:0];
        begin : task_load_all
            integer     digit;
            integer     num_digits = 14;

            // Generate random contents for all registers, digit-by-digit
            for (digit = 0; digit < num_digits; digit = digit+1) begin
                intArray_regA[digit] = $urandom_range(9,0); // Set current digit to random value in the range 0 to 9.
                intArray_regB[digit] = $urandom_range(9,0); 
                intArray_regC[digit] = $urandom_range(9,0); 
                intArray_regD[digit] = $urandom_range(9,0); 
                intArray_regE[digit] = $urandom_range(9,0); 
                intArray_regF[digit] = $urandom_range(9,0); 
                intArray_regM[digit] = $urandom_range(9,0); 
            end

                                                    //                          |                                 Contents of Registers after Instruction                                       |
                                                    //                          |       A       |       B       |       C       |       D       |       E       |       F       |       M       |
                                                    //                          |---------------|---------------|---------------|---------------|---------------|---------------|---------------|
            issueInstruction("CLRALL", "", -1);     // 0 -> A, B, C, D, E, F, M |       0       |       0       |       0       |       0       |       0       |       0       |       0       |
            loadBCDandIssueMOVBCDC(intArray_regM);  // intArray_regM -> C       |       0       |       0       | intArray_regM |       0       |       0       |       0       |       0       |
            issueInstruction("XCHCM", "",  -1);     // C <-> M                  |       0       |       0       |       0       |       0       |       0       |       0       | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regF);  // intArray_regF -> C       |       0       |       0       | intArray_regF |       0       |       0       |       0       | intArray_regM |
            issueInstruction("PUSHC", "",  -1);     // C -> C -> D -> E -> F    |       0       |       0       | intArray_regF | intArray_regF |       0       |       0       | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regE);  // intArray_regE -> C       |       0       |       0       | intArray_regE | intArray_regF |       0       |       0       | intArray_regM |
            issueInstruction("PUSHC", "",  -1);     // C -> C -> D -> E -> F    |       0       |       0       | intArray_regE | intArray_regE | intArray_regF |       0       | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regD);  // intArray_regD -> C       |       0       |       0       | intArray_regD | intArray_regE | intArray_regF |       0       | intArray_regM |
            issueInstruction("PUSHC", "",  -1);     // C -> C -> D -> E -> F    |       0       |       0       | intArray_regD | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regB);  // intArray_regB -> C       |       0       |       0       | intArray_regB | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
            issueInstruction("XCHBC", "W", -1);     // B <-> C                  |       0       | intArray_regB |       0       | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regA);  // intArray_regA -> C       |       0       | intArray_regB | intArray_regA | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
            issueInstruction("XCHAC", "W", -1);     // A <-> C                  | intArray_regA | intArray_regB |       0       | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
            loadBCDandIssueMOVBCDC(intArray_regC);  // intArray_regC -> C       | intArray_regA | intArray_regB | intArray_regC | intArray_regD | intArray_regE | intArray_regF | intArray_regM |
        end
    endtask

    task loadRegsABCRandomUsingBCD;
    // Generate an array of random integers for each register, A, B, and C.
    // Use integers so we can perform digit-by-digit arithmetic on their contents.
    // Uses MOVBCDC so the C register can be loaded in 1 word cycle.
        output integer  intArray_regA   [13:0];
        output integer  intArray_regB   [13:0];
        output integer  intArray_regC   [13:0];
        begin : task_load_abc
            integer     digit;
            integer     num_digits = 14;

            // Generate random contents for all registers, digit-by-digit
            for (digit = 0; digit < num_digits; digit = digit+1) begin
                intArray_regA[digit] = $urandom_range(9,0); // Set current digit to random value in the range 0 to 9.
                intArray_regB[digit] = $urandom_range(9,0); 
                intArray_regC[digit] = $urandom_range(9,0); 
            end
                                                    //                          |                                 Contents of Registers after Instruction                                       |
                                                    //                          |       A       |       B       |       C       |       D       |       E       |       F       |       M       |
                                                    //                          |---------------|---------------|---------------|---------------|---------------|---------------|---------------|
                                                    //                          |   old Reg A   |   old Reg B   |   old Reg C   |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
            loadBCDandIssueMOVBCDC(intArray_regB);  // intArray_regB -> C       |   old Reg A   |   old Reg B   | intArray_regB |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
            issueInstruction("XCHBC", "W", -1);     // B <-> C                  |   old Reg A   | intArray_regB |   old Reg B   |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
            loadBCDandIssueMOVBCDC(intArray_regA);  // intArray_regA -> C       |   old Reg A   | intArray_regB | intArray_regA |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
            issueInstruction("XCHAC", "W", -1);     // A <-> C                  | intArray_regA | intArray_regB |   old Reg A   |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
            loadBCDandIssueMOVBCDC(intArray_regC);  // intArray_regC -> C       | intArray_regA | intArray_regB | intArray_regC |   old Reg D   |   old Reg E   |   old Reg F   |   old Reg M   |
        end
    endtask

    function [56:1] intArray2Reg;
    // Convert a given 14-digit integer array into a 56-bit register value.
        input integer intArray_regInput [13:0];
        begin : func_ia2r
            integer     digit;
            integer     num_digits = 14;
            for (digit = 0; digit < num_digits; digit = digit+1) begin
                // Digit  0 -> register bits  [4: 1]
                // Digit  1 -> register bits  [8: 5]
                // Digit 13 -> register bits [56:53]
                // Use variable vector part select like this:  [<startbit> -: <width>].
                intArray2Reg[(4*digit+4)-:4] = intArray_regInput[digit];
            end
        end
    endfunction
            
    task saveBeforeCall;
    // Save the state of all instruction parameters before making the simulated instruction call.
        input integer clear_em;   // 1 = Clear all the values for initialization, 0 = Save the values normally.
        begin : task_save_before
            beforeCall_s_mnemonic   = (clear_em)? ""      : s_mnemonic;
            beforeCall_i_sub        = (clear_em)? 0       : i_sub;         
            beforeCall_i_ci         = (clear_em)? 0       : i_ci;          
            beforeCall_ia_regA      = (clear_em)? ia_zero : ia_regA;
            beforeCall_ia_regB      = (clear_em)? ia_zero : ia_regB;
            beforeCall_ia_regC      = (clear_em)? ia_zero : ia_regC;
            beforeCall_ia_regD      = (clear_em)? ia_zero : ia_regD;
            beforeCall_ia_regE      = (clear_em)? ia_zero : ia_regE;
            beforeCall_ia_regF      = (clear_em)? ia_zero : ia_regF;
            beforeCall_ia_regM      = (clear_em)? ia_zero : ia_regM;
            beforeCall_s_fs         = (clear_em)? ""      : s_fs;   
            beforeCall_i_ptr        = (clear_em)? 0       : i_ptr;
        end
    endtask

    task saveAfterCall;
    // Save the state of instruction results after making the simulated instruction call.
        input integer clear_em;   // 1 = Clear all the values for initialization, 0 = Save the values normally.
        begin : task_save_after
            afterCall_ia_regA   = (clear_em)? ia_zero : ia_regA; 
            afterCall_ia_regB   = (clear_em)? ia_zero : ia_regB; 
            afterCall_ia_regC   = (clear_em)? ia_zero : ia_regC; 
            afterCall_ia_regD   = (clear_em)? ia_zero : ia_regD; 
            afterCall_ia_regE   = (clear_em)? ia_zero : ia_regE; 
            afterCall_ia_regF   = (clear_em)? ia_zero : ia_regF; 
            afterCall_ia_regM   = (clear_em)? ia_zero : ia_regM; 
            afterCall_i_co      = (clear_em)? 0       : i_carry;           
            afterCall_ready     = (clear_em)? 1'b0    : 1'b1;
        end
    endtask

    task simRandomType2;
    // Choose a random Type 2 instruction with random field select and pointer,
    // save the "before call" parameters,
    // call the instruction simulation task using the random parameters,
    // save the "after call" parameters,
    // issue the actual instruction in the DUT using the random parameters,
    // compare the results.
        begin : task_sim_random_type2
            i_opcode    = $urandom_range(31,0); // (max,min)
            i_fs        = $urandom_range(7,0);  // (max,min)
            i_ptr       = $urandom_range(13,0); // (max,min)
            case (i_fs)
                0 : s_fs = "P";         // Pointer position.           
                1 : s_fs = "WP";        // Up to pointer position.     
                2 : s_fs = "X";         // Exponent field (with sign). 
                3 : s_fs = "XS";        // Exponent sign.              
                4 : s_fs = "M";         // Mantissa field without sign.
                5 : s_fs = "MS";        // Mantissa field with sign.   
                6 : s_fs = "W";         // Entire word.                
                7 : s_fs = "S";         // Mantissa sign only.         
                default : s_fs = "";    // If no valid field select.   
            endcase

            case (i_opcode)
                0 : begin   // CMP0B:   0 - B           (Compare B to zero)
                        s_mnemonic = "CMP0B"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_regB), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMP0B:  zero - ia_regB[fs] -> (, i_carry)
                    end
                1 : begin   // CLRB:    0 -> B          (Clear B)
                        s_mnemonic = "CLRB"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_zero), .r(ia_regB), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CLRB:  zero[fs] + zero[fs] -> (ia_regB, i_carry)
                    end
                2 : begin   // CMPAC:   A - C           (Compare A and C)
                        s_mnemonic = "CMPAC"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regC), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMPAC:  ia_regA[fs] - ia_regC[fs] -> (, i_carry)
                    end
                3 : begin   // CMPC1:   C - 1           (Compare C to one)
                        s_mnemonic = "CMPC1"; i_sub = 1; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regC), .y(ia_zero), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMPC1:  ia_regC[fs] - zero - i_ci -> (, i_carry)
                    end
                4 : begin   // MOVBC:   B -> C
                        s_mnemonic = "MOVBC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        moveRegs(ia_regB, ia_regC, i_carry, s_fs, i_ptr); // MOVBC:  ia_regB[fs]  -> ia_regC[fs]
                    end
                5 : begin   // TCC:     0 - C -> C      (Tens Complement register C)
                        s_mnemonic = "TCC"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_regC), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // TCC:  zero - ia_regC[fs] -> (ia_regC[fs], i_carry)
                    end
                6 : begin   // CLRC:    0 -> C
                        s_mnemonic = "CLRC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_zero), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CLRC:  zero[fs] + zero[fs] -> (ia_regC, i_carry)
                    end
                7 : begin   // NCC:     0 - C - 1 -> C  (Nines Complement register C)
                        s_mnemonic = "NCC"; i_sub = 1; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_regC), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // NCC:  zero - ia_regC[fs] - i_ci -> (ia_regC[fs], i_carry)
                    end
                8 : begin   // SHLA:    A << 1          (Shift Left register A)
                        s_mnemonic = "SHLA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        shiftLeftReg(ia_regA, i_carry, s_fs, i_ptr); // SHLA:  ia_regA[fs] << 1 -> ia_regA[fs]
                    end
                9 : begin   // MOVAB:   A -> B
                        s_mnemonic = "MOVAB"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        moveRegs(ia_regA, ia_regB, i_carry, s_fs, i_ptr); // MOVAB:  ia_regA[fs]  -> ia_regB[fs]
                    end
                10 : begin  // SUBACC:  A - C -> C 
                        s_mnemonic = "SUBACC"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regC), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // SUBACC:  ia_regA[fs] - ia_regC[fs] -> (ia_regC[fs], i_carry)
                    end
                11 : begin  // DECC:    C - 1 -> C      (Decrement register C)
                        s_mnemonic = "DECC"; i_sub = 1; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regC), .y(ia_zero), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // DECC:  ia_regC[fs] - zero - i_ci -> (ia_regC[fs], i_carry)
                    end
                12 : begin  // MOVCA:   C -> A
                        s_mnemonic = "MOVCA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        moveRegs(ia_regC, ia_regA, i_carry, s_fs, i_ptr); // MOVCA:  ia_regC[fs]  -> ia_regA[fs]
                    end
                13 : begin  // CMP0C:   0 - C           (Compare C to zero)
                        s_mnemonic = "CMP0C"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_regC), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMP0C:  zero - ia_regC[fs] -> (, i_carry)
                    end
                14 : begin  // ADDACC:  A + C -> C
                        s_mnemonic = "ADDACC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regC), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // ADDACC:  ia_regA[fs] + ia_regC[fs] -> (ia_regC[fs], i_carry)
                    end
                15 : begin  // INCC:    C + 1 -> C      (Increment register C)
                        s_mnemonic = "INCC"; i_sub = 0; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regC), .y(ia_zero), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // INCC:  ia_regC[fs] + zero + i_ci -> (ia_regC[fs], i_carry)
                    end
                16 : begin  // CMPAB:   A - B           (Compare A and B)
                        s_mnemonic = "CMPAB"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regB), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMPAB:  ia_regA[fs] - ia_regB[fs] -> (, i_carry)
                    end
                17 : begin  // XCHBC:   B <-> C
                        s_mnemonic = "XCHBC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        exchangeRegs(ia_regB, ia_regC, i_carry, s_fs, i_ptr); // XCHBC:  ia_regB[fs] <-> ia_regC[fs]
                    end
                18 : begin  // SHRC:    C >> 1          (Shift Right register C)
                        s_mnemonic = "SHRC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        shiftRightReg(ia_regC, i_carry, s_fs, i_ptr); // SHRC:  ia_regC[fs] >> 1 -> ia_regC[fs]
                    end
                19 : begin  // CMPA1:   A - 1           (Compare A to one)
                        s_mnemonic = "CMPA1"; i_sub = 1; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_zero), .r(ia_dummy), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CMPA1:  ia_regA[fs] - zero - i_ci -> (, i_carry)
                    end
                20 : begin  // SHRB:    B >> 1          (Shift Right register B)
                        s_mnemonic = "SHRB"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        shiftRightReg(ia_regB, i_carry, s_fs, i_ptr); // SHRB:  ia_regB[fs] >> 1 -> ia_regB[fs]
                    end
                21 : begin  // ADDCCC:  C + C -> C
                        s_mnemonic = "ADDCCC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regC), .y(ia_regC), .r(ia_regC), .co(i_carry), .fs(s_fs), .p(i_ptr)); // ADDCCC:  ia_regC[fs] + ia_regC[fs] -> (ia_regC[fs], i_carry)
                    end
                22 : begin  // SHRA:    A >> 1          (Shift Right register A)
                        s_mnemonic = "SHRA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        shiftRightReg(ia_regA, i_carry, s_fs, i_ptr); // SHRA:  ia_regA[fs] >> 1 -> ia_regA[fs]
                    end
                23 : begin  // CLRA:    0 -> A
                        s_mnemonic = "CLRA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_zero), .y(ia_zero), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // CLRA:  zero[fs] + zero[fs] -> (ia_regA, i_carry)
                    end
                24 : begin  // SUBABA:  A - B -> A
                        s_mnemonic = "SUBABA"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regB), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // SUBABA:  ia_regA[fs] - ia_regB[fs] -> (ia_regA[fs], i_carry)
                    end
                25 : begin  // XCHAB:   A <-> B
                        s_mnemonic = "XCHAB"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        exchangeRegs(ia_regA, ia_regB, i_carry, s_fs, i_ptr); // XCHAB:  ia_regA[fs]  <-> ia_regB[fs]
                    end
                26 : begin  // SUBACA:  A - C -> A
                        s_mnemonic = "SUBACA"; i_sub = 1; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regC), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // SUBACA:  ia_regA[fs] - ia_regC[fs] -> (ia_regA[fs], i_carry)
                    end
                27 : begin  // DECA:    A - 1 -> A      (Decrement register A)
                        s_mnemonic = "DECA"; i_sub = 1; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_zero), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // DECA:  ia_regA[fs] - zero - i_ci -> (ia_regA[fs], i_carry)
                    end
                28 : begin  // ADDABA:  A + B -> A
                        s_mnemonic = "ADDABA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regB), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // ADDABA:  ia_regA[fs] + ia_regB[fs] -> (ia_regA[fs], i_carry)
                    end
                29 : begin  // XCHAC:   A <-> C
                        s_mnemonic = "XCHAC"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        exchangeRegs(ia_regA, ia_regC, i_carry, s_fs, i_ptr); // XCHAC:  ia_regA[fs]  <-> ia_regC[fs]
                    end
                30 : begin  // ADDACA:  A + C -> A
                        s_mnemonic = "ADDACA"; i_sub = 0; i_ci = 0; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_regC), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // ADDACA:  ia_regA[fs] + ia_regC[fs] -> (ia_regA[fs], i_carry)
                    end
                31 : begin  // INCA:    A + 1 -> A      (Increment register A)
                        s_mnemonic = "INCA"; i_sub = 0; i_ci = 1; saveBeforeCall(.clear_em(0));
                        addSubIntRegs(.sub(i_sub), .ci(i_ci), .x(ia_regA), .y(ia_zero), .r(ia_regA), .co(i_carry), .fs(s_fs), .p(i_ptr)); // INCA:  ia_regA[fs] + zero + i_ci -> (ia_regA[fs], i_carry)
                    end
            endcase
            saveAfterCall(.clear_em(0));
            issueInstruction(s_mnemonic, s_fs, i_ptr);
        end
    endtask

    task tallyInstructionCounts;
        begin : task_tally_instr
            case (pipe3_beforeCall_s_mnemonic)
                // Type 2 Instructions
                // -- Class 1) Clear
                "CLRA"      : begin 
                    CLRA_count = CLRA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CLRA_P_count  = CLRA_P_count  + 1;
                        "WP" : CLRA_WP_count = CLRA_WP_count + 1;
                        "X"  : CLRA_X_count  = CLRA_X_count  + 1;
                        "XS" : CLRA_XS_count = CLRA_XS_count + 1;
                        "M"  : CLRA_M_count  = CLRA_M_count  + 1;
                        "MS" : CLRA_MS_count = CLRA_MS_count + 1;
                        "W"  : CLRA_W_count  = CLRA_W_count  + 1;
                        "S"  : CLRA_S_count  = CLRA_S_count  + 1;
                    endcase
                end
                "CLRB"      : begin
                    CLRB_count = CLRB_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CLRB_P_count  = CLRB_P_count  + 1;
                        "WP" : CLRB_WP_count = CLRB_WP_count + 1;
                        "X"  : CLRB_X_count  = CLRB_X_count  + 1;
                        "XS" : CLRB_XS_count = CLRB_XS_count + 1;
                        "M"  : CLRB_M_count  = CLRB_M_count  + 1;
                        "MS" : CLRB_MS_count = CLRB_MS_count + 1;
                        "W"  : CLRB_W_count  = CLRB_W_count  + 1;
                        "S"  : CLRB_S_count  = CLRB_S_count  + 1;
                    endcase
                end
                "CLRC"      : begin
                    CLRC_count = CLRC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CLRC_P_count  = CLRC_P_count  + 1;
                        "WP" : CLRC_WP_count = CLRC_WP_count + 1;
                        "X"  : CLRC_X_count  = CLRC_X_count  + 1;
                        "XS" : CLRC_XS_count = CLRC_XS_count + 1;
                        "M"  : CLRC_M_count  = CLRC_M_count  + 1;
                        "MS" : CLRC_MS_count = CLRC_MS_count + 1;
                        "W"  : CLRC_W_count  = CLRC_W_count  + 1;
                        "S"  : CLRC_S_count  = CLRC_S_count  + 1;
                    endcase
                end
                // -- Class 2) Transfer/Exchange
                "MOVAB"     : begin
                    MOVAB_count = MOVAB_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : MOVAB_P_count  = MOVAB_P_count  + 1;
                        "WP" : MOVAB_WP_count = MOVAB_WP_count + 1;
                        "X"  : MOVAB_X_count  = MOVAB_X_count  + 1;
                        "XS" : MOVAB_XS_count = MOVAB_XS_count + 1;
                        "M"  : MOVAB_M_count  = MOVAB_M_count  + 1;
                        "MS" : MOVAB_MS_count = MOVAB_MS_count + 1;
                        "W"  : MOVAB_W_count  = MOVAB_W_count  + 1;
                        "S"  : MOVAB_S_count  = MOVAB_S_count  + 1;
                    endcase
                end
                "MOVBC"     : begin
                    MOVBC_count = MOVBC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : MOVBC_P_count  = MOVBC_P_count  + 1;
                        "WP" : MOVBC_WP_count = MOVBC_WP_count + 1;
                        "X"  : MOVBC_X_count  = MOVBC_X_count  + 1;
                        "XS" : MOVBC_XS_count = MOVBC_XS_count + 1;
                        "M"  : MOVBC_M_count  = MOVBC_M_count  + 1;
                        "MS" : MOVBC_MS_count = MOVBC_MS_count + 1;
                        "W"  : MOVBC_W_count  = MOVBC_W_count  + 1;
                        "S"  : MOVBC_S_count  = MOVBC_S_count  + 1;
                    endcase
                end
                "MOVCA"     : begin
                    MOVCA_count = MOVCA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : MOVCA_P_count  = MOVCA_P_count  + 1;
                        "WP" : MOVCA_WP_count = MOVCA_WP_count + 1;
                        "X"  : MOVCA_X_count  = MOVCA_X_count  + 1;
                        "XS" : MOVCA_XS_count = MOVCA_XS_count + 1;
                        "M"  : MOVCA_M_count  = MOVCA_M_count  + 1;
                        "MS" : MOVCA_MS_count = MOVCA_MS_count + 1;
                        "W"  : MOVCA_W_count  = MOVCA_W_count  + 1;
                        "S"  : MOVCA_S_count  = MOVCA_S_count  + 1;
                    endcase
                end
                "XCHAB"     : begin
                    XCHAB_count = XCHAB_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : XCHAB_P_count  = XCHAB_P_count  + 1;
                        "WP" : XCHAB_WP_count = XCHAB_WP_count + 1;
                        "X"  : XCHAB_X_count  = XCHAB_X_count  + 1;
                        "XS" : XCHAB_XS_count = XCHAB_XS_count + 1;
                        "M"  : XCHAB_M_count  = XCHAB_M_count  + 1;
                        "MS" : XCHAB_MS_count = XCHAB_MS_count + 1;
                        "W"  : XCHAB_W_count  = XCHAB_W_count  + 1;
                        "S"  : XCHAB_S_count  = XCHAB_S_count  + 1;
                    endcase
                end
                "XCHBC"     : begin
                    XCHBC_count = XCHBC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : XCHBC_P_count  = XCHBC_P_count  + 1;
                        "WP" : XCHBC_WP_count = XCHBC_WP_count + 1;
                        "X"  : XCHBC_X_count  = XCHBC_X_count  + 1;
                        "XS" : XCHBC_XS_count = XCHBC_XS_count + 1;
                        "M"  : XCHBC_M_count  = XCHBC_M_count  + 1;
                        "MS" : XCHBC_MS_count = XCHBC_MS_count + 1;
                        "W"  : XCHBC_W_count  = XCHBC_W_count  + 1;
                        "S"  : XCHBC_S_count  = XCHBC_S_count  + 1;
                    endcase
                end
                "XCHAC"     : begin
                    XCHAC_count = XCHAC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : XCHAC_P_count  = XCHAC_P_count  + 1;
                        "WP" : XCHAC_WP_count = XCHAC_WP_count + 1;
                        "X"  : XCHAC_X_count  = XCHAC_X_count  + 1;
                        "XS" : XCHAC_XS_count = XCHAC_XS_count + 1;
                        "M"  : XCHAC_M_count  = XCHAC_M_count  + 1;
                        "MS" : XCHAC_MS_count = XCHAC_MS_count + 1;
                        "W"  : XCHAC_W_count  = XCHAC_W_count  + 1;
                        "S"  : XCHAC_S_count  = XCHAC_S_count  + 1;
                    endcase
                end
                // -- Class 3) Add/Subtract
                "ADDACC"    : begin
                    ADDACC_count = ADDACC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : ADDACC_P_count  = ADDACC_P_count  + 1;
                        "WP" : ADDACC_WP_count = ADDACC_WP_count + 1;
                        "X"  : ADDACC_X_count  = ADDACC_X_count  + 1;
                        "XS" : ADDACC_XS_count = ADDACC_XS_count + 1;
                        "M"  : ADDACC_M_count  = ADDACC_M_count  + 1;
                        "MS" : ADDACC_MS_count = ADDACC_MS_count + 1;
                        "W"  : ADDACC_W_count  = ADDACC_W_count  + 1;
                        "S"  : ADDACC_S_count  = ADDACC_S_count  + 1;
                    endcase
                end
                "SUBACC"    : begin
                    SUBACC_count = SUBACC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SUBACC_P_count  = SUBACC_P_count  + 1;
                        "WP" : SUBACC_WP_count = SUBACC_WP_count + 1;
                        "X"  : SUBACC_X_count  = SUBACC_X_count  + 1;
                        "XS" : SUBACC_XS_count = SUBACC_XS_count + 1;
                        "M"  : SUBACC_M_count  = SUBACC_M_count  + 1;
                        "MS" : SUBACC_MS_count = SUBACC_MS_count + 1;
                        "W"  : SUBACC_W_count  = SUBACC_W_count  + 1;
                        "S"  : SUBACC_S_count  = SUBACC_S_count  + 1;
                    endcase
                end
                "ADDABA"    : begin
                    ADDABA_count = ADDABA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : ADDABA_P_count  = ADDABA_P_count  + 1;
                        "WP" : ADDABA_WP_count = ADDABA_WP_count + 1;
                        "X"  : ADDABA_X_count  = ADDABA_X_count  + 1;
                        "XS" : ADDABA_XS_count = ADDABA_XS_count + 1;
                        "M"  : ADDABA_M_count  = ADDABA_M_count  + 1;
                        "MS" : ADDABA_MS_count = ADDABA_MS_count + 1;
                        "W"  : ADDABA_W_count  = ADDABA_W_count  + 1;
                        "S"  : ADDABA_S_count  = ADDABA_S_count  + 1;
                    endcase
                end
                "SUBABA"    : begin
                    SUBABA_count = SUBABA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SUBABA_P_count  = SUBABA_P_count  + 1;
                        "WP" : SUBABA_WP_count = SUBABA_WP_count + 1;
                        "X"  : SUBABA_X_count  = SUBABA_X_count  + 1;
                        "XS" : SUBABA_XS_count = SUBABA_XS_count + 1;
                        "M"  : SUBABA_M_count  = SUBABA_M_count  + 1;
                        "MS" : SUBABA_MS_count = SUBABA_MS_count + 1;
                        "W"  : SUBABA_W_count  = SUBABA_W_count  + 1;
                        "S"  : SUBABA_S_count  = SUBABA_S_count  + 1;
                    endcase
                end
                "ADDACA"    : begin
                    ADDACA_count = ADDACA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : ADDACA_P_count  = ADDACA_P_count  + 1;
                        "WP" : ADDACA_WP_count = ADDACA_WP_count + 1;
                        "X"  : ADDACA_X_count  = ADDACA_X_count  + 1;
                        "XS" : ADDACA_XS_count = ADDACA_XS_count + 1;
                        "M"  : ADDACA_M_count  = ADDACA_M_count  + 1;
                        "MS" : ADDACA_MS_count = ADDACA_MS_count + 1;
                        "W"  : ADDACA_W_count  = ADDACA_W_count  + 1;
                        "S"  : ADDACA_S_count  = ADDACA_S_count  + 1;
                    endcase
                end
                "SUBACA"    : begin
                    SUBACA_count = SUBACA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SUBACA_P_count  = SUBACA_P_count  + 1;
                        "WP" : SUBACA_WP_count = SUBACA_WP_count + 1;
                        "X"  : SUBACA_X_count  = SUBACA_X_count  + 1;
                        "XS" : SUBACA_XS_count = SUBACA_XS_count + 1;
                        "M"  : SUBACA_M_count  = SUBACA_M_count  + 1;
                        "MS" : SUBACA_MS_count = SUBACA_MS_count + 1;
                        "W"  : SUBACA_W_count  = SUBACA_W_count  + 1;
                        "S"  : SUBACA_S_count  = SUBACA_S_count  + 1;
                    endcase
                end
                "ADDCCC"    : begin
                    ADDCCC_count = ADDCCC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : ADDCCC_P_count  = ADDCCC_P_count  + 1;
                        "WP" : ADDCCC_WP_count = ADDCCC_WP_count + 1;
                        "X"  : ADDCCC_X_count  = ADDCCC_X_count  + 1;
                        "XS" : ADDCCC_XS_count = ADDCCC_XS_count + 1;
                        "M"  : ADDCCC_M_count  = ADDCCC_M_count  + 1;
                        "MS" : ADDCCC_MS_count = ADDCCC_MS_count + 1;
                        "W"  : ADDCCC_W_count  = ADDCCC_W_count  + 1;
                        "S"  : ADDCCC_S_count  = ADDCCC_S_count  + 1;
                    endcase
                end
                // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                "CMP0B"     : begin
                    CMP0B_count = CMP0B_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMP0B_P_count  = CMP0B_P_count  + 1;
                        "WP" : CMP0B_WP_count = CMP0B_WP_count + 1;
                        "X"  : CMP0B_X_count  = CMP0B_X_count  + 1;
                        "XS" : CMP0B_XS_count = CMP0B_XS_count + 1;
                        "M"  : CMP0B_M_count  = CMP0B_M_count  + 1;
                        "MS" : CMP0B_MS_count = CMP0B_MS_count + 1;
                        "W"  : CMP0B_W_count  = CMP0B_W_count  + 1;
                        "S"  : CMP0B_S_count  = CMP0B_S_count  + 1;
                    endcase
                end
                "CMP0C"     : begin
                    CMP0C_count = CMP0C_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMP0C_P_count  = CMP0C_P_count  + 1;
                        "WP" : CMP0C_WP_count = CMP0C_WP_count + 1;
                        "X"  : CMP0C_X_count  = CMP0C_X_count  + 1;
                        "XS" : CMP0C_XS_count = CMP0C_XS_count + 1;
                        "M"  : CMP0C_M_count  = CMP0C_M_count  + 1;
                        "MS" : CMP0C_MS_count = CMP0C_MS_count + 1;
                        "W"  : CMP0C_W_count  = CMP0C_W_count  + 1;
                        "S"  : CMP0C_S_count  = CMP0C_S_count  + 1;
                    endcase
                end
                "CMPAC"     : begin
                    CMPAC_count = CMPAC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMPAC_P_count  = CMPAC_P_count  + 1;
                        "WP" : CMPAC_WP_count = CMPAC_WP_count + 1;
                        "X"  : CMPAC_X_count  = CMPAC_X_count  + 1;
                        "XS" : CMPAC_XS_count = CMPAC_XS_count + 1;
                        "M"  : CMPAC_M_count  = CMPAC_M_count  + 1;
                        "MS" : CMPAC_MS_count = CMPAC_MS_count + 1;
                        "W"  : CMPAC_W_count  = CMPAC_W_count  + 1;
                        "S"  : CMPAC_S_count  = CMPAC_S_count  + 1;
                    endcase
                end
                "CMPAB"     : begin
                    CMPAB_count = CMPAB_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMPAB_P_count  = CMPAB_P_count  + 1;
                        "WP" : CMPAB_WP_count = CMPAB_WP_count + 1;
                        "X"  : CMPAB_X_count  = CMPAB_X_count  + 1;
                        "XS" : CMPAB_XS_count = CMPAB_XS_count + 1;
                        "M"  : CMPAB_M_count  = CMPAB_M_count  + 1;
                        "MS" : CMPAB_MS_count = CMPAB_MS_count + 1;
                        "W"  : CMPAB_W_count  = CMPAB_W_count  + 1;
                        "S"  : CMPAB_S_count  = CMPAB_S_count  + 1;
                    endcase
                end
                "CMPA1"     : begin
                    CMPA1_count = CMPA1_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMPA1_P_count  = CMPA1_P_count  + 1;
                        "WP" : CMPA1_WP_count = CMPA1_WP_count + 1;
                        "X"  : CMPA1_X_count  = CMPA1_X_count  + 1;
                        "XS" : CMPA1_XS_count = CMPA1_XS_count + 1;
                        "M"  : CMPA1_M_count  = CMPA1_M_count  + 1;
                        "MS" : CMPA1_MS_count = CMPA1_MS_count + 1;
                        "W"  : CMPA1_W_count  = CMPA1_W_count  + 1;
                        "S"  : CMPA1_S_count  = CMPA1_S_count  + 1;
                    endcase
                end
                "CMPC1"     : begin
                    CMPC1_count = CMPC1_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : CMPC1_P_count  = CMPC1_P_count  + 1;
                        "WP" : CMPC1_WP_count = CMPC1_WP_count + 1;
                        "X"  : CMPC1_X_count  = CMPC1_X_count  + 1;
                        "XS" : CMPC1_XS_count = CMPC1_XS_count + 1;
                        "M"  : CMPC1_M_count  = CMPC1_M_count  + 1;
                        "MS" : CMPC1_MS_count = CMPC1_MS_count + 1;
                        "W"  : CMPC1_W_count  = CMPC1_W_count  + 1;
                        "S"  : CMPC1_S_count  = CMPC1_S_count  + 1;
                    endcase
                end
                // -- Class 5) Complement
                "TCC"       : begin
                    TCC_count = TCC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : TCC_P_count  = TCC_P_count  + 1;
                        "WP" : TCC_WP_count = TCC_WP_count + 1;
                        "X"  : TCC_X_count  = TCC_X_count  + 1;
                        "XS" : TCC_XS_count = TCC_XS_count + 1;
                        "M"  : TCC_M_count  = TCC_M_count  + 1;
                        "MS" : TCC_MS_count = TCC_MS_count + 1;
                        "W"  : TCC_W_count  = TCC_W_count  + 1;
                        "S"  : TCC_S_count  = TCC_S_count  + 1;
                    endcase
                end
                "NCC"       : begin
                    NCC_count = NCC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : NCC_P_count  = NCC_P_count  + 1;
                        "WP" : NCC_WP_count = NCC_WP_count + 1;
                        "X"  : NCC_X_count  = NCC_X_count  + 1;
                        "XS" : NCC_XS_count = NCC_XS_count + 1;
                        "M"  : NCC_M_count  = NCC_M_count  + 1;
                        "MS" : NCC_MS_count = NCC_MS_count + 1;
                        "W"  : NCC_W_count  = NCC_W_count  + 1;
                        "S"  : NCC_S_count  = NCC_S_count  + 1;
                    endcase
                end
                // -- Class 6) Increment
                "INCA"      : begin
                    INCA_count = INCA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : INCA_P_count  = INCA_P_count  + 1;
                        "WP" : INCA_WP_count = INCA_WP_count + 1;
                        "X"  : INCA_X_count  = INCA_X_count  + 1;
                        "XS" : INCA_XS_count = INCA_XS_count + 1;
                        "M"  : INCA_M_count  = INCA_M_count  + 1;
                        "MS" : INCA_MS_count = INCA_MS_count + 1;
                        "W"  : INCA_W_count  = INCA_W_count  + 1;
                        "S"  : INCA_S_count  = INCA_S_count  + 1;
                    endcase
                end
                "INCC"      : begin
                    INCC_count = INCC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : INCC_P_count  = INCC_P_count  + 1;
                        "WP" : INCC_WP_count = INCC_WP_count + 1;
                        "X"  : INCC_X_count  = INCC_X_count  + 1;
                        "XS" : INCC_XS_count = INCC_XS_count + 1;
                        "M"  : INCC_M_count  = INCC_M_count  + 1;
                        "MS" : INCC_MS_count = INCC_MS_count + 1;
                        "W"  : INCC_W_count  = INCC_W_count  + 1;
                        "S"  : INCC_S_count  = INCC_S_count  + 1;
                    endcase
                end
                // -- Class 7) Decrement
                "DECA"      : begin
                    DECA_count = DECA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : DECA_P_count  = DECA_P_count  + 1;
                        "WP" : DECA_WP_count = DECA_WP_count + 1;
                        "X"  : DECA_X_count  = DECA_X_count  + 1;
                        "XS" : DECA_XS_count = DECA_XS_count + 1;
                        "M"  : DECA_M_count  = DECA_M_count  + 1;
                        "MS" : DECA_MS_count = DECA_MS_count + 1;
                        "W"  : DECA_W_count  = DECA_W_count  + 1;
                        "S"  : DECA_S_count  = DECA_S_count  + 1;
                    endcase
                end
                "DECC"      : begin
                    DECC_count = DECC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : DECC_P_count  = DECC_P_count  + 1;
                        "WP" : DECC_WP_count = DECC_WP_count + 1;
                        "X"  : DECC_X_count  = DECC_X_count  + 1;
                        "XS" : DECC_XS_count = DECC_XS_count + 1;
                        "M"  : DECC_M_count  = DECC_M_count  + 1;
                        "MS" : DECC_MS_count = DECC_MS_count + 1;
                        "W"  : DECC_W_count  = DECC_W_count  + 1;
                        "S"  : DECC_S_count  = DECC_S_count  + 1;
                    endcase
                end
                // -- Class 8) Shift
                "SHRA"      : begin
                    SHRA_count = SHRA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SHRA_P_count  = SHRA_P_count  + 1;
                        "WP" : SHRA_WP_count = SHRA_WP_count + 1;
                        "X"  : SHRA_X_count  = SHRA_X_count  + 1;
                        "XS" : SHRA_XS_count = SHRA_XS_count + 1;
                        "M"  : SHRA_M_count  = SHRA_M_count  + 1;
                        "MS" : SHRA_MS_count = SHRA_MS_count + 1;
                        "W"  : SHRA_W_count  = SHRA_W_count  + 1;
                        "S"  : SHRA_S_count  = SHRA_S_count  + 1;
                    endcase
                end
                "SHRB"      : begin
                    SHRB_count = SHRB_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SHRB_P_count  = SHRB_P_count  + 1;
                        "WP" : SHRB_WP_count = SHRB_WP_count + 1;
                        "X"  : SHRB_X_count  = SHRB_X_count  + 1;
                        "XS" : SHRB_XS_count = SHRB_XS_count + 1;
                        "M"  : SHRB_M_count  = SHRB_M_count  + 1;
                        "MS" : SHRB_MS_count = SHRB_MS_count + 1;
                        "W"  : SHRB_W_count  = SHRB_W_count  + 1;
                        "S"  : SHRB_S_count  = SHRB_S_count  + 1;
                    endcase
                end
                "SHRC"      : begin
                    SHRC_count = SHRC_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SHRC_P_count  = SHRC_P_count  + 1;
                        "WP" : SHRC_WP_count = SHRC_WP_count + 1;
                        "X"  : SHRC_X_count  = SHRC_X_count  + 1;
                        "XS" : SHRC_XS_count = SHRC_XS_count + 1;
                        "M"  : SHRC_M_count  = SHRC_M_count  + 1;
                        "MS" : SHRC_MS_count = SHRC_MS_count + 1;
                        "W"  : SHRC_W_count  = SHRC_W_count  + 1;
                        "S"  : SHRC_S_count  = SHRC_S_count  + 1;
                    endcase
                end
                "SHLA"      : begin
                    SHLA_count = SHLA_count + 1;
                    case (pipe3_beforeCall_s_fs)
                        "P"  : SHLA_P_count  = SHLA_P_count  + 1;
                        "WP" : SHLA_WP_count = SHLA_WP_count + 1;
                        "X"  : SHLA_X_count  = SHLA_X_count  + 1;
                        "XS" : SHLA_XS_count = SHLA_XS_count + 1;
                        "M"  : SHLA_M_count  = SHLA_M_count  + 1;
                        "MS" : SHLA_MS_count = SHLA_MS_count + 1;
                        "W"  : SHLA_W_count  = SHLA_W_count  + 1;
                        "S"  : SHLA_S_count  = SHLA_S_count  + 1;
                    endcase
                end

            endcase
        end
    endtask

    task clearInstructionCounts;
        begin : task_clear_instr
            CLRA_count = 0;
            CLRA_P_count  = 0;
            CLRA_WP_count = 0;
            CLRA_X_count  = 0;
            CLRA_XS_count = 0;
            CLRA_M_count  = 0;
            CLRA_MS_count = 0;
            CLRA_W_count  = 0;
            CLRA_S_count  = 0;
            CLRB_count = 0;
            CLRB_P_count  = 0;
            CLRB_WP_count = 0;
            CLRB_X_count  = 0;
            CLRB_XS_count = 0;
            CLRB_M_count  = 0;
            CLRB_MS_count = 0;
            CLRB_W_count  = 0;
            CLRB_S_count  = 0;
            CLRC_count = 0;
            CLRC_P_count  = 0;
            CLRC_WP_count = 0;
            CLRC_X_count  = 0;
            CLRC_XS_count = 0;
            CLRC_M_count  = 0;
            CLRC_MS_count = 0;
            CLRC_W_count  = 0;
            CLRC_S_count  = 0;
            MOVAB_count = 0;
            MOVAB_P_count  = 0;
            MOVAB_WP_count = 0;
            MOVAB_X_count  = 0;
            MOVAB_XS_count = 0;
            MOVAB_M_count  = 0;
            MOVAB_MS_count = 0;
            MOVAB_W_count  = 0;
            MOVAB_S_count  = 0;
            MOVBC_count = 0;
            MOVBC_P_count  = 0;
            MOVBC_WP_count = 0;
            MOVBC_X_count  = 0;
            MOVBC_XS_count = 0;
            MOVBC_M_count  = 0;
            MOVBC_MS_count = 0;
            MOVBC_W_count  = 0;
            MOVBC_S_count  = 0;
            MOVCA_count = 0;
            MOVCA_P_count  = 0;
            MOVCA_WP_count = 0;
            MOVCA_X_count  = 0;
            MOVCA_XS_count = 0;
            MOVCA_M_count  = 0;
            MOVCA_MS_count = 0;
            MOVCA_W_count  = 0;
            MOVCA_S_count  = 0;
            XCHAB_count = 0;
            XCHAB_P_count  = 0;
            XCHAB_WP_count = 0;
            XCHAB_X_count  = 0;
            XCHAB_XS_count = 0;
            XCHAB_M_count  = 0;
            XCHAB_MS_count = 0;
            XCHAB_W_count  = 0;
            XCHAB_S_count  = 0;
            XCHBC_count = 0;
            XCHBC_P_count  = 0;
            XCHBC_WP_count = 0;
            XCHBC_X_count  = 0;
            XCHBC_XS_count = 0;
            XCHBC_M_count  = 0;
            XCHBC_MS_count = 0;
            XCHBC_W_count  = 0;
            XCHBC_S_count  = 0;
            XCHAC_count = 0;
            XCHAC_P_count  = 0;
            XCHAC_WP_count = 0;
            XCHAC_X_count  = 0;
            XCHAC_XS_count = 0;
            XCHAC_M_count  = 0;
            XCHAC_MS_count = 0;
            XCHAC_W_count  = 0;
            XCHAC_S_count  = 0;
            ADDACC_count = 0;
            ADDACC_P_count  = 0;
            ADDACC_WP_count = 0;
            ADDACC_X_count  = 0;
            ADDACC_XS_count = 0;
            ADDACC_M_count  = 0;
            ADDACC_MS_count = 0;
            ADDACC_W_count  = 0;
            ADDACC_S_count  = 0;
            SUBACC_count = 0;
            SUBACC_P_count  = 0;
            SUBACC_WP_count = 0;
            SUBACC_X_count  = 0;
            SUBACC_XS_count = 0;
            SUBACC_M_count  = 0;
            SUBACC_MS_count = 0;
            SUBACC_W_count  = 0;
            SUBACC_S_count  = 0;
            ADDABA_count = 0;
            ADDABA_P_count  = 0;
            ADDABA_WP_count = 0;
            ADDABA_X_count  = 0;
            ADDABA_XS_count = 0;
            ADDABA_M_count  = 0;
            ADDABA_MS_count = 0;
            ADDABA_W_count  = 0;
            ADDABA_S_count  = 0;
            SUBABA_count = 0;
            SUBABA_P_count  = 0;
            SUBABA_WP_count = 0;
            SUBABA_X_count  = 0;
            SUBABA_XS_count = 0;
            SUBABA_M_count  = 0;
            SUBABA_MS_count = 0;
            SUBABA_W_count  = 0;
            SUBABA_S_count  = 0;
            ADDACA_count = 0;
            ADDACA_P_count  = 0;
            ADDACA_WP_count = 0;
            ADDACA_X_count  = 0;
            ADDACA_XS_count = 0;
            ADDACA_M_count  = 0;
            ADDACA_MS_count = 0;
            ADDACA_W_count  = 0;
            ADDACA_S_count  = 0;
            SUBACA_count = 0;
            SUBACA_P_count  = 0;
            SUBACA_WP_count = 0;
            SUBACA_X_count  = 0;
            SUBACA_XS_count = 0;
            SUBACA_M_count  = 0;
            SUBACA_MS_count = 0;
            SUBACA_W_count  = 0;
            SUBACA_S_count  = 0;
            ADDCCC_count = 0;
            ADDCCC_P_count  = 0;
            ADDCCC_WP_count = 0;
            ADDCCC_X_count  = 0;
            ADDCCC_XS_count = 0;
            ADDCCC_M_count  = 0;
            ADDCCC_MS_count = 0;
            ADDCCC_W_count  = 0;
            ADDCCC_S_count  = 0;
            CMP0B_count = 0;
            CMP0B_P_count  = 0;
            CMP0B_WP_count = 0;
            CMP0B_X_count  = 0;
            CMP0B_XS_count = 0;
            CMP0B_M_count  = 0;
            CMP0B_MS_count = 0;
            CMP0B_W_count  = 0;
            CMP0B_S_count  = 0;
            CMP0C_count = 0;
            CMP0C_P_count  = 0;
            CMP0C_WP_count = 0;
            CMP0C_X_count  = 0;
            CMP0C_XS_count = 0;
            CMP0C_M_count  = 0;
            CMP0C_MS_count = 0;
            CMP0C_W_count  = 0;
            CMP0C_S_count  = 0;
            CMPAC_count = 0;
            CMPAC_P_count  = 0;
            CMPAC_WP_count = 0;
            CMPAC_X_count  = 0;
            CMPAC_XS_count = 0;
            CMPAC_M_count  = 0;
            CMPAC_MS_count = 0;
            CMPAC_W_count  = 0;
            CMPAC_S_count  = 0;
            CMPAB_count = 0;
            CMPAB_P_count  = 0;
            CMPAB_WP_count = 0;
            CMPAB_X_count  = 0;
            CMPAB_XS_count = 0;
            CMPAB_M_count  = 0;
            CMPAB_MS_count = 0;
            CMPAB_W_count  = 0;
            CMPAB_S_count  = 0;
            CMPA1_count = 0;
            CMPA1_P_count  = 0;
            CMPA1_WP_count = 0;
            CMPA1_X_count  = 0;
            CMPA1_XS_count = 0;
            CMPA1_M_count  = 0;
            CMPA1_MS_count = 0;
            CMPA1_W_count  = 0;
            CMPA1_S_count  = 0;
            CMPC1_count = 0;
            CMPC1_P_count  = 0;
            CMPC1_WP_count = 0;
            CMPC1_X_count  = 0;
            CMPC1_XS_count = 0;
            CMPC1_M_count  = 0;
            CMPC1_MS_count = 0;
            CMPC1_W_count  = 0;
            CMPC1_S_count  = 0;
            TCC_count = 0;
            TCC_P_count  = 0;
            TCC_WP_count = 0;
            TCC_X_count  = 0;
            TCC_XS_count = 0;
            TCC_M_count  = 0;
            TCC_MS_count = 0;
            TCC_W_count  = 0;
            TCC_S_count  = 0;
            NCC_count = 0;
            NCC_P_count  = 0;
            NCC_WP_count = 0;
            NCC_X_count  = 0;
            NCC_XS_count = 0;
            NCC_M_count  = 0;
            NCC_MS_count = 0;
            NCC_W_count  = 0;
            NCC_S_count  = 0;
            INCA_count = 0;
            INCA_P_count  = 0;
            INCA_WP_count = 0;
            INCA_X_count  = 0;
            INCA_XS_count = 0;
            INCA_M_count  = 0;
            INCA_MS_count = 0;
            INCA_W_count  = 0;
            INCA_S_count  = 0;
            INCC_count = 0;
            INCC_P_count  = 0;
            INCC_WP_count = 0;
            INCC_X_count  = 0;
            INCC_XS_count = 0;
            INCC_M_count  = 0;
            INCC_MS_count = 0;
            INCC_W_count  = 0;
            INCC_S_count  = 0;
            DECA_count = 0;
            DECA_P_count  = 0;
            DECA_WP_count = 0;
            DECA_X_count  = 0;
            DECA_XS_count = 0;
            DECA_M_count  = 0;
            DECA_MS_count = 0;
            DECA_W_count  = 0;
            DECA_S_count  = 0;
            DECC_count = 0;
            DECC_P_count  = 0;
            DECC_WP_count = 0;
            DECC_X_count  = 0;
            DECC_XS_count = 0;
            DECC_M_count  = 0;
            DECC_MS_count = 0;
            DECC_W_count  = 0;
            DECC_S_count  = 0;
            SHRA_count = 0;
            SHRA_P_count  = 0;
            SHRA_WP_count = 0;
            SHRA_X_count  = 0;
            SHRA_XS_count = 0;
            SHRA_M_count  = 0;
            SHRA_MS_count = 0;
            SHRA_W_count  = 0;
            SHRA_S_count  = 0;
            SHRB_count = 0;
            SHRB_P_count  = 0;
            SHRB_WP_count = 0;
            SHRB_X_count  = 0;
            SHRB_XS_count = 0;
            SHRB_M_count  = 0;
            SHRB_MS_count = 0;
            SHRB_W_count  = 0;
            SHRB_S_count  = 0;
            SHRC_count = 0;
            SHRC_P_count  = 0;
            SHRC_WP_count = 0;
            SHRC_X_count  = 0;
            SHRC_XS_count = 0;
            SHRC_M_count  = 0;
            SHRC_MS_count = 0;
            SHRC_W_count  = 0;
            SHRC_S_count  = 0;
            SHLA_count = 0;
            SHLA_P_count  = 0;
            SHLA_WP_count = 0;
            SHLA_X_count  = 0;
            SHLA_XS_count = 0;
            SHLA_M_count  = 0;
            SHLA_MS_count = 0;
            SHLA_W_count  = 0;
            SHLA_S_count  = 0;
        end
    endtask

// -------------------------------------------------------------------------------------------------
// Test Processes

    // Generate 200 kHz clock
    always #2.5us PHI2 = ~PHI2;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    // -----------------------------------------------
    // Two-Process System Counter
    // --
    // Combinational next count process...
    always @* begin : proc_nextsyscount
        if (sysCount == 55)
            nextSysCount = 0;
        else
            nextSysCount = sysCount + 1;
    end
    // --
    // Registered count process...
    always@(posedge PHI2 or posedge rst) begin : proc_syscount
        if (rst) begin
            sysCount <= 0;
            digitCount <= 0;
        end
        else begin
            sysCount <= nextSysCount;
            digitCount <= nextSysCount / 4;
        end
    end
    // --
    // End Two-Process System Counter
    // -----------------------------------------------

    // Generate SYNC and WS pulses
    always@(posedge PHI2 or posedge rst) begin : proc_sync
        if (rst) 
            SYNC <= 1'b0;
        else if (nextSysCount == 45)
            SYNC <= 1'b1;
        else if (nextSysCount == 55)
            SYNC <= 1'b0;
        // --
        if (rst) 
            {t1, t2, t3, t4} <= 4'b0000;
        else if (nextSysCount == 0)
            {t1, t2, t3, t4} <= 4'b1000;
        else
            {t1, t2, t3, t4} <= {t4, t1, t2, t3};
        // --
        if (rst) 
            is_shifter <= {1'b0, {10{1'b0}}, {33{1'b0}}, 1'b1, {11{1'b0}}};
        else if (nextSysCount == 0)                                                 // If we're about to start the next word cycle, and ...
            if (load_instruction)                                                   //    if a new instruction is issued, then ...
                is_shifter <= {1'b0, instruction, {33{1'b0}}, 1'b1, {11{1'b0}}};    //       load the new instruction in bits [54:45] and set bit [11] for the display,
            else                                                                    //    otherwise no new instruction was issued, so ...
                is_shifter <= {1'b0, `NOP, {33{1'b0}}, 1'b1, {11{1'b0}}};           //       load a NOP and set bit [11] for the display.
        else                                                                        // Otherwise we're not about to start the next word cycle, so ...
            is_shifter <= {1'b0, is_shifter[55:1]};                                 //    shift the IS shifter to the right.
        // --
        if (rst) begin
            fetching_BCD    <= 1'b0;
            BCD_load_value  <= 'b0;
            BCD_shifter     <= 'b0;
            BCD_out_en      <= 1'b0;
        end
        else if (nextSysCount == 0) begin                           // If we're about to start the next word cycle, then ...
            fetching_BCD    <= request_BCD;                         //    Hold the request for the duration of the next word cycle.
            BCD_load_value  <= BCD_request_value;                   //    Hold the value to be loaded for the duration of the next word cycle.
            if (fetching_BCD) begin                                 //    if a new BCD value should be loaded, then ...
                BCD_shifter <= BCD_load_value;                      //       load the new BCD value
                BCD_out_en  <= 1'b1;                                //       and set the output enable;
            end                                                     //
            else begin                                              //    otherwise there's no new BCD value to load, so ...
                BCD_shifter <= {BCD_shifter[1], BCD_shifter[56:2]}; //       start recirculating the BCD shifter
                BCD_out_en  <= 1'b0;                                //       and reset the output enable.
            end                                                     //
        end                                                         //
        else                                                        // Otherwise we're not about to start the next word cycle, so ...
            BCD_shifter <= {BCD_shifter[1], BCD_shifter[56:2]};     //    continue recirculating the BCD shifter.
        // --
        if (rst) begin
            wsBeginFetch    <= -1;
            wsEndFetch     <= -1;
            wsBeginExecute  <= -1;
            wsEndExecute   <= -1;
        end
        else begin
            if (load_instruction) begin         // If a new instruction is issued, then ...
                wsBeginFetch <= wsBegin;        //    hold the digit number where WS begins and
                wsEndFetch   <= wsEnd;          //    hold the digit number where WS ends.
            end                                 //
            if (nextSysCount == 0) begin        // Otherwise, if we're about to start the next word cycle, then ...
                wsBeginExecute <= wsBeginFetch; //    load the digit number where WS begins and
                wsEndExecute   <= wsEndFetch;   //    load the digit number where WS ends.
            end
        end
        //--
        if (rst)
            carry_ff_66 <= 1'b0;
        else if (t4 && WS)
            // '569:  "Any carry signal out of the adder in arithmetic and register
            // circuit 20, with word select, also high, will set carry flip-flop 66."
            carry_ff_66 <= CARRY;
        //--
    end

    assign BCD = (BCD_out_en)? BCD_shifter[1] : 1'bz;
    assign BCD_in = BCD;
    assign IS = is_shifter[0];
    assign WS = ((digitCount >= wsBeginExecute) && (digitCount <= wsEndExecute))? 1'b1 : 1'b0;

    // -------------------------------------------------------------------------
    // Test results pipeline
    always@(posedge PHI2 or posedge rst) begin : proc_tpipe
        if (rst) begin
            fetch_instruction   <= 1'b0;
            exec_instruction    <= 1'b0;
            grab_em             <= 1'b0;
            compare_em          <= 1'b0;
            save_em             <= 1'b0;

            pipe1_beforeCall_s_mnemonic <= "";
            pipe1_beforeCall_i_sub      <= 0;
            pipe1_beforeCall_i_ci       <= 0;
            pipe1_beforeCall_ia_regA    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regB    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regC    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regD    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regE    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regF    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_ia_regM    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_beforeCall_s_fs       <= "";
            pipe1_beforeCall_i_ptr      <= 0;
            pipe1_afterCall_ia_regA     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regB     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regC     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regD     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regE     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regF     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_ia_regM     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe1_afterCall_i_co        <= 0;
            pipe1_afterCall_ready       <= 1'b0;

            pipe2_beforeCall_s_mnemonic <= "";
            pipe2_beforeCall_i_sub      <= 0;
            pipe2_beforeCall_i_ci       <= 0;
            pipe2_beforeCall_ia_regA    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regB    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regC    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regD    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regE    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regF    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_ia_regM    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_beforeCall_s_fs       <= "";
            pipe2_beforeCall_i_ptr      <= 0;
            pipe2_afterCall_ia_regA     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regB     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regC     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regD     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regE     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regF     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_ia_regM     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe2_afterCall_i_co        <= 0;
            pipe2_afterCall_ready       <= 1'b0;

            pipe3_beforeCall_s_mnemonic <= "";
            pipe3_beforeCall_i_sub      <= 0;
            pipe3_beforeCall_i_ci       <= 0;
            pipe3_beforeCall_ia_regA    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regB    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regC    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regD    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regE    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regF    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_ia_regM    <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_beforeCall_s_fs       <= "";
            pipe3_beforeCall_i_ptr      <= 0;
            pipe3_afterCall_ia_regA     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regB     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regC     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regD     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regE     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regF     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_ia_regM     <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            pipe3_afterCall_i_co        <= 0;
            pipe3_afterCall_ready       <= 1'b0;

            modeled_beforeCall_s_mnemonic   <= "";
            modeled_beforeCall_i_sub        <= 0;
            modeled_beforeCall_i_ci         <= 0;
            modeled_beforeCall_ia_regA      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regB      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regC      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regD      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regE      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regF      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_ia_regM      <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_beforeCall_s_fs         <= "";
            modeled_beforeCall_i_ptr        <= 0;
            modeled_afterCall_ia_regA       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regB       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regC       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regD       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regE       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regF       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_ia_regM       <= {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            modeled_afterCall_i_co          <= 0;
            modeled_afterCall_ready         <= 1'b0;

            expected_regA   <= 'b0;
            expected_regB   <= 'b0;
            expected_regC   <= 'b0;
            expected_regD   <= 'b0;
            expected_regE   <= 'b0;
            expected_regF   <= 'b0;
            expected_regM   <= 'b0;
            expected_carry  <= 1'b0;

            actual_regA     <= 'b0;
            actual_regB     <= 'b0;
            actual_regC     <= 'b0;
            actual_regD     <= 'b0;
            actual_regE     <= 'b0;
            actual_regF     <= 'b0;
            actual_regM     <= 'b0;
            actual_carry    <= 1'b0;

            good_count      <= 0;
            bad_count       <= 0;

            clearInstructionCounts();
        end
        else begin
            if (nextSysCount == 0) begin
                fetch_instruction <= load_instruction;
                exec_instruction  <= fetch_instruction;
                grab_em  <= exec_instruction;
            end
            else begin
                // 'grab_em' pulses simultaneously with START.
                grab_em  <= 1'b0;  // End the pulse.  
            end

            compare_em <= grab_em;
            save_em <= compare_em;

//            if (nextSysCount == 0 && load_instruction) begin
            if (nextSysCount == 0) begin
                pipe1_beforeCall_s_mnemonic <= beforeCall_s_mnemonic;
                pipe1_beforeCall_i_sub      <= beforeCall_i_sub;         
                pipe1_beforeCall_i_ci       <= beforeCall_i_ci;          
                pipe1_beforeCall_ia_regA    <= beforeCall_ia_regA;
                pipe1_beforeCall_ia_regB    <= beforeCall_ia_regB;
                pipe1_beforeCall_ia_regC    <= beforeCall_ia_regC;
                pipe1_beforeCall_ia_regD    <= beforeCall_ia_regD;
                pipe1_beforeCall_ia_regE    <= beforeCall_ia_regE;
                pipe1_beforeCall_ia_regF    <= beforeCall_ia_regF;
                pipe1_beforeCall_ia_regM    <= beforeCall_ia_regM;
                pipe1_beforeCall_s_fs       <= beforeCall_s_fs;   
                pipe1_beforeCall_i_ptr      <= beforeCall_i_ptr;     
                pipe1_afterCall_ia_regA     <= afterCall_ia_regA; 
                pipe1_afterCall_ia_regB     <= afterCall_ia_regB; 
                pipe1_afterCall_ia_regC     <= afterCall_ia_regC; 
                pipe1_afterCall_ia_regD     <= afterCall_ia_regD;
                pipe1_afterCall_ia_regE     <= afterCall_ia_regE;
                pipe1_afterCall_ia_regF     <= afterCall_ia_regF;
                pipe1_afterCall_ia_regM     <= afterCall_ia_regM;
                pipe1_afterCall_i_co        <= afterCall_i_co;           
                pipe1_afterCall_ready       <= afterCall_ready;
//            end
//            if (nextSysCount == 0 && fetch_instruction) begin
                pipe2_beforeCall_s_mnemonic <= pipe1_beforeCall_s_mnemonic;
                pipe2_beforeCall_i_sub      <= pipe1_beforeCall_i_sub;         
                pipe2_beforeCall_i_ci       <= pipe1_beforeCall_i_ci;          
                pipe2_beforeCall_ia_regA    <= pipe1_beforeCall_ia_regA;
                pipe2_beforeCall_ia_regB    <= pipe1_beforeCall_ia_regB;
                pipe2_beforeCall_ia_regC    <= pipe1_beforeCall_ia_regC;
                pipe2_beforeCall_ia_regD    <= pipe1_beforeCall_ia_regD;
                pipe2_beforeCall_ia_regE    <= pipe1_beforeCall_ia_regE;
                pipe2_beforeCall_ia_regF    <= pipe1_beforeCall_ia_regF;
                pipe2_beforeCall_ia_regM    <= pipe1_beforeCall_ia_regM;
                pipe2_beforeCall_s_fs       <= pipe1_beforeCall_s_fs;   
                pipe2_beforeCall_i_ptr      <= pipe1_beforeCall_i_ptr;     
                pipe2_afterCall_ia_regA     <= pipe1_afterCall_ia_regA;  
                pipe2_afterCall_ia_regB     <= pipe1_afterCall_ia_regB;  
                pipe2_afterCall_ia_regC     <= pipe1_afterCall_ia_regC;  
                pipe2_afterCall_ia_regD     <= pipe1_afterCall_ia_regD;
                pipe2_afterCall_ia_regE     <= pipe1_afterCall_ia_regE;
                pipe2_afterCall_ia_regF     <= pipe1_afterCall_ia_regF;
                pipe2_afterCall_ia_regM     <= pipe1_afterCall_ia_regM;
                pipe2_afterCall_i_co        <= pipe1_afterCall_i_co;           
                pipe2_afterCall_ready       <= pipe1_afterCall_ready;
//            end
//            if (nextSysCount == 0 && exec_instruction) begin
                pipe3_beforeCall_s_mnemonic <= pipe2_beforeCall_s_mnemonic;
                pipe3_beforeCall_i_sub      <= pipe2_beforeCall_i_sub;         
                pipe3_beforeCall_i_ci       <= pipe2_beforeCall_i_ci;          
                pipe3_beforeCall_ia_regA    <= pipe2_beforeCall_ia_regA;
                pipe3_beforeCall_ia_regB    <= pipe2_beforeCall_ia_regB;
                pipe3_beforeCall_ia_regC    <= pipe2_beforeCall_ia_regC;
                pipe3_beforeCall_ia_regD    <= pipe2_beforeCall_ia_regD;
                pipe3_beforeCall_ia_regE    <= pipe2_beforeCall_ia_regE;
                pipe3_beforeCall_ia_regF    <= pipe2_beforeCall_ia_regF;
                pipe3_beforeCall_ia_regM    <= pipe2_beforeCall_ia_regM;
                pipe3_beforeCall_s_fs       <= pipe2_beforeCall_s_fs;   
                pipe3_beforeCall_i_ptr      <= pipe2_beforeCall_i_ptr;     
                pipe3_afterCall_ia_regA     <= pipe2_afterCall_ia_regA;  
                pipe3_afterCall_ia_regB     <= pipe2_afterCall_ia_regB;  
                pipe3_afterCall_ia_regC     <= pipe2_afterCall_ia_regC;  
                pipe3_afterCall_ia_regD     <= pipe2_afterCall_ia_regD;
                pipe3_afterCall_ia_regE     <= pipe2_afterCall_ia_regE;
                pipe3_afterCall_ia_regF     <= pipe2_afterCall_ia_regF;
                pipe3_afterCall_ia_regM     <= pipe2_afterCall_ia_regM;
                pipe3_afterCall_i_co        <= pipe2_afterCall_i_co;           
                pipe3_afterCall_ready       <= pipe2_afterCall_ready;
            end
            if (grab_em) begin
                modeled_beforeCall_s_mnemonic <= pipe3_beforeCall_s_mnemonic;
                modeled_beforeCall_i_sub      <= pipe3_beforeCall_i_sub;         
                modeled_beforeCall_i_ci       <= pipe3_beforeCall_i_ci;          
                modeled_beforeCall_ia_regA    <= pipe3_beforeCall_ia_regA;
                modeled_beforeCall_ia_regB    <= pipe3_beforeCall_ia_regB;
                modeled_beforeCall_ia_regC    <= pipe3_beforeCall_ia_regC;
                modeled_beforeCall_ia_regD    <= pipe3_beforeCall_ia_regD;
                modeled_beforeCall_ia_regE    <= pipe3_beforeCall_ia_regE;
                modeled_beforeCall_ia_regF    <= pipe3_beforeCall_ia_regF;
                modeled_beforeCall_ia_regM    <= pipe3_beforeCall_ia_regM;
                modeled_beforeCall_s_fs       <= pipe3_beforeCall_s_fs;   
                modeled_beforeCall_i_ptr      <= pipe3_beforeCall_i_ptr;     
                modeled_afterCall_ia_regA     <= pipe3_afterCall_ia_regA;  
                modeled_afterCall_ia_regB     <= pipe3_afterCall_ia_regB;  
                modeled_afterCall_ia_regC     <= pipe3_afterCall_ia_regC;  
                modeled_afterCall_ia_regD     <= pipe3_afterCall_ia_regD;
                modeled_afterCall_ia_regE     <= pipe3_afterCall_ia_regE;
                modeled_afterCall_ia_regF     <= pipe3_afterCall_ia_regF;
                modeled_afterCall_ia_regM     <= pipe3_afterCall_ia_regM;
                modeled_afterCall_i_co        <= pipe3_afterCall_i_co;           
                modeled_afterCall_ready       <= pipe3_afterCall_ready;

                // Capture expected results:
                expected_regA   <= intArray2Reg(pipe3_afterCall_ia_regA);
                expected_regB   <= intArray2Reg(pipe3_afterCall_ia_regB);
                expected_regC   <= intArray2Reg(pipe3_afterCall_ia_regC);
                expected_regD   <= intArray2Reg(pipe3_afterCall_ia_regD);
                expected_regE   <= intArray2Reg(pipe3_afterCall_ia_regE);
                expected_regF   <= intArray2Reg(pipe3_afterCall_ia_regF);
                expected_regM   <= intArray2Reg(pipe3_afterCall_ia_regM);
                expected_carry  <= (pipe3_afterCall_i_co == 1)? 1'b1 : 1'b0;

                // Capture actual results:
                actual_regA     <= inst_a_and_r.regA[56:1];
                actual_regB     <= inst_a_and_r.regB;
                actual_regC     <= inst_a_and_r.regC[56:1];
                actual_regD     <= inst_a_and_r.regD;
                actual_regE     <= inst_a_and_r.regE;
                actual_regF     <= inst_a_and_r.regF;
                actual_regM     <= inst_a_and_r.regM;
                actual_carry    <= carry_ff_66;
            end
//            if (compare_em && modeled_afterCall_ready) begin
            if (compare_em && pipe3_afterCall_ready) begin
                if (all_regs_match) 
                    good_count <= good_count + 1;
                else
                    bad_count <= bad_count + 1;
                //--
                tallyInstructionCounts();
            end
            // At the completion of each instruction, write the output file with the results of each instruction.
            if (sim_start_pulse)
                $fdisplay(fd, "A Before, B Before, C Before, Instr, Field, Pointer, A Result, B Result, C Result, CARRY Result, NumGood, NumBad");
//            else if (save_em && modeled_afterCall_ready) begin
            else if (save_em && pipe3_afterCall_ready) begin
                $fdisplay(fd, "%h, %h, %h, %s, %s, %d, %h, %h, %h, %d, %d", 
                intArray2Reg(modeled_beforeCall_ia_regA), 
                intArray2Reg(modeled_beforeCall_ia_regB), 
                intArray2Reg(modeled_beforeCall_ia_regC), 
                modeled_beforeCall_s_mnemonic, 
                modeled_beforeCall_s_fs, 
                modeled_beforeCall_i_ptr, 
                actual_regA, 
                actual_regB, 
                actual_regC,
                actual_carry,
                good_count, 
                bad_count);
            end

            // At the end of simulation, write the instruction mix histogram.
            if (sim_end_pulse) begin
                $fdisplay(fd, "");
                $fdisplay(fd, "Histogram of Type 2 Instructions:");
                $fdisplay(fd, "");
                $fdisplay(fd, "Instruction:   Total,         [P],        [WP],         [X],        [XS],         [M],        [MS],         [W],         [S]");
                $fdisplay(fd, "CLRA:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CLRA_count, CLRA_P_count, CLRA_WP_count, CLRA_X_count, CLRA_XS_count, CLRA_M_count, CLRA_MS_count, CLRA_W_count, CLRA_S_count);

                $fdisplay(fd, "CLRB:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CLRB_count, CLRB_P_count, CLRB_WP_count, CLRB_X_count, CLRB_XS_count, CLRB_M_count, CLRB_MS_count, CLRB_W_count, CLRB_S_count);

                $fdisplay(fd, "CLRC:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CLRC_count, CLRC_P_count, CLRC_WP_count, CLRC_X_count, CLRC_XS_count, CLRC_M_count, CLRC_MS_count, CLRC_W_count, CLRC_S_count);

                $fdisplay(fd, "MOVAB:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                MOVAB_count, MOVAB_P_count, MOVAB_WP_count, MOVAB_X_count, MOVAB_XS_count, MOVAB_M_count, MOVAB_MS_count, MOVAB_W_count, MOVAB_S_count);

                $fdisplay(fd, "MOVBC:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                MOVBC_count, MOVBC_P_count, MOVBC_WP_count, MOVBC_X_count, MOVBC_XS_count, MOVBC_M_count, MOVBC_MS_count, MOVBC_W_count, MOVBC_S_count);

                $fdisplay(fd, "MOVCA:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                MOVCA_count, MOVCA_P_count, MOVCA_WP_count, MOVCA_X_count, MOVCA_XS_count, MOVCA_M_count, MOVCA_MS_count, MOVCA_W_count, MOVCA_S_count);

                $fdisplay(fd, "XCHAB:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                XCHAB_count, XCHAB_P_count, XCHAB_WP_count, XCHAB_X_count, XCHAB_XS_count, XCHAB_M_count, XCHAB_MS_count, XCHAB_W_count, XCHAB_S_count);

                $fdisplay(fd, "XCHBC:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                XCHBC_count, XCHBC_P_count, XCHBC_WP_count, XCHBC_X_count, XCHBC_XS_count, XCHBC_M_count, XCHBC_MS_count, XCHBC_W_count, XCHBC_S_count);

                $fdisplay(fd, "XCHAC:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                XCHAC_count, XCHAC_P_count, XCHAC_WP_count, XCHAC_X_count, XCHAC_XS_count, XCHAC_M_count, XCHAC_MS_count, XCHAC_W_count, XCHAC_S_count);

                $fdisplay(fd, "ADDACC:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                ADDACC_count, ADDACC_P_count, ADDACC_WP_count, ADDACC_X_count, ADDACC_XS_count, ADDACC_M_count, ADDACC_MS_count, ADDACC_W_count, ADDACC_S_count);

                $fdisplay(fd, "SUBACC:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SUBACC_count, SUBACC_P_count, SUBACC_WP_count, SUBACC_X_count, SUBACC_XS_count, SUBACC_M_count, SUBACC_MS_count, SUBACC_W_count, SUBACC_S_count);

                $fdisplay(fd, "ADDABA:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                ADDABA_count, ADDABA_P_count, ADDABA_WP_count, ADDABA_X_count, ADDABA_XS_count, ADDABA_M_count, ADDABA_MS_count, ADDABA_W_count, ADDABA_S_count);

                $fdisplay(fd, "SUBABA:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SUBABA_count, SUBABA_P_count, SUBABA_WP_count, SUBABA_X_count, SUBABA_XS_count, SUBABA_M_count, SUBABA_MS_count, SUBABA_W_count, SUBABA_S_count);

                $fdisplay(fd, "ADDACA:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                ADDACA_count, ADDACA_P_count, ADDACA_WP_count, ADDACA_X_count, ADDACA_XS_count, ADDACA_M_count, ADDACA_MS_count, ADDACA_W_count, ADDACA_S_count);

                $fdisplay(fd, "SUBACA:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SUBACA_count, SUBACA_P_count, SUBACA_WP_count, SUBACA_X_count, SUBACA_XS_count, SUBACA_M_count, SUBACA_MS_count, SUBACA_W_count, SUBACA_S_count);

                $fdisplay(fd, "ADDCCC:  %d, %d, %d, %d, %d, %d, %d, %d, %d",
                ADDCCC_count, ADDCCC_P_count, ADDCCC_WP_count, ADDCCC_X_count, ADDCCC_XS_count, ADDCCC_M_count, ADDCCC_MS_count, ADDCCC_W_count, ADDCCC_S_count);

                $fdisplay(fd, "CMP0B:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMP0B_count, CMP0B_P_count, CMP0B_WP_count, CMP0B_X_count, CMP0B_XS_count, CMP0B_M_count, CMP0B_MS_count, CMP0B_W_count, CMP0B_S_count);

                $fdisplay(fd, "CMP0C:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMP0C_count, CMP0C_P_count, CMP0C_WP_count, CMP0C_X_count, CMP0C_XS_count, CMP0C_M_count, CMP0C_MS_count, CMP0C_W_count, CMP0C_S_count);

                $fdisplay(fd, "CMPAC:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMPAC_count, CMPAC_P_count, CMPAC_WP_count, CMPAC_X_count, CMPAC_XS_count, CMPAC_M_count, CMPAC_MS_count, CMPAC_W_count, CMPAC_S_count);

                $fdisplay(fd, "CMPAB:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMPAB_count, CMPAB_P_count, CMPAB_WP_count, CMPAB_X_count, CMPAB_XS_count, CMPAB_M_count, CMPAB_MS_count, CMPAB_W_count, CMPAB_S_count);

                $fdisplay(fd, "CMPA1:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMPA1_count, CMPA1_P_count, CMPA1_WP_count, CMPA1_X_count, CMPA1_XS_count, CMPA1_M_count, CMPA1_MS_count, CMPA1_W_count, CMPA1_S_count);

                $fdisplay(fd, "CMPC1:   %d, %d, %d, %d, %d, %d, %d, %d, %d",
                CMPC1_count, CMPC1_P_count, CMPC1_WP_count, CMPC1_X_count, CMPC1_XS_count, CMPC1_M_count, CMPC1_MS_count, CMPC1_W_count, CMPC1_S_count);

                $fdisplay(fd, "TCC:     %d, %d, %d, %d, %d, %d, %d, %d, %d",
                TCC_count, TCC_P_count, TCC_WP_count, TCC_X_count, TCC_XS_count, TCC_M_count, TCC_MS_count, TCC_W_count, TCC_S_count);

                $fdisplay(fd, "NCC:     %d, %d, %d, %d, %d, %d, %d, %d, %d",
                NCC_count, NCC_P_count, NCC_WP_count, NCC_X_count, NCC_XS_count, NCC_M_count, NCC_MS_count, NCC_W_count, NCC_S_count);

                $fdisplay(fd, "INCA:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                INCA_count, INCA_P_count, INCA_WP_count, INCA_X_count, INCA_XS_count, INCA_M_count, INCA_MS_count, INCA_W_count, INCA_S_count);

                $fdisplay(fd, "INCC:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                INCC_count, INCC_P_count, INCC_WP_count, INCC_X_count, INCC_XS_count, INCC_M_count, INCC_MS_count, INCC_W_count, INCC_S_count);

                $fdisplay(fd, "DECA:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                DECA_count, DECA_P_count, DECA_WP_count, DECA_X_count, DECA_XS_count, DECA_M_count, DECA_MS_count, DECA_W_count, DECA_S_count);

                $fdisplay(fd, "DECC:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                DECC_count, DECC_P_count, DECC_WP_count, DECC_X_count, DECC_XS_count, DECC_M_count, DECC_MS_count, DECC_W_count, DECC_S_count);

                $fdisplay(fd, "SHRA:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SHRA_count, SHRA_P_count, SHRA_WP_count, SHRA_X_count, SHRA_XS_count, SHRA_M_count, SHRA_MS_count, SHRA_W_count, SHRA_S_count);

                $fdisplay(fd, "SHRB:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SHRB_count, SHRB_P_count, SHRB_WP_count, SHRB_X_count, SHRB_XS_count, SHRB_M_count, SHRB_MS_count, SHRB_W_count, SHRB_S_count);

                $fdisplay(fd, "SHRC:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SHRC_count, SHRC_P_count, SHRC_WP_count, SHRC_X_count, SHRC_XS_count, SHRC_M_count, SHRC_MS_count, SHRC_W_count, SHRC_S_count);

                $fdisplay(fd, "SHLA:    %d, %d, %d, %d, %d, %d, %d, %d, %d",
                SHLA_count, SHLA_P_count, SHLA_WP_count, SHLA_X_count, SHLA_XS_count, SHLA_M_count, SHLA_MS_count, SHLA_W_count, SHLA_S_count);
            end
        end
    end
    // -------------------------------------------------------------------------

    assign regA_match   = (expected_regA == actual_regA)? 1'b1 : 1'b0;
    assign regB_match   = (expected_regB == actual_regB)? 1'b1 : 1'b0;
    assign regC_match   = (expected_regC == actual_regC)? 1'b1 : 1'b0;
    assign regD_match   = (expected_regD == actual_regD)? 1'b1 : 1'b0;
    assign regE_match   = (expected_regE == actual_regE)? 1'b1 : 1'b0;
    assign regF_match   = (expected_regF == actual_regF)? 1'b1 : 1'b0;
    assign regM_match   = (expected_regM == actual_regM)? 1'b1 : 1'b0;
    assign carry_match  = (expected_carry == actual_carry)? 1'b1 : 1'b0;
    assign all_regs_match = (regA_match & regB_match & regC_match & regD_match & regE_match & regF_match & regM_match & carry_match);

    // -------------------------------------------------------------------------
    // Debug:  Capture and hold the register contents at the start of every word cycle.
    always@(posedge PHI2 or posedge rst) begin : proc_hold_regs
        if (rst) begin
            holdRegA    <= 'b0;
            holdRegB    <= 'b0;
            holdRegC    <= 'b0;
            holdRegD    <= 'b0;
            holdRegE    <= 'b0;
            holdRegF    <= 'b0;
            holdRegM    <= 'b0;
        end
        else if (START) begin
            holdRegA    <= inst_a_and_r.regA[56:1];
            holdRegB    <= inst_a_and_r.regB;
            holdRegC    <= inst_a_and_r.regC[56:1];
            holdRegD    <= inst_a_and_r.regD;
            holdRegE    <= inst_a_and_r.regE;
            holdRegF    <= inst_a_and_r.regF;
            holdRegM    <= inst_a_and_r.regM;
        end
    end
    // End debug
    // -------------------------------------------------------------------------

    initial begin
        TestIterations = 1000000;    // **** Specify the number of random instructions to simulate (use a multiple of 10) ****
                                    // With waveform output, 100,000 iterations takes 3 minutes to complete.
                                    // Without waveform output, 100,000 iterations takes 22 seconds to complete.
                                    // Without waveform output, 1,000,000 iterations takes 3 minutes 37 seconds to complete.
                                    // Without waveform output, 2,000,000 iterations takes 7 minutes 36 seconds to complete.
        PHI2 = 1'b0;
        request_BCD = 1'b0;
        fd = $fopen("arith_reg_output.csv", "w");
        sim_end_pulse = 1'b0;
        loop_done_1 = 1'b0;
        loop_done_2 = 1'b0;
        loop_done_3 = 1'b0;
        wait (rst == 1'b1);
        wait (rst == 1'b0);
        @(posedge PHI2);
        sim_start_pulse = 1'b1;
        @(posedge PHI2);
        sim_start_pulse = 1'b0;
        @(posedge PHI2);
        afterCall_ready = 1'b0;
        saveBeforeCall(.clear_em(1));
        saveAfterCall(.clear_em(1));

        issueInstruction("DSPOFF", "", -1);
        issueInstruction("DSPTOG", "", -1);
        loadAllRegsRandomUsingBCD(ia_regA, ia_regB, ia_regC, ia_regD, ia_regE, ia_regF, ia_regM);

        wait (nextSysCount == 0); 
        @(posedge PHI2);
        wait (nextSysCount == 0); 
        @(posedge PHI2);

        for (i = 1; i <= TestIterations/10; i = i+1) begin
            for (j = 1; j <= 10; j = j+1) begin
                simRandomType2();
                wait (nextSysCount == 0); 
                afterCall_ready = 1'b0;
//                wait (nextSysCount == 1); // Force a gap between instructions (optional).
            end
            loadRegsABCRandomUsingBCD(ia_regA, ia_regB, ia_regC);   // Re-randomize A, B, and C.
        end
        loop_done_1 = 1'b1;
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_2 = 1'b1;
        wait (nextSysCount != 0); // Advance into the machine cycle.
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_3 = 1'b1;


        repeat (56) @(posedge PHI2);
        repeat (56) @(posedge PHI2);
        repeat (10) @(posedge PHI2);

        sim_end_pulse = 1'b1;
        @(posedge PHI2);
        sim_end_pulse = 1'b0;
        @(posedge PHI2);

        $fclose(fd);
        $stop;
    end

// =================================================================================================
// Instances
// =================================================================================================

     arithmetic_and_register_20 #(
        .NewArch    (NewArch)   // parameter        // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_a_and_r (
        // Output Ports
        .A      (A),            // output reg       // ('569 item 38) Partially decoded LED segment sequence, bit A.
        .B      (B),            // output reg       //                    "        "     "    "        "    , bit B.
        .C      (C),            // output reg       //                    "        "     "    "        "    , bit C.
        .D      (D),            // output reg       //                    "        "     "    "        "    , bit D.
        .E      (E),            // output reg       //                    "        "     "    "        "    , bit E.
        .START  (START),        // output reg       // ('569 item 40) Word synchronization pulse for digit scanner in Cathode Driver.
        .CARRY  (CARRY),        // output reg       // ('569 item 34) Status of the carry output of this block's adder, sent to the Control & Timing block.
        // Input Ports
        .PHI1   (PHI1),         // input            // Bit-Rate Clock Input, Phase 1
        .PHI2   (PHI2),         // input            // Bit-Rate Clock Input, Phase 2
        .IS     (IS),           // input            // ('569 item 28) Serial Instruction from ROM Output
        .WS     (WS),           // input            // ('569 item 30) ROM Word Select
        .SYNC   (SYNC),         // input            // ('569 item 26) Word Cycle Sync
        .BCD    (BCD)           // inout            // ('569 item 35) BCD input/output line 35 from/to auxiliary data storage circuit 25.
    );


// =================================================================================================



endmodule

