onerror { resume }
transcript off
add wave -noreg -logic {/tb/rst}
add wave -noreg -decimal -literal -signed2 {/tb/tcnt}
add wave -noreg -logic {/tb/tclk}
add wave -noreg -decimal -literal -signed2 {/tb/digit_count}
add wave -noreg -logic {/tb/T1}
add wave -noreg -logic {/tb/T2}
add wave -noreg -logic {/tb/T3}
add wave -noreg -logic {/tb/T4}
add wave -noreg -logic {/tb/phi1}
add wave -noreg -logic {/tb/phi2}
add wave -noreg -logic {/tb/c_cl}
add wave -noreg -logic {/tb/start}
add wave -noreg -logic {/tb/o15}
add wave -noreg -logic {/tb/o1}
add wave -noreg -logic {/tb/o2}
add wave -noreg -logic {/tb/o3}
add wave -noreg -logic {/tb/o4}
add wave -noreg -logic {/tb/o5}
add wave -noreg -logic {/tb/o6}
add wave -noreg -logic {/tb/o7}
add wave -noreg -logic {/tb/o8}
add wave -noreg -logic {/tb/o9}
add wave -noreg -logic {/tb/o10}
add wave -noreg -logic {/tb/o11}
add wave -noreg -logic {/tb/o12}
add wave -noreg -logic {/tb/o13}
add wave -noreg -logic {/tb/o14}
add wave -noreg -vgroup "NewArch"  {/tb/inst_cathode_new/scope_new/start_req} {/tb/new_o15} {/tb/new_o1} {/tb/new_o2} {/tb/new_o3} {/tb/new_o4} {/tb/new_o5} {/tb/new_o6} {/tb/new_o7} {/tb/new_o8} {/tb/new_o9} {/tb/new_o10} {/tb/new_o11} {/tb/new_o12} {/tb/new_o13} {/tb/new_o14}
add wave -noreg -vgroup "NewArch ActiveHigh"  {/tb/inst_cathode_new_hi/scope_new/start_req} {/tb/new_high_o15} {/tb/new_high_o1} {/tb/new_high_o2} {/tb/new_high_o3} {/tb/new_high_o4} {/tb/new_high_o5} {/tb/new_high_o6} {/tb/new_high_o7} {/tb/new_high_o8} {/tb/new_high_o9} {/tb/new_high_o10} {/tb/new_high_o11} {/tb/new_high_o12} {/tb/new_high_o13} {/tb/new_high_o14}
cursor "Cursor 1" 283069381ps  
transcript on
