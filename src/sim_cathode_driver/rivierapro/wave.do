onerror { resume }
set curr_transcript [transcript]
transcript off

add wave /tb/rst
add wave /tb/tcnt
add wave /tb/tclk
add wave /tb/digit_count
add wave /tb/T1
add wave /tb/T2
add wave /tb/T3
add wave /tb/T4
add wave /tb/phi1
add wave /tb/phi2
add wave /tb/c_cl
add wave /tb/start
add wave -logic /tb/o15
add wave /tb/o1
add wave /tb/o2
add wave /tb/o3
add wave /tb/o4
add wave /tb/o5
add wave /tb/o6
add wave /tb/o7
add wave /tb/o8
add wave /tb/o9
add wave /tb/o10
add wave /tb/o11
add wave /tb/o12
add wave /tb/o13
add wave /tb/o14
add wave /tb/NewArch
add wave -expand -vgroup /tb/inst_cathode \
	/tb/inst_cathode/START \
	/tb/inst_cathode/C_CL \
	/tb/inst_cathode/A \
	/tb/inst_cathode/ACn \
	/tb/inst_cathode/AnCn \
	/tb/inst_cathode/XnSn \
	/tb/inst_cathode/X \
	/tb/inst_cathode/Xn
add wave -expand -vgroup /tb/inst_cathode/inst_FFX \
	/tb/inst_cathode/inst_FFX/Q \
	/tb/inst_cathode/inst_FFX/Qn \
	/tb/inst_cathode/inst_FFX/S \
	/tb/inst_cathode/inst_FFX/R \
	/tb/inst_cathode/inst_FFX/qint \
	/tb/inst_cathode/inst_FFX/qintn
wv.cursors.add -time 360400ns -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.cursors.subcursor.add -time 361025ns -name {Cursor 1}
wv.cursors.setactive -name {Default cursor}
wv.zoom.range -from 0fs -to 415312500ps
wv.time.unit.auto.set
transcript $curr_transcript
