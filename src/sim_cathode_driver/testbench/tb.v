//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Cathode Driver 
//
// Description:
// 
// Include Files : None
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2047 HP-35 Cathode Driver Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

    // Basic clock, ce, and reset
    integer tcnt = 0;
    reg tclk = 1'b0;    // Test Clock. Fastest clock in the testbench.
    reg rst;
    integer digit_count;
    reg phi1 = 1'b0;
    reg phi2 = 1'b0;
    reg T1 = 1'b0;
    reg T2 = 1'b0;
    reg T3 = 1'b0;
    reg T4 = 1'b0;

    // Inputs to Cathode Driver
    reg  c_cl;          // input        // Counter Clock to Cathode Driver.
    reg  start;         // input        // ('569 item 40) Word synchronization pulse for digit scanner in Cathode Driver.
    // Outputs from Cathode Driver
    wire o1;            // output       // LED Cathode for Digit 1.
    wire o2;            // output       //  "     "     "    "   2.
    wire o3;            // output       //  "     "     "    "   3.
    wire o4;            // output       //  "     "     "    "   4.
    wire o5;            // output       //  "     "     "    "   5.
    wire o6;            // output       //  "     "     "    "   6.
    wire o7;            // output       //  "     "     "    "   7.
    wire o8;            // output       //  "     "     "    "   8.
    wire o9;            // output       //  "     "     "    "   9.
    wire o10;           // output       //  "     "     "    "   10.
    wire o11;           // output       //  "     "     "    "   11.
    wire o12;           // output       //  "     "     "    "   12.
    wire o13;           // output       //  "     "     "    "   13.
    wire o14;           // output       //  "     "     "    "   14.
    wire o15;           // output       //  "     "     "    "   15.

    // Outputs from Cathode Driver
    wire new_o1;            // output       // LED Cathode for Digit 1.
    wire new_o2;            // output       //  "     "     "    "   2.
    wire new_o3;            // output       //  "     "     "    "   3.
    wire new_o4;            // output       //  "     "     "    "   4.
    wire new_o5;            // output       //  "     "     "    "   5.
    wire new_o6;            // output       //  "     "     "    "   6.
    wire new_o7;            // output       //  "     "     "    "   7.
    wire new_o8;            // output       //  "     "     "    "   8.
    wire new_o9;            // output       //  "     "     "    "   9.
    wire new_o10;           // output       //  "     "     "    "   10.
    wire new_o11;           // output       //  "     "     "    "   11.
    wire new_o12;           // output       //  "     "     "    "   12.
    wire new_o13;           // output       //  "     "     "    "   13.
    wire new_o14;           // output       //  "     "     "    "   14.
    wire new_o15;           // output       //  "     "     "    "   15.

    // Outputs from Cathode Driver
    wire new_high_o1;            // output       // LED Cathode for Digit 1.
    wire new_high_o2;            // output       //  "     "     "    "   2.
    wire new_high_o3;            // output       //  "     "     "    "   3.
    wire new_high_o4;            // output       //  "     "     "    "   4.
    wire new_high_o5;            // output       //  "     "     "    "   5.
    wire new_high_o6;            // output       //  "     "     "    "   6.
    wire new_high_o7;            // output       //  "     "     "    "   7.
    wire new_high_o8;            // output       //  "     "     "    "   8.
    wire new_high_o9;            // output       //  "     "     "    "   9.
    wire new_high_o10;           // output       //  "     "     "    "   10.
    wire new_high_o11;           // output       //  "     "     "    "   11.
    wire new_high_o12;           // output       //  "     "     "    "   12.
    wire new_high_o13;           // output       //  "     "     "    "   13.
    wire new_high_o14;           // output       //  "     "     "    "   14.
    wire new_high_o15;           // output       //  "     "     "    "   15.

    initial begin
      rst = 1'b1;
      #10;
      rst = 1'b0;
      #1000000;
    end

    // Generate 1.6 MHz testbench clock.
    // Twice the frequency of the actual calculator so all control signals can be registered.
    always #312.5ns tclk = ~tclk;

    // Generate 800 kHz clock
//    always #625ns clk = ~clk;

//         _ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ __ _
// tcnt    _|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|_
//           30 31  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31  0 
//           ____________________    ____________________    ____________________    ____________________    __________
// phi1    _|                    |__|                    |__|                    |__|                    |__|
//         ____    ____________________    ____________________    ____________________    ____________________    ____
// phi2        |__|                    |__|                    |__|                    |__|                    |__| 
//         ___________________       _________________________________________________________________       __________
// c_cl                       |_____|                                                                 |_____| <- if digit_count == n
//                 _______________________                                                                         ____
//  T1     _______|                       |_______________________________________________________________________|
//                                         _______________________                                                 
//  T2     _______________________________|                       |____________________________________________________
//                                                                 _______________________                                                 
//  T3     _______________________________________________________|                       |____________________________
//         _______                                                                         _______________________                                                 
//  T4            |_______________________________________________________________________|                       |____
//             ___ _______________________________________________________________________________________________ ____
//  digit_count___|_______________________________________________n_______________________________________________|_n+1
//                                                                
//
    always @(posedge tclk or posedge rst) begin : proc_tclk
        if (rst) begin
            tcnt <= 31;
            c_cl <= 1'b1;
            phi1 <= 1'b1;
            phi2 <= 1'b1;
            digit_count <= 0;
        end
        else begin
            if (tcnt == 31) 
                tcnt <= 0;
            else
                tcnt <= tcnt + 1;
            //
            if (tcnt == 3 || tcnt == 4) // During T1
                c_cl <= 1'b0;
            else if ((tcnt == 27 || tcnt == 28) && (digit_count == 13))  // During T4 of digit n
                c_cl <= 1'b0;
            else
                c_cl <= 1'b1;
            //
            if (tcnt == 4 || tcnt == 12 || tcnt == 20 || tcnt == 28) 
                phi1 <= 1'b0;
            else
                phi1 <= 1'b1;
            //
            if (tcnt == 6 || tcnt == 14 || tcnt == 22 || tcnt == 30) 
                phi2 <= 1'b0;
            else
                phi2 <= 1'b1;
            //
            case (tcnt)
                31 ,  0 ,  1 ,  2 ,  3 ,  4 ,  5 ,  6 : {T1, T2, T3, T4} <= 4'b1000;
                 7 ,  8 ,  9 , 10 , 11 , 12 , 13 , 14 : {T1, T2, T3, T4} <= 4'b0100;
                15 , 16 , 17 , 18 , 19 , 20 , 21 , 22 : {T1, T2, T3, T4} <= 4'b0010;
                23 , 24 , 25 , 26 , 27 , 28 , 29 , 30 : {T1, T2, T3, T4} <= 4'b0001;
            endcase
            //
            if (tcnt == 31) 
                if (digit_count == 13)
                    digit_count <= 0;
                else
                    digit_count <= digit_count + 1;
            //
        end
    end 


//    assign start = ((digit_count == 2) & T1)? 1'b1 : 1'b0;

    always @* begin : proc_start
        start = (digit_count == 13) & T4;
    end 

    initial begin
//        clk   = 1'b0;          // input        // Clock input operating at the HP-35's oscillator rate.
//        c_cl  = 1'b0;
//        start = 1'b0;
        wait (rst == 1'b0);
        @(negedge phi2);
        @(negedge phi2);
//        start = 1'b1;
        @(negedge phi2);
//        start = 1'b0;
        //
        #800us
        $stop;
    end

// -------------------------------------------------------
// Instances

    cathode_driver_14 #(
        .NewArch    (0),        // parameter    // 1 = Use new architecture, 0 = Like the patent.
        .ActiveHigh (0)         // parameter    // 1 = Outputs, O1 to O15 are active high, 0 = Like the patent.  Only for NewArch.
    )
    inst_cathode_old(
        // Output Ports
        .O1    (o1),           // output       // LED Cathode for Digit 1.
        .O2    (o2),           // output       //  "     "     "    "   2.
        .O3    (o3),           // output       //  "     "     "    "   3.
        .O4    (o4),           // output       //  "     "     "    "   4.
        .O5    (o5),           // output       //  "     "     "    "   5.
        .O6    (o6),           // output       //  "     "     "    "   6.
        .O7    (o7),           // output       //  "     "     "    "   7.
        .O8    (o8),           // output       //  "     "     "    "   8.
        .O9    (o9),           // output       //  "     "     "    "   9.
        .O10   (o10),          // output       //  "     "     "    "   10.
        .O11   (o11),          // output       //  "     "     "    "   11.
        .O12   (o12),          // output       //  "     "     "    "   12.
        .O13   (o13),          // output       //  "     "     "    "   13.
        .O14   (o14),          // output       //  "     "     "    "   14.
        .O15   (o15),          // output       //  "     "     "    "   15.
        // Input Ports                
        .START (start),        // input        // ('569 item 40) Word synchronization
                                               // pulse for digit scanner in Cathode Driver.
        .C_CL  (c_cl)          // input        // '569: "Once each digit time a signal
                                               // (counter-clock) is sent to the cathode
                                               // driver of output display unit 14 (the
                                               // trailing edge of this signal will step
                                               // the display to the next digit)."
    );

    cathode_driver_14 #(
        .NewArch    (1),    // parameter    // 1 = Use new architecture, 0 = Like the patent.
        .ActiveHigh (0)     // parameter    // 1 = Outputs, O1 to O15 are active high, 0 = Like the patent.  Only for NewArch.
    )
    inst_cathode_new(
        // Output Ports
        .O1    (new_o1),           // output       // LED Cathode for Digit 1.
        .O2    (new_o2),           // output       //  "     "     "    "   2.
        .O3    (new_o3),           // output       //  "     "     "    "   3.
        .O4    (new_o4),           // output       //  "     "     "    "   4.
        .O5    (new_o5),           // output       //  "     "     "    "   5.
        .O6    (new_o6),           // output       //  "     "     "    "   6.
        .O7    (new_o7),           // output       //  "     "     "    "   7.
        .O8    (new_o8),           // output       //  "     "     "    "   8.
        .O9    (new_o9),           // output       //  "     "     "    "   9.
        .O10   (new_o10),          // output       //  "     "     "    "   10.
        .O11   (new_o11),          // output       //  "     "     "    "   11.
        .O12   (new_o12),          // output       //  "     "     "    "   12.
        .O13   (new_o13),          // output       //  "     "     "    "   13.
        .O14   (new_o14),          // output       //  "     "     "    "   14.
        .O15   (new_o15),          // output       //  "     "     "    "   15.
        // Input Ports                
        .START (start),        // input        // ('569 item 40) Word synchronization
                                               // pulse for digit scanner in Cathode Driver.
        .C_CL  (c_cl)          // input        // '569: "Once each digit time a signal
                                               // (counter-clock) is sent to the cathode
                                               // driver of output display unit 14 (the
                                               // trailing edge of this signal will step
                                               // the display to the next digit)."
    );

    cathode_driver_14 #(
        .NewArch    (1),    // parameter    // 1 = Use new architecture, 0 = Like the patent.
        .ActiveHigh (1)     // parameter    // 1 = Outputs, O1 to O15 are active high, 0 = Like the patent.  Only for NewArch.
    )
    inst_cathode_new_hi(
        // Output Ports
        .O1    (new_high_o1),           // output       // LED Cathode for Digit 1.
        .O2    (new_high_o2),           // output       //  "     "     "    "   2.
        .O3    (new_high_o3),           // output       //  "     "     "    "   3.
        .O4    (new_high_o4),           // output       //  "     "     "    "   4.
        .O5    (new_high_o5),           // output       //  "     "     "    "   5.
        .O6    (new_high_o6),           // output       //  "     "     "    "   6.
        .O7    (new_high_o7),           // output       //  "     "     "    "   7.
        .O8    (new_high_o8),           // output       //  "     "     "    "   8.
        .O9    (new_high_o9),           // output       //  "     "     "    "   9.
        .O10   (new_high_o10),          // output       //  "     "     "    "   10.
        .O11   (new_high_o11),          // output       //  "     "     "    "   11.
        .O12   (new_high_o12),          // output       //  "     "     "    "   12.
        .O13   (new_high_o13),          // output       //  "     "     "    "   13.
        .O14   (new_high_o14),          // output       //  "     "     "    "   14.
        .O15   (new_high_o15),          // output       //  "     "     "    "   15.
        // Input Ports                
        .START (start),        // input        // ('569 item 40) Word synchronization
                                               // pulse for digit scanner in Cathode Driver.
        .C_CL  (c_cl)          // input        // '569: "Once each digit time a signal
                                               // (counter-clock) is sent to the cathode
                                               // driver of output display unit 14 (the
                                               // trailing edge of this signal will step
                                               // the display to the next digit)."
    );

endmodule

