onerror { resume }
set curr_transcript [transcript]
transcript off

add wave /tb/rst
add wave /tb/nextSysCount
add wave -literal /tb/sysCount
add wave /tb/digitCount
add wave -literal /tb/NewArch
add wave -logic /tb/WS_rom
add wave /tb/WS_ct
add wave /tb/WS
add wave /tb/IA
add wave /tb/WS
add wave /tb/SYNC
add wave /tb/ROW0
add wave /tb/ROW1
add wave /tb/ROW2
add wave /tb/ROW3
add wave /tb/ROW4
add wave /tb/ROW5
add wave /tb/ROW6
add wave /tb/ROW7
add wave /tb/PHI1
add wave /tb/PHI2
add wave /tb/PWO
add wave /tb/IS
add wave /tb/CARRY
add wave /tb/COL0
add wave /tb/COL2
add wave /tb/COL3
add wave /tb/COL4
add wave /tb/COL6
add wave /tb/s_mnemonic
add wave /tb/s_fs
add wave /tb/load_instruction
add wave /tb/fetch_instruction
add wave /tb/exec_instruction
add wave /tb/instruction
add wave /tb/is_shifter
add wave /tb/sysCount
add wave /tb/nextSysCount
add wave -oct /tb/ROM_Address_58_HoldReg_27
add wave -unsigned -literal /tb/Status_Bits_62_HoldReg_27
add wave -unsigned -literal /tb/Return_Address_60_HoldReg_27
add wave -unsigned -literal /tb/ia_address_HoldReg_27
add wave -oct /tb/ROM_Address_58_HoldReg_55
add wave -unsigned /tb/Status_Bits_62_HoldReg_55
add wave -unsigned /tb/Return_Address_60_HoldReg_55
add wave /tb/i_ptrHoldingReg
add wave /tb/s_dut_state
add wave -expand -vgroup Keyboard-Stuff \
	/tb/sysCount \
	/tb/pressingThisKey \
	/tb/inst_ct/kdn \
	/tb/inst_ct/keydown \
	/tb/inst_ct/newkey \
	/tb/inst_ct/keyset_s0 \
	( -oct /tb/inst_ct/keybuf ) \
	/tb/inst_ct/CARRY_FF
add wave -expand -vgroup /tb/inst_ct/inst_mpc \
	( -logic /tb/inst_ct/inst_mpc/JSB ) \
	( -logic /tb/inst_ct/inst_mpc/BRH ) \
	( -logic /tb/inst_ct/inst_mpc/PTONLY ) \
	( -logic /tb/inst_ct/inst_mpc/UP2PT ) \
	( -logic /tb/inst_ct/inst_mpc/ARITHW ) \
	( -logic /tb/inst_ct/inst_mpc/ISTW ) \
	( -logic /tb/inst_ct/inst_mpc/STDECN ) \
	( -logic /tb/inst_ct/inst_mpc/SST ) \
	( -logic /tb/inst_ct/inst_mpc/RST ) \
	( -logic /tb/inst_ct/inst_mpc/IST ) \
	( -logic /tb/inst_ct/inst_mpc/SPT ) \
	( -logic /tb/inst_ct/inst_mpc/IPTR ) \
	( -logic /tb/inst_ct/inst_mpc/IPTS ) \
	( -logic /tb/inst_ct/inst_mpc/PTD ) \
	( -logic /tb/inst_ct/inst_mpc/PTI ) \
	( -logic /tb/inst_ct/inst_mpc/TKR ) \
	( -logic /tb/inst_ct/inst_mpc/RET ) \
	( -logic /tb/inst_ct/inst_mpc/PHI2 ) \
	( -logic /tb/inst_ct/inst_mpc/pwor ) \
	( -logic /tb/inst_ct/inst_mpc/IS ) \
	( -logic /tb/inst_ct/inst_mpc/b5 ) \
	( -logic /tb/inst_ct/inst_mpc/b14 ) \
	( -logic /tb/inst_ct/inst_mpc/b18 ) \
	( -logic /tb/inst_ct/inst_mpc/b26 ) \
	( -logic /tb/inst_ct/inst_mpc/b35 ) \
	( -logic /tb/inst_ct/inst_mpc/b45 ) \
	( -logic /tb/inst_ct/inst_mpc/b54 ) \
	( -logic /tb/inst_ct/inst_mpc/b55 ) \
	( -logic /tb/inst_ct/inst_mpc/CARRY_FF ) \
	( -logic /tb/inst_ct/inst_mpc/stqzero ) \
	( -logic /tb/inst_ct/inst_mpc/is_eq_ptr ) \
	( -literal /tb/inst_ct/inst_mpc/state ) \
	( -literal /tb/inst_ct/inst_mpc/next )
add wave -expand -vgroup /tb/inst_ct \
	/tb/inst_ct/IA \
	/tb/inst_ct/WS \
	/tb/inst_ct/SYNC \
	/tb/inst_ct/ROW0 \
	/tb/inst_ct/ROW1 \
	/tb/inst_ct/ROW2 \
	/tb/inst_ct/ROW3 \
	/tb/inst_ct/ROW4 \
	/tb/inst_ct/ROW5 \
	/tb/inst_ct/ROW6 \
	/tb/inst_ct/ROW7 \
	/tb/inst_ct/PHI1 \
	/tb/inst_ct/PHI2 \
	/tb/inst_ct/PWO \
	/tb/inst_ct/IS \
	/tb/inst_ct/CARRY \
	/tb/inst_ct/COL0 \
	/tb/inst_ct/COL2 \
	/tb/inst_ct/COL3 \
	/tb/inst_ct/COL4 \
	/tb/inst_ct/COL6 \
	/tb/inst_ct/debug_lfsr_fb_xnor \
	/tb/inst_ct/debug_lfsr_fb_xor \
	/tb/inst_ct/pwor \
	/tb/inst_ct/q \
	/tb/inst_ct/T1 \
	/tb/inst_ct/T2 \
	/tb/inst_ct/T3 \
	/tb/inst_ct/T4 \
	/tb/inst_ct/b5 \
	/tb/inst_ct/b14 \
	/tb/inst_ct/b18 \
	/tb/inst_ct/b26 \
	/tb/inst_ct/b35 \
	/tb/inst_ct/b45 \
	/tb/inst_ct/b54 \
	/tb/inst_ct/b55 \
	/tb/inst_ct/b19_b26 \
	/tb/inst_ct/stepaddr \
	/tb/inst_ct/kdn \
	/tb/inst_ct/keydown \
	/tb/inst_ct/newkey \
	/tb/inst_ct/keyset_s0 \
	( -oct /tb/inst_ct/keybuf ) \
	/tb/inst_ct/JSB \
	/tb/inst_ct/BRH \
	/tb/inst_ct/PTONLY \
	/tb/inst_ct/UP2PT \
	/tb/inst_ct/ARITHW \
	/tb/inst_ct/ISTW \
	/tb/inst_ct/STDECN \
	/tb/inst_ct/SST \
	/tb/inst_ct/RST \
	/tb/inst_ct/IST \
	/tb/inst_ct/SPT \
	/tb/inst_ct/IPTR \
	/tb/inst_ct/IPTS \
	/tb/inst_ct/PTD \
	/tb/inst_ct/PTI \
	/tb/inst_ct/TKR \
	/tb/inst_ct/RET \
	/tb/inst_ct/sr \
	/tb/inst_ct/abuf \
	/tb/inst_ct/ptr \
	/tb/inst_ct/CARRY_FF \
	/tb/inst_ct/dSr28 \
	/tb/inst_ct/iamux \
	/tb/inst_ct/dPtr4 \
	/tb/inst_ct/sum2sr \
	/tb/inst_ct/sr2sr \
	/tb/inst_ct/makesum \
	/tb/inst_ct/makesum_r \
	/tb/inst_ct/chgstat \
	/tb/inst_ct/sr2x \
	/tb/inst_ct/p2x \
	/tb/inst_ct/pn2x \
	/tb/inst_ct/one2y \
	/tb/inst_ct/srn2y \
	/tb/inst_ct/sr2y \
	/tb/inst_ct/sr2ci \
	/tb/inst_ct/co2ci \
	/tb/inst_ct/sum2p \
	/tb/inst_ct/sumn2p \
	/tb/inst_ct/pwq \
	/tb/inst_ct/ld_dig0 \
	/tb/inst_ct/set_ws \
	/tb/inst_ct/clr_ws \
	/tb/inst_ct/ws_out \
	/tb/inst_ct/ws_req \
	/tb/inst_ct/ws_oe \
	/tb/inst_ct/ws_in \
	/tb/inst_ct/stq \
	/tb/inst_ct/stqzero \
	/tb/inst_ct/arith_cyc \
	/tb/inst_ct/ipt_cyc \
	/tb/inst_ct/ist_cyc \
	/tb/inst_ct/arith_cset \
	/tb/inst_ct/arith_crst \
	/tb/inst_ct/ist_cset \
	/tb/inst_ct/ist_crst \
	/tb/inst_ct/ipt_cset \
	/tb/inst_ct/ipt_crst \
	/tb/inst_ct/x_in \
	/tb/inst_ct/y_in \
	/tb/inst_ct/c_in \
	/tb/inst_ct/sum \
	/tb/inst_ct/co \
	/tb/inst_ct/cor
wv.cursors.add -time 113172500ns+1 -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.cursors.subcursor.add -time 210332500ns+1 -name {Cursor 1}
wv.cursors.setactive -name {Cursor 1}
wv.zoom.range -from 0fs -to 220849125ns
wv.time.unit.auto.set
transcript $curr_transcript
