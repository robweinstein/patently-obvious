//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Control and Timing Circuit 16
//
// Description:
// 
// Include Files : state_names.v
//
// Output Files : None
//
// Conventions:
//    - UPPER case for signals described in the '569 patent.  Since many of the
//      Microprogrammed Controller's outputs (control lines) are described in
//      the patent, UPPER case is extended to all control line outputs from the
//      DUT.
//    - Internal wires and registers are 'lower' case.
//    - State name parameters are camelCase starting with a lower case 's',
//      e.g., 'sIdle'.
//    - Other parameters are first letter 'Upper'.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2025 - HP-35 Control and Timing Circuit 16 - Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

// -----------------------------------------------------------------------------
// Signal Declarations
// 
    localparam NewArch = 0;

    // Include the parameter that defines the state names:
    `include "../../state_names.v"

    // Type 10 Instruction
    `define NOP     10'b0000000000

    //----------------------------------------------------------------------------------------------
    // Signals for DUT Hookup:
    //  
    // Control & Timing Output Ports
    wire    IA;                 // output reg   // ('569 item 32) Serial ROM Address
    tri     WS_ct;              // output reg   // ('569 item 30) Pointer Word Select
    wire    SYNC;               // output reg   // ('569 item 26) Word Cycle Sync
    wire    ROW0;               // output reg   // ('569 item 50) Keyboard Row Driver 0
    wire    ROW1;               // output reg   //                   "      "    "    1
    wire    ROW2;               // output reg   //                   "      "    "    2
    wire    ROW3;               // output reg   //                   "      "    "    3
    wire    ROW4;               // output reg   //                   "      "    "    4
    wire    ROW5;               // output reg   //                   "      "    "    5
    wire    ROW6;               // output reg   //                   "      "    "    6
    wire    ROW7;               // output reg   //                   "      "    "    7
    // Control & Timing Input Ports               
    reg     PHI1    = 1'b0;     // input        // Bit-Rate Clock Input, Phase 1, not used here.
    reg     PHI2;               // input        // Bit-Rate Clock Input, Phase 2
    reg     PWO;                // input        // ('569 item 36) PoWer On pulse.  '569 (7) "As the system
                                                //    power comes on, the PWO signal is held at
                                                //    logic l for at least 20 milliseconds."
    wire    IS;                 // input        // ('569 item 28) Serial Instruction from ROM Output
    reg     CARRY   = 1'b0;     // input        // ('569 item 34) Carry flag from Arithmetic & Register block.
    reg     COL0    = 1'b0;     // input        // ('569 item 54) Keyboard Column Input 0
    reg     COL1    = 1'b0;     // input        //                   "       "      "   1
    reg     COL2    = 1'b0;     // input        //                   "       "      "   2
    reg     COL3    = 1'b0;     // input        //                   "       "      "   3
    reg     COL4    = 1'b0;     // input        //                   "       "      "   4
    reg     COL5    = 1'b0;     // input        //                   "       "      "   5
    reg     COL6    = 1'b0;     // input        //                   "       "      "   6
    //----------------------------------------------------------------------------------------------

    // Testbench signals
    reg         rst;
    reg         T1 = 1'b0, T2 = 1'b0, T3 = 1'b0, T4 = 1'b0;
    reg         go = 1'b0;
    reg         sim_start_pulse = 1'b0;
    reg         sim_end_pulse   = 1'b0;
    reg         loop_done_1;
    reg         loop_done_2;
    reg         loop_done_3;
    integer     fd; // File Descriptor
    integer     i, j, N, P, setP, testP;
    integer     TestIterations;
    reg         load_instruction = 1'b0;
    reg         fetch_instruction = 1'b0;
    reg         exec_instruction = 1'b0;
    reg         grab_em;
    reg         compare_em;
    reg         save_em;
    reg [9:0]   instruction     = 'b0;
    reg [55:0]  is_shifter      = 'b0;

    integer     sysCount        = 0;
    integer     nextSysCount    = 1;
    integer     wsBegin         = -1;
    integer     wsEnd           = -1;
    integer     wsBeginFetch    = -1;
    integer     wsEndFetch      = -1;
    integer     wsBeginExecute  = -1;
    integer     wsEndExecute    = -1;
    integer     digitCount      = 0;
    tri         WS_rom;                 // Word Select generated by emulated ROM.
    tri0        WS;                     // Board-level WS with pull-down.
    reg         SYNCr           = 1'b0; // Registered version of SYNC for edge detect.
    reg [7:0]   ROM_Address_58_HoldReg_27,    ROM_Address_58_HoldReg_55;
    reg [11:0]  Status_Bits_62_HoldReg_27,    Status_Bits_62_HoldReg_55;
    reg [7:0]   Return_Address_60_HoldReg_27, Return_Address_60_HoldReg_55;
    reg [7:0]   ia_debug_shifter;
    reg [7:0]   ia_address_HoldReg_27;
    integer     branchTarget = 0;
    integer     jsrTarget = 0;
    string      pressingThisKey = "";  // No press.

    integer     i_ptrHoldingReg;
    integer     i_carry;
    integer     i_opcode;

    // For fdisplay:
    string      s_mnemonic;
    integer     i_sub;
    integer     i_ci;
    integer     i_fs;
    string      s_fs;
    integer     i_ptr;
    integer     i_address;
    integer     i_n_field;
    integer     i_p_field;
    integer     i_romnum; 


    reg         expected_carry  = 1'b0;
    reg         actual_carry    = 1'b0;
    reg         carry_match     = 1'b0;
    reg         all_regs_match  = 1'b0;

    integer     good_count;
    integer     bad_count; 

    reg [1:62]  state, next;
    string      s_dut_state;


// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 

    task buildInstruction;
    // Build the 10-bit instruction from a given instruction mnemonic and field-select (both strings).
    // Called by issueInstruction().
        output reg [9:0]    op_code;
        output integer      ws_begin;
        output integer      ws_end;
        input string        op_mnemonic;
        input string        field_select;
//      input integer       pointer_val = -1;   // Default arguments supported in SystemVerilog only.
        input integer       pointer_val;
        input integer       address;
        input integer       n_field;
        input integer       p_field;
        input integer       romnum;
        begin : task_make_instruction 
            integer itype = -1;
            case (op_mnemonic)
                // Type 1 Instructions
                "JSR"       : begin itype = 1; op_code = {address[7:0],2'b01}; end  // Jump to Subroutine
                "CBRH"      : begin itype = 1; op_code = {address[7:0],2'b11}; end  // Conditional Branch (Branch when Carry = 0)
                // Type 2 Instructions
                // -- Class 1) Clear
                "CLRA"      : begin itype = 2; op_code = 10'b10111_000_10; end  // 0 -> A
                "CLRB"      : begin itype = 2; op_code = 10'b00001_000_10; end  // 0 -> B
                "CLRC"      : begin itype = 2; op_code = 10'b00110_000_10; end  // 0 -> C
                // -- Class 2) Transfer/Exchange
                "MOVAB"     : begin itype = 2; op_code = 10'b01001_000_10; end  // A -> B
                "MOVBC"     : begin itype = 2; op_code = 10'b00100_000_10; end  // B -> C
                "MOVCA"     : begin itype = 2; op_code = 10'b01100_000_10; end  // C -> A
                "XCHAB"     : begin itype = 2; op_code = 10'b11001_000_10; end  // A <-> B
                "XCHBC"     : begin itype = 2; op_code = 10'b10001_000_10; end  // B <-> C
                "XCHAC"     : begin itype = 2; op_code = 10'b11101_000_10; end  // A <-> C
                // -- Class 3) Add/Subtract
                "ADDACC"    : begin itype = 2; op_code = 10'b01110_000_10; end  // A + C -> C
                "SUBACC"    : begin itype = 2; op_code = 10'b01010_000_10; end  // A - C -> C
                "ADDABA"    : begin itype = 2; op_code = 10'b11100_000_10; end  // A + B -> A
                "SUBABA"    : begin itype = 2; op_code = 10'b11000_000_10; end  // A - B -> A
                "ADDACA"    : begin itype = 2; op_code = 10'b11110_000_10; end  // A + C -> A
                "SUBACA"    : begin itype = 2; op_code = 10'b11010_000_10; end  // A - C -> A
                "ADDCCC"    : begin itype = 2; op_code = 10'b10101_000_10; end  // C + C -> C
                // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                "CMP0B"     : begin itype = 2; op_code = 10'b00000_000_10; end  // 0 - B            (Compare B to zero)
                "CMP0C"     : begin itype = 2; op_code = 10'b01101_000_10; end  // 0 - C            (Compare C to zero)
                "CMPAC"     : begin itype = 2; op_code = 10'b00010_000_10; end  // A - C            (Compare A and C)
                "CMPAB"     : begin itype = 2; op_code = 10'b10000_000_10; end  // A - B            (Compare A and B)
                "CMPA1"     : begin itype = 2; op_code = 10'b10011_000_10; end  // A - 1            (Compare A to one)
                "CMPC1"     : begin itype = 2; op_code = 10'b00011_000_10; end  // C - 1            (Compare C to one)    
                // -- Class 5) Complement
                "TCC"       : begin itype = 2; op_code = 10'b00101_000_10; end  // 0 - C -> C       (Tens Complement register C)
                "NCC"       : begin itype = 2; op_code = 10'b00111_000_10; end  // 0 - C - 1 -> C   (Nines Complement register C)
                // -- Class 6) Increment
                "INCA"      : begin itype = 2; op_code = 10'b11111_000_10; end  // A + 1 -> A       (Increment register A)
                "INCC"      : begin itype = 2; op_code = 10'b01111_000_10; end  // C + 1 -> C       (Increment register C)
                // -- Class 7) Decrement
                "DECA"      : begin itype = 2; op_code = 10'b11011_000_10; end  // A - 1 -> A       (Decrement register A)
                "DECC"      : begin itype = 2; op_code = 10'b01011_000_10; end  // C - 1 -> C       (Decrement register C)
                // -- Class 8) Shift
                "SHRA"      : begin itype = 2; op_code = 10'b10110_000_10; end  // A >> 1           (Shift Right register A)
                "SHRB"      : begin itype = 2; op_code = 10'b10100_000_10; end  // B >> 1           (Shift Right register B)
                "SHRC"      : begin itype = 2; op_code = 10'b10010_000_10; end  // C >> 1           (Shift Right register C)
                "SHLA"      : begin itype = 2; op_code = 10'b01000_000_10; end  // A << 1           (Shift Left register A)
                // Type 3 Instructions (Status Operations)
                "SSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b00_0100}; end // Set Status Flag N
                "ISTN"      : begin itype = 3; op_code = {n_field[3:0],6'b01_0100}; end // Interrogate Status Flag N
                "RSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b10_0100}; end // Reset Status Flag N
                "CSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b11_0100}; end // Clear All Flags  (If N=0 then clear all flags, if N>0 then clear flags N down to 0)
                // Type 4 Instructions (Pointer Operations)
                "SPTP"      : begin itype = 4; op_code = {p_field[3:0],6'b00_1100}; end // Set Pointer to P     (P-field = New Pointer Value)
                "PTD"       : begin itype = 4; op_code = {p_field[3:0],6'b01_1100}; end // Decrement Pointer    (P-field = xxxx)
                "IPTP"      : begin itype = 4; op_code = {p_field[3:0],6'b10_1100}; end // Interrogate Pointer  (Set Carry if Pointer = P-field)
                "PTI"       : begin itype = 4; op_code = {p_field[3:0],6'b11_1100}; end // Increment Pointer    (P-field = xxxx)
                // Type 5 Instructions
//              "AIx"       : begin itype = 5; op_code = 10'b????_00_1000; end          // Available Instructions (16)
                "LDC"       : begin itype = 5; op_code = {n_field[3:0],6'b01_1000}; end // Load Constant, N-field -> C at Pointer, post-decrement Pointer
                "DSPTOG"    : begin itype = 5; op_code = 10'b000_010_1000; end          // Display Toggle
                "XCHCM"     : begin itype = 5; op_code = 10'b001_010_1000; end          // C -> M -> C              (Exchange Memory)
                "PUSHC"     : begin itype = 5; op_code = 10'b010_010_1000; end          // C -> C -> D -> E -> F    (Up Stack or Push C) 
                "POPA"      : begin itype = 5; op_code = 10'b011_010_1000; end          // F -> F -> E -> D -> A    (Down Stack or Pop A)
                "DSPOFF"    : begin itype = 5; op_code = 10'b100_010_1000; end          // Display Off
                "RCLM"      : begin itype = 5; op_code = 10'b101_010_1000; end          // M -> M -> C              (Recall Memory)
                "ROTDN"     : begin itype = 5; op_code = 10'b110_010_1000; end          // C -> F -> E -> D -> C    (Rotate Down)
                "CLRALL"    : begin itype = 5; op_code = 10'b111_010_1000; end          // 0 -> A, B, C, D, E, F, M (Clear all 56 bits of all registers)
                "MOVISA"    : begin itype = 5; op_code = 10'b000_110_1000; end          // Is -> A                  (Load 56-bit instruction sequence into register A)
                "MOVBCDC"   : begin itype = 5; op_code = 10'b001_110_1000; end          // BCD -> C                 (Load 56-bit data storage value into register C)
                // Type 6 Instruction (ROM Select, Misc.)
                "ROMSEL"    : begin itype = 6; op_code = {romnum[2:0],7'b00_10000}; end // ROM Select
                "RET"       : begin itype = 6; op_code = {10'b000_01_10000}; end        // Subroutine Return
                "EKEY"      : begin itype = 6; op_code = {10'b00_010_10000}; end        // External Keyboard Entry (Not implemented in HP-35)
                "KEYJMP"    : begin itype = 6; op_code = {10'b00_110_10000}; end        // Keyboard Entry (Jump to ROM address specified by key-code)
                "SNDA"      : begin itype = 6; op_code = {10'b100_11_10000}; end        // Send Address from C Register to Aux Data Storage (Not implemented in HP-35)
                "SNDD"      : begin itype = 6; op_code = {10'b101_11_10000}; end        // Send Data from C Register to Aux Data Storage (Not implemented in HP-35)
                "RCVD"      : begin itype = 6; op_code = {10'b101_11_11000}; end        // Same as Type 5, MOVBCDC.  Receive Data from Aux Data Storage into C Register (Not implemented in HP-35)

                // ==================================================================
                // Finish Me - Maybe add dummy instructions for types 7, 8, 9.
                // ==================================================================

                // Type 7 Instruction (Reserved for Program Storage MOS Circuit)
                // Type 8 Instruction (Reserved for Program Storage MOS Circuit)
                // Type 9 Instruction (Available)
                // Type 10 Instruction
                "NOP"       : begin itype = 10; op_code = 10'b0000000000; end   // No Operation             (All registers recirculate)
                // If none of the above
                default     : begin itype = -1; op_code = 10'bzzzzzzzzzz; end
            endcase

            case (field_select)
                "P"     : begin if (itype == 2) op_code[4:2] = 3'b000;  ws_begin = pointer_val; ws_end = pointer_val; end   // Pointer position.
//              "WP"    : begin if (itype == 2) op_code[4:2] = 3'b001;  ws_begin = 0;           ws_end = pointer_val; end   // Bit-reversed encoding.   // Up to pointer position.
                "WP"    : begin if (itype == 2) op_code[4:2] = 3'b100;  ws_begin = 0;           ws_end = pointer_val; end   // Up to pointer position.
                "X"     : begin if (itype == 2) op_code[4:2] = 3'b010;  ws_begin = 0;           ws_end = 2;  end            // Exponent field (with sign).
//              "XS"    : begin if (itype == 2) op_code[4:2] = 3'b011;  ws_begin = 2;           ws_end = 2;  end            // Bit-reversed encoding.   // Exponent sign.
                "XS"    : begin if (itype == 2) op_code[4:2] = 3'b110;  ws_begin = 2;           ws_end = 2;  end            // Exponent sign.
//              "M"     : begin if (itype == 2) op_code[4:2] = 3'b100;  ws_begin = 3;           ws_end = 12; end            // Bit-reversed encoding.   // Mantissa field without sign.
                "M"     : begin if (itype == 2) op_code[4:2] = 3'b001;  ws_begin = 3;           ws_end = 12; end            // Mantissa field without sign.
                "MS"    : begin if (itype == 2) op_code[4:2] = 3'b101;  ws_begin = 3;           ws_end = 13; end            // Mantissa field with sign.
//              "W"     : begin if (itype == 2) op_code[4:2] = 3'b110;  ws_begin = 0;           ws_end = 13; end            // Bit-reversed encoding.   // Entire word.
                "W"     : begin if (itype == 2) op_code[4:2] = 3'b011;  ws_begin = 0;           ws_end = 13; end            // Entire word.
                "S"     : begin if (itype == 2) op_code[4:2] = 3'b111;  ws_begin = 13;          ws_end = 13; end            // Mantissa sign only.
                default : begin                                         ws_begin = -1;          ws_end = -1; end            // If no valid field select.
            endcase

        end
    endtask

    task issueInstruction;
    // Using the given mnemonic, field-select, and pointer value, build the 10-bit instruction,
    // then issue the instruction at the right time.
        input string        op_mnemonic;
        input string        field_select;
        input integer       pointer_val;
        input integer       address;
        input integer       n_field;
        input integer       p_field;
        input integer       romnum;
        begin : task_issue_instr 
            wait (nextSysCount == 0);
            buildInstruction(instruction, wsBegin, wsEnd, op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum);
            load_instruction = 1'b1;
            s_mnemonic  = op_mnemonic;  // Echo to a signal for display.
            s_fs        = field_select; // <ditto>
            i_ptr       = pointer_val;  // <ditto>
            i_address   = address;      // <ditto>
            i_n_field   = n_field;      // <ditto>
            i_p_field   = p_field;      // <ditto>
            i_romnum    = romnum;       // <ditto>
            @(posedge PHI2);
            buildInstruction(instruction, wsBegin, wsEnd, "", "", -1, -1, -1, -1, -1);
            load_instruction = 1'b0;
            #1;
        end
    endtask

    function string state2string;
        input [1:62] state_val;
        begin : func_st2str
            case (1'b1)
                // Main Instruction Parsing Loop
                state_val[sIdle]        : state2string = "sIdle";       // Power On (PWO) state, this bit is set, all others reset.  Test for Type 1.                                       
                state_val[s0x]          : state2string = "s0x";         // Test for Type 2.                                                                                                 
                state_val[s00x]         : state2string = "s00x";        // Test for Type 3 or Type 4.                                                                                       
                state_val[s001x]        : state2string = "s001x";       // Test for Type 4.                                                                                                 
                state_val[s000x]        : state2string = "s000x";       // Test for Type 5.                                                                                                 
                state_val[s0000x]       : state2string = "s0000x";      // Test for Type 6.                                                                                                 
                state_val[sNop]         : state2string = "sNop";        // Instruction failed tests for Types 1 through 6, so it's a No Op.                                                 
                state_val[sAddrOut]     : state2string = "sAddrOut";    // Return point for instructions that use the default ROM Address 58.                                               
                // Type 1 (Jump/Branch) Instructions                                                                                                                                        
                state_val[s1x]          : state2string = "s1x";         // Decoded a Type 1 instruction so test for JSB or BRH.                                                             
                state_val[sJsb]         : state2string = "sJsb";        // Decoded JSB (Jump Subroutine).                                                                                   
                state_val[sJsbWait]     : state2string = "sJsbWait";    // Wait for completeion of JSB.                                                                                     
                state_val[sBrhWait]     : state2string = "sBrhWait";    // Decoded BRH (Conditional Branch).                                                                                
                state_val[sBrhOut]      : state2string = "sBrhOut";     // Drive the captured branch address out Ia.                                                                        
                // Type 2 (Arithmetic) Instructions                                                                                                                                         
                state_val[s01x]         : state2string = "s01x";        // Decoded a Type 2 (Arithmetic) instruction so parse the first Word Select bit.                                    
                state_val[s010x]        : state2string = "s010x";       // Parse the second Word Select bit.                                                                                
                state_val[s0100x]       : state2string = "s0100x";      // Parse the third Word Select bit.                                                                                 
                state_val[sArithP]      : state2string = "sArithP";     // Word Select is 'Pointer Only'.                                                                                   
                state_val[sArithWP]     : state2string = "sArithWP";    // Word Select is 'Up to Pointer'.                                                                                  
                state_val[sArithWait]   : state2string = "sArithWait";  // Wait for arithmetic instruction to complete in the A&R chip.                                                     
                // Type 3 (Status Flag) Instructions                                                                                                                                              
                state_val[s0010x]       : state2string = "s0010x";      // Decoded a Type 3 (Status) instruction so parse the next two bits to narrow it down.                              
                state_val[s00100x]      : state2string = "s00100x";     // Determine whether the instruction is Set Status Flag (F=00) or Reset Status Flag (F=10).                         
                state_val[sSst]         : state2string = "sSst";        // Decoded the Set Status Flag instruction.                                                                         
                state_val[sSstDecr]     : state2string = "sSstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sSstThis]     : state2string = "sSstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sRst]         : state2string = "sRst";        // Decoded the Reset Status Flag instruction.                                                                       
                state_val[sRstDecr]     : state2string = "sRstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sRstThis]     : state2string = "sRstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[s00101x]      : state2string = "s00101x";     // Determine whether the instruction is Interrogate Status Flag (F=01) or Clear All Status Flags (F=11).            
                state_val[sIst]         : state2string = "sIst";        // Decoded the Interrogate Status Flag instruction.                                                                 
                state_val[sIstDecr]     : state2string = "sIstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sIstThis]     : state2string = "sIstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sStDone]      : state2string = "sStDone";     // Wait for the last status bit position.                                                                           
                state_val[sCst]         : state2string = "sCst";        // Decoded the Clear All Status Flags instruction.                                                                  
                state_val[sCstDecr]     : state2string = "sCstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sCstThis]     : state2string = "sCstThis";    // The selected status bit is now at the end of the 28-bit shift register so begin clearing to the end of the field.
                // Type 4 (Pointer) Instructions                                                                                                                                            
                state_val[s0011x]       : state2string = "s0011x";      // Decoded a Type 4 (Pointer) instruction so parse the next two bits to narrow it down.                             
                state_val[s00110x]      : state2string = "s00110x";     // Determine whether the instruction is Set Pointer (F=00) or Interrogate Pointer (F=10).                           
                state_val[sSpt]         : state2string = "sSpt";        // Decoded the Set Pointer instruction.                                                                             
                state_val[sSptDone]     : state2string = "sSptDone";    // Set Pointer is done so wait until it's time to enter the address output state.                                   
                state_val[sIpt]         : state2string = "sIpt";        // Decoded the Interrogate Pointer instruction.                                                                     
                state_val[sIptSetC]     : state2string = "sIptSetC";    // If pointer is equal to this instruction's P field then set carry.      
                state_val[sIptRstC]     : state2string = "sIptRstC";    // If pointer is NOT equal to this instruction's P field then RESET carry.
                state_val[s00111x]      : state2string = "s00111x";     // Determine whether the instruction is Decrement Pointer (F=01) or Increment Pointer (F=11).                       
                state_val[sPtd]         : state2string = "sPtd";        // Decoded the Decrement Pointer instruction.                                                                       
                state_val[sPtdNow]      : state2string = "sPtdNow";     // The four clock periods in which the pointer is decremented.                                                      
                state_val[sPti]         : state2string = "sPti";        // Decoded the Increment Pointer instruction.                                                                       
                state_val[sPtiNow]      : state2string = "sPtiNow";     // The four clock periods in which the pointer is incremented.                                                      
                // Type 5 (Data Entry/Display) Instructions                                                                                                                                 
                state_val[s0001x]       : state2string = "s0001x";      // Decoded a Type 5 (Data Entry/Display) instruction so parse the next two bits to narrow it down.
                state_val[s00011x]      : state2string = "s00011x";     // Determine whether the instruction is LDC (F=01) or other (F=11).
                state_val[sLdc]         : state2string = "sLdc";        // Decoded the LOAD CONSTANT (LDC) instruction.  Generate Word Select at pointer-only then decrement pointer.
                state_val[sType5Wait]   : state2string = "sType5Wait";  // All Type 5 instructions other than LDC are executed in the the A&R circuit so wait here for completion.
                // Type 6 (Misc) Instructions                                                                                                                                               
                state_val[sType6]       : state2string = "sType6";      // Decoded a Type 6 instruction so parse the next two bits to narrow it down.                                       
                state_val[s000010x]     : state2string = "s000010x";    // Determine whether the instruction is ROM Select (F=00) or one of two Key Entry instructions (F=10).              
                state_val[sRomSel]      : state2string = "sRomSel";     // Instruction is ROM Select so wait here while the ROMs execute the function.                                      
                state_val[s0000101x]    : state2string = "s0000101x";   // Determine whether the instruction is External Key-Code Entry or Keyboard Entry.                                  
                state_val[sExtKey]      : state2string = "sExtKey";     // Decoded the External Key-Code Entry instruction that's not supported in the HP-35 so just wait.                  
                state_val[sKey]         : state2string = "sKey";        // Decoded the Keyboard Entry instruction.                                                                          
                state_val[sKeyOut]      : state2string = "sKeyOut";     // Shift the contents of the Key-Code Buffer 56 to the address line, Ia.                                            
                state_val[s000011x]     : state2string = "s000011x";    // Determine whether the instruction is Return from Subroutine (F=01) or Data Store (F=11).                         
                state_val[sRet]         : state2string = "sRet";        // Decoded the Subroutine Return instruction.                                                                       
                state_val[sRetOut]      : state2string = "sRetOut";     // Shift the contents of the Return Address 60 shift register field to the address line, Ia.                        
                state_val[sDataStore]   : state2string = "sDataStore";  // Decoded a Data Storage instruction that's not part of the C&T chip so just wait. (Not supported in the HP-35.)   
            endcase
        end
    endfunction

    // ---------------------------------------------------------------------------------------------
    // For the specified key, this function returns the five COL inputs as they would be driven by
    // the ROWs of the keyboard matrix when that key is pressed.  The keyboard matrix is connected
    // as follows:
    //                                                   keydown
    //                                                     ^
    //                                                     |
    //                                 ----------------------------------------
    //                               /                                          \
    //      System Counter [2:0]--> /   110      100      011      010      000  \ 
    //                             /   COL6     COL4     COL3     COL2     COL0   \
    //                             ------------------------------------------------
    //    ----------------------         ^        ^        ^        ^        ^ 
    //                          |        |        |        |        |        |
    //              000 -> ROW0 |->----[x^y]----[log]----[ln]-----[e^x]----[clr]   
    //                          |        |        |        |        |        |
    //              101 -> ROW5 |->--[sqrt(x)]--[arc]----[sin]----[cos]----[tan]   
    //                          |        |        |        |        |        |
    //              001 -> ROW1 |->----[1/x]---[x<->y]--[roll]----[sto]----[rcl]   
    //                          |        |        |        |        |        |
    //    System    111 -> ROW7 |->---[Enter]-----|------[chs]----[eex]----[clx]   
    //    Counter               |        |        |        |        |        |
    //    [5:3]     110 -> ROW6 |->---[Minus]----[7]------[8]------[9]-------|-  
    //                          |        |        |        |        |        |
    //              010 -> ROW2 |->---[Plus]-----[4]------[5]------[6]-------|-
    //                          |        |        |        |        |        |
    //              011 -> ROW3 |->---[Mult]-----[1]------[2]------[3]-------|-
    //                          |        |        |        |        |        |
    //              100 -> ROW4 |->--[Divide]----[0]------[.]-----[pi]-------|-
    //                          |
    //    ---------------------- 
    //
    function [4:0] columnsForKeyPress;
        input string keyName;
        input [7:0] row;
        begin : func_keyMatrix
            case (keyName)
                // Column 6                              C6      C4      C3      C2      C0
                "key_x^y"       : columnsForKeyPress = {row[0], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 0,6
                "key_sqrt_x"    : columnsForKeyPress = {row[5], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 5,6
                "key_1/x"       : columnsForKeyPress = {row[1], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 1,6
                "key_enter"     : columnsForKeyPress = {row[7], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 7,6
                "key_minus"     : columnsForKeyPress = {row[6], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 6,6
                "key_plus"      : columnsForKeyPress = {row[2], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 2,6
                "key_mult"      : columnsForKeyPress = {row[3], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 3,6
                "key_div"       : columnsForKeyPress = {row[4], 1'b0,   1'b0,   1'b0,   1'b0};   // Row,Col = 4,6
                // Column 4                              C6      C4      C3      C2      C0      
                "key_log"       : columnsForKeyPress = {1'b0, row[0],   1'b0,   1'b0,   1'b0};   // Row,Col = 0,4
                "key_arc"       : columnsForKeyPress = {1'b0, row[5],   1'b0,   1'b0,   1'b0};   // Row,Col = 5,4
                "key_x<->y"     : columnsForKeyPress = {1'b0, row[1],   1'b0,   1'b0,   1'b0};   // Row,Col = 1,4
                "key_spare74"   : columnsForKeyPress = {1'b0, row[7],   1'b0,   1'b0,   1'b0};   // Row,Col = 7,4
                "key_7"         : columnsForKeyPress = {1'b0, row[6],   1'b0,   1'b0,   1'b0};   // Row,Col = 6,4
                "key_4"         : columnsForKeyPress = {1'b0, row[2],   1'b0,   1'b0,   1'b0};   // Row,Col = 2,4
                "key_1"         : columnsForKeyPress = {1'b0, row[3],   1'b0,   1'b0,   1'b0};   // Row,Col = 3,4
                "key_0"         : columnsForKeyPress = {1'b0, row[4],   1'b0,   1'b0,   1'b0};   // Row,Col = 4,4
                // Column 3                              C6      C4      C3      C2      C0      
                "key_ln"        : columnsForKeyPress = {1'b0,   1'b0, row[0],   1'b0,   1'b0};   // Row,Col = 0,3
                "key_sin"       : columnsForKeyPress = {1'b0,   1'b0, row[5],   1'b0,   1'b0};   // Row,Col = 5,3
                "key_roll"      : columnsForKeyPress = {1'b0,   1'b0, row[1],   1'b0,   1'b0};   // Row,Col = 1,3
                "key_chs"       : columnsForKeyPress = {1'b0,   1'b0, row[7],   1'b0,   1'b0};   // Row,Col = 7,3
                "key_8"         : columnsForKeyPress = {1'b0,   1'b0, row[6],   1'b0,   1'b0};   // Row,Col = 6,3
                "key_5"         : columnsForKeyPress = {1'b0,   1'b0, row[2],   1'b0,   1'b0};   // Row,Col = 2,3
                "key_2"         : columnsForKeyPress = {1'b0,   1'b0, row[3],   1'b0,   1'b0};   // Row,Col = 3,3
                "key_."         : columnsForKeyPress = {1'b0,   1'b0, row[4],   1'b0,   1'b0};   // Row,Col = 4,3
                // Column 2                              C6      C4      C3      C2      C0      
                "key_e^x"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[0],   1'b0};   // Row,Col = 0,2
                "key_cos"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[5],   1'b0};   // Row,Col = 5,2
                "key_sto"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[1],   1'b0};   // Row,Col = 1,2
                "key_eex"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[7],   1'b0};   // Row,Col = 7,2
                "key_9"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[6],   1'b0};   // Row,Col = 6,2
                "key_6"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[2],   1'b0};   // Row,Col = 2,2
                "key_3"         : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[3],   1'b0};   // Row,Col = 3,2
                "key_pi"        : columnsForKeyPress = {1'b0,   1'b0,   1'b0, row[4],   1'b0};   // Row,Col = 4,2
                // Column 0                              C6      C4      C3      C2      C0      
                "key_clr"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[0]};   // Row,Col = 0,0
                "key_tan"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[5]};   // Row,Col = 5,0
                "key_rcl"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[1]};   // Row,Col = 1,0
                "key_clx"       : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[7]};   // Row,Col = 7,0
                "key_spr60"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[6]};   // Row,Col = 6,0
                "key_spr20"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[2]};   // Row,Col = 2,0
                "key_spr30"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[3]};   // Row,Col = 3,0
                "key_spr40"     : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0, row[4]};   // Row,Col = 4,0
                // No key pressed                        C6      C4      C3      C2      C0      
                default         : columnsForKeyPress = {1'b0,   1'b0,   1'b0,   1'b0,   1'b0};   // No key press.
            endcase
        end
    endfunction


    function [1:0] fullAdd;
    // output  sum;         // Sum
    // output  co;          // Carry out
        input   x;           // Augend
        input   y;           // Addend
        input   ci;          // Carry In
        begin : func_fullAdd
            reg sum = 1'b0;
            reg co  = 1'b0;
            sum = x ^ y ^ ci;
            co  = x & y | x & ci | y & ci;
            fullAdd = {sum, co};
        end
    endfunction

    task pressAndServiceKey;
        input string keyName;
        begin : task_serv_key 
            pressingThisKey <= keyName; #1ms; pressingThisKey <= "";  // Press - Wait - No press.
            @(posedge PHI2);
            issueInstruction("ISTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            issueInstruction("RSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            issueInstruction("KEYJMP", "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            #280us;
        end
    endtask


// -------------------------------------------------------------------------------------------------
// Test Processes

    // Generate 200 kHz clock
    always #2.5us PHI2 = ~PHI2;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    initial begin
        PWO = 1'b0;
        #10;
        PWO = 1'b1;
        #300us;
        PWO = 1'b0;
        #10ms;
//        wait;
    end

    // -----------------------------------------------
    // Two-Process System Counter
    // --
    // Combinational next count process...
    always @* begin : proc_nextsyscount
        if (~SYNC & SYNCr)  // Falling edge of SYNC
            nextSysCount = 0;
        else
            nextSysCount = sysCount + 1;
        // --
        {T1, T2, T3, T4} <= {inst_ct.T4, inst_ct.T1, inst_ct.T2, inst_ct.T3};
    end
    // --
    // Registered count process...
    always@(posedge PHI2 or posedge rst) begin : proc_syscount
        SYNCr <= SYNC;
        if (rst) begin
            sysCount <= 0;
            digitCount <= 0;
        end
        else begin
            sysCount <= nextSysCount;
            digitCount <= nextSysCount / 4;
        end
        s_dut_state <= state2string(inst_ct.inst_mpc.next); // For fdisplay.
    end
    // --
    // End Two-Process System Counter
    // -----------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Generate Serial Instruction (IS) and Word Select (WS) Pulse for ROM Emulation
    always@(posedge PHI2 or posedge rst) begin : proc_sync
        // --
        if (rst) 
            is_shifter <= {1'b0, {10{1'b0}}, {33{1'b0}}, 1'b1, {11{1'b0}}};
        else if (nextSysCount == 0)                                                 // If we're about to start the next word cycle, and ...
            if (load_instruction)                                                   //    if a new instruction is issued, then ...
                is_shifter <= {1'b0, instruction, {33{1'b0}}, 1'b1, {11{1'b0}}};    //       load the new instruction in bits [54:45] and set bit [11] for the display,
            else                                                                    //    otherwise no new instruction was issued, so ...
                is_shifter <= {1'b0, `NOP, {33{1'b0}}, 1'b1, {11{1'b0}}};           //       load a NOP and set bit [11] for the display.
        else                                                                        // Otherwise we're not about to start the next word cycle, so ...
            is_shifter <= {1'b0, is_shifter[55:1]};                                 //    shift the IS shifter to the right.
        // --
        if (rst) begin
            wsBeginFetch    <= -1;
            wsEndFetch     <= -1;
            wsBeginExecute  <= -1;
            wsEndExecute   <= -1;
        end
        else begin
            if (load_instruction) begin         // If a new instruction is issued, then ...
                wsBeginFetch <= wsBegin;        //    hold the digit number where WS begins and
                wsEndFetch   <= wsEnd;          //    hold the digit number where WS ends.
            end                                 //
            if (nextSysCount == 0) begin        // Otherwise, if we're about to start the next word cycle, then ...
                wsBeginExecute <= wsBeginFetch; //    load the digit number where WS begins and
                wsEndExecute   <= wsEndFetch;   //    load the digit number where WS ends.
            end
        end
        //--
    end

    assign IS = is_shifter[0];
    assign WS_rom = ((digitCount >= wsBeginExecute) && (digitCount <= wsEndExecute))? 1'b1 : 1'bz;
    assign WS = WS_rom; // Connect ROM-generated word select to WS net.
    assign WS = WS_ct;  // Connect C&T-generated word select to WS net.
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Process to simulate a keypress.  
    // 
    // For the specified key, this process connects one of the eight ROW outputs to one of the five
    // COL inputs based on the HP-35 keyboard matrix layout.
    //   
    // To press a key, simply assign the appropriate string value to pressingThisKey.
    // For example:
    //      pressingThisKey <= "key_sqrt_x";
    //      #1ms; 
    //      pressingThisKey <= "";  // No press.
    // 
    always @* begin : proc_pressKey
        {COL6,COL4,COL3,COL2,COL0} = columnsForKeyPress(pressingThisKey,{ROW7,ROW6,ROW5,ROW4,ROW3,ROW2,ROW1,ROW0});
    end
    // ---------------------------------------------------------------------------------------------

    // -------------------------------------------------------------------------
    // Test results pipeline
    always@(posedge PHI2 or posedge rst) begin : proc_tpipe
        if (rst) begin
            fetch_instruction   <= 1'b0;
            exec_instruction    <= 1'b0;
            grab_em             <= 1'b0;
            compare_em          <= 1'b0;
            save_em             <= 1'b0;


//            good_count      <= 0;
//            bad_count       <= 0;
//
//            clearInstructionCounts();
        end
        else begin
            if (nextSysCount == 0) begin
                fetch_instruction <= load_instruction;
                exec_instruction  <= fetch_instruction;
                grab_em  <= exec_instruction;
            end
            else begin
                // 'grab_em' pulses simultaneously with START.
                grab_em  <= 1'b0;  // End the pulse.  
            end

            compare_em <= grab_em;
            save_em <= compare_em;

            if (nextSysCount == 0) begin
            end

            if (grab_em) begin
                // Capture expected results:
                // Capture actual results:
            end
        end
    end
    // -------------------------------------------------------------------------
//
//    assign all_regs_match = 1'b1;
//
    // ---------------------------------------------------------------------------------------------
    // Debug:  Capture and hold the register contents at the midpoint and end of every word cycle.
    //
    // The shift register contents at bit time 27 and bit time 55:
    // 
    //          |<-- ROM Address 58 --->|<--------- Status Bits 62 -------->|<- Return Address 60 ->|
    //          |                       |                                   |                       |
    // sr[28:1] |28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1|
    //
    always@(posedge PHI2 or posedge rst) begin : proc_hold_regs
        if (rst) begin
            ROM_Address_58_HoldReg_27       <= 'b0;
            Status_Bits_62_HoldReg_27       <= 'b0;
            Return_Address_60_HoldReg_27    <= 'b0;
            ROM_Address_58_HoldReg_55       <= 'b0;
            Status_Bits_62_HoldReg_55       <= 'b0;
            Return_Address_60_HoldReg_55    <= 'b0;
            ia_address_HoldReg_27           <= 'b0;
        end
        else if (sysCount == 27) begin
            ROM_Address_58_HoldReg_27       <= inst_ct.sr[28:21];
            Status_Bits_62_HoldReg_27       <= inst_ct.sr[20:9];
            Return_Address_60_HoldReg_27    <= inst_ct.sr[8:1];
            ia_address_HoldReg_27           <= ia_debug_shifter;
        end
        else if (sysCount == 55) begin
            ROM_Address_58_HoldReg_55       <= inst_ct.sr[28:21];
            Status_Bits_62_HoldReg_55       <= inst_ct.sr[20:9]; 
            Return_Address_60_HoldReg_55    <= inst_ct.sr[8:1];  
        end
        // --
        if (rst)
            ia_debug_shifter    <= 'b0;
        else
            ia_debug_shifter    <= {IA,ia_debug_shifter[7:1]};  // Shift right
        // --
    end
    // End debug
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Dump to a file the 56-clock state machine sequence for the most recently issued instruction.
    always@* begin
        if (fetch_instruction && sysCount == 27) begin
            $fdisplay(fd, "Inst: %s, WS: %s, Ptr: %0d, Addr: %0d, N: %0d, P: %0d, ROM: %0d, CARRY_FF: %b",
                      s_mnemonic,
                      s_fs,
                      i_ptr,    
                      i_address,
                      i_n_field,
                      i_p_field,
                      i_romnum,
                      inst_ct.CARRY_FF
                      );
            $fdisplay(fd, "");
            $fdisplay(fd, "                                               i s C");
            $fdisplay(fd, "                                S   A   P      s t A");
            $fdisplay(fd, "                                T   R U T      e q R");
            $fdisplay(fd, "                    I I         D I I P O      q z R                   p   S");
            $fdisplay(fd, "            R T P P P P S I R S E S T 2 N B J  p e Y b b b b b b b     w   Y");
            $fdisplay(fd, "            E K T T T T P S S S C T H P L R S  t r F 5 5 4 3 2 1 1 b I o I N                                                               state   ");
            $fdisplay(fd, "    LineNum T R I D S R T T T T N W W T Y H B  r o F 5 4 5 5 6 8 4 5 S r A C state                                                         name     sysCount");
            $fdisplay(fd, "");

            for (i = 1; i <= 56; i = i + 1) begin
                @(posedge PHI2);
                $fdisplay(fd, "%d %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b  %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %10s %2d", 
                          i,
                          inst_ct.RET,
                          inst_ct.TKR,
                          inst_ct.PTI,
                          inst_ct.PTD,
                          inst_ct.IPTS,
                          inst_ct.IPTR,
                          inst_ct.SPT,
                          inst_ct.IST,
                          inst_ct.RST,
                          inst_ct.SST,
                          inst_ct.STDECN,
                          inst_ct.ISTW,
                          inst_ct.ARITHW,
                          inst_ct.UP2PT,
                          inst_ct.PTONLY,
                          inst_ct.BRH,
                          inst_ct.JSB,
                          inst_ct.inst_mpc.is_eq_ptr,
                          inst_ct.stqzero,
                          inst_ct.CARRY_FF,
                          inst_ct.b55,
                          inst_ct.b54,
                          inst_ct.b45,
                          inst_ct.b35,
                          inst_ct.b26,
                          inst_ct.b18,
                          inst_ct.b14,
                          inst_ct.b5,
                          IS,
                          inst_ct.pwor,
                          IA,
                          SYNC,
                          inst_ct.inst_mpc.state,
                          s_dut_state,
                          sysCount
                         );
            end
            $fdisplay(fd, "------------------------------------------------------------------------------------------------------------------------------------------------");
        end
    end
    // ---------------------------------------------------------------------------------------------

    initial begin
        PHI2        = 1'b0;
        fd = $fopen("ct_output.txt", "w");
        sim_end_pulse = 1'b0;
        loop_done_1 = 1'b0;
        loop_done_2 = 1'b0;
        loop_done_3 = 1'b0;
        wait (rst == 1'b1);
        wait (rst == 1'b0);
        wait (PWO == 1'b0);
        @(posedge PHI2);
        sim_start_pulse = 1'b1;
        @(posedge PHI2);
        sim_start_pulse = 1'b0;
        @(posedge PHI2);
//        afterCall_ready = 1'b0;

        // =====================================================
        // **** Test Pointer-Generated Word Select ****
        // =====================================================
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        for (setP = 0; setP <= 13; setP = setP+1) begin
            issueInstruction("SPTP"  , "-",  -1, -1, -1,  setP, -1);    // Point to specified digit.
            issueInstruction("CLRA"  , "P",  -1, -1, -1, -1, -1);       // Issue Type 2 (Arithmetic) instruction for digit specified by pointer (P) using C&T-generated word select.
            issueInstruction("CLRA"  , "W",  -1, -1, -1, -1, -1);       // Issue Type 2 (Arithmetic) instruction for entire word using ROM-generated word select.
            issueInstruction("CLRA"  , "WP", -1, -1, -1, -1, -1);       // Issue Type 2 (Arithmetic) instruction for digits 0 to pointer (WP) using C&T-generated word select.
            wait (nextSysCount == 0); 
        end

        // =============================================================
        // **** Test Keyboard Scan and Jump to Key Code Instruction ****
        // =============================================================
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        N = 0;
        //Row 0
        pressAndServiceKey("key_clr");
        pressAndServiceKey("key_e^x");
        pressAndServiceKey("key_ln");
        pressAndServiceKey("key_log");
        pressAndServiceKey("key_x^y");
        //Row 1
        pressAndServiceKey("key_rcl");
        pressAndServiceKey("key_sto");
        pressAndServiceKey("key_roll");
        pressAndServiceKey("key_x<->y");
        pressAndServiceKey("key_1/x");
        //Row 2
        pressAndServiceKey("key_6");
        pressAndServiceKey("key_5");
        pressAndServiceKey("key_4");
        pressAndServiceKey("key_plus");
        //Row 3
        pressAndServiceKey("key_3");
        pressAndServiceKey("key_2");
        pressAndServiceKey("key_1");
        pressAndServiceKey("key_mult");
        //Row 4
        pressAndServiceKey("key_pi");
        pressAndServiceKey("key_.");
        pressAndServiceKey("key_0");
        pressAndServiceKey("key_div");
        //Row 5
        pressAndServiceKey("key_tan");
        pressAndServiceKey("key_cos");
        pressAndServiceKey("key_sin");
        pressAndServiceKey("key_arc");
        pressAndServiceKey("key_sqrt_x");
        //Row 6
        pressAndServiceKey("key_9");
        pressAndServiceKey("key_8");
        pressAndServiceKey("key_7");
        pressAndServiceKey("key_minus");
        //Row 7
        pressAndServiceKey("key_clx");
        pressAndServiceKey("key_eex");
        pressAndServiceKey("key_chs");
        pressAndServiceKey("key_enter");

        // =====================================================
        // **** Test Jump Subroutine and Return ****
        // =====================================================
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        jsrTarget = 55;
        issueInstruction("JSR"  , "-",  -1, jsrTarget, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("RET"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // =====================================================
        // **** Test Conditional Branch ****
        // =====================================================
        // Set pointer to a value, then interrogate the pointer for the same value; this should reset the carry FF and take the branch.
        setP = 1;
        testP = 2;
        branchTarget = 42;
        issueInstruction("SPTP"  , "-",  -1, -1, -1,  setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("IPTP"   , "-",  -1, -1, -1, setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("CBRH"  , "-",  -1, branchTarget, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // Jump back to 0 to set up for the next test.
        branchTarget = 0;
        issueInstruction("CBRH"  , "-",  -1, branchTarget, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // Now interrogate the pointer for a different value; this should set the carry FF and not take the branch.
        branchTarget = 42;
        issueInstruction("IPTP"   , "-",  -1, -1, -1, testP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("CBRH"  , "-",  -1, branchTarget, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // =====================================================
        // **** Test Pointer Ops ****
        // =====================================================

        @(posedge PHI2);
        // Set pointer to P
        for (P = 0; P <= 15; P = P+1) begin
            issueInstruction("SPTP"  , "-",  -1, -1, -1,  P, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        // Increment pointer
        for (j = 0; j <= 15; j = j+1) begin
            issueInstruction("PTI"  , "-",  -1, -1, -1,  -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        // Decrement pointer
        for (j = 0; j <= 15; j = j+1) begin
            issueInstruction("PTD"  , "-",  -1, -1, -1,  -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        for (setP = 0; setP <= 15; setP = setP+1) begin
            issueInstruction("SPTP"  , "-",  -1, -1, -1,  setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            // Interrogate all pointers
            for (testP = 0; testP <= 15; testP = testP+1) begin
                issueInstruction("IPTP"   , "-",  -1, -1, -1, testP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            end
            issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end

        // =====================================================
        // **** Test Status Flag Ops ****
        // =====================================================

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("SSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("RSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("ISTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("CSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        wait (nextSysCount == 0); 
        @(posedge PHI2);
        wait (nextSysCount == 0); 
        @(posedge PHI2);

//         for (i = 1; i <= TestIterations/10; i = i+1) begin
//             for (j = 1; j <= 10; j = j+1) begin
//                 simRandomType2();
//                 wait (nextSysCount == 0); 
//                 afterCall_ready = 1'b0;
// //                wait (nextSysCount == 1); // Force a gap between instructions (optional).
//             end
//         end
        loop_done_1 = 1'b1;
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_2 = 1'b1;
        wait (nextSysCount != 0); // Advance into the machine cycle.
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_3 = 1'b1;


        repeat (56) @(posedge PHI2);
        repeat (56) @(posedge PHI2);
        repeat (10) @(posedge PHI2);

        sim_end_pulse = 1'b1;
        @(posedge PHI2);
        sim_end_pulse = 1'b0;
        @(posedge PHI2);

//        $fclose(fd);
        $stop;
    end

// =================================================================================================
// Instances
// =================================================================================================

    control_and_timing_16 #(
        .NewArch    (0)             // parameter    // 1 = Use new architecture, 0 = Like the patent.
    )
    inst_ct(
        // Output Ports
        .IA      (IA),              // output           // ('569 item 32) Serial ROM Address                          
        .WS      (WS_ct),           // inout            // ('569 item 30) Pointer Word Select                         
        .SYNC    (SYNC),            // output reg       // ('569 item 26) Word Cycle Sync                             
        .ROW0    (ROW0),            // output reg       // ('569 item 50) Keyboard Row Driver 0                       
        .ROW1    (ROW1),            // output reg       //                   "      "    "    1                       
        .ROW2    (ROW2),            // output reg       //                   "      "    "    2                       
        .ROW3    (ROW3),            // output reg       //                   "      "    "    3                       
        .ROW4    (ROW4),            // output reg       //                   "      "    "    4                       
        .ROW5    (ROW5),            // output reg       //                   "      "    "    5                       
        .ROW6    (ROW6),            // output reg       //                   "      "    "    6                       
        .ROW7    (ROW7),            // output reg       //                   "      "    "    7                       
        // Input Ports                                                                                                
        .PHI1    (1'b0),            // input            // Bit-Rate Clock Input, Phase 1, not used.              
        .PHI2    (PHI2),            // input            // Bit-Rate Clock Input, Phase 2                              
        .PWO     (PWO),             // input            // ('569 item 36) PoWer On pulse.  '569 (7) "As the system    
                                                        //    power comes on, the PWO signal is held at               
                                                        //    logic l for at least 20 milliseconds."                  
        .IS      (IS),              // input            // ('569 item 28) Serial Instruction from ROM Output          
        .CARRY   (1'b0),            // input            // ('569 item 34) Carry flag from Arithmetic & Register block.
        .COL0    (COL0),            // input            // ('569 item 54) Keyboard Column Input 0                     
        .COL2    (COL2),            // input            //                   "       "      "   2                     
        .COL3    (COL3),            // input            //                   "       "      "   3                     
        .COL4    (COL4),            // input            //                   "       "      "   4                     
        .COL6    (COL6)             // input            //                   "       "      "   6                     
    );

// =================================================================================================

endmodule



