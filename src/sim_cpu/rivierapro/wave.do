onerror { resume }
set curr_transcript [transcript]
transcript off

add wave /tb/rst
add wave /tb/nextSysCount
add wave -literal /tb/sysCount
add wave /tb/digitCount
add wave -logic /tb/WS_rom
add wave /tb/WS
add wave /tb/IA
add wave /tb/WS
add wave /tb/SYNC
add wave /tb/ROW0
add wave /tb/ROW1
add wave /tb/ROW2
add wave /tb/ROW3
add wave /tb/ROW4
add wave /tb/ROW5
add wave /tb/ROW6
add wave /tb/ROW7
add wave /tb/PHI1
add wave /tb/PHI2
add wave /tb/PWO
add wave /tb/IS
add wave /tb/CARRY
add wave /tb/COL0
add wave /tb/COL2
add wave /tb/COL3
add wave /tb/COL4
add wave /tb/COL6
add wave /tb/i
add wave /tb/s_mnemonic
add wave /tb/s_fs
add wave /tb/load_instruction
add wave /tb/fetch_instruction
add wave /tb/exec_instruction
add wave /tb/instruction
add wave /tb/is_shifter
add wave /tb/sysCount
add wave /tb/nextSysCount
add wave -oct /tb/ROM_Address_58_HoldReg_27
add wave -unsigned -literal /tb/Status_Bits_62_HoldReg_27
add wave -unsigned -literal /tb/Return_Address_60_HoldReg_27
add wave -unsigned -literal /tb/ia_address_HoldReg_27
add wave -oct /tb/ROM_Address_58_HoldReg_55
add wave -unsigned /tb/Status_Bits_62_HoldReg_55
add wave -unsigned /tb/Return_Address_60_HoldReg_55
add wave /tb/holdRegA
add wave /tb/holdRegB
add wave /tb/holdRegC
add wave /tb/holdRegD
add wave /tb/holdRegE
add wave /tb/holdRegF
add wave /tb/holdRegM
add wave /tb/i_ptrHoldingReg_27
add wave /tb/i_ptrHoldingReg_55
add wave -expand -vgroup {Carry Debug} \
	/tb/START \
	/tb/s_dut_state \
	/tb/WS_rom \
	/tb/WS_ctc \
	/tb/WS \
	( -logic /tb/CARRY ) \
	( -logic /tb/T1 ) \
	( -logic /tb/T2 ) \
	( -logic /tb/T3 ) \
	( -logic /tb/T4 ) \
	/tb/inst_ctc/CARRY_FF \
	/tb/inst_ctc/ws_out \
	/tb/inst_ctc/ws_req \
	/tb/inst_ctc/ws_oe \
	/tb/inst_ctc/ws_in \
	/tb/inst_ctc/clr_ws \
	/tb/inst_ctc/set_ws \
	/tb/inst_ctc/arith_cyc \
	/tb/inst_ctc/ipt_cyc \
	/tb/inst_ctc/ist_cyc \
	/tb/inst_ctc/arith_cset \
	/tb/inst_ctc/arith_crst \
	/tb/inst_ctc/ist_cset \
	/tb/inst_ctc/ist_crst \
	/tb/inst_ctc/ipt_cset \
	/tb/inst_ctc/ipt_crst \
	/tb/inst_ctc/T1 \
	/tb/inst_ctc/T2 \
	/tb/inst_ctc/T3 \
	/tb/inst_ctc/T4
add wave -expand -vgroup Keyboard-Stuff \
	/tb/sysCount \
	/tb/pressingThisKey \
	/tb/inst_ctc/kdn \
	/tb/inst_ctc/keydown \
	/tb/inst_ctc/newkey \
	/tb/inst_ctc/keyset_s0 \
	( -oct /tb/inst_ctc/keybuf ) \
	/tb/inst_ctc/CARRY_FF
add wave -vgroup /tb/inst_ctc/inst_mpc \
	( -logic /tb/inst_ctc/inst_mpc/JSB ) \
	( -logic /tb/inst_ctc/inst_mpc/BRH ) \
	( -logic /tb/inst_ctc/inst_mpc/PTONLY ) \
	( -logic /tb/inst_ctc/inst_mpc/UP2PT ) \
	( -logic /tb/inst_ctc/inst_mpc/ARITHW ) \
	( -logic /tb/inst_ctc/inst_mpc/ISTW ) \
	( -logic /tb/inst_ctc/inst_mpc/STDECN ) \
	( -logic /tb/inst_ctc/inst_mpc/SST ) \
	( -logic /tb/inst_ctc/inst_mpc/RST ) \
	( -logic /tb/inst_ctc/inst_mpc/IST ) \
	( -logic /tb/inst_ctc/inst_mpc/SPT ) \
	( -logic /tb/inst_ctc/inst_mpc/IPTR ) \
	( -logic /tb/inst_ctc/inst_mpc/IPTS ) \
	( -logic /tb/inst_ctc/inst_mpc/PTD ) \
	( -logic /tb/inst_ctc/inst_mpc/PTI ) \
	( -logic /tb/inst_ctc/inst_mpc/TKR ) \
	( -logic /tb/inst_ctc/inst_mpc/RET ) \
	( -logic /tb/inst_ctc/inst_mpc/PHI2 ) \
	( -logic /tb/inst_ctc/inst_mpc/pwor ) \
	( -logic /tb/inst_ctc/inst_mpc/IS ) \
	( -logic /tb/inst_ctc/inst_mpc/b5 ) \
	( -logic /tb/inst_ctc/inst_mpc/b14 ) \
	( -logic /tb/inst_ctc/inst_mpc/b18 ) \
	( -logic /tb/inst_ctc/inst_mpc/b26 ) \
	( -logic /tb/inst_ctc/inst_mpc/b35 ) \
	( -logic /tb/inst_ctc/inst_mpc/b45 ) \
	( -logic /tb/inst_ctc/inst_mpc/b54 ) \
	( -logic /tb/inst_ctc/inst_mpc/b55 ) \
	( -logic /tb/inst_ctc/inst_mpc/CARRY_FF ) \
	( -logic /tb/inst_ctc/inst_mpc/stqzero ) \
	( -logic /tb/inst_ctc/inst_mpc/is_eq_ptr ) \
	( -literal /tb/inst_ctc/inst_mpc/state ) \
	( -literal /tb/inst_ctc/inst_mpc/next )
add wave -expand -vgroup /tb/inst_ctc \
	/tb/inst_ctc/IA \
	/tb/inst_ctc/WS \
	/tb/inst_ctc/SYNC \
	/tb/inst_ctc/ROW0 \
	/tb/inst_ctc/ROW1 \
	/tb/inst_ctc/ROW2 \
	/tb/inst_ctc/ROW3 \
	/tb/inst_ctc/ROW4 \
	/tb/inst_ctc/ROW5 \
	/tb/inst_ctc/ROW6 \
	/tb/inst_ctc/ROW7 \
	/tb/inst_ctc/PHI1 \
	/tb/inst_ctc/PHI2 \
	/tb/inst_ctc/PWO \
	/tb/inst_ctc/IS \
	/tb/inst_ctc/CARRY \
	/tb/inst_ctc/COL0 \
	/tb/inst_ctc/COL2 \
	/tb/inst_ctc/COL3 \
	/tb/inst_ctc/COL4 \
	/tb/inst_ctc/COL6 \
	/tb/inst_ctc/debug_lfsr_fb_xnor \
	/tb/inst_ctc/debug_lfsr_fb_xor \
	/tb/inst_ctc/pwor \
	/tb/inst_ctc/q \
	/tb/inst_ctc/T1 \
	/tb/inst_ctc/T2 \
	/tb/inst_ctc/T3 \
	/tb/inst_ctc/T4 \
	/tb/inst_ctc/b5 \
	/tb/inst_ctc/b14 \
	/tb/inst_ctc/b18 \
	/tb/inst_ctc/b26 \
	/tb/inst_ctc/b35 \
	/tb/inst_ctc/b45 \
	/tb/inst_ctc/b54 \
	/tb/inst_ctc/b55 \
	/tb/inst_ctc/b19_b26 \
	/tb/inst_ctc/stepaddr \
	/tb/inst_ctc/kdn \
	/tb/inst_ctc/keydown \
	/tb/inst_ctc/newkey \
	/tb/inst_ctc/keyset_s0 \
	( -oct /tb/inst_ctc/keybuf ) \
	/tb/inst_ctc/JSB \
	/tb/inst_ctc/BRH \
	/tb/inst_ctc/PTONLY \
	/tb/inst_ctc/UP2PT \
	/tb/inst_ctc/ARITHW \
	/tb/inst_ctc/ISTW \
	/tb/inst_ctc/STDECN \
	/tb/inst_ctc/SST \
	/tb/inst_ctc/RST \
	/tb/inst_ctc/IST \
	/tb/inst_ctc/SPT \
	/tb/inst_ctc/IPTR \
	/tb/inst_ctc/IPTS \
	/tb/inst_ctc/PTD \
	/tb/inst_ctc/PTI \
	/tb/inst_ctc/TKR \
	/tb/inst_ctc/RET \
	/tb/inst_ctc/sr \
	/tb/inst_ctc/abuf \
	/tb/inst_ctc/ptr \
	/tb/inst_ctc/CARRY_FF \
	/tb/inst_ctc/dSr28 \
	/tb/inst_ctc/iamux \
	/tb/inst_ctc/dPtr4 \
	/tb/inst_ctc/sum2sr \
	/tb/inst_ctc/sr2sr \
	/tb/inst_ctc/makesum \
	/tb/inst_ctc/makesum_r \
	/tb/inst_ctc/chgstat \
	/tb/inst_ctc/sr2x \
	/tb/inst_ctc/p2x \
	/tb/inst_ctc/pn2x \
	/tb/inst_ctc/one2y \
	/tb/inst_ctc/srn2y \
	/tb/inst_ctc/sr2y \
	/tb/inst_ctc/sr2ci \
	/tb/inst_ctc/co2ci \
	/tb/inst_ctc/sum2p \
	/tb/inst_ctc/sumn2p \
	/tb/inst_ctc/pwq \
	/tb/inst_ctc/ld_dig0 \
	/tb/inst_ctc/set_ws \
	/tb/inst_ctc/clr_ws \
	/tb/inst_ctc/ws_out \
	/tb/inst_ctc/ws_req \
	/tb/inst_ctc/ws_oe \
	/tb/inst_ctc/ws_in \
	/tb/inst_ctc/stq \
	/tb/inst_ctc/stqzero \
	/tb/inst_ctc/arith_cyc \
	/tb/inst_ctc/ipt_cyc \
	/tb/inst_ctc/ist_cyc \
	/tb/inst_ctc/arith_cset \
	/tb/inst_ctc/arith_crst \
	/tb/inst_ctc/ist_cset \
	/tb/inst_ctc/ist_crst \
	/tb/inst_ctc/ipt_cset \
	/tb/inst_ctc/ipt_crst \
	/tb/inst_ctc/x_in \
	/tb/inst_ctc/y_in \
	/tb/inst_ctc/c_in \
	/tb/inst_ctc/sum \
	/tb/inst_ctc/co \
	/tb/inst_ctc/cor
add wave -expand -vgroup /tb/inst_arc \
	/tb/inst_arc/A \
	/tb/inst_arc/B \
	/tb/inst_arc/C \
	/tb/inst_arc/D \
	/tb/inst_arc/E \
	/tb/inst_arc/START \
	/tb/inst_arc/CARRY \
	/tb/inst_arc/PHI1 \
	/tb/inst_arc/PHI2 \
	/tb/inst_arc/IS \
	/tb/inst_arc/WS \
	/tb/inst_arc/SYNC \
	/tb/inst_arc/BCD \
	/tb/inst_arc/isbuf \
	/tb/inst_arc/isreg \
	/tb/inst_arc/istype2 \
	/tb/inst_arc/istype5 \
	/tb/inst_arc/syncr \
	/tb/inst_arc/wsr \
	/tb/inst_arc/ws2 \
	/tb/inst_arc/ws3 \
	/tb/inst_arc/ws4 \
	/tb/inst_arc/ws1 \
	/tb/inst_arc/ds1 \
	/tb/inst_arc/dsn1 \
	/tb/inst_arc/sub \
	/tb/inst_arc/c_in \
	/tb/inst_arc/a2x \
	/tb/inst_arc/c2x \
	/tb/inst_arc/b2y \
	/tb/inst_arc/c2y \
	/tb/inst_arc/a2a \
	/tb/inst_arc/is2a \
	/tb/inst_arc/b2a \
	/tb/inst_arc/c2a \
	/tb/inst_arc/d2a \
	/tb/inst_arc/res2a \
	/tb/inst_arc/hld2a \
	/tb/inst_arc/sra \
	/tb/inst_arc/a2hld \
	/tb/inst_arc/s22hld \
	/tb/inst_arc/b2b \
	/tb/inst_arc/a2b \
	/tb/inst_arc/c2b \
	/tb/inst_arc/srb \
	/tb/inst_arc/c2c \
	/tb/inst_arc/con2c \
	/tb/inst_arc/bcd2c \
	/tb/inst_arc/a2c \
	/tb/inst_arc/b2c \
	/tb/inst_arc/d2c \
	/tb/inst_arc/m2c \
	/tb/inst_arc/res2c \
	/tb/inst_arc/src \
	/tb/inst_arc/d2d \
	/tb/inst_arc/c2d \
	/tb/inst_arc/e2d \
	/tb/inst_arc/e2e \
	/tb/inst_arc/d2e \
	/tb/inst_arc/f2e \
	/tb/inst_arc/f2f \
	/tb/inst_arc/c2f \
	/tb/inst_arc/e2f \
	/tb/inst_arc/m2m \
	/tb/inst_arc/c2m \
	/tb/inst_arc/dspt \
	/tb/inst_arc/dspn \
	/tb/inst_arc/t1 \
	/tb/inst_arc/t2 \
	/tb/inst_arc/t3 \
	/tb/inst_arc/t4 \
	/tb/inst_arc/dsbf \
	/tb/inst_arc/dsbf_dp \
	/tb/inst_arc/a \
	/tb/inst_arc/b \
	/tb/inst_arc/c \
	/tb/inst_arc/d \
	/tb/inst_arc/e \
	/tb/inst_arc/f \
	/tb/inst_arc/g \
	/tb/inst_arc/selA \
	/tb/inst_arc/selB \
	/tb/inst_arc/selC \
	/tb/inst_arc/selD \
	/tb/inst_arc/selE \
	/tb/inst_arc/selF \
	/tb/inst_arc/selM \
	/tb/inst_arc/inA \
	/tb/inst_arc/inB \
	/tb/inst_arc/inC \
	/tb/inst_arc/inD \
	/tb/inst_arc/inE \
	/tb/inst_arc/inF \
	/tb/inst_arc/inM \
	/tb/inst_arc/x_in \
	/tb/inst_arc/y_in \
	/tb/inst_arc/sum1 \
	/tb/inst_arc/sum2 \
	/tb/inst_arc/use_sum2 \
	/tb/inst_arc/sa_carry \
	/tb/inst_arc/regA \
	/tb/inst_arc/regB \
	/tb/inst_arc/regC \
	/tb/inst_arc/regD \
	/tb/inst_arc/regE \
	/tb/inst_arc/regF \
	/tb/inst_arc/regM \
	/tb/inst_arc/dA60 \
	/tb/inst_arc/dA56 \
	/tb/inst_arc/dA55 \
	/tb/inst_arc/dA54 \
	/tb/inst_arc/dA53 \
	/tb/inst_arc/dA52 \
	/tb/inst_arc/dB56 \
	/tb/inst_arc/dB52 \
	/tb/inst_arc/dC60 \
	/tb/inst_arc/dC56 \
	/tb/inst_arc/dC55 \
	/tb/inst_arc/dC54 \
	/tb/inst_arc/dC53 \
	/tb/inst_arc/dC52 \
	/tb/inst_arc/dD56 \
	/tb/inst_arc/dE56 \
	/tb/inst_arc/dF56 \
	/tb/inst_arc/dM56 \
	/tb/inst_arc/display_ff \
	/tb/inst_arc/t2dp \
	/tb/inst_arc/bcd_in \
	/tb/inst_arc/NewArch
wv.cursors.add -time 113172500ns+1 -name {Default cursor}
wv.cursors.setactive -name {Default cursor}
wv.cursors.subcursor.add -time 211732500ns+1 -name {Cursor 1}
wv.cursors.setactive -name {Cursor 1}
wv.zoom.range -from 205071304102ps -to 212083089258ps
wv.time.unit.auto.set
transcript $curr_transcript
