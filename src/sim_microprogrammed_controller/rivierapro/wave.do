onerror { resume }
set curr_transcript [transcript]
transcript off

add wave -literal /tb/NewArch
add wave /tb/PHI2
add wave /tb/IS
add wave /tb/WS
add wave /tb/SYNC
add wave -logic /tb/rst
add wave -logic /tb/go
add wave /tb/T1
add wave /tb/T2
add wave /tb/T3
add wave /tb/T4
add wave /tb/instruction
add wave /tb/is_shifter
add wave /tb/sysCount
add wave /tb/nextSysCount
add wave /tb/digitCount
add wave /tb/i_carry
add wave /tb/load_instruction
add wave /tb/fetch_instruction
add wave /tb/exec_instruction
add wave /tb/grab_em
add wave /tb/compare_em
add wave /tb/save_em
add wave /tb/loop_done_1
add wave /tb/loop_done_2
add wave /tb/loop_done_3
add wave /tb/s_mnemonic
add wave /tb/i_sub
add wave /tb/i_ci
add wave /tb/s_fs
add wave /tb/i_ptr
add wave -divider ---
add wave -divider ---
add wave -divider ---
add wave /tb/i
add wave /tb/j
add wave /tb/N
add wave -unsigned /tb/stq
add wave /tb/stqzero
add wave /tb/arith_cyc
add wave /tb/ipt_cyc
add wave /tb/ist_cyc
add wave /tb/arith_cset
add wave /tb/arith_crst
add wave /tb/ist_cset
add wave /tb/ist_crst
add wave /tb/ipt_cset
add wave /tb/ipt_crst
add wave /tb/PTI_reg
add wave /tb/PTD_reg
add wave /tb/dPtr4
add wave /tb/dco
add wave /tb/co_reg
add wave /tb/ptr
add wave /tb/i_ptrHoldingReg
add wave /tb/expected_carry
add wave /tb/actual_carry
add wave /tb/carry_match
add wave /tb/all_regs_match
add wave /tb/good_count
add wave /tb/bad_count
add wave /tb/s_dut_state
add wave -expand -vgroup /tb/inst_mpc \
	( -literal /tb/inst_mpc/state ) \
	( -literal /tb/inst_mpc/next ) \
	/tb/inst_mpc/JSB \
	/tb/inst_mpc/BRH \
	/tb/inst_mpc/PTONLY \
	/tb/inst_mpc/UP2PT \
	/tb/inst_mpc/ARITHW \
	/tb/inst_mpc/ISTW \
	/tb/inst_mpc/STDECN \
	/tb/inst_mpc/SST \
	/tb/inst_mpc/RST \
	/tb/inst_mpc/IST \
	/tb/inst_mpc/SPT \
	/tb/inst_mpc/IPTR \
	/tb/inst_mpc/IPTS \
	/tb/inst_mpc/PTD \
	/tb/inst_mpc/PTI \
	/tb/inst_mpc/TKR \
	/tb/inst_mpc/RET \
	/tb/inst_mpc/PHI2 \
	/tb/inst_mpc/pwor \
	/tb/inst_mpc/IS \
	/tb/inst_mpc/b5 \
	/tb/inst_mpc/b14 \
	/tb/inst_mpc/b18 \
	/tb/inst_mpc/b26 \
	/tb/inst_mpc/b35 \
	/tb/inst_mpc/b45 \
	/tb/inst_mpc/b54 \
	/tb/inst_mpc/b55 \
	/tb/inst_mpc/CARRY_FF \
	/tb/inst_mpc/stqzero \
	/tb/inst_mpc/is_eq_ptr
add wave /tb/sysCount
wv.cursors.add -time 110937500ns+1 -name {Default cursor}
wv.cursors.add -time 88760600ns -name {Cursor 2}
wv.cursors.setactive -name {Default cursor}
wv.zoom.range -from 0fs -to 116484375ns
wv.time.unit.auto.set
transcript $curr_transcript
