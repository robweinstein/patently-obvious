//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Microprogrammed Controller 46
//
// Description:
// 
// Include Files : state_names.v
//
// Output Files : microprog_ctrl_output.txt
//
// Conventions:
//    - UPPER case for signals described in the '569 patent.  Since many of the
//      Microprogrammed Controller's outputs (control lines) are described in
//      the patent, UPPER case is extended to all control line outputs from the
//      DUT.
//    - Internal wires and registers are 'lower' case.
//    - State name parameters are camelCase starting with a lower case 's',
//      e.g., 'sIdle'.
//    - Other parameters are first letter 'Upper'.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2025 - HP-35 Control and Timing Circuit 16 - Logic and Timing Diagrams
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

// -----------------------------------------------------------------------------
// Signal Declarations
// 
    localparam NewArch = 0;

    // Include the parameter that defines the state names:
    `include "../../state_names.v"

    // Type 10 Instruction
    `define NOP     10'b0000000000

    // Inputs to DUT
    // Clock Input                                                                                                                 
    reg         PHI2;           // input        // System Clock                                                                     
    // Qualifier Inputs                                                                                                            
    reg         pwor;           // input        // Qualifier, (PWO 36 in '569 Fig. 4), Power On, Registered Copy
    reg         IS;             // input        // Qualifier, (Is 28 in '569 Fig. 4), Serial Instruction from ROM                      
    reg         b5;             // input        // Qualifier, (from TIMING DECODER in '569 Fig. 4) System Counter 42 at Bit Time b5   
    reg         b14;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b14  
    reg         b18;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b18  
    reg         b26;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b28  
    reg         b35;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b35  
    reg         b45;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b45  
    reg         b54;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b54  
    reg         b55;            // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b55  
    reg         CARRY_FF;       // input        // Qualifier, (CARRY Flip-Flop 66 in '569 Fig. 4), Carry Flip-Flop 66                        
    reg         stqzero;        // input        // Qualifier, Status Bit N has shifted to Right-Most Position of 28-bit Shift Register.
    wire        is_eq_ptr;      // input        // Qualifier, Interrogate Pointer Instruction's P Field (in Is) is Equal to Pointer

    // Output from DUT
    wire        JSB;            // output reg   // Control Line, (JSB in '569 Fig. 4), Jump Subroutine                             
    wire        BRH;            // output reg   // Control Line, (BRH in '569 Fig. 4), Conditional Branch                          
    wire        PTONLY;         // output reg   // Control Line, Arithmetic Instruction with WS = Pointer-Only                     
    wire        UP2PT;          // output reg   // Control Line, Arithmetic Instruction with WS = Up-to-Pointer                    
    wire        ARITHW;         // output reg   // Control Line, Arithmetic Instruction Wait for A&R Chip                          
    wire        ISTW;           // output reg   // Control Line, Interrogate Status Wait for Shifter                               
    wire        STDECN;         // output reg   // Control Line, Status Instruction, Decrement Counter from N                             
    wire        SST;            // output reg   // Control Line, Set Status Bit                                                    
    wire        RST;            // output reg   // Control Line, Reset Status Bit                                                  
    wire        IST;            // output reg   // Control Line, (IST in '569 Fig. 4), Interrogate Status Bit                      
    wire        SPT;            // output reg   // Control Line, Set Pointer to P                                                  
    wire        IPTR;           // output reg   // Control Line, (IPT in '569 Fig. 4), Interrogate Pointer, Reset Carry            
    wire        IPTS;           // output reg   // Control Line, (IPT in '569 Fig. 4), Interrogate Pointer, Set Carry              
    wire        PTD;            // output reg   // Control Line, Pointer Decrement                                                 
    wire        PTI;            // output reg   // Control Line, Pointer Increment                                                 
    wire        TKR;            // output reg   // Control Line, (TKR in '569 Fig. 4), Keyboard Entry                              
    wire        RET;            // output reg   // Control Line, (RET in '569 Fig. 4), Return from Subroutine                      

    // Testbench signals
    reg         rst;
    reg         T1 = 1'b0, T2 = 1'b0, T3 = 1'b0, T4 = 1'b0;
    reg         go = 1'b0;
    reg         sim_start_pulse = 1'b0;
    reg         sim_end_pulse   = 1'b0;
    reg         loop_done_1;
    reg         loop_done_2;
    reg         loop_done_3;
    integer     fd; // File Descriptor
    integer     i, j, N, P, setP, testP;
    integer     TestIterations;
    reg         load_instruction = 1'b0;
    reg         fetch_instruction = 1'b0;
    reg         exec_instruction = 1'b0;
    reg         grab_em;
    reg         compare_em;
    reg         save_em;
    reg [9:0]   instruction     = 'b0;
    reg [55:0]  is_shifter      = 'b0;
    reg         SYNC;
    reg         PWO;
    reg         IA = 1'b0;

    integer     sysCount        = 0;
    integer     nextSysCount    = 1;
    integer     wsBegin         = -1;
    integer     wsEnd           = -1;
    integer     wsBeginFetch    = -1;
    integer     wsEndFetch      = -1;
    integer     wsBeginExecute  = -1;
    integer     wsEndExecute    = -1;
    integer     digitCount      = 0;

    // Support logic that would normally be found in the Control & Timing module.
    reg [3:0]   stq             = 'b0;  // Status bit position counter.
    wire        arith_cset;
    wire        arith_crst;
    wire        ist_cset;
    wire        ist_crst;
    wire        ipt_cset;
    wire        ipt_crst;
//    reg         b0              = 1'b0; // From TIMING DECODER in '569 Fig. 4, System Counter 42 at Bit Time b0.
    // Asserted during the entire word cycle (bit times b0 through b55) for the following
    // instructions:
    reg         arith_cyc       = 1'b0; // Any Type 2 Arithmetic Instuction
    reg         ipt_cyc         = 1'b0; // Interrogate Pointer Instruction 
    reg         ist_cyc         = 1'b0; // Interrogate Status Instruction
    // 4-bit serial pointer register...
    reg         PTI_reg = 1'b0;
    reg         PTD_reg = 1'b0;
    reg         dPtr4   = 1'b0;
    reg         dco     = 1'b0;
    reg         co_reg  = 1'b0;
    reg [4:1]   ptr     = 'b0;

    integer     i_ptrHoldingReg;
    integer     i_carry;
    integer     i_opcode;
    string      s_mnemonic;
    integer     i_sub;
    integer     i_ci;
    integer     i_fs;
    string      s_fs;
    integer     i_ptr;
    integer     i_address;
    integer     i_n_field;
    integer     i_p_field;
    integer     i_romnum; 


    reg         expected_carry  = 1'b0;
    reg         actual_carry    = 1'b0;
    reg         carry_match     = 1'b0;
    reg         all_regs_match  = 1'b0;

    integer     good_count;
    integer     bad_count; 

    reg [1:62]  state, next;
    string      s_dut_state;


// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 

    task buildInstruction;
    // Build the 10-bit instruction from a given instruction mnemonic and field-select (both strings).
    // Called by issueInstruction().
        output reg [9:0]    op_code;
        output integer      ws_begin;
        output integer      ws_end;
        input string        op_mnemonic;
        input string        field_select;
//      input integer       pointer_val = -1;   // Default arguments supported in SystemVerilog only.
        input integer       pointer_val;
        input integer       address;
        input integer       n_field;
        input integer       p_field;
        input integer       romnum;
        begin : task_make_instruction 
            integer itype = -1;
            case (op_mnemonic)
                // Type 1 Instructions
                "JSR"       : begin itype = 1; op_code = {address[7:0],2'b01}; end  // Jump to Subroutine
                "CBRH"      : begin itype = 1; op_code = {address[7:0],2'b11}; end  // Conditional Branch (Branch when Carry = 0)
                // Type 2 Instructions
                // -- Class 1) Clear
                "CLRA"      : begin itype = 2; op_code = 10'b10111_zzz_10; end  // 0 -> A
                "CLRB"      : begin itype = 2; op_code = 10'b00001_zzz_10; end  // 0 -> B
                "CLRC"      : begin itype = 2; op_code = 10'b00110_zzz_10; end  // 0 -> C
                // -- Class 2) Transfer/Exchange
                "MOVAB"     : begin itype = 2; op_code = 10'b01001_zzz_10; end  // A -> B
                "MOVBC"     : begin itype = 2; op_code = 10'b00100_zzz_10; end  // B -> C
                "MOVCA"     : begin itype = 2; op_code = 10'b01100_zzz_10; end  // C -> A
                "XCHAB"     : begin itype = 2; op_code = 10'b11001_zzz_10; end  // A <-> B
                "XCHBC"     : begin itype = 2; op_code = 10'b10001_zzz_10; end  // B <-> C
                "XCHAC"     : begin itype = 2; op_code = 10'b11101_zzz_10; end  // A <-> C
                // -- Class 3) Add/Subtract
                "ADDACC"    : begin itype = 2; op_code = 10'b01110_zzz_10; end  // A + C -> C
                "SUBACC"    : begin itype = 2; op_code = 10'b01010_zzz_10; end  // A - C -> C
                "ADDABA"    : begin itype = 2; op_code = 10'b11100_zzz_10; end  // A + B -> A
                "SUBABA"    : begin itype = 2; op_code = 10'b11000_zzz_10; end  // A - B -> A
                "ADDACA"    : begin itype = 2; op_code = 10'b11110_zzz_10; end  // A + C -> A
                "SUBACA"    : begin itype = 2; op_code = 10'b11010_zzz_10; end  // A - C -> A
                "ADDCCC"    : begin itype = 2; op_code = 10'b10101_zzz_10; end  // C + C -> C
                // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                "CMP0B"     : begin itype = 2; op_code = 10'b00000_zzz_10; end  // 0 - B            (Compare B to zero)
                "CMP0C"     : begin itype = 2; op_code = 10'b01101_zzz_10; end  // 0 - C            (Compare C to zero)
                "CMPAC"     : begin itype = 2; op_code = 10'b00010_zzz_10; end  // A - C            (Compare A and C)
                "CMPAB"     : begin itype = 2; op_code = 10'b10000_zzz_10; end  // A - B            (Compare A and B)
                "CMPA1"     : begin itype = 2; op_code = 10'b10011_zzz_10; end  // A - 1            (Compare A to one)
                "CMPC1"     : begin itype = 2; op_code = 10'b00011_zzz_10; end  // C - 1            (Compare C to one)    
                // -- Class 5) Complement
                "TCC"       : begin itype = 2; op_code = 10'b00101_zzz_10; end  // 0 - C -> C       (Tens Complement register C)
                "NCC"       : begin itype = 2; op_code = 10'b00111_zzz_10; end  // 0 - C - 1 -> C   (Nines Complement register C)
                // -- Class 6) Increment
                "INCA"      : begin itype = 2; op_code = 10'b11111_zzz_10; end  // A + 1 -> A       (Increment register A)
                "INCC"      : begin itype = 2; op_code = 10'b01111_zzz_10; end  // C + 1 -> C       (Increment register C)
                // -- Class 7) Decrement
                "DECA"      : begin itype = 2; op_code = 10'b11011_zzz_10; end  // A - 1 -> A       (Decrement register A)
                "DECC"      : begin itype = 2; op_code = 10'b01011_zzz_10; end  // C - 1 -> C       (Decrement register C)
                // -- Class 8) Shift
                "SHRA"      : begin itype = 2; op_code = 10'b10110_zzz_10; end  // A >> 1           (Shift Right register A)
                "SHRB"      : begin itype = 2; op_code = 10'b10100_zzz_10; end  // B >> 1           (Shift Right register B)
                "SHRC"      : begin itype = 2; op_code = 10'b10010_zzz_10; end  // C >> 1           (Shift Right register C)
                "SHLA"      : begin itype = 2; op_code = 10'b01000_zzz_10; end  // A << 1           (Shift Left register A)
                // Type 3 Instructions (Status Operations)
                "SSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b00_0100}; end // Set Status Flag N
                "ISTN"      : begin itype = 3; op_code = {n_field[3:0],6'b01_0100}; end // Interrogate Status Flag N
                "RSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b10_0100}; end // Reset Status Flag N
                "CSTN"      : begin itype = 3; op_code = {n_field[3:0],6'b11_0100}; end // Clear All Flags  (If N=0 then clear all flags, if N>0 then clear flags N down to 0)
                // Type 4 Instructions (Pointer Operations)
                "SPTP"      : begin itype = 4; op_code = {p_field[3:0],6'b00_1100}; end // Set Pointer to P     (P-field = New Pointer Value)
                "PTD"       : begin itype = 4; op_code = {p_field[3:0],6'b01_1100}; end // Decrement Pointer    (P-field = xxxx)
                "IPTP"      : begin itype = 4; op_code = {p_field[3:0],6'b10_1100}; end // Interrogate Pointer  (Set Carry if Pointer /= P-field)
                "PTI"       : begin itype = 4; op_code = {p_field[3:0],6'b11_1100}; end // Increment Pointer    (P-field = xxxx)
                // Type 5 Instructions
//              "AIx"       : begin itype = 5; op_code = 10'b????_00_1000; end          // Available Instructions (16)
                "LDC"       : begin itype = 5; op_code = {n_field[3:0],6'b01_1000}; end // Load Constant, N-field -> C at Pointer, post-decrement Pointer
                "DSPTOG"    : begin itype = 5; op_code = 10'b000_01z_1000; end          // Display Toggle
                "XCHCM"     : begin itype = 5; op_code = 10'b001_01z_1000; end          // C -> M -> C              (Exchange Memory)
                "PUSHC"     : begin itype = 5; op_code = 10'b010_01z_1000; end          // C -> C -> D -> E -> F    (Up Stack or Push C) 
                "POPA"      : begin itype = 5; op_code = 10'b011_01z_1000; end          // F -> F -> E -> D -> A    (Down Stack or Pop A)
                "DSPOFF"    : begin itype = 5; op_code = 10'b100_01z_1000; end          // Display Off
                "RCLM"      : begin itype = 5; op_code = 10'b101_01z_1000; end          // M -> M -> C              (Recall Memory)
                "ROTDN"     : begin itype = 5; op_code = 10'b110_01z_1000; end          // C -> F -> E -> D -> C    (Rotate Down)
                "CLRALL"    : begin itype = 5; op_code = 10'b111_01z_1000; end          // 0 -> A, B, C, D, E, F, M (Clear all 56 bits of all registers)
                "MOVISA"    : begin itype = 5; op_code = 10'bzz0_11z_1000; end          // Is -> A                  (Load 56-bit instruction sequence into register A)
                "MOVBCDC"   : begin itype = 5; op_code = 10'bzz1_11z_1000; end          // BCD -> C                 (Load 56-bit data storage value into register C)
                // Type 6 Instruction (ROM Select, Misc.)
                "ROMSEL"    : begin itype = 6; op_code = {romnum[2:0],7'b00_10000}; end // ROM Select
                "RET"       : begin itype = 6; op_code = {10'bzzz_01_10000}; end        // Subroutine Return
                "EKEY"      : begin itype = 6; op_code = {10'bzz_010_10000}; end        // External Keyboard Entry (Not implemented in HP-35)
                "KEYJMP"    : begin itype = 6; op_code = {10'bzz_110_10000}; end        // Keyboard Entry (Jump to ROM address specified by key-code)
                "SNDA"      : begin itype = 6; op_code = {10'b1z0_11_10000}; end        // Send Address from C Register to Aux Data Storage (Not implemented in HP-35)
                "SNDD"      : begin itype = 6; op_code = {10'b101_11_10000}; end        // Send Data from C Register to Aux Data Storage (Not implemented in HP-35)
                "RCVD"      : begin itype = 6; op_code = {10'b101_11_11000}; end        // Same as Type 5, MOVBCDC.  Receive Data from Aux Data Storage into C Register (Not implemented in HP-35)

                // ==================================================================
                // Finish Me - Maybe add dummy instructions for types 7, 8, 9.
                // ==================================================================

                // Type 7 Instruction (Reserved for Program Storage MOS Circuit)
                // Type 8 Instruction (Reserved for Program Storage MOS Circuit)
                // Type 9 Instruction (Available)
                // Type 10 Instruction
                "NOP"       : begin itype = 10; op_code = 10'b0000000000; end   // No Operation             (All registers recirculate)
                // If none of the above
                default     : begin itype = -1; op_code = 10'bzzzzzzzzzz; end
            endcase

            case (field_select)
                "P"     : begin if (itype == 2) op_code[4:2] = 3'b000;  ws_begin = pointer_val; ws_end = pointer_val; end   // Pointer position.
//              "WP"    : begin if (itype == 2) op_code[4:2] = 3'b001;  ws_begin = 0;           ws_end = pointer_val; end   // Bit-reversed encoding.   // Up to pointer position.
                "WP"    : begin if (itype == 2) op_code[4:2] = 3'b100;  ws_begin = 0;           ws_end = pointer_val; end   // Up to pointer position.
                "X"     : begin if (itype == 2) op_code[4:2] = 3'b010;  ws_begin = 0;           ws_end = 2;  end            // Exponent field (with sign).
//              "XS"    : begin if (itype == 2) op_code[4:2] = 3'b011;  ws_begin = 2;           ws_end = 2;  end            // Bit-reversed encoding.   // Exponent sign.
                "XS"    : begin if (itype == 2) op_code[4:2] = 3'b110;  ws_begin = 2;           ws_end = 2;  end            // Exponent sign.
//              "M"     : begin if (itype == 2) op_code[4:2] = 3'b100;  ws_begin = 3;           ws_end = 12; end            // Bit-reversed encoding.   // Mantissa field without sign.
                "M"     : begin if (itype == 2) op_code[4:2] = 3'b001;  ws_begin = 3;           ws_end = 12; end            // Mantissa field without sign.
                "MS"    : begin if (itype == 2) op_code[4:2] = 3'b101;  ws_begin = 3;           ws_end = 13; end            // Mantissa field with sign.
//              "W"     : begin if (itype == 2) op_code[4:2] = 3'b110;  ws_begin = 0;           ws_end = 13; end            // Bit-reversed encoding.   // Entire word.
                "W"     : begin if (itype == 2) op_code[4:2] = 3'b011;  ws_begin = 0;           ws_end = 13; end            // Entire word.
                "S"     : begin if (itype == 2) op_code[4:2] = 3'b111;  ws_begin = 13;          ws_end = 13; end            // Mantissa sign only.
                default : begin                                         ws_begin = -1;          ws_end = -1; end            // If no valid field select.
            endcase

        end
    endtask

    task issueInstruction;
    // Using the given mnemonic, field-select, and pointer value, build the 10-bit instruction,
    // then issue the instruction at the right time.
        input string        op_mnemonic;
        input string        field_select;
        input integer       pointer_val;
        input integer       address;
        input integer       n_field;
        input integer       p_field;
        input integer       romnum;
        begin : task_issue_instr 
            wait (nextSysCount == 0);
            buildInstruction(instruction, wsBegin, wsEnd, op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum);
            load_instruction = 1'b1;
            s_mnemonic  = op_mnemonic;  // Echo to a signal for display.
            s_fs        = field_select; // <ditto>
            i_ptr       = pointer_val;  // <ditto>
            i_address   = address;      // <ditto>
            i_n_field   = n_field;      // <ditto>
            i_p_field   = p_field;      // <ditto>
            i_romnum    = romnum;       // <ditto>
            @(posedge PHI2);
            buildInstruction(instruction, wsBegin, wsEnd, "", "", -1, -1, -1, -1, -1);
            load_instruction = 1'b0;
            #1;
        end
    endtask

    function string state2string;
        input [1:62] state_val;
        begin : func_st2str
            case (1'b1)
                // Main Instruction Parsing Loop
                state_val[sIdle]        : state2string = "sIdle";       // Power On (PWO) state, this bit is set, all others reset.  Test for Type 1.                                       
                state_val[s0x]          : state2string = "s0x";         // Test for Type 2.                                                                                                 
                state_val[s00x]         : state2string = "s00x";        // Test for Type 3 or Type 4.                                                                                       
                state_val[s001x]        : state2string = "s001x";       // Test for Type 4.                                                                                                 
                state_val[s000x]        : state2string = "s000x";       // Test for Type 5.                                                                                                 
                state_val[s0000x]       : state2string = "s0000x";      // Test for Type 6.                                                                                                 
                state_val[sNop]         : state2string = "sNop";        // Instruction failed tests for Types 1 through 6, so it's a No Op.                                                 
                state_val[sAddrOut]     : state2string = "sAddrOut";    // Return point for instructions that use the default ROM Address 58.                                               
                // Type 1 (Jump/Branch) Instructions                                                                                                                                        
                state_val[s1x]          : state2string = "s1x";         // Decoded a Type 1 instruction so test for JSB or BRH.                                                             
                state_val[sJsb]         : state2string = "sJsb";        // Decoded JSB (Jump Subroutine).                                                                                   
                state_val[sJsbWait]     : state2string = "sJsbWait";    // Wait for completeion of JSB.                                                                                     
                state_val[sBrhWait]     : state2string = "sBrhWait";    // Decoded BRH (Conditional Branch).                                                                                
                state_val[sBrhOut]      : state2string = "sBrhOut";     // Drive the captured branch address out Ia.                                                                        
                // Type 2 (Arithmetic) Instructions                                                                                                                                         
                state_val[s01x]         : state2string = "s01x";        // Decoded a Type 2 (Arithmetic) instruction so parse the first Word Select bit.                                    
                state_val[s010x]        : state2string = "s010x";       // Parse the second Word Select bit.                                                                                
                state_val[s0100x]       : state2string = "s0100x";      // Parse the third Word Select bit.                                                                                 
                state_val[sArithP]      : state2string = "sArithP";     // Word Select is 'Pointer Only'.                                                                                   
                state_val[sArithWP]     : state2string = "sArithWP";    // Word Select is 'Up to Pointer'.                                                                                  
                state_val[sArithWait]   : state2string = "sArithWait";  // Wait for arithmetic instruction to complete in the A&R chip.                                                     
                // Type 3 (Status Flag) Instructions                                                                                                                                              
                state_val[s0010x]       : state2string = "s0010x";      // Decoded a Type 3 (Status) instruction so parse the next two bits to narrow it down.                              
                state_val[s00100x]      : state2string = "s00100x";     // Determine whether the instruction is Set Status Flag (F=00) or Reset Status Flag (F=10).                         
                state_val[sSst]         : state2string = "sSst";        // Decoded the Set Status Flag instruction.                                                                         
                state_val[sSstDecr]     : state2string = "sSstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sSstThis]     : state2string = "sSstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sRst]         : state2string = "sRst";        // Decoded the Reset Status Flag instruction.                                                                       
                state_val[sRstDecr]     : state2string = "sRstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sRstThis]     : state2string = "sRstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[s00101x]      : state2string = "s00101x";     // Determine whether the instruction is Interrogate Status Flag (F=01) or Clear All Status Flags (F=11).            
                state_val[sIst]         : state2string = "sIst";        // Decoded the Interrogate Status Flag instruction.                                                                 
                state_val[sIstDecr]     : state2string = "sIstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sIstThis]     : state2string = "sIstThis";    // The selected status bit is now at the end of the 28-bit shift register.                                          
                state_val[sStDone]      : state2string = "sStDone";     // Wait for the last status bit position.                                                                           
                state_val[sCst]         : state2string = "sCst";        // Decoded the Clear All Status Flags instruction.                                                                  
                state_val[sCstDecr]     : state2string = "sCstDecr";    // Wait for the selected status bit (status bit N) to arrive at the end of the 28-bit shift register.               
                state_val[sCstThis]     : state2string = "sCstThis";    // The selected status bit is now at the end of the 28-bit shift register so begin clearing to the end of the field.
                // Type 4 (Pointer) Instructions                                                                                                                                            
                state_val[s0011x]       : state2string = "s0011x";      // Decoded a Type 4 (Pointer) instruction so parse the next two bits to narrow it down.                             
                state_val[s00110x]      : state2string = "s00110x";     // Determine whether the instruction is Set Pointer (F=00) or Interrogate Pointer (F=10).                           
                state_val[sSpt]         : state2string = "sSpt";        // Decoded the Set Pointer instruction.                                                                             
                state_val[sSptDone]     : state2string = "sSptDone";    // Set Pointer is done so wait until it's time to enter the address output state.                                   
                state_val[sIpt]         : state2string = "sIpt";        // Decoded the Interrogate Pointer instruction.                                                                     
                state_val[sIptSetC]     : state2string = "sIptSetC";    // If pointer is equal to this instruction's P field then set carry.      
                state_val[sIptRstC]     : state2string = "sIptRstC";    // If pointer is NOT equal to this instruction's P field then RESET carry.
                state_val[s00111x]      : state2string = "s00111x";     // Determine whether the instruction is Decrement Pointer (F=01) or Increment Pointer (F=11).                       
                state_val[sPtd]         : state2string = "sPtd";        // Decoded the Decrement Pointer instruction.                                                                       
                state_val[sPtdNow]      : state2string = "sPtdNow";     // The four clock periods in which the pointer is decremented.                                                      
                state_val[sPti]         : state2string = "sPti";        // Decoded the Increment Pointer instruction.                                                                       
                state_val[sPtiNow]      : state2string = "sPtiNow";     // The four clock periods in which the pointer is incremented.                                                      
                // Type 5 (Data Entry/Display) Instructions                                                                                                                                 
                state_val[s0001x]       : state2string = "s0001x";      // Decoded a Type 5 (Data Entry/Display) instruction so parse the next two bits to narrow it down.
                state_val[s00011x]      : state2string = "s00011x";     // Determine whether the instruction is LDC (F=01) or other (F=11).
                state_val[sLdc]         : state2string = "sLdc";        // Decoded the LOAD CONSTANT (LDC) instruction.  Generate Word Select at pointer-only then decrement pointer.
                state_val[sType5Wait]   : state2string = "sType5Wait";  // All Type 5 instructions other than LDC are executed in the the A&R circuit so wait here for completion.
                // Type 6 (Misc) Instructions                                                                                                                                               
                state_val[sType6]       : state2string = "sType6";      // Decoded a Type 6 instruction so parse the next two bits to narrow it down.                                       
                state_val[s000010x]     : state2string = "s000010x";    // Determine whether the instruction is ROM Select (F=00) or one of two Key Entry instructions (F=10).              
                state_val[sRomSel]      : state2string = "sRomSel";     // Instruction is ROM Select so wait here while the ROMs execute the function.                                      
                state_val[s0000101x]    : state2string = "s0000101x";   // Determine whether the instruction is External Key-Code Entry or Keyboard Entry.                                  
                state_val[sExtKey]      : state2string = "sExtKey";     // Decoded the External Key-Code Entry instruction that's not supported in the HP-35 so just wait.                  
                state_val[sKey]         : state2string = "sKey";        // Decoded the Keyboard Entry instruction.                                                                          
                state_val[sKeyOut]      : state2string = "sKeyOut";     // Shift the contents of the Key-Code Buffer 56 to the address line, Ia.                                            
                state_val[s000011x]     : state2string = "s000011x";    // Determine whether the instruction is Return from Subroutine (F=01) or Data Store (F=11).                         
                state_val[sRet]         : state2string = "sRet";        // Decoded the Subroutine Return instruction.                                                                       
                state_val[sRetOut]      : state2string = "sRetOut";     // Shift the contents of the Return Address 60 shift register field to the address line, Ia.                        
                state_val[sDataStore]   : state2string = "sDataStore";  // Decoded a Data Storage instruction that's not part of the C&T chip so just wait. (Not supported in the HP-35.)   
            endcase
        end
    endfunction

     function [1:0] fullAdd;
     // output  sum;         // Sum
     // output  co;          // Carry out
        input   x;           // Augend
        input   y;           // Addend
        input   ci;          // Carry In
        begin : func_fullAdd
            reg sum = 1'b0;
            reg co  = 1'b0;
            sum = x ^ y ^ ci;
            co  = x & y | x & ci | y & ci;
            fullAdd = {sum, co};
        end
    endfunction

// -------------------------------------------------------------------------------------------------
// Test Processes

    // Generate 200 kHz clock
    always #2.5us PHI2 = ~PHI2;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    initial begin
        PWO = 1'b0;
        #10;
        PWO = 1'b1;
        #5us;
        PWO = 1'b0;
        #10ms;
//        wait;
    end

    always @ (posedge PHI2) begin : proc_clocked
        pwor <= PWO;
    end

    // -----------------------------------------------
    // Two-Process System Counter
    // --
    // Combinational next count process...
    always @* begin : proc_nextsyscount
        if (sysCount == 55)
            nextSysCount = 0;
        else
            nextSysCount = sysCount + 1;
    end
    // --
    // Registered count process...
    always@(posedge PHI2 or posedge rst) begin : proc_syscount
        if (rst) begin
            sysCount <= 0;
            digitCount <= 0;
        end
        else begin
            sysCount <= nextSysCount;
            digitCount <= nextSysCount / 4;
        end
    end
    // --
    // End Two-Process System Counter
    // -----------------------------------------------

    // Generate SYNC and WS pulses
    always@(posedge PHI2 or posedge rst) begin : proc_sync
        if (rst) 
            SYNC <= 1'b0;
        else if (nextSysCount == 45)
            SYNC <= 1'b1;
        else if (nextSysCount == 55)
            SYNC <= 1'b0;
        // --
        b5  <= 1'b0;
        b14 <= 1'b0;
        b18 <= 1'b0;
        b26 <= 1'b0;
        b35 <= 1'b0;
        b45 <= 1'b0;
        b54 <= 1'b0;
        b55 <= 1'b0;
        case (nextSysCount)
            5   : b5  <= 1'b1;
            14  : b14 <= 1'b1;
            18  : b18 <= 1'b1;
            26  : b26 <= 1'b1;
            35  : b35 <= 1'b1;
            45  : b45 <= 1'b1;
            54  : b54 <= 1'b1;
            55  : b55 <= 1'b1;
        endcase
        // --
        s_dut_state <= state2string(inst_mpc.next);
        // --
        if (rst) 
            {T1, T2, T3, T4} <= 4'b0000;
        else if (nextSysCount == 0)
            {T1, T2, T3, T4} <= 4'b1000;
        else
            {T1, T2, T3, T4} <= {T4, T1, T2, T3};
        // --
        if (rst) 
            is_shifter <= {1'b0, {10{1'b0}}, {33{1'b0}}, 1'b1, {11{1'b0}}};
        else if (nextSysCount == 0)                                                 // If we're about to start the next word cycle, and ...
            if (load_instruction)                                                   //    if a new instruction is issued, then ...
                is_shifter <= {1'b0, instruction, {33{1'b0}}, 1'b1, {11{1'b0}}};    //       load the new instruction in bits [54:45] and set bit [11] for the display,
            else                                                                    //    otherwise no new instruction was issued, so ...
                is_shifter <= {1'b0, `NOP, {33{1'b0}}, 1'b1, {11{1'b0}}};           //       load a NOP and set bit [11] for the display.
        else                                                                        // Otherwise we're not about to start the next word cycle, so ...
            is_shifter <= {1'b0, is_shifter[55:1]};                                 //    shift the IS shifter to the right.
        // --
        // --
        if (rst) begin
            wsBeginFetch    <= -1;
            wsEndFetch     <= -1;
            wsBeginExecute  <= -1;
            wsEndExecute   <= -1;
        end
        else begin
            if (load_instruction) begin         // If a new instruction is issued, then ...
                wsBeginFetch <= wsBegin;        //    hold the digit number where WS begins and
                wsEndFetch   <= wsEnd;          //    hold the digit number where WS ends.
            end                                 //
            if (nextSysCount == 0) begin        // Otherwise, if we're about to start the next word cycle, then ...
                wsBeginExecute <= wsBeginFetch; //    load the digit number where WS begins and
                wsEndExecute   <= wsEndFetch;   //    load the digit number where WS ends.
            end
        end
        //--
    end

    assign IS = is_shifter[0];
    assign WS = ((digitCount >= wsBeginExecute) && (digitCount <= wsEndExecute))? 1'b1 : 1'b0;

    // -------------------------------------------------------------------------
    // Test results pipeline
    always@(posedge PHI2 or posedge rst) begin : proc_tpipe
        if (rst) begin
            fetch_instruction   <= 1'b0;
            exec_instruction    <= 1'b0;
            grab_em             <= 1'b0;
            compare_em          <= 1'b0;
            save_em             <= 1'b0;


//            good_count      <= 0;
//            bad_count       <= 0;
//
//            clearInstructionCounts();
        end
        else begin
            if (nextSysCount == 0) begin
                fetch_instruction <= load_instruction;
                exec_instruction  <= fetch_instruction;
                grab_em  <= exec_instruction;
            end
            else begin
                // 'grab_em' pulses simultaneously with START.
                grab_em  <= 1'b0;  // End the pulse.  
            end

            compare_em <= grab_em;
            save_em <= compare_em;

            if (nextSysCount == 0) begin
            end

            if (grab_em) begin
                // Capture expected results:
                // Capture actual results:
            end
        end
    end
    // -------------------------------------------------------------------------
//
//    assign all_regs_match = 1'b1;
//
//    // -------------------------------------------------------------------------
//    // Debug:  Capture and hold the register contents at the start of every word cycle.
//    always@(posedge PHI2 or posedge rst) begin : proc_hold_regs
//        if (rst) begin
//            holdRegA    <= 'b0;
//            holdRegB    <= 'b0;
//            holdRegC    <= 'b0;
//            holdRegD    <= 'b0;
//            holdRegE    <= 'b0;
//            holdRegF    <= 'b0;
//            holdRegM    <= 'b0;
//        end
//        else if (START) begin
//            holdRegA    <= inst_a_and_r.regA[56:1];
//            holdRegB    <= inst_a_and_r.regB;
//            holdRegC    <= inst_a_and_r.regC[56:1];
//            holdRegD    <= inst_a_and_r.regD;
//            holdRegE    <= inst_a_and_r.regE;
//            holdRegF    <= inst_a_and_r.regF;
//            holdRegM    <= inst_a_and_r.regM;
//        end
//    end
//    // End debug
//    // -------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------
    // Dump to a file the 56-clock state machine sequence for the most recently issued instruction.
    always@* begin
        if (fetch_instruction && sysCount == 27) begin
            $fdisplay(fd, "Inst: %s, WS: %s, Ptr: %0d, Addr: %0d, N: %0d, P: %0d, ROM: %0d, CARRY_FF: %b",
                      s_mnemonic,
                      s_fs,
                      i_ptr,    
                      i_address,
                      i_n_field,
                      i_p_field,
                      i_romnum,
                      CARRY_FF
                      );
            $fdisplay(fd, "");
            $fdisplay(fd, "                                               i s C");
            $fdisplay(fd, "                                S   A   P      s t A");
            $fdisplay(fd, "                                T   R U T      e q R");
            $fdisplay(fd, "                    I I         D I I P O      q z R                   p   S");
            $fdisplay(fd, "            R T P P P P S I R S E S T 2 N B J  p e Y b b b b b b b     w   Y");
            $fdisplay(fd, "            E K T T T T P S S S C T H P L R S  t r F 5 5 4 3 2 1 1 b I o I N                                                               state   ");
            $fdisplay(fd, "    LineNum T R I D S R T T T T N W W T Y H B  r o F 5 4 5 5 6 8 4 5 S r A C state                                                         name     sysCount");
            $fdisplay(fd, "");

            for (i = 1; i <= 56; i = i + 1) begin
                @(posedge PHI2);
                $fdisplay(fd, "%d %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b  %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %b %10s %2d", 
                          i,
                          RET,
                          TKR,
                          PTI,
                          PTD,
                          IPTS,
                          IPTR,
                          SPT,
                          IST,
                          RST,
                          SST,
                          STDECN,
                          ISTW,
                          ARITHW,
                          UP2PT,
                          PTONLY,
                          BRH,
                          JSB,
                          is_eq_ptr,
                          stqzero,
                          CARRY_FF,
                          b55,
                          b54,
                          b45,
                          b35,
                          b26,
                          b18,
                          b14,
                          b5,
                          IS,
                          pwor,
                          IA,
                          SYNC,
                          inst_mpc.state,
                          s_dut_state,
                          sysCount
                         );
            end
            $fdisplay(fd, "------------------------------------------------------------------------------------------------------------------------------------------------");
        end
    end
    // ---------------------------------------------------------------------------------------------

    initial begin
        TestIterations = 100000;    // **** Specify the number of random instructions to simulate (use a multiple of 10) ****
                                    // With waveform output, 100,000 iterations takes 3 minutes to complete.
                                    // Without waveform output, 100,000 iterations takes 22 seconds to complete.
                                    // Without waveform output, 1,000,000 iterations takes 3 minutes 37 seconds to complete.
                                    // Without waveform output, 2,000,000 iterations takes 7 minutes 36 seconds to complete.
        PHI2        = 1'b0;
//        CARRY_FF    = 1'b0;
//        request_BCD = 1'b0;
        fd = $fopen("microprog_ctrl_output.txt", "w");
        sim_end_pulse = 1'b0;
        loop_done_1 = 1'b0;
        loop_done_2 = 1'b0;
        loop_done_3 = 1'b0;
        wait (rst == 1'b1);
        wait (rst == 1'b0);
        @(posedge PHI2);
        sim_start_pulse = 1'b1;
        @(posedge PHI2);
        sim_start_pulse = 1'b0;
        @(posedge PHI2);
//        afterCall_ready = 1'b0;

        // =====================================================
        // **** Test Conditional Branch ****
        // =====================================================
        setP = 1;
        testP = 2;

        // Set pointer to a value, then interrogate the pointer for the same value; this should reset the carry FF.
        issueInstruction("SPTP"  , "-",  -1, -1, -1,  setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("IPTP"   , "-",  -1, -1, -1, setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("CBRH"  , "-",  -1, 42, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // Now interrogate the pointer for a different value; this should set the carry FF.
        issueInstruction("IPTP"   , "-",  -1, -1, -1, testP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("CBRH"  , "-",  -1, 42, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        // =====================================================
        // **** Test Pointer Ops ****
        // =====================================================

        @(posedge PHI2);
        // Set pointer to P
        for (P = 0; P <= 15; P = P+1) begin
            issueInstruction("SPTP"  , "-",  -1, -1, -1,  P, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        // Increment pointer
        for (j = 0; j <= 15; j = j+1) begin
            issueInstruction("PTI"  , "-",  -1, -1, -1,  -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        // Decrement pointer
        for (j = 0; j <= 15; j = j+1) begin
            issueInstruction("PTD"  , "-",  -1, -1, -1,  -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        for (setP = 0; setP <= 15; setP = setP+1) begin
            issueInstruction("SPTP"  , "-",  -1, -1, -1,  setP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            // Interrogate all pointers
            for (testP = 0; testP <= 15; testP = testP+1) begin
                issueInstruction("IPTP"   , "-",  -1, -1, -1, testP, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
            end
            issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end

        // =====================================================
        // **** Test Status Flag Ops ****
        // =====================================================

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("SSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("RSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("ISTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        @(posedge PHI2);
        for (N = 0; N <= 11; N = N+1) begin
            issueInstruction("CSTN"  , "-",  -1, -1,  N, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
        end
        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)


//        issueInstruction("JSR"   , "-",  -1, 99, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

//        issueInstruction("CBRH"  , "-",  -1, 42, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

//        issueInstruction("CBRH"  , "-",  -1, 42, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("CLRA"  , "W",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("SSTN"  , "-",  -1, -1,  2, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("SPTP"  , "-",  -1, -1, -1,  9, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("DSPOFF", "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("DSPTOG", "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//
//        issueInstruction("ROMSEL", "-",  -1, -1, -1, -1,  2); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)
//        issueInstruction("NOP"   , "-",  -1, -1, -1, -1, -1); // (op_mnemonic, field_select, pointer_val, address, n_field, p_field, romnum)

        wait (nextSysCount == 0); 
        @(posedge PHI2);
        wait (nextSysCount == 0); 
        @(posedge PHI2);

//         for (i = 1; i <= TestIterations/10; i = i+1) begin
//             for (j = 1; j <= 10; j = j+1) begin
//                 simRandomType2();
//                 wait (nextSysCount == 0); 
//                 afterCall_ready = 1'b0;
// //                wait (nextSysCount == 1); // Force a gap between instructions (optional).
//             end
//         end
        loop_done_1 = 1'b1;
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_2 = 1'b1;
        wait (nextSysCount != 0); // Advance into the machine cycle.
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_3 = 1'b1;


        repeat (56) @(posedge PHI2);
        repeat (56) @(posedge PHI2);
        repeat (10) @(posedge PHI2);

        sim_end_pulse = 1'b1;
        @(posedge PHI2);
        sim_end_pulse = 1'b0;
        @(posedge PHI2);

//        $fclose(fd);
        $stop;
    end

// =================================================================================================
// Instances
// =================================================================================================

    // ---------------------------------------------------------------------------------------------
    // Instance of the Micro-Programmed Controller 46
    //
    // '569 (5):  "The control unit of control and timing circuit 16 is a microprogrammed controller
    // 46 comprising a 58 word (25 bits per word) control ROM, which receives qualifier or status
    // conditions from throughout the calculator and sequentially outputs signals to control the
    // flow of data. Each bit in this control ROM either corresponds to a signal control line or is
    // part of a group of N bits encoded into 2N mutually exclusive control lines and decoded
    // external to the control ROM. At each phase 2 clock, a word is read from the control ROM as
    // determined by its present address. Part of the output is fed back to become the next
    // address."
    //
    // '569 (5)  "Several types of qualifiers are checked. Since most commands are issued only at
    // certain bit times during the word cycle, timing qualifiers are necessary. This means the
    // control ROM may sit in a wait loop until the appropriate timing qualifier comes true, then
    // move to the next address to issue a command. Other qualifiers are the state of the pointer
    // register, the PWO (power on) line, the CARRY flip flop, and the state of each of the 12
    // status bits."
    // 
    microprogrammed_controller_46 inst_mpc (
        // Output Ports (Control Lines)
        .JSB        (JSB),          // output reg   // Control Line, (JSB in '569 Fig. 4), Jump Subroutine                             
        .BRH        (BRH),          // output reg   // Control Line, (BRH in '569 Fig. 4), Conditional Branch                          
        .PTONLY     (PTONLY),       // output reg   // Control Line, Arithmetic Instruction with WS = Pointer-Only                     
        .UP2PT      (UP2PT),        // output reg   // Control Line, Arithmetic Instruction with WS = Up-to-Pointer                    
        .ARITHW     (ARITHW),       // output reg   // Control Line, Arithmetic Instruction Wait for A&R Chip                          
        .ISTW       (ISTW),         // output reg   // Control Line, Interrogate Status Wait for Shifter                               
        .STDECN     (STDECN),       // output reg   // Control Line, Status Instruction, Decrement Counter from N                             
        .SST        (SST),          // output reg   // Control Line, Set Status Bit                                                    
        .RST        (RST),          // output reg   // Control Line, Reset Status Bit                                                  
        .IST        (IST),          // output reg   // Control Line, (IST in '569 Fig. 4), Interrogate Status Bit                      
        .SPT        (SPT),          // output reg   // Control Line, Set Pointer to P                                                  
        .IPTR       (IPTR),         // output reg   // Control Line, (IPT in '569 Fig. 4), Interrogate Pointer, Reset Carry            
        .IPTS       (IPTS),         // output reg   // Control Line, (IPT in '569 Fig. 4), Interrogate Pointer, Set Carry              
        .PTD        (PTD),          // output reg   // Control Line, Pointer Decrement                                                 
        .PTI        (PTI),          // output reg   // Control Line, Pointer Increment                                                 
        .TKR        (TKR),          // output reg   // Control Line, (TKR in '569 Fig. 4), Keyboard Entry                              
        .RET        (RET),          // output reg   // Control Line, (RET in '569 Fig. 4), Return from Subroutine                      
        // Clock Input                                                                                                                 
        .PHI2       (PHI2),         // input        // System Clock                                                                     
        // Qualifier Inputs                                                                                                            
        .pwor       (pwor),         // input        // Qualifier, (PWO 36 in '569 Fig. 4), Power On, Registered Copy
        .IS         (IS),           // input        // Qualifier, (Is 28 in '569 Fig. 4), Serial Instruction from ROM                      
        .b5         (b5),           // input        // Qualifier, (from TIMING DECODER in '569 Fig. 4) System Counter 42 at Bit Time b5   
        .b14        (b14),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b14  
        .b18        (b18),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b18  
        .b26        (b26),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b28  
        .b35        (b35),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b35  
        .b45        (b45),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b45  
        .b54        (b54),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b54  
        .b55        (b55),          // input        // Qualifier, ( "     "       "    "    "   "   ")   "       "    "  "   "   "   b55  
        .CARRY_FF   (CARRY_FF),     // input        // Qualifier, (CARRY Flip-Flop 66 in '569 Fig. 4), Carry Flip-Flop 66                        
        .stqzero    (stqzero),      // input        // Qualifier, Status Bit N at Right-Most Position of 28-bit Shift Register.        
        .is_eq_ptr  (is_eq_ptr)     // input        // Qualifier, Interrogate Pointer Instruction's P Field (in Is) is Equal to Pointer
    );
    // ---------------------------------------------------------------------------------------------

    assign is_eq_ptr = IS ~^ ptr[1];

    // ---------------------------------------------------------------------------------------------
    // Status Bit Position Index Counter (not described in '569) 
    // 
    // The 10-bit Is value is shifted into the 4-bit down counter during the SYNC pulse.  The first
    // 6 bits fall off the end, leaving only the last 4 bits in the counter when SYNC goes low.
    // When SYNC goes low, the 4 bits are held.  If the instruction is any of the Type 3 Status
    // Operations then the 4-bit value will be decremented to zero to identify the status bit or
    // bits on which to operate. 
    // 
    always@(posedge PHI2) begin : proc_status_bit_counter
        if (SYNC)                   // During the 10-bit SYNC pulse...
            stq <= {IS, stq[3:1]};  //    shift 10-bit Is into 4-bit counter (only last 4 bits are
                                    //    held).
        else if (STDECN)            // Parsing any of the Type 3 Status instructions...
            stq <= stq - 1;         //    decrement to find bit N.
    end
    //
    // Asserted during status instructions when the status bit specified by N in the instruction
    // word is at the rightmost position of the 28-bit shift register.
    assign stqzero = (stq == 4'b0000)? 1'b1 : 1'b0;
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // '569 (6):  "An important feature of the calculator system is the capability of select and
    // operate upon a single digit or a group of digits (such as the exponent field) from the 14
    // digit registers. This feature is implemented through the use of a four-bit pointer 44 which
    // points at the digit of interest. Instructions are available to set, increment, decrement, and
    // interrogate pointer 44. The pointer is incremented or decremented by the same serial adder/
    // subtracter 64 used for addresses. A yes answer to the instruction �is pointer /= N� will set
    // carry flip-flop 66 via control signal IPT in FIG. 4."
    //
    // Multiplexer Input to the 4-bit Pointer Shift Register
//    always @* begin : proc_ptr4_inmux
//        dPtr4   =  sum      & PTI                         // Select sum when PTI is asserted,
//                | ~sum      & PTD                        // Select not(sum) when PTD is asserted,
//                | IS        & SPT                          // Select Is when SPT is asserted,
//                | ptr[1]    & (~PTI & ~PTD & ~SPT);   // otherwise, recirculate when none of the above are asserted.
//    end

    always @* begin : proc_ptr4_inmux
        if (PTI & ~PTI_reg) 
            // Process the LS-bit.
            {dPtr4, dco} = fullAdd(ptr[1], 1'b1, 1'b0);    // {sum, co} = fullAdd(x, y, ci);
        else if (PTI)
            // Process the 3 remaining bits.
            {dPtr4, dco} = fullAdd(ptr[1], 1'b0, co_reg);
        else if (PTD & ~PTD_reg)
            // Process the LS-bit (add '1' using minuend complementation).
            {dPtr4, dco} = fullAdd(~ptr[1], 1'b1, 1'b0) ^ 2'b10;    // Complement the minuend (ptr[1]) and complement the sum (dPtr4) using XOR.
        else if (PTD)
            // Process the 3 remaining bits (add '0's using minuend complementation).
            {dPtr4, dco} = fullAdd(~ptr[1], 1'b0, co_reg) ^ 2'b10;  // Complement the minuend (ptr[1]) and complement the sum (dPtr4) using XOR.
        else if (SPT)
            {dPtr4, dco} = {IS, 1'b0};
        else
            {dPtr4, dco} = {ptr[1], 1'b0};
    end

    // 4-bit Pointer Shift Register
    always@(posedge PHI2) begin : proc_ptr
        PTI_reg <= PTI;
        PTD_reg <= PTD;
        co_reg <= dco;
        ptr <= {dPtr4, ptr[4:2]};
        if (T4) 
            i_ptrHoldingReg <= ptr; // Capture the pointer on every T4 for easy reading.
    end
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // Carry Flip-Flop 66
    //   
    // '569 (6):  "A yes answer to a status interrogation will set the carry flip-flop 66 as
    // indicated by control signal IST in FIG. 4."
    // 
    // '569 (6):  "A yes answer to the instruction �is pointer /= N� will set carry flip-flop 66 via
    // control signal IPT in FIG. 4."
    // 
    // What's not said in '569 is that carry flip-flop 66 must be cleared if we get a 'no' answer to
    // the instruction �is pointer /= N�.  Why?  Because an arithmetic instruction that leaves carry
    // set might precede the Interrogate Pointer instruction.  Alternatively, we could always clear
    // carry flip-flop 66 upon decoding the Interrogate Pointer instruction and subsequently set it
    // if the set condition is met. 
    //
    // '569 (7):  "Any carry signal out of the adder in arithmetic and register circuit 20, with
    // word select, also high, will set carry flip-flop 66."
    //
    // '569 (17):  "There are three ways the carry flip-flop 66 can be set: (1) by a carry generated
    // in the arithmetic and register circuit 20; (2) by a successful interrogation of the pointer
    // position; and (3) by a successful interrogation of one of the 12 status bits."
    // 
    // '569 (18):  "The carry flip-flop 66 is reset during execution of every instruction except
    // arithmetic (type 2) and interrogation of pointer or status (types 3 and 4). Since only
    // arithmetic and interrogation instructions can set the carry flip-flop 66, the constraint is
    // not severe."
    //--- 
    // Set or reset carry flip-flop 66 (CARRY_FF) in response to any arithmetic instruction.  Carry
    // line 34 (CARRY) from arithmetic and register circuit 20 should only set or reset CARRY_FF
    // when word select (ws_in) is active.  Carry line 34 can toggle during each bit period in a
    // given digit so its state should only be considered during the last bit period of each digit
    // (T4).  During execution of an arithmetic instruction, CARRY_FF might toggle between set and
    // reset as each digit is calculated.  In this way, the carry result of the last bit of the last
    // digit of the Word Select interval determines the final state of CARRY_FF.
//    assign arith_cset   =  CARRY & ws_in & T4 & arith_cyc;
//    assign arith_crst   = ~CARRY & ws_in & T4 & arith_cyc;
    assign arith_cset   = 1'b0;
    assign arith_crst   = 1'b0;
    //---
    // Set or reset carry flip-flop 66 (CARRY_FF) in response to an interrogate status (IST)
    // instruction.  '569 says, "Any status bit can be ... interrogated while circulating through
    // the adder 64 in response to the appropriate instruction."  In this implementation, the carry
    // output (co) from adder/subtractor 64 is used to set or reset CARRY_FF during IST.
//    assign ist_cset     = IST &  co;
//    assign ist_crst     = IST & ~co;
    assign ist_cset     = 1'b0;
    assign ist_crst     = 1'b0;
    //---
    // Set or reset carry flip-flop 66 (CARRY_FF) in response to an interrogate pointer (IPT)
    // instruction.  During the IPT fetch cycle, the microprogrammed controller 46 determines
    // whether to set or reset and asserts either IPTS or IPTR accordingly.  Because the IPT fetch
    // cycle might overlap an arithmetic execution cycle with its attendant carry result, we must
    // wait for the IPT execution cycle to start before setting or resetting CARRY_FF (using an
    // existing decode of system counter 42, arbitrarily chosen at bit-time b18).
    assign ipt_cset     = IPTS & b18;   // Set carry flip-flop in IPT execution cycle.
    assign ipt_crst     = IPTR & b18;   // Reset carry flip-flop in IPT execution cycle.
    //---
    always@(posedge PHI2) begin : proc_carry_ff
        if (b55) begin
            // Asserted during the entire word cycle (bit times b0 through b55) for the following
            // instructions:
            arith_cyc   <= ARITHW;      // Any Type 2 Arithmetic Instuction
            ipt_cyc     <= IPTS | IPTR; // Interrogate Pointer Instruction 
            ist_cyc     <= ISTW;        // Interrogate Status Instruction
            // The above are used to prevent reset of the carry flip-flop at the end of those
            // instructions.
        end
        //
        if (arith_cset || ist_cset || ipt_cset) 
            CARRY_FF <= 1'b1;
        else if (arith_crst || ist_crst || ipt_crst || b55 && !(arith_cyc || ipt_cyc || ist_cyc)) 
            CARRY_FF <= 1'b0;
        //
    end
    // ---------------------------------------------------------------------------------------------

// =================================================================================================



endmodule

