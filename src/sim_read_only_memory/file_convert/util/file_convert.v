//------------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//------------------------------------------------------------------------------
//
// FileName:
//      file_convert.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Read-Only Memory File Conversion Utility
//
// Description:
//      Converts an HP-35 ROM dump file into three binary text files suitable
//      for Verilog's $readmemb system function.
//
//------------------------------------------------------------------------------

module file_convert ();

// =================================================================================================
// Signal Declarations
// =================================================================================================

    integer     fd;                 // File Descriptor for loading ROM contents.
    integer     i, j;               // Iteration variables.
    integer     r;                  // Return value from $fscanf()
    reg [9:0]   rom0 [0:255];       // First ROM.
    reg [9:0]   rom1 [0:255];       // Middle ROM.
    reg [9:0]   rom2 [0:255];       // Last ROM.
    string      object_file_v1          = "../util/35v2.obj";
    string      rom0_output_file_v1     = "../util/35v2_rom0_binary.txt";
    string      rom1_output_file_v1     = "../util/35v2_rom1_binary.txt";
    string      rom2_output_file_v1     = "../util/35v2_rom2_binary.txt";

    string      object_file_v2          = "../util/35_rom_brian_nemetz.obj";
    string      rom0_output_file_v2     = "../util/35bn_rom0_binary.txt";
    string      rom1_output_file_v2     = "../util/35bn_rom1_binary.txt";
    string      rom2_output_file_v2     = "../util/35bn_rom2_binary.txt";

    string      object_file_v3          = "../util/35_rom_tony_nixon.obj";
    string      rom0_output_file_v3     = "../util/35tn_rom0_binary.txt";
    string      rom1_output_file_v3     = "../util/35tn_rom1_binary.txt";
    string      rom2_output_file_v3     = "../util/35tn_rom2_binary.txt";
    
// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 
    task parseRomObjectFile;
    // This task reads a ROM object file containing the contents of all three HP-35 ROMs and stores
    // the appropriate third of the file (256 words) into this ROM's array.  The object file format
    // is from Peter Monta's "New ROM dump of HP-35 version 2: 35v2.obj" located at his website
    // <www.pmonta.com/calculators/hp-35>  He says, "The three ROM chips included in this dump have
    // part numbers 1818-0006, 1818-0017, and 1818-0020".
    // 
    // The ROM dump is an ASCII file containing all three ROMs.  The first few lines are:
    //
    //    0000:0335
    //    0001:1377
    //    0002:1044
    //     .
    //     .
    //     .
    // 
    // Each line represents a 10-bit instruction word.  The first four digits are the address in
    // octal and the last four digits are the 10-bit instruction word in octal.
    // 
    // The following initial block employs two nested for loops, the outer loop steps through each
    // of the three ROMs (0, 1, 2) while the inner loop steps through 256 instruction words.  Each
    // pass through the inner loop reads one line of the file and if that line belongs to ROM 0,
    // then the instruction value contained in that line is written to the array 'rom0[i]'.  Similar
    // tests are made for ROMs 1 and 2 and the instruction values are written to their corresponding
    // arrays.  The upshot of all this is that rom0[] is loaded from the first third of the object
    // file, rom1[] from the middle third, and rom2[] from the last third.

        begin : parse_rom 
            for (j = 0; j <= 2; j = j + 1) begin            // For each ROM...
                for (i = 0; i <= 255; i = i + 1) begin      //    for each instruction word in a single ROM...
                    if (j == 0)                             //       if we're reading the first third of the object file, then...
                        r = $fscanf(fd, "%*o:%o", rom0[i]); //          write the indexed instruction into the ROM 0 array,
                    else if (j == 1)                        //       if we're reading the middle third of the object file, then...
                        r = $fscanf(fd, "%*o:%o", rom1[i]); //          write the indexed instruction into the ROM 1 array,
                    else                                    //       if we're reading the last third of the object file, then...
                        r = $fscanf(fd, "%*o:%o", rom2[i]); //          write the indexed instruction into the ROM 2 array,
                end
            end
        end
    endtask

// =================================================================================================
// Convert ROMs
// =================================================================================================

    initial begin
        // Convert the specified object file into three output files.  Run this process three times,
        // one for each object file I've been able to find.

        // Convert first Object File ---------------------------------------------------------------
        fd = $fopen(object_file_v1, "r");		// Open the object file to be converted.
        parseRomObjectFile;                     // Parse it into three ROM arrays.
        $fclose(fd);                            // Close the file.
        
        // Save ROM0 
        fd = $fopen(rom0_output_file_v1, "w");  // Open the ROM 0 output file.
        for (i = 0; i <= 255; i = i + 1)        // for each instruction word in a single ROM...
            $fdisplay(fd, "%b", rom0[i]);       //    write its value as a binary formatted string to a line in the file.
        $fclose(fd);                            // Close the file.
        // ---

        // Save ROM1
        fd = $fopen(rom1_output_file_v1, "w");  // Open the ROM 1 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom1[i]);
        $fclose(fd);
        // ---
        
        // Save ROM2 
        fd = $fopen(rom2_output_file_v1, "w");  // Open the ROM 2 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom2[i]);
        $fclose(fd);        
        // ---
        // -----------------------------------------------------------------------------------------

        // Convert second Object File --------------------------------------------------------------
        fd = $fopen(object_file_v2, "r");		// Open the object file to be converted.
        parseRomObjectFile;                     // Parse it into three ROM arrays.
        $fclose(fd);                            // Close the file.
        
        // Save ROM0 
        fd = $fopen(rom0_output_file_v2, "w");  // Open the ROM 0 output file.
        for (i = 0; i <= 255; i = i + 1)        // for each instruction word in a single ROM...
            $fdisplay(fd, "%b", rom0[i]);       //    write its value as a binary formatted string to a line in the file.
        $fclose(fd);                            // Close the file.
        // ---

        // Save ROM1
        fd = $fopen(rom1_output_file_v2, "w");  // Open the ROM 1 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom1[i]);
        $fclose(fd);
        // ---
        
        // Save ROM2 
        fd = $fopen(rom2_output_file_v2, "w");  // Open the ROM 2 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom2[i]);
        $fclose(fd);        
        // ---
        // -----------------------------------------------------------------------------------------

        // Convert third Object File ---------------------------------------------------------------
        fd = $fopen(object_file_v3, "r");		// Open the object file to be converted.
        parseRomObjectFile;                     // Parse it into three ROM arrays.
        $fclose(fd);                            // Close the file.
        
        // Save ROM0 
        fd = $fopen(rom0_output_file_v3, "w");  // Open the ROM 0 output file.
        for (i = 0; i <= 255; i = i + 1)        // for each instruction word in a single ROM...
            $fdisplay(fd, "%b", rom0[i]);       //    write its value as a binary formatted string to a line in the file.
        $fclose(fd);                            // Close the file.
        // ---

        // Save ROM1
        fd = $fopen(rom1_output_file_v3, "w");  // Open the ROM 1 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom1[i]);
        $fclose(fd);
        // ---
        
        // Save ROM2 
        fd = $fopen(rom2_output_file_v3, "w");  // Open the ROM 2 output file.
        for (i = 0; i <= 255; i = i + 1)
            $fdisplay(fd, "%b", rom2[i]);
        $fclose(fd);        
        // ---
        // -----------------------------------------------------------------------------------------
        
        $finish;

    end

endmodule