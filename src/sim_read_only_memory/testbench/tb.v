//-------------------------------------------------------------------------
// OpenBSD License
// 
// Copyright (c) 2022 Robert J. Weinstein
// 
// Permission to use, copy, modify, and distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
//-------------------------------------------------------------------------
//
// FileName:
//      tb.v
//
// Author:
//      Robert J. Weinstein
//      patently.obvious.2021@gmail.com
//      
// Title:
//      HP-35 Project:  Testbench for Read Only Memory 18
//
// Description:
// 
// Include Files : None
//
// Output Files : rom_array_dump.obj
//      On Windows systems, use dos2unix to convert the output file to UNIX
//      line endings before comparing to Peter Monta's original 35v2.obj.
//      You can use the included batch file:  compare-sim-to-original.bat
//
// Conventions:
//      Port names are 'UPPER' case.
//      Internal wires and registers are 'lower' case.
//      Parameters are first character 'Upper' case.
//      Active low signals are identified with '_n' or '_N'
//      appended to the wire, register, or port name.
//       
//      Uses Verilog 2001 Features
// 
// Drawing:
//    RJW2022 - HP-35 Read Only Memory 18 - Logic and Timing Diagrams
//    RJW2024 - HP-35 Read Only Memory - Word Select Output Simulation Results 
//
// ----------------------------------------------------------------------
// Revision History
// ----------------------------------------------------------------------
//
// 14-Feb-2022 rjw
//    Released as open-source.
//
// ----------------------------------------------------------------------
// 
`timescale 1 ns / 100 ps

module tb ();

// -----------------------------------------------------------------------------
// Local Parameter Declarations
// 
    localparam  NumROMs         = 3;
    localparam  SingleRomWords  = 256;
    localparam  TotalRomWords   = SingleRomWords * NumROMs;

// -----------------------------------------------------------------------------
// Signal Declarations
// 

    // DUT Bidirectional Ports
    wire        IS;                 // inout    // ('569 item 28) Serial Instruction from ROM Output
    // DUT Output Ports
    wire        WS;                 // output   // ('569 item 30) Word Select
    // DUT Input Ports                  
    reg         PHI1;               // input    // Bit-Rate Clock Input, Phase 1.  Not used here.
    reg         PHI2;               // input    // Bit-Rate Clock Input, Phase 2.
    reg         PWO;                // input    // ('569 item 36) PoWer On pulse.  "As the system
                                                //    power comes on, the PWO signal is held at
                                                //    logic l for at least 20 milliseconds."
    reg         SYNC;               // input    // ('569 item 26) Word Cycle Sync
    reg         IA;                 // input    // ('569 item 32) Serial ROM Address


    // Testbench signals
    reg         rst;
    integer     sysCount        = 0;
    integer     nextSysCount    = 1;
    integer     digitCount      = 0;
    reg         t1 = 1'b0, t2 = 1'b0, t3 = 1'b0, t4 = 1'b0;
    reg         go = 1'b0;
    reg         sim_start_pulse = 1'b0;
    reg         sim_end_pulse   = 1'b0;
    reg         loop_done_1;
    reg         loop_done_2;
    reg         loop_done_3;
    integer     fd;                             // File Descriptor.
    string      outFile = "rom_array_dump.obj"; // Simulation dump file.  On Windows, the simulator
                                                //    saves this in DOS file format with CR LF line
                                                //    endings.
    integer     i, j;
    reg [7:0]   address             = 'b0;
    reg [55:0]  ia_shifter          = 'b0;
    reg [9:0]   is_shifter          = 'b0;
    reg         load_address        = 1'b0;
    reg         fetch               = 1'b0;
    reg [7:0]   latched_addr        = 'b0;
    reg [7:0]   save_address        = 'b0;
    reg [9:0]   save_instruction    = 'b0;
    reg [1:0]   save_active_rom     = 'b0;
    reg         save_em             = 1'b0;
    reg         save_enable         = 1'b0;
    reg         save_rom_done       = 1'b1;
    reg [9:0]   save_rom_result[0:TotalRomWords-1]; // Array in which the testbench saves a copy of
                                                    //    the randomly addressed ROM contents.
    reg [0:TotalRomWords-1] save_rom_tag;           // Vector comprising a single bit position for
                                                    //    each entry in the 'save_rom_result' array.
                                                    //    In the ROM read loop, a bit in this vector
                                                    //    is set corresponding to the address that
                                                    //    was read.
    reg [11:0]  file_out_addr       = 'b0;          // A 12-bit register version of the index into
                                                    //    'save_rom_result' so the output file's
                                                    //    address field will be formatted as 4-digit
                                                    //    octal.

    integer     intRomAddr          = 0;
    integer     numRandomIterations = 0;

    // Instruction Parsing Results
    string      instType;
    string      instTypeName;
    string      instArithFieldSel;
    string      instMnemonic;
    reg [7:0]   targetAddr;
    reg [3:0]   flagNum;
    reg [3:0]   pointerNum;
    reg [3:0]   constantVal;
    reg [2:0]   romSelNum;
    string      arithOp;
    string      instDescription;


// -------------------------------------------------------------------------------------------------
// Tasks & Functions
// 
    task issueAddress;
    // Using the given integer address, issue the address at the right time.
        input integer       addr_val;
        begin : task_issue_addr 
            wait (nextSysCount == 0); address = addr_val; load_address = 1'b1; @(posedge PHI2); address = 0; load_address = 1'b0; #1;
        end
    endtask

    task nullCycle;
    // Spend a cycle without issuing an address.
        begin : task_null_cycle
            wait (nextSysCount == 0); @(posedge PHI2); #1;
        end
    endtask

// -------------------------------------------------------------------------------------------------
// Test Processes

    // Generate 200 kHz clock
    always #2.5us PHI2 = ~PHI2;

    initial begin
        rst = 1'b0;
        #10;
        rst = 1'b1;
        #5us;
        rst = 1'b0;
        #10ms;
    end

    initial begin
        PWO = 1'b0;
        #10;
        PWO = 1'b1;
        #1ms;
        PWO = 1'b0;
        #100ms;
    end

    // -----------------------------------------------
    // Two-Process System Counter
    // --
    // Combinational next count process...
    always @* begin : proc_nextsyscount
        if (sysCount == 55)
            nextSysCount = 0;
        else
            nextSysCount = sysCount + 1;
    end
    // --
    // Registered count process...
    always@(posedge PHI2 or posedge rst) begin : proc_syscount
        if (rst) begin
            sysCount <= 0;
            digitCount <= 0;
        end
        else begin
            sysCount <= nextSysCount;
            digitCount <= nextSysCount / 4;
        end
    end
    // --
    // End Two-Process System Counter
    // -----------------------------------------------

    // -----------------------------------------------
    // Generate SYNC pulse and address shifter.
    always@(posedge PHI2 or posedge rst) begin : proc_sync
        if (rst) 
            SYNC <= 1'b0;
        else if (nextSysCount == 45)
            SYNC <= 1'b1;
        else if (nextSysCount == 55)
            SYNC <= 1'b0;
        // --
        if (rst) 
            {t1, t2, t3, t4} <= 4'b0000;
        else if (nextSysCount == 0)
            {t1, t2, t3, t4} <= 4'b1000;
        else
            {t1, t2, t3, t4} <= {t4, t1, t2, t3};
        // --
        if (rst) begin
            ia_shifter <= {{29{1'b0}}, {8{1'b0}}, {19{1'b0}}};
            fetch <= 1'b0;
        end
        else if (nextSysCount == 0)                                 // If we're about to start the next word cycle, and ...
            if (load_address) begin                                 //    if a new address is issued, then ...
                ia_shifter <= {{29{1'b0}}, address, {19{1'b0}}};    //       load the new address in bits [26:19],
                fetch <= 1'b1;
            end
            else begin                                              //    otherwise no new address was issued, so ...
                ia_shifter <= {{29{1'b0}}, {8{1'b0}}, {19{1'b0}}};  //       load all zeros.
                fetch <= 1'b0;
            end
        else                                                        // Otherwise we're not about to start the next word cycle, so ...
            ia_shifter <= {1'b0, ia_shifter[55:1]};                 //    shift the IA shifter to the right.

    end

    assign IA = ia_shifter[0];
    // -----------------------------------------------

    // -----------------------------------------------
    // Parse the instruction as it shifts out of IS.
    always@(posedge PHI2 or posedge rst) begin : proc_parse
        if (rst) 
            is_shifter <= 'b0;
        else if (SYNC)                                              // '569:  "During bit times b45-b54 the instruction is 
            is_shifter <= {IS, is_shifter[9:1]};                    // read serially onto Is buss 28 from the active ROM..."
        // --
        if (rst) begin
            instType            <= "-";
            instTypeName        <= "-";
            instArithFieldSel   <= "-";
            instMnemonic        <= "-";       
            targetAddr          <= 'bz;
            flagNum             <= 'bz;
            pointerNum          <= 'bz;
            constantVal         <= 'bz;
            romSelNum           <= 'bz;
            arithOp             <= "-";
            instDescription     <= "-";
        end
        else if (nextSysCount == 0) begin                           // If we're about to start the next word cycle, then...
            instType            <= "-";                             //    Assign default values.
            instTypeName        <= "-";                             //    ditto
            instArithFieldSel   <= "-";                             //    ditto
            instMnemonic        <= "-";                             //    ditto
            targetAddr          <= 'bz;                             //    ditto
            flagNum             <= 'bz;                             //    ditto
            pointerNum          <= 'bz;                             //    ditto
            constantVal         <= 'bz;                             //    ditto
            romSelNum           <= 'bz;                             //    ditto
            arithOp             <= "-";                             //    ditto
            instDescription     <= "-";                             //    ditto
            if (fetch) begin                                        //    If we just fetched an instruction, then...
                casez (is_shifter)                                  //       parse the instruction stored in the instruction shifter...
                    10'b????????01 : begin instType <= "Type 1";  instTypeName <= "Jump Subroutine";    targetAddr <= is_shifter[9:2];  end
                    10'b????????11 : begin instType <= "Type 1";  instTypeName <= "Conditional Branch"; targetAddr <= is_shifter[9:2];  end
                    10'b????????10 : begin
                        instType <= "Type 2";   instTypeName <= "Arithmetic";
                        case (is_shifter[9:5])
                            // -- Class 1) Clear
                            5'b10111 : begin instMnemonic <= "CLRA";    arithOp <= "0 -> A";            instDescription <= "Clear register A";                   end
                            5'b00001 : begin instMnemonic <= "CLRB";    arithOp <= "0 -> B";            instDescription <= "Clear register B";                   end
                            5'b00110 : begin instMnemonic <= "CLRC";    arithOp <= "0 -> C";            instDescription <= "Clear register C";                   end
                            // -- Class 2) Transfer/Exchange
                            5'b01001 : begin instMnemonic <= "MOVAB";   arithOp <= "A -> B";            instDescription <= "Move A to B";                        end
                            5'b00100 : begin instMnemonic <= "MOVBC";   arithOp <= "B -> C";            instDescription <= "Move B to C";                        end
                            5'b01100 : begin instMnemonic <= "MOVCA";   arithOp <= "C -> A";            instDescription <= "Move C to A";                        end
                            5'b11001 : begin instMnemonic <= "XCHAB";   arithOp <= "A <-> B";           instDescription <= "Exchange A and B";                   end
                            5'b10001 : begin instMnemonic <= "XCHBC";   arithOp <= "B <-> C";           instDescription <= "Exchange B and C";                   end
                            5'b11101 : begin instMnemonic <= "XCHAC";   arithOp <= "A <-> C";           instDescription <= "Exchange A and C";                   end
                            // -- Class 3) Add/Subtract
                            5'b01110 : begin instMnemonic <= "ADDACC";  arithOp <= "A + C -> C";        instDescription <= "Add A plus C, result in C";          end
                            5'b01010 : begin instMnemonic <= "SUBACC";  arithOp <= "A - C -> C";        instDescription <= "Subtract A minus C, result in C";    end
                            5'b11100 : begin instMnemonic <= "ADDABA";  arithOp <= "A + B -> A";        instDescription <= "Add A plus B, result in A";          end
                            5'b11000 : begin instMnemonic <= "SUBABA";  arithOp <= "A - B -> A";        instDescription <= "Subtract A minus B, result in A";    end
                            5'b11110 : begin instMnemonic <= "ADDACA";  arithOp <= "A + C -> A";        instDescription <= "Add A plus C, result in A";          end
                            5'b11010 : begin instMnemonic <= "SUBACA";  arithOp <= "A - C -> A";        instDescription <= "Subtract A minus C, result in A";    end
                            5'b10101 : begin instMnemonic <= "ADDCCC";  arithOp <= "C + C -> C";        instDescription <= "Add C plus C, result in C";          end
                            // -- Class 4) Compare (Typically precedes a conditional branch instruction)
                            5'b00000 : begin instMnemonic <= "CMP0B";   arithOp <= "0 - B";             instDescription <= "Compare B to zero";                  end
                            5'b01101 : begin instMnemonic <= "CMP0C";   arithOp <= "0 - C";             instDescription <= "Compare C to zero";                  end
                            5'b00010 : begin instMnemonic <= "CMPAC";   arithOp <= "A - C";             instDescription <= "Compare A and C";                    end
                            5'b10000 : begin instMnemonic <= "CMPAB";   arithOp <= "A - B";             instDescription <= "Compare A and B";                    end
                            5'b10011 : begin instMnemonic <= "CMPA1";   arithOp <= "A - 1";             instDescription <= "Compare A to one";                   end
                            5'b00011 : begin instMnemonic <= "CMPC1";   arithOp <= "C - 1";             instDescription <= "Compare C to one";                   end
                            // -- Class 5) Complement
                            5'b00101 : begin instMnemonic <= "TCC";     arithOp <= "0 - C -> C";        instDescription <= "Tens Complement register C";         end
                            5'b00111 : begin instMnemonic <= "NCC";     arithOp <= "0 - C - 1 -> C";    instDescription <= "Nines Complement register C";        end
                            // -- Class 6) Increment
                            5'b11111 : begin instMnemonic <= "INCA";    arithOp <= "A + 1 -> A";        instDescription <= "Increment register A";               end
                            5'b01111 : begin instMnemonic <= "INCC";    arithOp <= "C + 1 -> C";        instDescription <= "Increment register C";               end
                            // -- Class 7) Decrement
                            5'b11011 : begin instMnemonic <= "DECA";    arithOp <= "A - 1 -> A";        instDescription <= "Decrement register A";               end
                            5'b01011 : begin instMnemonic <= "DECC";    arithOp <= "C - 1 -> C";        instDescription <= "Decrement register C";               end
                            // -- Class 8) Shift
                            5'b10110 : begin instMnemonic <= "SHRA";    arithOp <= "A >> 1";            instDescription <= "Shift Right register A";             end
                            5'b10100 : begin instMnemonic <= "SHRB";    arithOp <= "B >> 1";            instDescription <= "Shift Right register B";             end
                            5'b10010 : begin instMnemonic <= "SHRC";    arithOp <= "C >> 1";            instDescription <= "Shift Right register C";             end
                            5'b01000 : begin instMnemonic <= "SHLA";    arithOp <= "A << 1";            instDescription <= "Shift Left register A";              end
                        endcase

                        case (is_shifter[4:2])
                            3'b000 : instArithFieldSel <= "[P] Pointer Only";
                            3'b100 : instArithFieldSel <= "[WP] Up to Pointer";
                            3'b010 : instArithFieldSel <= "[X] Exponent (w/sign)";
                            3'b110 : instArithFieldSel <= "[XS] Exponent Sign Only";
                            3'b001 : instArithFieldSel <= "[M] Mantissa Only";
                            3'b101 : instArithFieldSel <= "[MS] Mantissa with Sign";
                            3'b011 : instArithFieldSel <= "[W] Entire Word";
                            3'b111 : instArithFieldSel <= "[S] Mantissa Sign Only";
                        endcase
                    end
                    10'b??????0100 : begin
                        instType <= "Type 3";  instTypeName <= "Status Flags"; 
                        case (is_shifter[5:4])
                            2'b00 : instDescription <= "Set Flag N";
                            2'b01 : instDescription <= "Interrogate Flag N";
                            2'b10 : instDescription <= "Reset Flag N";
                            2'b11 : instDescription <= "Clear All Flags (N = 0000)";
                        endcase
                        flagNum <= is_shifter[9:6];
                    end
                    10'b??????1100 : begin
                        instType <= "Type 4";  instTypeName <= "Pointer";
                        case (is_shifter[5:4])
                            2'b00 : instDescription <= "Set Pointer to P";
                            2'b10 : instDescription <= "Interrogate if Pointer at P";
                            2'b01 : instDescription <= "Decrement Pointer (P = don't care)";
                            2'b11 : instDescription <= "Increment Pointer (P = don't care)";
                        endcase
                        pointerNum <= is_shifter[9:6];
                    end
                    10'b??????1000 : begin
                        instType <= "Type 5";  instTypeName <= "Data Entry or Display";
                        if (is_shifter[5:4] == 2'b00)
                            instDescription <= "One of 16 Available Instructions";
                        else if (is_shifter[5:4] == 2'b01) begin
                            instMnemonic <= "LDC"; instDescription <= "Load Constant"; constantVal <= is_shifter[9:6];
                        end
                        else if (is_shifter[6:5] == 2'b01) begin
                            case (is_shifter[9:7])
                                3'b000 : begin instMnemonic <= "DSPTOG";                                            instDescription <= "Display Toggle";                        end
                                3'b001 : begin instMnemonic <= "XCHCM";     arithOp <= "C -> M -> C";               instDescription <= "(Exchange Memory)";                     end
                                3'b010 : begin instMnemonic <= "PUSHC";     arithOp <= "C -> C -> D -> E -> F";     instDescription <= "(Up Stack or Push C)";                  end
                                3'b011 : begin instMnemonic <= "POPA";      arithOp <= "F -> F -> E -> D -> A";     instDescription <= "(Down Stack or Pop A)";                 end
                                3'b100 : begin instMnemonic <= "DSPOFF";                                            instDescription <= "Display Off";                           end
                                3'b101 : begin instMnemonic <= "RCLM";      arithOp <= "M -> M -> C";               instDescription <= "(Recall Memory)";                       end
                                3'b110 : begin instMnemonic <= "ROTDN";     arithOp <= "C -> F -> E -> D -> C";     instDescription <= "(Rotate Down)";                         end
                                3'b111 : begin instMnemonic <= "CLRALL";    arithOp <= "0 -> A, B, C, D, E, F, M";  instDescription <= "(Clear all 56 bits of all registers)";  end
                            endcase
                        end
                        else if (is_shifter[7:5] == 3'b011) begin
                            instMnemonic <= "MOVISA";  instDescription <= "Is -> A (Load 56-bit instruction sequence into register A)";
                        end
                        else if (is_shifter[7:5] == 3'b111) begin
                            instMnemonic <= "MOVBCDC";  instDescription <= "BCD -> C (Load 56-bit data storage value into register C)";
                        end
                        else
                            instDescription <= "**** Type 5 Parse Error ****";
                    end
                    10'b?????10000 : begin
                        instType <= "Type 6";  instTypeName <= "ROM Select, Misc.";
                        if (is_shifter[6:5] == 2'b00) begin
                            instDescription <= "ROM Select N"; romSelNum <= is_shifter[9:7];
                        end
                        else if (is_shifter[6:5] == 2'b01) begin
                            instMnemonic <= "RET"; instDescription <= "Return from Subroutine";
                        end
                        else if (is_shifter[7:5] == 3'b010) begin
                            instDescription <= "External Key Code Entry to C&T";
                        end
                        else if (is_shifter[7:5] == 3'b110) begin
                            instDescription <= "KEYBOARD ENTRY";
                        end
                        else if (is_shifter[9] == 1'b1 && is_shifter[7:5] == 3'b011) begin
                            instMnemonic <= "SNDADDR"; instDescription <= "SEND ADDRESS from C to Aux Data Storage";
                        end
                        else if (is_shifter[9:5] == 5'b10111) begin
                            instMnemonic <= "SNDDATA"; instDescription <= "SEND DATA from C to Aux Data Storage";
                        end
                        else
                            instDescription <= "**** Type 6 Parse Error ****";
                    end
                    10'b????100000 : begin instType <= "Type 7";  instTypeName <= "Rsvd for Program Store"; end
                    10'b???1000000 : begin instType <= "Type 8";  instTypeName <= "Rsvd for Program Store"; end
                    10'b??10000000 : begin instType <= "Type 9";  instTypeName <= "Available";              end
                    10'b?1?0000000 : begin instType <= "Type 9";  instTypeName <= "Available";              end
                    10'b1??0000000 : begin instType <= "Type 9";  instTypeName <= "Available";              end
                    10'b0000000000 : begin instType <= "Type 10"; instTypeName <= "No Operation";
                            instMnemonic <= "NOP"; instDescription <= "No Operation";
                    end
                endcase
            end
//          else begin                                              //    otherwise this wasn't a fetch cycle, so...
//                                                                  //       do nothing.
//          end
        end
    end
    // -----------------------------------------------

    // -----------------------------------------------
    // Save the address, instruction, and active ROM number at the end of each fetch cycle.
    always@(posedge PHI2 or posedge rst) begin : proc_save
        if (rst) begin
            latched_addr        <= 'b0;             // reg [7:0]   
            save_address        <= 'b0;             // reg [7:0]   
            save_instruction    <= 'b0;             // reg [9:0]   
            save_active_rom     <= 'b0;             // reg [1:0]   
            save_em             <= 1'b0;            // reg         
            for (i = 0; i < TotalRomWords; i = i+1) begin
                save_rom_result[i]  <= 10'd0;       // reg [9:0]   
                save_rom_tag[i]     <= 1'b0;        // reg         
            end
        end
        else begin                                              
            save_em <= 1'b0;                                    // Default value forces pulse back to zero.
            if (load_address)                                   // If we're loading the address into the address shifter, then...
                latched_addr <= address;                        //    grab a copy of the address.
            //--
            if (save_enable && fetch && sysCount == 55) begin   // If we're in the process of fetching an instruction and we reach the last bit of the word cycle, then...
                save_address        <= latched_addr;            //    save the address that was shifted into the ROM,
                save_instruction    <= is_shifter;              //    save the instruction that was shifted out of the ROM,
                case (1'b1)                                     //    interrogate the 'active' flags inside each ROM to determine which one was active,
                    inst_rom0.active : save_active_rom <= 2'd0; //       (interrogate ROM0's 'active' flag)
                    inst_rom1.active : save_active_rom <= 2'd1; //       (interrogate ROM1's 'active' flag)
                    inst_rom2.active : save_active_rom <= 2'd2; //       (interrogate ROM2's 'active' flag)
                    default          : save_active_rom <= 2'd3; //       (Error: lands here if no ROM is active)
                endcase                                         // 
                save_em <= 1'b1;                                //    and make a pulse on 'save_em' so another process can evaluate the results.
            end
            //--
            if (save_em) begin                                                          // If we're ready to save the address and its stored instruction, then...
                save_rom_result[{save_active_rom, save_address}] <= save_instruction;   //    save the addressed instruction,
                save_rom_tag[{save_active_rom, save_address}]   <= 1'b1;                //    and save a tag bit each time a ROM result is saved at a particular address.
            end                                                                         //
            save_rom_done <= &save_rom_tag;                                             // Assert done flag when all tag bits are set.
        end
    end
    // -----------------------------------------------

    initial begin
        fd = $fopen(outFile, "w");
        PHI1 = 1'b0;
        PHI2 = 1'b0;
        loop_done_1 = 1'b0;
        loop_done_2 = 1'b0;
        loop_done_3 = 1'b0;
        sim_end_pulse = 1'b0;
        save_enable = 1'b0;
        wait (rst == 1'b1);
        wait (rst == 1'b0);
        @(posedge PHI2);

        wait (PWO == 1'b0); 
        @(posedge PHI2);
        sim_start_pulse = 1'b1;
        @(posedge PHI2);
        sim_start_pulse = 1'b0;
        @(posedge PHI2);

        wait (nextSysCount == 0); 
        @(posedge PHI2);
        wait (nextSysCount == 0); 
        @(posedge PHI2);

        //----------------------
        // Test all word select (WS) sequences with a null cycle between.
        // 
        issueAddress(104);  // Arith [P]
        nullCycle();
        issueAddress(102);  // Arith [WP]
        nullCycle();
        issueAddress(18);   // Arith [X]
        nullCycle();
        issueAddress(69);   // Arith [XS]
        nullCycle();
        issueAddress(111);  // Arith [M]
        nullCycle();
        issueAddress(70);   // Arith [MS]
        nullCycle();
        issueAddress(14);   // Arith [W]
        nullCycle();
        issueAddress(82);   // Arith [S]
        nullCycle();
        // 
        // End test all word select (WS) sequences
        //----------------------

        //----------------------
        // Test all word select (WS) sequences back-to-back.
        // 
        issueAddress(104);  // Arith [P]
        issueAddress(102);  // Arith [WP]
        issueAddress(18);   // Arith [X]
        issueAddress(69);   // Arith [XS]
        issueAddress(111);  // Arith [M]
        issueAddress(70);   // Arith [MS]
        issueAddress(14);   // Arith [W]
        issueAddress(82);   // Arith [S]
        nullCycle();
        // 
        // End test all word select (WS) sequences
        //----------------------

        //----------------------
        // Test all word select (WS) sequences with a NOP between.
        // 
        issueAddress(104);  // Arith [P]
        issueAddress(37);   // NOP
        issueAddress(102);  // Arith [WP]
        issueAddress(37);   // NOP
        issueAddress(18);   // Arith [X]
        issueAddress(37);   // NOP
        issueAddress(69);   // Arith [XS]
        issueAddress(37);   // NOP
        issueAddress(111);  // Arith [M]
        issueAddress(37);   // NOP
        issueAddress(70);   // Arith [MS]
        issueAddress(37);   // NOP
        issueAddress(14);   // Arith [W]
        issueAddress(37);   // NOP
        issueAddress(82);   // Arith [S]
        issueAddress(37);   // NOP
        // 
        // End test all word select (WS) sequences
        //----------------------

        nullCycle();
        nullCycle();

        //----------------------
        // Test the ROM Output Enable (ROE) flip-flops.
        // 
        issueAddress(17);   // In ROM 0, Select ROM 1

        issueAddress(3);    // In ROM 1, Stack -> A
        issueAddress(155);  // In ROM 1, Select ROM 2

        issueAddress(6);    // In ROM 2, Stack -> A
        issueAddress(0);    // In ROM 2, Select ROM 0

        issueAddress(37);   // In ROM 0, NOP
        issueAddress(17);   // In ROM 0, Select ROM 1

        issueAddress(3);    // In ROM 1, Stack -> A
        issueAddress(155);  // In ROM 1, Select ROM 2

        issueAddress(6);    // In ROM 2, Stack -> A
        issueAddress(0);    // In ROM 2, Select ROM 0

        issueAddress(37);   // In ROM 0, NOP
        // 
        // End test the ROM Output Enable (ROE) flip-flops.
        //----------------------

        nullCycle();
        nullCycle();

        //----------------------
        // Randomly read ROM locations, saving results into an array, until we've read every one.
        // 
        save_enable = 1'b1;                                     // Enable writing into the save array.
        numRandomIterations = 0;                                
        while (!save_rom_done) begin                            // Do the following until all addresses have been read.
            numRandomIterations = numRandomIterations + 1;      //    Tally the number of random ROM accesses it takes to read the entire array.
            intRomAddr = $urandom_range(SingleRomWords-1,0);    //    Set ROM address to random value in the range 0 to 255.
            issueAddress(intRomAddr);                           //    Initate the word cycle using the randomly chosen address.
            wait (nextSysCount == 0);                           //    Wait until the start of next machine cycle.
        end
        save_enable = 1'b0;                                     // Disable writing into the save array.
        // 
        // End reading ROM locations.
        //----------------------

        //----------------------
        // Write saved ROM results array to a file. 
        // 
        for (i = 0; i < TotalRomWords; i=i+1) begin
            file_out_addr = i;
            $fdisplay(fd, "%o:%o", file_out_addr, save_rom_result[i]);
        end
        // 
        // End writing saved ROM results array to a file. 
        //----------------------


        loop_done_1 = 1'b1;
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_2 = 1'b1;
        wait (nextSysCount != 0); // Advance into the machine cycle.
        wait (nextSysCount == 0); // Start of next machine cycle.
        loop_done_3 = 1'b1;


        repeat (56) @(posedge PHI2);
        repeat (56) @(posedge PHI2);
        repeat (10) @(posedge PHI2);

        sim_end_pulse = 1'b1;
        @(posedge PHI2);
        sim_end_pulse = 1'b0;
        @(posedge PHI2);

        $fclose(fd);
        $stop;
    end

// =================================================================================================
// Instances
// =================================================================================================

    //---------------------------------
    // ROM 0
    //---------------------------------
    read_only_memory_18 #(
        .RomNum (0),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
        .RomFilename("35v2_rom0_binary.txt")    // parameter [80*8:1]  
//        .RomFilename("35bn_rom0_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom0 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (PHI1),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );

    //---------------------------------
    // ROM 1
    //---------------------------------
    read_only_memory_18 #(
        .RomNum (1),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
        .RomFilename("35v2_rom1_binary.txt")    // parameter [80*8:1]  
//        .RomFilename("35bn_rom1_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom1 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (PHI1),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );

    //---------------------------------
    // ROM 2
    //---------------------------------
    read_only_memory_18 #(
        .RomNum (2),                            // parameter    // Rom Number.  0 = ROM0, 1 = ROM1, 2 = ROM2.
        .RomFilename("35v2_rom2_binary.txt")    // parameter [80*8:1]  
//        .RomFilename("35bn_rom2_binary.txt")    // parameter [80*8:1]  
    )
    inst_rom2 (
        // Bidirectional Ports
        .IS     (IS),       // inout        // ('569 item 28) Serial Instruction.  Active ROM Output.
                                            //    Inactive ROM input.
        // Output Ports                     
        .WS     (WS),       // output       // ('569 item 30) Word Select.  Active ROM Output.
        // Input Ports                      
        .PHI1   (PHI1),     // input        // Bit-Rate Clock Input, Phase 1.  Not used here.
        .PHI2   (PHI2),     // input        // Bit-Rate Clock Input, Phase 2.
        .PWO    (PWO),      // input        // ('569 item 36) PoWer On pulse.  "As the system power
                                            //    comes on, the PWO signal is held at logic l for at
                                            //    least 20 milliseconds."
        .SYNC   (SYNC),     // input        // ('569 item 26) Word Cycle Sync
        .IA     (IA)        // input        // ('569 item 32) Serial ROM Address
    );


// =================================================================================================



endmodule

